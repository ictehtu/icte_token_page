"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var MdManager_1 = require("./transports/MdManager");
var ModelManager_1 = require("./model/ModelManager");
var MatrixModel_1 = require("./model/MatrixModel");
var events_1 = require("events");
var ICTE = /** @class */ (function () {
    function ICTE(tickers, exchanges) {
        var _this = this;
        this.modelData = new ModelManager_1.ModelManager();
        this.marketData = new MdManager_1.MdManager(this);
        this.matrixModel = new MatrixModel_1.MatrixModel(0, 0);
        this.marketDataEvents = new events_1.EventEmitter();
        this.init = function () {
            //TODO Create config service
            _this.modelData.connect();
            _this.marketData.connect(_this.tickers, _this.exchanges);
        };
        this.tickers = tickers;
        this.exchanges = exchanges;
        if (this.constructor !== ICTE) {
            throw new Error('Subclassing is not allowed');
        }
    }
    ICTE.prototype.on = function (msg) {
        this.marketDataEvents.emit("msg", msg);
        this.modelData.setUpdate(msg);
    };
    ICTE.prototype.getData = function () {
        var _this = this;
        return new rxjs_1.Observable(function (observer) {
            var interval = setInterval(function () {
                observer.next(_this.modelData.getUpdate());
            }, 66);
            return function () {
                clearInterval(interval);
            };
        });
    };
    return ICTE;
}());
exports.ICTE = ICTE;
