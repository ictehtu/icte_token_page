"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Instrument = /** @class */ (function () {
    function Instrument() {
    }
    Object.defineProperty(Instrument.prototype, "childs", {
        get: function () {
            return this._childs;
        },
        set: function (value) {
            this._childs = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Instrument.prototype, "seq", {
        get: function () {
            return this._seq;
        },
        set: function (value) {
            this._seq = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Instrument.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Instrument.prototype, "sectype", {
        get: function () {
            return this._sectype;
        },
        set: function (value) {
            this._sectype = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Instrument.prototype, "symbol", {
        get: function () {
            return this._symbol;
        },
        set: function (value) {
            this._symbol = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Instrument.prototype, "code", {
        get: function () {
            return this._code;
        },
        set: function (value) {
            this._code = value;
        },
        enumerable: true,
        configurable: true
    });
    return Instrument;
}());
exports.Instrument = Instrument;
