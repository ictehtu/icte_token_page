"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Constants of coints
 */
var MdConstant = /** @class */ (function () {
    function MdConstant() {
    }
    MdConstant.SystemMsgTopic = '0';
    MdConstant.BusinessMsgTopic = '1';
    MdConstant.CRYPTOSTART = 0;
    MdConstant.CRYPTOEND = 92;
    MdConstant.CURRENCYSTART = 93;
    MdConstant.CURRENCYEND = 164;
    MdConstant.IeoTransport = 0;
    MdConstant.PoloniexTransport = 1;
    MdConstant.CoinCapTransport = 2;
    MdConstant.BinanceTransport = 3;
    MdConstant.BitfinexTransport = 4;
    MdConstant.HitBtcTransport = 5;
    MdConstant.ModelManager = 6;
    MdConstant.VENUES = {
        0: 'IEO Trans',
        1: 'Poloniex',
        2: 'CoinCap',
        3: 'Binance',
        4: 'Bitfinex',
        5: 'RESERVED',
        6: 'ModelManager'
    };
    MdConstant.USDT = 0;
    MdConstant.IEO = 1;
    MdConstant.BTC = 2;
    MdConstant.ETH = 3;
    MdConstant.ZEC = 4;
    MdConstant.XTZ = 5;
    MdConstant.SEM = 6;
    MdConstant.IOTA = 7;
    MdConstant.XRP = 8;
    MdConstant.XLM = 9;
    MdConstant.WAVES = 10;
    MdConstant.DOGE = 11;
    MdConstant.XMR = 12;
    MdConstant.BCN = 13;
    MdConstant.AMP = 14;
    MdConstant.BTG = 15;
    MdConstant.DGB = 16;
    MdConstant.GAME = 17;
    MdConstant.LSK = 18;
    MdConstant.NAV = 19;
    MdConstant.NLG = 20;
    MdConstant.NXT = 21;
    MdConstant.POT = 22;
    MdConstant.STRAT = 23;
    MdConstant.SYS = 24;
    MdConstant.XVG = 25;
    MdConstant.DLT = 26; /*RADIX*/
    MdConstant.LTC = 27;
    MdConstant.EOS = 28;
    MdConstant.DASH = 29;
    MdConstant.QTUM = 30;
    MdConstant.OMNI = 31;
    MdConstant.TRX = 32;
    MdConstant.MKR = 33;
    MdConstant.ONT = 34;
    MdConstant.VET = 35;
    // news cryptos
    MdConstant.AOA = 36;
    MdConstant.ELF = 37;
    MdConstant.AE = 38;
    MdConstant.AION = 39;
    MdConstant.ARDR = 40;
    MdConstant.ARK = 41;
    MdConstant.REP = 42;
    MdConstant.BAT = 43;
    MdConstant.BTM = 44;
    MdConstant.BCH = 45;
    MdConstant.BCD = 46;
    MdConstant.BNB = 47;
    MdConstant.BSV = 48;
    MdConstant.ADA = 49;
    MdConstant.ATOM = 50;
    MdConstant.CRO = 51;
    MdConstant.CNX = 52;
    MdConstant.MANA = 53;
    MdConstant.DCR = 54;
    MdConstant.DGD = 55;
    MdConstant.ELA = 56;
    MdConstant.ENJ = 57;
    MdConstant.FCT = 58;
    MdConstant.GNT = 59;
    MdConstant.GXS = 60;
    MdConstant.HOT = 61; // Holo
    MdConstant.ZEN = 62;
    MdConstant.HC = 63;
    MdConstant.ICX = 64;
    MdConstant.IOST = 65;
    MdConstant.KMD = 66;
    MdConstant.KCS = 67;
    MdConstant.LA = 68;
    MdConstant.LOOM = 69;
    MdConstant.ETP = 70;
    MdConstant.MONA = 71;
    MdConstant.DAI = 72;
    MdConstant.NANO = 73;
    MdConstant.XEM = 74;
    MdConstant.NEO = 75;
    MdConstant.OMG = 76;
    MdConstant.PAI = 77;
    MdConstant.PAX = 78;
    MdConstant.NPXS = 79;
    MdConstant.RVN = 80;
    MdConstant.RDD = 81;
    MdConstant.SAN = 82;
    MdConstant.SC = 83;
    MdConstant.SNT = 84;
    MdConstant.THETA = 85;
    MdConstant.TUSD = 86;
    MdConstant.USDC = 87;
    MdConstant.WTC = 88;
    MdConstant.WAX = 89;
    MdConstant.XZC = 90;
    MdConstant.ZIL = 91;
    MdConstant.ZRX = 92;
    //---------- currencies --------
    MdConstant.ALL = 93;
    MdConstant.DZD = 94;
    MdConstant.ARS = 95;
    MdConstant.AMD = 96;
    MdConstant.AWG = 97;
    MdConstant.AUD = 98;
    MdConstant.AZN = 99;
    MdConstant.BSD = 100;
    MdConstant.BHD = 101;
    MdConstant.BYN = 102;
    MdConstant.BMD = 103;
    MdConstant.BAM = 104;
    MdConstant.BRL = 105;
    MdConstant.GBP = 106;
    MdConstant.BGN = 107;
    MdConstant.CAD = 108;
    MdConstant.XPF = 109;
    MdConstant.CLP = 110;
    MdConstant.CNY = 111;
    MdConstant.COP = 112;
    MdConstant.CRC = 113;
    MdConstant.HRK = 114;
    MdConstant.CUP = 115;
    MdConstant.CZK = 116;
    MdConstant.DKK = 117;
    MdConstant.DOP = 118;
    MdConstant.EGP = 119;
    MdConstant.EUR = 120;
    MdConstant.GEL = 121;
    MdConstant.HKD = 122;
    MdConstant.HUF = 123;
    MdConstant.ISK = 124;
    MdConstant.INR = 125;
    MdConstant.IDR = 126;
    MdConstant.IRR = 127;
    MdConstant.ILS = 128;
    MdConstant.JMD = 129;
    MdConstant.JPY = 130;
    MdConstant.JOD = 131;
    MdConstant.KZT = 132;
    MdConstant.KWD = 133;
    MdConstant.LBP = 134;
    MdConstant.MKD = 135;
    MdConstant.MYR = 136;
    MdConstant.MXN = 137;
    MdConstant.MDL = 138;
    MdConstant.MAD = 139;
    MdConstant.TWD = 140;
    MdConstant.NZD = 141;
    MdConstant.NOK = 142;
    MdConstant.OMR = 143;
    MdConstant.PKR = 144;
    MdConstant.PAB = 145;
    MdConstant.PEN = 146;
    MdConstant.PHP = 147;
    MdConstant.PLN = 148;
    MdConstant.QAR = 149;
    MdConstant.RON = 150;
    MdConstant.RUB = 151;
    MdConstant.SAR = 152;
    MdConstant.RSD = 153;
    MdConstant.SGD = 154;
    MdConstant.ZAR = 155;
    MdConstant.KRW = 156;
    MdConstant.SEK = 157;
    MdConstant.CHF = 158;
    MdConstant.THB = 159;
    MdConstant.TRY = 160;
    MdConstant.UAH = 161;
    MdConstant.AED = 162;
    MdConstant.USD = 163;
    MdConstant.VND = 164;
    // static readonly USD: number = 36;
    // static readonly EUR: number = 37;
    // static readonly JPY: number = 38;
    // static readonly GBP: number = 39;
    // static readonly CHF: number = 40;
    // static readonly CAD: number = 94;
    // static readonly AUD: number = 42;
    // static readonly HKD: number = 43;
    // static readonly AED: number = 36;
    // static readonly AFN: number = 37;
    // static readonly ALL: number = 38;
    // static readonly AMD: number = 39;
    // static readonly ANG: number = 40;
    // static readonly ARS: number = 42;
    // static readonly AUD: number = 43;
    // static readonly AWG: number = 44;
    // static readonly AZN: number = 45;
    // static readonly BAM: number = 46;
    // static readonly BBD: number = 47;
    // static readonly BDT: number = 48;
    // static readonly BGN: number = 49;
    // static readonly BHD: number = 50;
    // static readonly BIF: number = 51;
    // static readonly BMD: number = 52;
    // static readonly BND: number = 53;
    // static readonly BOB: number = 54;
    // static readonly BRL: number = 55;
    // static readonly BSD: number = 56;
    // static readonly BTN: number = 57;
    // static readonly BWP: number = 58;
    // static readonly BYR: number = 59;
    // static readonly BZD: number = 60;
    // static readonly CAD: number = 61;
    // static readonly CDF: number = 62;
    // static readonly CHF: number = 63;
    // static readonly CLF: number = 64;
    // static readonly CLP: number = 65;
    // static readonly CNY: number = 66;
    // static readonly COP: number = 67;
    // static readonly CRC: number = 68;
    // static readonly CUC: number = 69;
    // static readonly CUP: number = 70;
    // static readonly CVE: number = 71;
    // static readonly CZK: number = 72;
    // static readonly DJF: number = 73;
    // static readonly DKK: number = 74;
    // static readonly DOP: number = 75;
    // static readonly DZD: number = 76;
    // static readonly EGP: number = 77;
    // static readonly ERN: number = 78;
    // static readonly ETB: number = 79;
    // static readonly EUR: number = 80;
    // static readonly FJD: number = 81;
    // static readonly FKP: number = 82;
    // static readonly GBP: number = 83;
    // static readonly GEL: number = 84;
    // static readonly GGP: number = 85;
    // static readonly GHS: number = 86;
    // static readonly GIP: number = 87;
    // static readonly GMD: number = 88;
    // static readonly GNF: number = 89;
    // static readonly GTQ: number = 90;
    // static readonly GYD: number = 91;
    // static readonly HKD: number = 92;
    // static readonly HNL: number = 93;
    // static readonly HRK: number = 94;
    // static readonly HTG: number = 95;
    // static readonly HUF: number = 96;
    // static readonly IDR: number = 90;
    // static readonly ILS: number = 91;
    // static readonly IMP: number = 92;
    // static readonly INR: number = 93;
    // static readonly IQD: number = 94;
    // static readonly IRR: number = 95;
    // static readonly ISK: number = 96;
    // static readonly JEP: number = 97;
    // static readonly JMD: number = 98;
    // static readonly JOD: number = 99;
    // static readonly JPY: number = 100;
    // static readonly KES: number = 101;
    // static readonly KGS: number = 102;
    // static readonly KHR: number = 103;
    // static readonly KMF: number = 104;
    // static readonly KPW: number = 105;
    // static readonly KRW: number = 106;
    // static readonly KWD: number = 107;
    // static readonly KYD: number = 108;
    // static readonly KZT: number = 109;
    // static readonly LAK: number = 110;
    // static readonly LBP: number = 111;
    // static readonly LKR: number = 112;
    // static readonly LRD: number = 113;
    // static readonly LSL: number = 114;
    // static readonly LTL: number = 115;
    // static readonly LVL: number = 116;
    // static readonly LYD: number = 117;
    // static readonly MAD: number = 118;
    // static readonly MDL: number = 119;
    // static readonly MGA: number = 120;
    // static readonly MKD: number = 121;
    // static readonly MMK: number = 122;
    // static readonly MNT: number = 123;
    // static readonly MOP: number = 124;
    // static readonly MRO: number = 125;
    // static readonly MUR: number = 126;
    // static readonly MVR: number = 127;
    // static readonly MWK: number = 128;
    // static readonly MXN: number = 129;
    // static readonly MYR: number = 130;
    // static readonly MZN: number = 131;
    // static readonly NAD: number = 132;
    // static readonly NGN: number = 133;
    // static readonly NIO: number = 134;
    // static readonly NOK: number = 135;
    // static readonly NPR: number = 136;
    // static readonly NZD: number = 137;
    // static readonly OMR: number = 138;
    // static readonly PAB: number = 139;
    // static readonly PEN: number = 140;
    // static readonly PGK: number = 11;
    // static readonly PHP: number = 142;
    // static readonly PKR: number = 143;
    // static readonly PLN: number = 144;
    // static readonly PYG: number = 145;
    // static readonly QAR: number = 146;
    // static readonly RON: number = 147;
    // static readonly RSD: number = 148;
    // static readonly RUB: number = 149;
    // static readonly RWF: number = 150;
    // static readonly SAR: number = 151;
    // static readonly SBD: number = 152;
    // static readonly SCR: number = 153;
    // static readonly SDG: number = 154;
    // static readonly SEK: number = 155;
    // static readonly SGD: number = 156;
    // static readonly SHP: number = 157;
    // static readonly SLL: number = 158;
    // static readonly SOS: number = 159;
    // static readonly SRD: number = 160;
    // static readonly STD: number = 161;
    // static readonly SVC: number = 162;
    // static readonly SYP: number = 163;
    // static readonly SZL: number = 164;
    // static readonly THB: number = 165;
    // static readonly TJS: number = 166;
    // static readonly TMT: number = 167;
    // static readonly TND: number = 168;
    // static readonly TOP: number = 169;
    // static readonly TRY: number = 170;
    // static readonly TTD: number = 171;
    // static readonly TWD: number = 172;
    // static readonly TZS: number = 173;
    // static readonly UAH: number = 174;
    // static readonly UGX: number = 175;
    // static readonly USD: number = 176;
    // static readonly UYU: number = 177;
    // static readonly UZS: number = 178;
    // static readonly VEF: number = 179;
    // static readonly VND: number = 180;
    // static readonly VUV: number = 181;
    // static readonly WST: number = 182;
    // static readonly XAF: number = 183;
    // static readonly XAG: number = 184;
    // static readonly XAU: number = 185;
    // static readonly XCD: number = 186;
    // static readonly XDR: number = 187;
    // static readonly XOF: number = 188;
    // static readonly XPF: number = 189;
    // static readonly YER: number = 190;
    // static readonly ZAR: number = 191;
    // static readonly ZMK: number = 192;
    // static readonly ZMW: number = 193;
    // static readonly ZWL: number = 194;
    MdConstant.SYMBOLS = [
        MdConstant.USDT,
        MdConstant.IEO,
        MdConstant.BTC,
        MdConstant.ETH,
        MdConstant.ZEC,
        MdConstant.XTZ,
        MdConstant.SEM,
        MdConstant.IOTA,
        MdConstant.XRP,
        MdConstant.XLM,
        MdConstant.WAVES,
        MdConstant.DOGE,
        MdConstant.XMR,
        MdConstant.BCN,
        MdConstant.AMP,
        MdConstant.BTG,
        MdConstant.DGB,
        MdConstant.GAME,
        MdConstant.LSK,
        MdConstant.NAV,
        MdConstant.NLG,
        MdConstant.NXT,
        MdConstant.POT,
        MdConstant.STRAT,
        MdConstant.SYS,
        MdConstant.XVG,
        MdConstant.DLT,
        MdConstant.LTC,
        MdConstant.EOS,
        MdConstant.DASH,
        MdConstant.QTUM,
        MdConstant.OMNI,
        MdConstant.TRX,
        MdConstant.MKR,
        MdConstant.ONT,
        MdConstant.VET,
        // news
        MdConstant.AOA,
        MdConstant.ELF,
        MdConstant.AE,
        MdConstant.AION,
        MdConstant.ARDR,
        MdConstant.ARK,
        MdConstant.REP,
        MdConstant.BAT,
        MdConstant.BTM,
        MdConstant.BCH,
        MdConstant.BCD,
        MdConstant.BNB,
        MdConstant.BSV,
        MdConstant.ADA,
        MdConstant.ATOM,
        MdConstant.CRO,
        MdConstant.CNX,
        MdConstant.MANA,
        MdConstant.DCR,
        MdConstant.DGD,
        MdConstant.ELA,
        MdConstant.ENJ,
        MdConstant.FCT,
        MdConstant.GNT,
        MdConstant.GXS,
        MdConstant.HOT,
        MdConstant.ZEN,
        MdConstant.HC,
        MdConstant.ICX,
        MdConstant.IOST,
        MdConstant.KMD,
        MdConstant.KCS,
        MdConstant.LA,
        MdConstant.LOOM,
        MdConstant.ETP,
        MdConstant.MONA,
        MdConstant.DAI,
        MdConstant.NANO,
        MdConstant.XEM,
        MdConstant.NEO,
        MdConstant.OMG,
        MdConstant.PAI,
        MdConstant.PAX,
        MdConstant.NPXS,
        MdConstant.RVN,
        MdConstant.RDD,
        MdConstant.SAN,
        MdConstant.SC,
        MdConstant.SNT,
        MdConstant.THETA,
        MdConstant.TUSD,
        MdConstant.USDC,
        MdConstant.WTC,
        MdConstant.WAX,
        MdConstant.XZC,
        MdConstant.ZIL,
        MdConstant.ZRX,
    ];
    MdConstant.COINS = [
        "USDT",
        "IEO",
        "BTC",
        "ETH",
        "ZEC",
        "XTZ",
        "SEM",
        "MIOTA",
        "XRP",
        "XLM",
        "WAVES",
        "DOGE",
        "XMR",
        "BCN",
        "AMP",
        "BTG",
        "DGB",
        "GAME",
        "LSK",
        "NAV",
        "NLG",
        "NXT",
        "POT",
        "STRAT",
        "SYS",
        "XVG",
        "DLT",
        "LTC",
        "EOS",
        "DASH",
        "QTUM",
        "OMNI",
        "TRX",
        "MKR",
        "ONT",
        "VET",
        // news
        "AOA",
        "ELF",
        "AE",
        "AION",
        "ARDR",
        "ARK",
        "REP",
        "BAT",
        "BTM",
        "BCH",
        "BCD",
        "BNB",
        "BSV",
        "ADA",
        "ATOM",
        "CRO",
        "CNX",
        "MANA",
        "DCR",
        "DGD",
        "ELA",
        "ENJ",
        "FCT",
        "GNT",
        "GXS",
        "HOT",
        "ZEN",
        "HC",
        "ICX",
        "IOST",
        "KMD",
        "KCS",
        "LA",
        "LOOM",
        "ETP",
        "MONA",
        "DAI",
        "NANO",
        "XEM",
        "NEO",
        "OMG",
        "PAI",
        "PAX",
        "NPXS",
        "RVN",
        "RDD",
        "SAN",
        "SC",
        "SNT",
        "THETA",
        "TUSD",
        "USDC",
        "WTC",
        "WAX",
        "XZC",
        "ZIL",
        "ZRX",
    ];
    MdConstant.COINSID = {
        'USDT': MdConstant.USDT,
        'IEO': MdConstant.IEO,
        'BTC': MdConstant.BTC,
        'ETH': MdConstant.ETH,
        'ZEC': MdConstant.ZEC,
        'XTZ': MdConstant.XTZ,
        'SEM': MdConstant.SEM,
        'MIOTA': MdConstant.IOTA,
        'XRP': MdConstant.XRP,
        'XLM': MdConstant.XLM,
        'WAVES': MdConstant.WAVES,
        'DOGE': MdConstant.DOGE,
        'XMR': MdConstant.XMR,
        'BCN': MdConstant.BCN,
        'AMP': MdConstant.AMP,
        'BTG': MdConstant.BTG,
        'DGB': MdConstant.DGB,
        'GAME': MdConstant.GAME,
        'LSK': MdConstant.LSK,
        'NAV': MdConstant.NAV,
        'NLG': MdConstant.NLG,
        'NXT': MdConstant.NXT,
        'POT': MdConstant.POT,
        'STRAT': MdConstant.STRAT,
        'SYS': MdConstant.SYS,
        'XVG': MdConstant.XVG,
        'DLT': MdConstant.DLT,
        'LTC': MdConstant.LTC,
        'EOS': MdConstant.EOS,
        'DASH': MdConstant.DASH,
        'QTUM': MdConstant.QTUM,
        'OMNI': MdConstant.OMNI,
        'TRX': MdConstant.TRX,
        'MKR': MdConstant.MKR,
        'ONT': MdConstant.ONT,
        'VET': MdConstant.VET,
        // news
        'AOA': MdConstant.AOA,
        'ELF': MdConstant.ELF,
        'AE': MdConstant.AE,
        'AION': MdConstant.AION,
        'ARDR': MdConstant.ARDR,
        'ARK': MdConstant.ARK,
        'REP': MdConstant.REP,
        'BAT': MdConstant.BAT,
        'BTM': MdConstant.BTM,
        'BCH': MdConstant.BCH,
        'BCD': MdConstant.BCD,
        'BNB': MdConstant.BNB,
        'BSV': MdConstant.BSV,
        'ADA': MdConstant.ADA,
        'ATOM': MdConstant.ATOM,
        'CRO': MdConstant.CRO,
        'CNX': MdConstant.CNX,
        'MANA': MdConstant.MANA,
        'DCR': MdConstant.DCR,
        'DGD': MdConstant.DGD,
        'ELA': MdConstant.ELA,
        'ENJ': MdConstant.ENJ,
        'FCT': MdConstant.FCT,
        'GNT': MdConstant.GNT,
        'GXS': MdConstant.GXS,
        'HOT': MdConstant.HOT,
        'ZEN': MdConstant.ZEN,
        'HC': MdConstant.HC,
        'ICX': MdConstant.ICX,
        'IOST': MdConstant.IOST,
        'KMD': MdConstant.KMD,
        'KCS': MdConstant.KCS,
        'LA': MdConstant.LA,
        'LOOM': MdConstant.LOOM,
        'ETP': MdConstant.ETP,
        'MONA': MdConstant.MONA,
        'DAI': MdConstant.DAI,
        'NANO': MdConstant.NANO,
        'XEM': MdConstant.XEM,
        'NEO': MdConstant.NEO,
        'OMG': MdConstant.OMG,
        'PAI': MdConstant.PAI,
        'PAX': MdConstant.PAX,
        'NPXS': MdConstant.NPXS,
        'RVN': MdConstant.RVN,
        'RDD': MdConstant.RDD,
        'SAN': MdConstant.SAN,
        'SC': MdConstant.SC,
        'SNT': MdConstant.SNT,
        'THETA': MdConstant.THETA,
        'TUSD': MdConstant.TUSD,
        'USDC': MdConstant.USDC,
        'WTC': MdConstant.WTC,
        'WAX': MdConstant.WAX,
        'XZC': MdConstant.XZC,
        'ZIL': MdConstant.ZIL,
        'ZRX': MdConstant.ZRX,
    };
    // HARDCODED
    MdConstant.ISSUERS = [
        {
            name: "IEO",
            id: 0,
            site: "https://icte.io",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 5231,
            ldate: "07-08-2019"
        },
        {
            name: "HTU",
            id: 1,
            site: "https://htu.io",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 1760,
            ldate: "07-08-2019"
        },
        {
            name: "A-1",
            id: 2,
            site: "https://a-1.io",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 240,
            ldate: "07-08-2019"
        },
        {
            name: "ZAG",
            id: 3,
            site: "https://zagat.io",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 811,
            ldate: "07-08-2019"
        },
        {
            name: "LAND",
            id: 4,
            site: "https://ieo.land",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 31,
            ldate: "07-08-2019"
        },
        {
            name: "HOT",
            id: 5,
            site: "https://hotelmarkets.net",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 123,
            ldate: "07-08-2019"
        },
        {
            name: "ECN",
            id: 6,
            site: "https://ecnscan.com",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 121,
            ldate: "07-08-2019"
        },
        {
            name: "JOB",
            id: 7,
            site: "https://jobex.me",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 11,
            ldate: "07-08-2019"
        },
        {
            name: "VOTE",
            id: 8,
            site: "https://ice.vote",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 431,
            ldate: "07-08-2019"
        },
        {
            name: "GGC",
            id: 9,
            site: "https://ggcrypto.io",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 122,
            ldate: "07-08-2019"
        },
        {
            name: "CUND",
            id: 10,
            site: "https://cund.io",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 120,
            ldate: "07-08-2019"
        },
        {
            name: "MEDT",
            id: 11,
            site: "https://medtoken.io",
            available: "250,000,000",
            cdate: "07-08-2019",
            votes: 120,
            ldate: "07-08-2019"
        },
    ];
    return MdConstant;
}());
exports.MdConstant = MdConstant;
