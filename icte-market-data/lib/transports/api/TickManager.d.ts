interface TickData {
    providerId: number;
    providerName: string;
    tickerpair: [Tickerpair];
}
interface Tickerpair {
    tickername: string;
    tick: [Tick];
}
interface Tick {
    price: number;
    timestamp: number;
}
export declare class TickManager {
    private static readonly MAX_TICKS_STORE_QTY;
    private static ticksData;
    constructor();
    static getTicks(): TickData[];
    static getTicksByProviderIdAndTicker(providerId: number, tickerpair: any): false | Tickerpair;
    static getTicksByProviderId(providerId: number): false | TickData;
    /**
     * addTick Adds ticks for a specicf ticker of a specific transport/provider
     * @param providerId - Id of the transport/provider
     * @param tickername - Ticker name
     * @param newTick - Tick price
     */
    static addTick(providerId: number, tickername: string, newTick: any): void;
    private static getProviderIndex;
    private static getProviderTickerPairIndex;
    private static providerNameResolver;
}
export {};
