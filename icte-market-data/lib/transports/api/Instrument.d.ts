import { InstrumentChild } from "./InstrumentChild";
export declare class Instrument {
    private _childs;
    private _seq;
    private _id;
    private _sectype;
    private _symbol;
    private _code;
    childs: InstrumentChild[];
    seq: number;
    id: string;
    sectype: number;
    symbol: string;
    code: string;
}
