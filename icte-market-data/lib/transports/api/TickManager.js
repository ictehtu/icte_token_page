"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MdConstant_1 = require("./MdConstant");
var TickManager = /** @class */ (function () {
    function TickManager() {
    }
    TickManager.getTicks = function () {
        return TickManager.ticksData;
    };
    TickManager.getTicksByProviderIdAndTicker = function (providerId, tickerpair) {
        var index = TickManager.getProviderIndex(providerId);
        if (index == -1)
            return false;
        var indexTickerpair = TickManager.getProviderTickerPairIndex(index, tickerpair);
        if (indexTickerpair == -1)
            return false;
        return TickManager.ticksData[index].tickerpair[indexTickerpair];
    };
    TickManager.getTicksByProviderId = function (providerId) {
        var index = TickManager.getProviderIndex(providerId);
        if (index == -1)
            return false;
        return TickManager.ticksData[index];
    };
    /**
     * addTick Adds ticks for a specicf ticker of a specific transport/provider
     * @param providerId - Id of the transport/provider
     * @param tickername - Ticker name
     * @param newTick - Tick price
     */
    TickManager.addTick = function (providerId, tickername, newTick) {
        var newTickerpair = {
            tickername: tickername,
            tick: [
                {
                    price: newTick,
                    timestamp: Date.now()
                }
            ]
        };
        var providerIndex = TickManager.getProviderIndex(providerId);
        if (providerIndex !== -1) {
            // Provider exist, update tickerpair
            var tickerPairIndex = TickManager.getProviderTickerPairIndex(providerIndex, tickername);
            if (tickerPairIndex !== -1) {
                // Tickerpair exist, update tick - Remove older if exceed the limits
                var pLen = TickManager.ticksData[providerIndex].tickerpair[tickerPairIndex].tick.length;
                var lastTick = 0;
                if (pLen > 0) {
                    lastTick = TickManager.ticksData[providerIndex].tickerpair[tickerPairIndex].tick[pLen - 1].price;
                }
                if (lastTick != newTick) { // chek if really is a tick
                    if (pLen > TickManager.MAX_TICKS_STORE_QTY - 1) { // remove older if exceed the limits
                        TickManager.ticksData[providerIndex].tickerpair[tickerPairIndex].tick.shift();
                    }
                    TickManager.ticksData[providerIndex].tickerpair[tickerPairIndex].tick.push({
                        price: newTick,
                        timestamp: Date.now()
                    });
                }
            }
            else {
                // Tickerpair doesnt exist, register newone with first tick
                TickManager.ticksData[providerIndex].tickerpair.push(newTickerpair);
            }
        }
        else {
            // Provider doesnt exist, create from 0 with a tickerpair and tick
            var tickData = {
                providerId: providerId,
                providerName: TickManager.providerNameResolver(providerId),
                tickerpair: [newTickerpair]
            };
            TickManager.ticksData.push(tickData);
        }
    };
    TickManager.getProviderIndex = function (providerId) {
        var index = TickManager.ticksData.findIndex(function (e) {
            return e.providerId == providerId;
        });
        return index;
    };
    TickManager.getProviderTickerPairIndex = function (providerIndex, tickerpair) {
        if (this.ticksData.length === 0)
            return -1;
        var index = TickManager.ticksData[providerIndex].tickerpair.findIndex(function (e) {
            return e.tickername == tickerpair;
        });
        return index;
    };
    TickManager.providerNameResolver = function (providerId) {
        return MdConstant_1.MdConstant.VENUES[providerId];
    };
    TickManager.MAX_TICKS_STORE_QTY = 133;
    TickManager.ticksData = [];
    return TickManager;
}());
exports.TickManager = TickManager;
