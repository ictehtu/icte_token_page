"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PortfolioChild = /** @class */ (function () {
    function PortfolioChild() {
        this._seq = 0;
        this._qty = 0;
    }
    Object.defineProperty(PortfolioChild.prototype, "seq", {
        get: function () {
            return this._seq;
        },
        set: function (value) {
            this._seq = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PortfolioChild.prototype, "qty", {
        get: function () {
            return this._qty;
        },
        set: function (value) {
            this._qty = value;
        },
        enumerable: true,
        configurable: true
    });
    return PortfolioChild;
}());
exports.PortfolioChild = PortfolioChild;
