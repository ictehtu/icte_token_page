"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InstrumentChild = /** @class */ (function () {
    function InstrumentChild() {
        this._seq = 0;
        this._percent = 0.0;
    }
    Object.defineProperty(InstrumentChild.prototype, "seq", {
        get: function () {
            return this._seq;
        },
        set: function (value) {
            this._seq = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InstrumentChild.prototype, "percent", {
        get: function () {
            return this._percent;
        },
        set: function (value) {
            this._percent = value;
        },
        enumerable: true,
        configurable: true
    });
    return InstrumentChild;
}());
exports.InstrumentChild = InstrumentChild;
