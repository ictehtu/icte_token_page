export interface MdTransport {
    connect(tickers: string[], exchanges: number[]): any;
    isConnected(): any;
}
