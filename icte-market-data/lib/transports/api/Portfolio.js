"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Portfolio = /** @class */ (function () {
    function Portfolio() {
        this._cash = 0.0;
    }
    Object.defineProperty(Portfolio.prototype, "cash", {
        get: function () {
            return this._cash;
        },
        set: function (value) {
            this._cash = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "childs", {
        get: function () {
            return this._childs;
        },
        set: function (value) {
            this._childs = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "cusip", {
        get: function () {
            return this._cusip;
        },
        set: function (value) {
            this._cusip = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "isin", {
        get: function () {
            return this._isin;
        },
        set: function (value) {
            this._isin = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "domestic", {
        get: function () {
            return this._domestic;
        },
        set: function (value) {
            this._domestic = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "sectype", {
        get: function () {
            return this._sectype;
        },
        set: function (value) {
            this._sectype = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "seq", {
        get: function () {
            return this._seq;
        },
        set: function (value) {
            this._seq = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "symbol", {
        get: function () {
            return this._symbol;
        },
        set: function (value) {
            this._symbol = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "unit", {
        get: function () {
            return this._unit;
        },
        set: function (value) {
            this._unit = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Portfolio.prototype, "subscribe_count", {
        get: function () {
            return this._subscribe_count;
        },
        set: function (value) {
            this._subscribe_count = value;
        },
        enumerable: true,
        configurable: true
    });
    return Portfolio;
}());
exports.Portfolio = Portfolio;
