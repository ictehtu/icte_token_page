import { Portfolio } from "./Portfolio";
import { Instrument } from "./Instrument";
export declare class RatesData {
    constructor();
    private _portfolios;
    portfolios: Portfolio[];
    private _instruments;
    instruments: Instrument[];
    private _symbols;
    symbols: string[];
}
