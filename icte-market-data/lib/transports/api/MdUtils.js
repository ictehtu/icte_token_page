"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MdUtils = /** @class */ (function () {
    function MdUtils() {
    }
    //Return the number of seconds sinsce midnight
    MdUtils.getMsSinceMidnight = function () {
        return this.getGmtZeroNow() - this.getGmtZeroMidnight();
    };
    MdUtils.getGmtZeroNow = function () {
        var d = new Date();
        var z = d.getTime() + (d.getTimezoneOffset() * 60000);
        var zd = new Date(z);
        return zd.getTime();
    };
    MdUtils.getGmtZeroMidnight = function () {
        var d = new Date();
        var z = d.getTime() + (d.getTimezoneOffset() * 60000);
        var zd = new Date(z);
        zd.setHours(0);
        zd.setMinutes(0);
        zd.setSeconds(0);
        zd.setMilliseconds(0);
        return zd.getTime();
    };
    return MdUtils;
}());
exports.MdUtils = MdUtils;
