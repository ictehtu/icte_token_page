import { PortfolioChild } from "./PortfolioChild";
export declare class Portfolio {
    private _cash;
    private _childs;
    private _cusip;
    private _isin;
    private _domestic;
    private _sectype;
    private _seq;
    private _symbol;
    private _unit;
    private _id;
    private _subscribe_count;
    cash: number;
    childs: PortfolioChild[];
    cusip: string;
    isin: string;
    domestic: boolean;
    sectype: number;
    seq: number;
    symbol: string;
    unit: number;
    id: string;
    subscribe_count: number;
}
