"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RatesData = /** @class */ (function () {
    function RatesData() {
        this._instruments = [];
        this._portfolios = [];
        this._symbols = [];
    }
    Object.defineProperty(RatesData.prototype, "portfolios", {
        get: function () {
            return this._portfolios;
        },
        set: function (value) {
            this._portfolios = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RatesData.prototype, "instruments", {
        get: function () {
            return this._instruments;
        },
        set: function (value) {
            this._instruments = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RatesData.prototype, "symbols", {
        get: function () {
            return this._symbols;
        },
        set: function (value) {
            this._symbols = value;
        },
        enumerable: true,
        configurable: true
    });
    return RatesData;
}());
exports.RatesData = RatesData;
