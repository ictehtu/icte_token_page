export declare class BinaryReader {
    private _buffer;
    private _pos;
    constructor(data: Uint8Array);
    readInt8(): number;
    readUInt8(): number;
    readInt16(): number;
    readUInt16(): number;
    readInt32(): number;
    readUInt32(): number;
    readUInt64(): number;
    readInt64(): number;
    readFloat(): number;
    readDouble(): number;
    readByte(): number;
    seek(pos: any): void;
    getPosition(): number;
    getSize(): number;
    private _decodeFloat;
    private _decodeInt;
    private _readByte;
    private _shl;
    private _readBits;
}
