import * as EventEmiter from 'events';
export declare class CurrencyProxy extends EventEmiter {
    private readonly apiKey;
    private readonly apiUrl;
    private readonly url;
    private isConnected;
    constructor();
    getRates(): Promise<void>;
}
