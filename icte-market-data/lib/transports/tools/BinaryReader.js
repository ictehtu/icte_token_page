"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BinaryReader = /** @class */ (function () {
    function BinaryReader(data) {
        this._pos = 0;
        this._buffer = Buffer.from(data);
        this._pos = 0;
    }
    /* Public */
    BinaryReader.prototype.readInt8 = function () {
        return this._decodeInt(8, true);
    };
    BinaryReader.prototype.readUInt8 = function () {
        return this._decodeInt(8, false);
    };
    BinaryReader.prototype.readInt16 = function () {
        return this._decodeInt(16, true);
    };
    BinaryReader.prototype.readUInt16 = function () {
        return this._decodeInt(16, false);
    };
    BinaryReader.prototype.readInt32 = function () {
        return this._decodeInt(32, true);
    };
    BinaryReader.prototype.readUInt32 = function () {
        return this._decodeInt(32, false);
    };
    BinaryReader.prototype.readUInt64 = function () {
        return this._decodeInt(64, false);
    };
    BinaryReader.prototype.readInt64 = function () {
        return this._decodeInt(64, true);
    };
    BinaryReader.prototype.readFloat = function () {
        return this._decodeFloat(23, 8);
    };
    BinaryReader.prototype.readDouble = function () {
        return this._decodeFloat(52, 11);
    };
    BinaryReader.prototype.readByte = function () {
        this._pos += 1;
        return this._readByte(1, 1);
    };
    BinaryReader.prototype.seek = function (pos) {
        this._pos = pos;
    };
    BinaryReader.prototype.getPosition = function () {
        return this._pos;
    };
    BinaryReader.prototype.getSize = function () {
        return this._buffer.length;
    };
    /* Private */
    BinaryReader.prototype._decodeFloat = function (precisionBits, exponentBits) {
        var length = precisionBits + exponentBits + 1;
        var size = length >> 3;
        var bias = Math.pow(2, exponentBits - 1) - 1;
        var signal = this._readBits(precisionBits + exponentBits, 1, size);
        var exponent = this._readBits(precisionBits, exponentBits, size);
        var significand = 0;
        var divisor = 2;
        var curByte = 0; //length + (-precisionBits >> 3) - 1;
        do {
            var byteValue = this._readByte(++curByte, size);
            var startBit = precisionBits % 8 || 8;
            var mask = 1 << startBit;
            while (mask >>= 1) {
                if (byteValue & mask) {
                    significand += 1 / divisor;
                }
                divisor *= 2;
            }
        } while (precisionBits -= startBit);
        this._pos += size;
        return exponent == (bias << 1) + 1 ? significand ? NaN : signal ? -Infinity : +Infinity
            : (1 + signal * -2) * (exponent || significand ? !exponent ? Math.pow(2, -bias + 1) * significand
                : Math.pow(2, exponent - bias) * (1 + significand) : 0);
    };
    BinaryReader.prototype._decodeInt = function (bits, signed) {
        var x = this._readBits(0, bits, bits / 8), max = Math.pow(2, bits);
        var result = signed && x >= max / 2 ? x - max : x;
        this._pos += bits / 8;
        return result;
    };
    BinaryReader.prototype._readByte = function (i, size) {
        return this._buffer.readInt8(this._pos + size - i - 1) & 0xff;
    };
    BinaryReader.prototype._shl = function (a, b) {
        for (++b; --b; a = ((a %= 0x7fffffff + 1) & 0x40000000) == 0x40000000 ? a * 2 : (a - 0x40000000) * 2 + 0x7fffffff + 1)
            ;
        return a;
    };
    BinaryReader.prototype._readBits = function (start, length, size) {
        var offsetLeft = (start + length) % 8;
        var offsetRight = start % 8;
        var curByte = size - (start >> 3) - 1;
        var lastByte = size + (-(start + length) >> 3);
        var diff = curByte - lastByte;
        var sum = (this._readByte(curByte, size) >> offsetRight) & ((1 << (diff ? 8 - offsetRight : length)) - 1);
        if (diff && offsetLeft) {
            sum += (this._readByte(lastByte++, size) & ((1 << offsetLeft) - 1)) << (diff-- << 3) - offsetRight;
        }
        while (diff) {
            sum += this._shl(this._readByte(lastByte++, size), (diff-- << 3) - offsetRight);
        }
        return sum;
    };
    return BinaryReader;
}());
exports.BinaryReader = BinaryReader;
