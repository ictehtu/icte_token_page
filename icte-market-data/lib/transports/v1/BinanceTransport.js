"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var BinanceProxy_1 = require("./BinanceProxy");
var BinanceConstant_1 = require("./BinanceConstant");
var MsgFactory_1 = require("./MsgFactory");
var MdConstant_1 = require("../api/MdConstant");
var MdUtils_1 = require("../api/MdUtils");
var Utilities_1 = require("../../model/Utilities");
var TickManager_1 = require("../api/TickManager");
// import { load } from "../../wasm";
var BinanceTransport = /** @class */ (function () {
    function BinanceTransport(callback) {
        this.subscriptions = {};
        this._connected = false;
        this.cb = callback;
    }
    BinanceTransport.prototype.initWasm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    BinanceTransport.prototype.isConnected = function () {
        return this._connected;
    };
    Object.defineProperty(BinanceTransport.prototype, "id", {
        get: function () {
            return 3;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BinanceTransport.prototype, "name", {
        get: function () {
            return MdConstant_1.MdConstant.VENUES[this.id];
        },
        enumerable: true,
        configurable: true
    });
    BinanceTransport.prototype.connect = function (tickers) {
        this.tickers = tickers;
        this.init(); //Start connection
    };
    BinanceTransport.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var wsLimiter;
            var _this = this;
            return __generator(this, function (_a) {
                wsLimiter = new Utilities_1.Utilities().wsLimiter;
                this.proxy = new BinanceProxy_1.BinanceProxy(this.tickers);
                this.proxy.on(BinanceConstant_1.BinanceConstant.OPENEVENT, function (data) {
                    _this._connected = true;
                    Utilities_1.Utilities.logger.info("Binance transport connected");
                });
                this.proxy.on(BinanceConstant_1.BinanceConstant.CLOSEEVENT, function (data) {
                    _this._connected = false;
                    Utilities_1.Utilities.logger.warn("** Binance close!!", data);
                });
                this.proxy.on(BinanceConstant_1.BinanceConstant.ERROREVENT, function (data) {
                    //TODO: Error system
                    Utilities_1.Utilities.logger.warn("*** Binance ERROR!!", data);
                });
                this.proxy.on(BinanceConstant_1.BinanceConstant.TICKEREVENT, wsLimiter(function (symbol, message) {
                    _this.processMessage(symbol, message);
                }));
                return [2 /*return*/];
            });
        });
    };
    BinanceTransport.prototype.processMessage = function (symbol, message) {
        if (symbol in BinanceConstant_1.BinanceConstant.PAIRS) {
            var cqMsg = MsgFactory_1.MsgFactory.level1Msg;
            var bestBid = Math.trunc(message.b * BinanceConstant_1.BinanceConstant.DIV) / BinanceConstant_1.BinanceConstant.DIV;
            var bestAsk = Math.trunc(message.a * BinanceConstant_1.BinanceConstant.DIV) / BinanceConstant_1.BinanceConstant.DIV;
            // let sizeBid = Math.trunc(message.B * BinanceConstant.DIV) / BinanceConstant.DIV;
            // let sizeAsk = Math.trunc(message.A * BinanceConstant.DIV) / BinanceConstant.DIV;
            var price = Math.trunc(message.c * BinanceConstant_1.BinanceConstant.DIV) / BinanceConstant_1.BinanceConstant.DIV;
            // let bestBid = this.wasm.parseAmount(message.b, BinanceConstant.DIV);
            // let bestAsk = this.wasm.parseAmount(message.a, BinanceConstant.DIV);
            // // let sizeBid = Math.trunc(message.B * BinanceConstant.DIV) / BinanceConstant.DIV;
            // // let sizeAsk = Math.trunc(message.A * BinanceConstant.DIV) / BinanceConstant.DIV;
            // let price = this.wasm.parseAmount(message.c, BinanceConstant.DIV);
            cqMsg.buffer(MdUtils_1.MdUtils.getMsSinceMidnight(), BinanceConstant_1.BinanceConstant.PAIRS[symbol].symbol, Math.trunc(price), Math.trunc(price % 1 * BinanceConstant_1.BinanceConstant.DIV), Math.trunc(bestBid), Math.trunc(bestBid % 1 * BinanceConstant_1.BinanceConstant.DIV), Math.trunc(bestAsk), Math.trunc(bestAsk % 1 * BinanceConstant_1.BinanceConstant.DIV), MdConstant_1.MdConstant.BinanceTransport, BinanceConstant_1.BinanceConstant.PAIRS[symbol].bench);
            this.cb.on(cqMsg);
            // fill ticks
            var tickername = BinanceConstant_1.BinanceConstant.PAIRS[symbol].symbol + "-" + BinanceConstant_1.BinanceConstant.PAIRS[symbol].bench;
            TickManager_1.TickManager.addTick(MdConstant_1.MdConstant.BinanceTransport, tickername, price);
        }
    };
    return BinanceTransport;
}());
exports.BinanceTransport = BinanceTransport;
