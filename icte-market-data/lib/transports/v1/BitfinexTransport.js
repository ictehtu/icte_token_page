"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var BitfinexProxy_1 = require("./BitfinexProxy");
var BitfinexConstant_1 = require("./BitfinexConstant");
var MsgFactory_1 = require("./MsgFactory");
var MdConstant_1 = require("../api/MdConstant");
var MdUtils_1 = require("../api/MdUtils");
var Utilities_1 = require("../../model/Utilities");
var TickManager_1 = require("../api/TickManager");
// import { load } from "../../wasm";
var BitfinexTransport = /** @class */ (function () {
    function BitfinexTransport(callback) {
        this._connected = false;
        this.cb = callback;
    }
    BitfinexTransport.prototype.isConnected = function () {
        return this._connected;
    };
    Object.defineProperty(BitfinexTransport.prototype, "id", {
        get: function () {
            return 4;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BitfinexTransport.prototype, "name", {
        get: function () {
            return MdConstant_1.MdConstant.VENUES[this.id];
        },
        enumerable: true,
        configurable: true
    });
    BitfinexTransport.prototype.connect = function (tickers) {
        this.tickers = tickers;
        this.init(); //Start connection
    };
    BitfinexTransport.prototype.initWasm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    BitfinexTransport.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var wsLimiter;
            var _this = this;
            return __generator(this, function (_a) {
                wsLimiter = new Utilities_1.Utilities().wsLimiter;
                this.proxy = new BitfinexProxy_1.BitfinexProxy(this.tickers);
                this.proxy.on(BitfinexConstant_1.BitfinexConstant.OPENEVENT, function (data) {
                    _this._connected = true;
                    Utilities_1.Utilities.logger.warn("Bitfinex is connected.");
                });
                this.proxy.on(BitfinexConstant_1.BitfinexConstant.CLOSEEVENT, function (data) {
                    _this._connected = false;
                    Utilities_1.Utilities.logger.warn("** Bitfinex close!!", data.code, data.reason);
                });
                this.proxy.on(BitfinexConstant_1.BitfinexConstant.ERROREVENT, function (data) {
                    //TODO: Error system
                    Utilities_1.Utilities.logger.warn("*** Bitfinex ERROR!!", data);
                });
                this.proxy.on(BitfinexConstant_1.BitfinexConstant.TICKEREVENT, wsLimiter(function (symbol, message) {
                    _this.processMessage(symbol, message);
                }));
                return [2 /*return*/];
            });
        });
    };
    BitfinexTransport.prototype.processMessage = function (symbol, message) {
        if (symbol in BitfinexConstant_1.BitfinexConstant.PAIRS) {
            var cqMsg = MsgFactory_1.MsgFactory.level1Msg;
            var bid = Math.trunc(message[BitfinexConstant_1.BitfinexConstant.MSG_BID] * BitfinexConstant_1.BitfinexConstant.DIV) / BitfinexConstant_1.BitfinexConstant.DIV;
            var ask = Math.trunc(message[BitfinexConstant_1.BitfinexConstant.MSG_ASK] * BitfinexConstant_1.BitfinexConstant.DIV) / BitfinexConstant_1.BitfinexConstant.DIV;
            var price = Math.trunc(message[BitfinexConstant_1.BitfinexConstant.MSG_LAST_PRICE] * BitfinexConstant_1.BitfinexConstant.DIV) / BitfinexConstant_1.BitfinexConstant.DIV;
            // let bid = this.wasm.parseAmount(message[BitfinexConstant.MSG_ASK], BitfinexConstant.DIV);
            // let ask = this.wasm.parseAmount(message[BitfinexConstant.MSG_ASK],  BitfinexConstant.DIV);
            // let price = this.wasm.parseAmount(message[BitfinexConstant.MSG_LAST_PRICE], BitfinexConstant.DIV);
            cqMsg.buffer(MdUtils_1.MdUtils.getMsSinceMidnight(), BitfinexConstant_1.BitfinexConstant.PAIRS[symbol].symbol, Math.trunc(price), Math.trunc(price % 1 * BitfinexConstant_1.BitfinexConstant.DIV), Math.trunc(bid), Math.trunc(bid % 1 * BitfinexConstant_1.BitfinexConstant.DIV), Math.trunc(ask), Math.trunc(ask % 1 * BitfinexConstant_1.BitfinexConstant.DIV), MdConstant_1.MdConstant.BitfinexTransport, BitfinexConstant_1.BitfinexConstant.PAIRS[symbol].bench);
            this.cb.on(cqMsg);
            // fill ticks
            var tickername = BitfinexConstant_1.BitfinexConstant.PAIRS[symbol].symbol + "-" + BitfinexConstant_1.BitfinexConstant.PAIRS[symbol].bench;
            TickManager_1.TickManager.addTick(MdConstant_1.MdConstant.BitfinexTransport, tickername, price);
        }
    };
    return BitfinexTransport;
}());
exports.BitfinexTransport = BitfinexTransport;
