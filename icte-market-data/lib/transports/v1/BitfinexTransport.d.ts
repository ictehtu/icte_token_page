import { MdTransport } from "../api/MdTransport";
import { MdCallback } from "../api/MdCallback";
export declare class BitfinexTransport implements MdTransport {
    private proxy;
    private _connected;
    private cb;
    private wasm;
    private tickers;
    constructor(callback: MdCallback);
    isConnected(): boolean;
    readonly id: number;
    readonly name: string;
    connect(tickers?: string[]): void;
    private initWasm;
    private init;
    private processMessage;
}
