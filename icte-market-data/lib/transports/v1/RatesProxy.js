"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmiter = require("events");
var Utilities_1 = require("../../model/Utilities");
var BinaryReader_1 = require("../tools/BinaryReader");
var Portfolio_1 = require("../api/Portfolio");
var PortfolioChild_1 = require("../api/PortfolioChild");
var RatesData_1 = require("../api/RatesData");
var Instrument_1 = require("../api/Instrument");
var InstrumentChild_1 = require("../api/InstrumentChild");
var JSZip = require("jszip");
var fetch = require("isomorphic-fetch");
var RatesProxy = /** @class */ (function () {
    function RatesProxy() {
        var _this = this;
        this.jsZip = new JSZip();
        this.downloadFileBlob = function (url) { return __awaiter(_this, void 0, void 0, function () {
            var file_name;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch(this.filedateurl)
                            .then(function (response) {
                            if (response.status === 200 || response.status === 0) {
                                return Promise.resolve(response.text());
                            }
                            else {
                                return Promise.reject(new Error(response.statusText));
                            }
                        }).then(function (text) {
                            return text;
                        }).catch(function (error) {
                            // throw error;
                        })];
                    case 1:
                        file_name = _a.sent();
                        this.url = "https://rates." + this.domain + "/fairpricer/cryptoasset/" + file_name + ".zip";
                        Utilities_1.Utilities.logger.debug(this.url);
                        fetch(this.url)
                            .then(function (response) {
                            if (response.status === 200 || response.status === 0) {
                                return Promise.resolve(response.blob());
                            }
                            else {
                                return Promise.reject(new Error(response.statusText));
                            }
                        })
                            .then(function (zip) {
                            return _this.loadData(zip);
                        })
                            .then(function (rates) {
                            RatesProxy.sendData("d", rates);
                        })
                            .catch(function (error) {
                            // throw error;
                        });
                        return [2 /*return*/];
                }
            });
        }); };
        this.downloadFile = function (url) { return __awaiter(_this, void 0, void 0, function () {
            var data, file_name;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch(this.filedateurl)
                            .then(function (response) {
                            if (response.status === 200 || response.status === 0) {
                                return Promise.resolve(response.text());
                            }
                            else {
                                return Promise.reject(new Error(response.statusText));
                            }
                        }).then(function (text) {
                            return text;
                        }).catch(function (error) {
                        })];
                    case 1:
                        file_name = _a.sent();
                        this.url = "https://rates." + this.domain + "/fairpricer/cryptoasset/" + file_name + ".zip";
                        return [2 /*return*/, fetch(this.url)
                                .then(function (response) {
                                if (response.status === 200 || response.status === 0) {
                                    return Promise.resolve(response.body);
                                }
                                else {
                                    return Promise.reject(new Error(response.statusText));
                                }
                            })
                                .then(function (body) {
                                if (body.on) {
                                    body.on("data", function (chunk) {
                                        var b = Buffer.from(chunk);
                                        if (!data) {
                                            data = b;
                                        }
                                        else
                                            data = Buffer.concat([data, b]);
                                    });
                                    body.on("end", function () {
                                        //this.getBinaryData(data).then(this.getTextData);
                                        _this.loadData(data).then(function (rates) {
                                            RatesProxy.sendData("d", rates);
                                        });
                                    });
                                }
                            }).catch(function (error) {
                            })];
                }
            });
        }); };
        this.loadData = function (data) {
            return _this.jsZip.loadAsync(data).then(function (zip) { return __awaiter(_this, void 0, void 0, function () {
                var forwardData, reverseData, instrumentsData, portfoliosData, instrument_list_arr, portfolio_list_arr, pos, _i, _a, line, rst, _b, _c, line, rst, b_forward, b_reverse, ratesData, portfolio_list_Count, i, components_count, j, instrument_list_Count, i, components_count, j;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0: return [4 /*yield*/, zip.file("forward.bin").async("uint8array")];
                        case 1:
                            forwardData = _d.sent();
                            return [4 /*yield*/, zip.file("reverse.bin").async("uint8array")];
                        case 2:
                            reverseData = _d.sent();
                            return [4 /*yield*/, zip.file("intruments.lst").async("string")];
                        case 3:
                            instrumentsData = _d.sent();
                            return [4 /*yield*/, zip.file("portfolios.lst").async("string")];
                        case 4:
                            portfoliosData = _d.sent();
                            instrument_list_arr = [];
                            portfolio_list_arr = [];
                            pos = 0;
                            for (_i = 0, _a = instrumentsData.split(/[\r\n]+/); _i < _a.length; _i++) {
                                line = _a[_i];
                                if (line.length > 0) {
                                    rst = line.split(",");
                                    instrument_list_arr.push({ securityId: rst[0], securityIdType: rst[1], symbol: rst[2] });
                                }
                            }
                            for (_b = 0, _c = portfoliosData.split(/[\r\n]+/); _b < _c.length; _b++) {
                                line = _c[_b];
                                if (line.length > 0) {
                                    rst = line.split(",");
                                    portfolio_list_arr.push({ seq: pos++, symbol: rst[2], type: rst[3] });
                                }
                            }
                            b_forward = new BinaryReader_1.BinaryReader(forwardData);
                            b_reverse = new BinaryReader_1.BinaryReader(reverseData);
                            ratesData = new RatesData_1.RatesData();
                            portfolio_list_Count = b_forward.readInt32();
                            for (i = 0; i < portfolio_list_Count; i++) {
                                ratesData.portfolios[i] = new Portfolio_1.Portfolio();
                                ratesData.portfolios[i].seq = b_forward.readInt32();
                                ratesData.portfolios[i].unit = b_forward.readInt32();
                                ratesData.portfolios[i].cash = b_forward.readDouble();
                                ratesData.portfolios[i].domestic = b_forward.readByte() != 0;
                                ratesData.portfolios[i].symbol = portfolio_list_arr[i].symbol;
                                ratesData.symbols[i] = portfolio_list_arr[i].symbol;
                                components_count = b_forward.readInt32();
                                ratesData.portfolios[i].childs = [];
                                for (j = 0; j < components_count; j++) {
                                    ratesData.portfolios[i].childs[j] = new PortfolioChild_1.PortfolioChild();
                                    ratesData.portfolios[i].childs[j].seq = b_forward.readInt32();
                                    ratesData.portfolios[i].childs[j].qty = b_forward.readInt64();
                                }
                            }
                            instrument_list_Count = b_reverse.readInt32();
                            for (i = 0; i < instrument_list_Count; i++) {
                                ratesData.instruments[i] = new Instrument_1.Instrument();
                                ratesData.instruments[i].seq = b_reverse.readInt32();
                                ratesData.instruments[i].id = instrument_list_arr[i].securityId;
                                ratesData.instruments[i].code = instrument_list_arr[i].securityIdType;
                                ratesData.instruments[i].symbol = instrument_list_arr[i].symbol;
                                if (ratesData.instruments[i].symbol == null || ratesData.instruments[i].symbol.length == 0) {
                                    ratesData.instruments[i].symbol = ratesData.instruments[i].id;
                                }
                                components_count = b_reverse.readInt32();
                                ratesData.instruments[i].childs = [];
                                for (j = 0; j < components_count; j++) {
                                    ratesData.instruments[i].childs[j] = new InstrumentChild_1.InstrumentChild();
                                    ratesData.instruments[i].childs[j].seq = b_reverse.readInt32();
                                    ratesData.instruments[i].childs[j].percent = b_reverse.readDouble();
                                }
                            }
                            return [2 /*return*/, Promise.resolve(ratesData)];
                    }
                });
            }); });
        };
        this.domain = "nyqex.org";
        this.filedateurl = "https://rates." + this.domain + "/api/fp/tradeDate";
    }
    RatesProxy.sendData = function (name, data) {
        this.event.emit(name, data);
    };
    RatesProxy.prototype.getRates = function () {
        // this.downloadFileBlob(this.url);
        this.downloadFile(this.url);
    };
    RatesProxy.prototype.getRatesNodeJS = function () {
        this.downloadFile(this.url);
    };
    RatesProxy.event = new EventEmiter();
    return RatesProxy;
}());
exports.RatesProxy = RatesProxy;
