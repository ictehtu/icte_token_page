import * as EventEmiter from 'events';
/***
 * Poloniex Wrapper Class
 */
export declare class PoloniexProxy extends EventEmiter {
    private webSocket;
    private wsState;
    private commandsQueue;
    private _keepAliveInterval;
    private tickers;
    constructor(tickers?: string[]);
    private subscribe;
    private send;
    private _marketEvent;
}
