"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../../model/Constant");
var TimeMsg_1 = require("../../parser/TimeMsg");
var BuyMsg_1 = require("../../parser/BuyMsg");
var SellMsg_1 = require("../../parser/SellMsg");
var RateMsg_1 = require("../../parser/RateMsg");
var Utilities_1 = require("../../model/Utilities");
var IpfsProxy_1 = require("./IpfsProxy");
var MdUtils_1 = require("../api/MdUtils");
var MdConstant_1 = require("../api/MdConstant");
require('es6-promise').polyfill();
var fetch = require('isomorphic-fetch');
var WebSocket = require('isomorphic-ws');
var HtuSocketIoTransport = /** @class */ (function () {
    function HtuSocketIoTransport(callback) {
        var _this = this;
        this.parsers = [];
        this.utf8 = 'utf8';
        this.isConnected = function () {
            return _this._connected;
        };
        this.connect = function (symbols) {
            Utilities_1.Utilities.logger.info("HtuSocket transport connected");
            _this._connected = true;
            _this.subs = symbols;
            _this.sendRate(0.32, MdConstant_1.MdConstant.USDT, MdConstant_1.MdConstant.HTU, MdConstant_1.MdConstant.HtuSocketIoTransport);
        };
        this.sendRate = function (rate, frontSymbol, backSymbol, venue) {
            _this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Rate].buffer(MdUtils_1.MdUtils.getMsSinceMidnight(), frontSymbol, backSymbol, Math.trunc(rate), Math.trunc(rate % 1 * Utilities_1.Utilities.DIV), venue);
            _this.callback.on(_this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Rate]);
        };
        this.callback = callback;
        this.ipfs = new IpfsProxy_1.IpfsProxy();
        this.parsers[Constant_1.Constant.Msg_Sys] = [];
        this.parsers[Constant_1.Constant.Msg_Sys][Constant_1.Constant.Sub_SysTime] = new TimeMsg_1.TimeMsg();
        this.parsers[Constant_1.Constant.Msg_Quote] = [];
        this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Buy] = new BuyMsg_1.BuyMsg();
        this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Sell] = new SellMsg_1.SellMsg();
        this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Rate] = new RateMsg_1.RateMsg();
    }
    return HtuSocketIoTransport;
}());
exports.HtuSocketIoTransport = HtuSocketIoTransport;
