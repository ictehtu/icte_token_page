/**
 By: Marco A. Barragan @ ICTE
 */
import { MdTransport } from "../api/MdTransport";
import { MdCallback } from "../api/MdCallback";
export declare class IeoTransport implements MdTransport {
    private subs;
    private callback;
    private ipfs;
    private socket;
    private parsers;
    private readonly rates;
    private readonly currencies;
    private readonly PRICE_RATE;
    private readonly utf8;
    private _connected;
    constructor(callback: MdCallback);
    readonly id: number;
    readonly name: string;
    isConnected: () => boolean;
    connect: () => void;
    processCrypto: () => void;
    processCurrency: () => void;
    sendRate: (rate: any, frontSymbol: number, backSymbol: number, venue: number) => void;
    sendPrice: () => void;
}
