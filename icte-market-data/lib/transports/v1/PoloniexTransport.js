"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var MdConstant_1 = require("../api/MdConstant");
var MsgFactory_1 = require("./MsgFactory");
var PoloniexProxy_1 = require("./PoloniexProxy");
var PoloniexConstants_1 = require("./PoloniexConstants");
var Utilities_1 = require("../../model/Utilities");
var TickManager_1 = require("../api/TickManager");
// import { load } from "../../wasm";
var PoloniexTransport = /** @class */ (function () {
    function PoloniexTransport(callback) {
        var _this = this;
        this.subscriptions = {};
        this._connected = false;
        this.subscribe = function () {
            if (_this.tickers) {
                for (var i = 0; i < _this.tickers.length; i++) {
                    var tickerCoin = _this.tickers[i].split('-')[0].toUpperCase();
                    var tickerBench = _this.tickers[i].split('-')[1].toUpperCase();
                    var tickerPair = tickerBench + "_" + tickerCoin;
                    //Check if the pair is in the valid pair list for Poloniex
                    if (PoloniexConstants_1.PoloniexConstants.validPairs[tickerPair] != null) {
                        var coinId = MdConstant_1.MdConstant.COINS.indexOf(tickerCoin);
                        var benchId = MdConstant_1.MdConstant.COINS.indexOf(tickerBench);
                        //Create the subscription cache
                        _this.subscriptions[PoloniexConstants_1.PoloniexConstants.validPairs[tickerPair]] = {
                            'symbol': MdConstant_1.MdConstant.COINS[coinId],
                            'id': coinId,
                            'price': 0,
                            'benchmark': benchId,
                            'bestBid': 0,
                            'bestAsk': 0,
                            'volume': 0
                        };
                    }
                }
            }
            else {
                //Process all the subscription requests
                for (var symbolId = 0; symbolId < MdConstant_1.MdConstant.COINS.length; symbolId++) {
                    //construct the other benchmarks
                    for (var bnmrk = 0; bnmrk < MdConstant_1.MdConstant.COINS.length; bnmrk++) {
                        var pair = MdConstant_1.MdConstant.COINS[bnmrk] + '_' + MdConstant_1.MdConstant.COINS[symbolId]; //Look for symbol in the constants array
                        //Check if the pair is in the valid pair list for Poloniex
                        if (PoloniexConstants_1.PoloniexConstants.validPairs[pair] != null) {
                            //Create the subscription cache
                            _this.subscriptions[PoloniexConstants_1.PoloniexConstants.validPairs[pair]] = {
                                'symbol': MdConstant_1.MdConstant.COINS[symbolId],
                                'id': symbolId,
                                'price': 0,
                                'benchmark': bnmrk,
                                'bestBid': 0,
                                'bestAsk': 0,
                                'volume': 0
                            };
                        }
                    }
                }
            }
        };
        this.cb = callback;
    }
    PoloniexTransport.prototype.initWasm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    Object.defineProperty(PoloniexTransport.prototype, "id", {
        get: function () {
            return 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PoloniexTransport.prototype, "name", {
        get: function () {
            return MdConstant_1.MdConstant.VENUES[this.id];
        },
        enumerable: true,
        configurable: true
    });
    //Return the number of seconds sinsce midnight
    PoloniexTransport.getMsSinceMidnight = function () {
        return this.getGmtZeroNow() - this.getGmtZeroMidnight();
    };
    PoloniexTransport.getGmtZeroNow = function () {
        var d = new Date();
        var z = d.getTime() + (d.getTimezoneOffset() * 60000);
        var zd = new Date(z);
        return zd.getTime();
    };
    PoloniexTransport.getGmtZeroMidnight = function () {
        var d = new Date();
        var z = d.getTime() + (d.getTimezoneOffset() * 60000);
        var zd = new Date(z);
        zd.setHours(0);
        zd.setMinutes(0);
        zd.setSeconds(0);
        zd.setMilliseconds(0);
        return zd.getTime();
    };
    PoloniexTransport.prototype.isConnected = function () {
        return this._connected;
    };
    /***
     * Connect to Poloniex Transport
     * @param symbols
     */
    PoloniexTransport.prototype.connect = function (tickers) {
        this.tickers = tickers;
        this.subscribe(); //Subscribe the symbols
        this.init(); //Start connection
    };
    /***
  * Start Poloniex connection and events
  */
    PoloniexTransport.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var wsLimiter;
            var _this = this;
            return __generator(this, function (_a) {
                wsLimiter = new Utilities_1.Utilities().wsLimiter;
                this.poloniex = new PoloniexProxy_1.PoloniexProxy(this.tickers);
                /**
                 * Trade Event
                 */
                this.poloniex.on(PoloniexConstants_1.PoloniexConstants.TRADEEVENT, wsLimiter(function (cid, sequence, message) {
                    var side = message[1];
                    if (_this.subscriptions[cid] != null) {
                        var price = Math.trunc(message[3] * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                        var size = Math.trunc(message[2] * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                        // let price = this.wasm.parseAmount(message[3], PoloniexConstants.DIV);
                        // let size = this.wasm.parseAmount(message[2], PoloniexConstants.DIV);
                        if (side === PoloniexConstants_1.PoloniexConstants.BUY) {
                            var cqMsg = MsgFactory_1.MsgFactory.buyMsg;
                            cqMsg.buffer(PoloniexTransport.getMsSinceMidnight(), _this.subscriptions[cid].id, Math.trunc(price), Math.trunc(price % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(size), Math.trunc(size % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(_this.subscriptions[cid].volume), Math.trunc(_this.subscriptions[cid].volume % 1 * PoloniexConstants_1.PoloniexConstants.DIV), MdConstant_1.MdConstant.PoloniexTransport, _this.subscriptions[cid].benchmark);
                            _this.cb.on(cqMsg);
                        }
                        else { /* Sell */
                            var cqMsg = MsgFactory_1.MsgFactory.sellMsg;
                            cqMsg.buffer(PoloniexTransport.getMsSinceMidnight(), _this.subscriptions[cid].id, Math.trunc(price), Math.trunc(price % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(size), Math.trunc(size % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(_this.subscriptions[cid].volume), Math.trunc(_this.subscriptions[cid].volume % 1 * PoloniexConstants_1.PoloniexConstants.DIV), MdConstant_1.MdConstant.PoloniexTransport, _this.subscriptions[cid].benchmark);
                            _this.cb.on(cqMsg);
                        }
                    }
                }));
                /**
                 * Snapshot Event
                 */
                this.poloniex.on(PoloniexConstants_1.PoloniexConstants.QUOTEEVENT, wsLimiter(function (cid, sequence, message) {
                    if (_this.subscriptions[cid] != null) {
                        /** Level 1 Information **/
                        var bidPriceStr = Object.keys(message[0].orderBook[1])[0];
                        var bidSize = message[0].orderBook[1][bidPriceStr];
                        var askPriceStr = Object.keys(message[0].orderBook[0])[0];
                        var askSize = message[0].orderBook[0][askPriceStr];
                        var bidPrice = Math.trunc(parseFloat(bidPriceStr) * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                        var askPrice = Math.trunc(parseFloat(askPriceStr) * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                        var cqMsg = MsgFactory_1.MsgFactory.quoteMsg;
                        cqMsg.buffer(PoloniexTransport.getMsSinceMidnight(), _this.subscriptions[cid].id, Math.trunc(bidPrice), Math.trunc(bidPrice % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(bidSize), Math.trunc(bidSize % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(askPrice), Math.trunc(askPrice % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(askSize), Math.trunc(askSize % 1 * PoloniexConstants_1.PoloniexConstants.DIV), MdConstant_1.MdConstant.PoloniexTransport, _this.subscriptions[cid].benchmark);
                        _this.cb.on(cqMsg);
                    }
                }));
                /**
                 * Bid and Ask events
                 */
                this.poloniex.on(PoloniexConstants_1.PoloniexConstants.ORDEREVENT, wsLimiter(function (cid, sequence, message) {
                    if (_this.subscriptions[cid] != null) {
                        var msgType = message[2] === PoloniexConstants_1.PoloniexConstants.AMOUNT_ZERO ? 1 /* Remove */ : 0 /* Modify */;
                        if (msgType === 0) {
                            var side = message[0];
                            var rate = Math.trunc(message[1] * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                            var size = Math.trunc(message[2] * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                            if (side === PoloniexConstants_1.PoloniexConstants.BID) {
                                if (rate > _this.subscriptions[cid].bestBid) {
                                    var cqMsg = MsgFactory_1.MsgFactory.bidMsg;
                                    _this.subscriptions[cid].bestBid = rate;
                                    cqMsg.buffer(PoloniexTransport.getMsSinceMidnight(), _this.subscriptions[cid].id, Math.trunc(rate), Math.trunc(rate % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(size), Math.trunc(size % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(_this.subscriptions[cid].volume), Math.trunc(_this.subscriptions[cid].volume % 1 * PoloniexConstants_1.PoloniexConstants.DIV), MdConstant_1.MdConstant.PoloniexTransport, _this.subscriptions[cid].benchmark);
                                    _this.cb.on(cqMsg);
                                }
                            }
                            else { /* Ask */
                                if (rate < _this.subscriptions[cid].bestAsk) {
                                    var cqMsg = MsgFactory_1.MsgFactory.askMsg;
                                    _this.subscriptions[cid].bestAsk = rate;
                                    cqMsg.buffer(PoloniexTransport.getMsSinceMidnight(), _this.subscriptions[cid].id, Math.trunc(rate), Math.trunc(rate % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(size), Math.trunc(size % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(_this.subscriptions[cid].volume), Math.trunc(_this.subscriptions[cid].volume % 1 * PoloniexConstants_1.PoloniexConstants.DIV), MdConstant_1.MdConstant.PoloniexTransport, _this.subscriptions[cid].benchmark);
                                    _this.cb.on(cqMsg);
                                }
                            }
                        }
                    }
                }));
                /**
                 * Quotes Event
                 */
                this.poloniex.on(PoloniexConstants_1.PoloniexConstants.TICKEREVENT, wsLimiter(function (cid, message) {
                    if (_this.subscriptions[cid] != null) {
                        try {
                            var cqMsg = MsgFactory_1.MsgFactory.level1Msg;
                            _this.subscriptions[cid].price = Math.trunc(message[1] * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                            _this.subscriptions[cid].bestBid = Math.trunc(message[3] * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                            _this.subscriptions[cid].bestAsk = Math.trunc(message[2] * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                            _this.subscriptions[cid].volume = Math.trunc(message[6] * PoloniexConstants_1.PoloniexConstants.DIV) / PoloniexConstants_1.PoloniexConstants.DIV;
                            cqMsg.buffer(PoloniexTransport.getMsSinceMidnight(), _this.subscriptions[cid].id, Math.trunc(_this.subscriptions[cid].price), Math.trunc(_this.subscriptions[cid].price % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(_this.subscriptions[cid].bestBid), Math.trunc(_this.subscriptions[cid].bestBid % 1 * PoloniexConstants_1.PoloniexConstants.DIV), Math.trunc(_this.subscriptions[cid].bestAsk), Math.trunc(_this.subscriptions[cid].bestAsk % 1 * PoloniexConstants_1.PoloniexConstants.DIV), MdConstant_1.MdConstant.PoloniexTransport, _this.subscriptions[cid].benchmark);
                            _this.cb.on(cqMsg);
                            // fill ticks
                            var tickername = _this.subscriptions[cid].id + "-" + _this.subscriptions[cid].benchmark;
                            TickManager_1.TickManager.addTick(MdConstant_1.MdConstant.PoloniexTransport, tickername, _this.subscriptions[cid].price);
                        }
                        catch (e) {
                            Utilities_1.Utilities.logger.warn(cid, message);
                            Utilities_1.Utilities.logger.warn(_this.subscriptions[cid]);
                            Utilities_1.Utilities.logger.warn(e);
                        }
                    }
                }));
                this.poloniex.on(PoloniexConstants_1.PoloniexConstants.ERROREVENT, function (message) {
                    //TODO: Error system
                });
                this.poloniex.on(PoloniexConstants_1.PoloniexConstants.CLOSEEVENT, function (message) {
                    _this._connected = false;
                });
                this.poloniex.on(PoloniexConstants_1.PoloniexConstants.OPENEVENT, function (message) {
                    _this._connected = true;
                });
                return [2 /*return*/];
            });
        });
    };
    return PoloniexTransport;
}());
exports.PoloniexTransport = PoloniexTransport;
