"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmiter = require("events");
var WebSocket = require('isomorphic-ws');
var IeoProxy = /** @class */ (function (_super) {
    __extends(IeoProxy, _super);
    function IeoProxy(tickers) {
        var _this = _super.call(this) || this;
        _this.wsState = WebSocket.CLOSE;
        var url = 'wss://QmcpKS4qHMZxfEtMJPEQac9zyxxbLsAzsBQ41CukhECtoQ.lidiya.tv/bc/core/endpoint';
        _this.webSocket = new WebSocket(url);
        _this.webSocket.onopen = function (e) {
            _this.wsState = e.target.readyState;
            _this.emit('n', "Connected");
        };
        _this.webSocket.onmessage = function (receive) {
            var msgType = receive.type;
            if (msgType[0] == 'm') {
                var msg = JSON.parse(receive.data);
                _this.emit('r', msg.s, msg);
            }
        };
        _this.webSocket.onclose = function (closeEvent) {
            _this.emit('c', closeEvent);
        };
        _this.webSocket.onerror = function (errorEvent) {
            _this.emit('e', errorEvent);
        };
        return _this;
    }
    return IeoProxy;
}(EventEmiter));
exports.IeoProxy = IeoProxy;
