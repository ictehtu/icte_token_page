"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmiter = require("events");
var Utilities_1 = require("../../model/Utilities");
require('es6-promise').polyfill();
var fetch = require('isomorphic-fetch');
var IpfsProxy = /** @class */ (function (_super) {
    __extends(IpfsProxy, _super);
    function IpfsProxy() {
        return _super.call(this) || this;
    }
    IpfsProxy.prototype.subscribe = function (files) {
        this.files = files;
    };
    IpfsProxy.prototype.Start = function () {
        var _this = this;
        var options = {
            method: 'GET',
            headers: {
                'Accept': 'application/octet-stream',
                'Accept-Charset': 'utf-8'
            }
        };
        for (var i = 0; i < this.files.length; i++) {
            fetch("https://ipfs.io/ipfs/" + this.files[i], options)
                .then(function (response) {
                if (response.status >= 400) {
                    Utilities_1.Utilities.logger.warn("*** HtuSocket Transport ERROR!!", "IPFS: Bad response from server");
                }
                Utilities_1.Utilities.logger.debug(response);
                return response;
            })
                .then(function (res) { /*res.buffer()*/ return res.arrayBuffer(); }) /* Angular compatibility, to test mocha change "arrayBuffer()" to "buffer()" */
                .then(function (arrayBuffer) {
                var uint8View = new Uint8Array(arrayBuffer);
                var buffer = new Buffer(uint8View);
                _this.emit("m", buffer);
            });
        }
    };
    return IpfsProxy;
}(EventEmiter));
exports.IpfsProxy = IpfsProxy;
