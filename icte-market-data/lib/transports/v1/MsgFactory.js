"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AskMsg_1 = require("../../parser/AskMsg");
var BidMsg_1 = require("../../parser/BidMsg");
var QuoteMsg_1 = require("../../parser/QuoteMsg");
var BuyMsg_1 = require("../../parser/BuyMsg");
var SellMsg_1 = require("../../parser/SellMsg");
var PriceMsg_1 = require("../../parser/PriceMsg");
var Level1Msg_1 = require("../../parser/Level1Msg");
var MsgFactory = /** @class */ (function () {
    function MsgFactory() {
    }
    Object.defineProperty(MsgFactory, "priceMsg", {
        get: function () {
            if (this._priceMsg == null) {
                this._priceMsg = new PriceMsg_1.PriceMsg();
            }
            return this._priceMsg;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsgFactory, "sellMsg", {
        get: function () {
            if (this._sellMsg == null) {
                this._sellMsg = new SellMsg_1.SellMsg();
            }
            return this._sellMsg;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsgFactory, "buyMsg", {
        get: function () {
            if (this._buyMsg == null) {
                this._buyMsg = new BuyMsg_1.BuyMsg();
            }
            return this._buyMsg;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsgFactory, "askMsg", {
        get: function () {
            if (this._askMsg == null) {
                this._askMsg = new AskMsg_1.AskMsg();
            }
            return this._askMsg;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsgFactory, "bidMsg", {
        get: function () {
            if (this._bidMsg == null) {
                this._bidMsg = new BidMsg_1.BidMsg();
            }
            return this._bidMsg;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsgFactory, "level1Msg", {
        get: function () {
            if (this._level1Msg == null) {
                this._level1Msg = new Level1Msg_1.Level1Msg();
            }
            return this._level1Msg;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MsgFactory, "quoteMsg", {
        get: function () {
            if (this._quoteMsg == null) {
                this._quoteMsg = new QuoteMsg_1.QuoteMsg();
            }
            return this._quoteMsg;
        },
        enumerable: true,
        configurable: true
    });
    return MsgFactory;
}());
exports.MsgFactory = MsgFactory;
