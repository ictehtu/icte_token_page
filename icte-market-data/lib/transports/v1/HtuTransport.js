/**
 By: Marco A. Barragan @ ICTE
 */
// import { MdTransport } from "../api/MdTransport";
// import { MdCallback } from "../api/MdCallback";
// import { Parser } from "../../parser/Parser";
// import { Constant } from "../../model/Constant";
// import { TimeMsg } from "../../parser/TimeMsg";
// import { BuyMsg } from "../../parser/BuyMsg";
// import { SellMsg } from "../../parser/SellMsg";
// import { RateMsg } from "../../parser/RateMsg";
// import { Utilities } from "../../model/Utilities";
// import { IpfsProxy } from "./IpfsProxy";
// import { MdUtils } from "../api/MdUtils";
// import { MdConstant } from "../api/MdConstant";
// import { Level1Msg } from "../../parser/Level1Msg";
// import { RatesProxy } from "./RatesProxy";
// import { RatesData } from "../api/RatesData";
// import { CurrencyProxy } from "../tools/CurrencyProxy";
// require('es6-promise').polyfill();
// const fetch = require('isomorphic-fetch');
// const WebSocket = require('isomorphic-ws');
// export class HtuTransport implements MdTransport {
//    private subs: number[];
//    private callback: MdCallback;
//    private ipfs: IpfsProxy;
//    private socket;
//    private parsers: Parser[][] = [];
//    private readonly rates: RatesProxy = new RatesProxy();
//    private readonly currencies: CurrencyProxy = new CurrencyProxy();
//    private readonly PRICE_RATE: number = 0.16;
//    private readonly utf8 = 'utf8';
//    private _connected: boolean;
//    constructor(callback: MdCallback) {
//       this.callback = callback;
//       this.ipfs = new IpfsProxy();
//       this.parsers[Constant.Msg_Sys] = [];
//       this.parsers[Constant.Msg_Sys][Constant.Sub_SysTime] = new TimeMsg();
//       this.parsers[Constant.Msg_Quote] = [];
//       this.parsers[Constant.Msg_Quote][Constant.Sub_Buy] = new BuyMsg();
//       this.parsers[Constant.Msg_Quote][Constant.Sub_Sell] = new SellMsg();
//       this.parsers[Constant.Msg_Quote][Constant.Sub_Rate] = new RateMsg();
//       this.parsers[Constant.Msg_Quote][Constant.Sub_Level1] = new Level1Msg();
//    }
//    isConnected = (): boolean => {
//       return this._connected;
//    }
//    connect = (symbols: number[]) => {
//       Utilities.logger.info("HtuSocket transport connected");
//       this._connected = true;
//       this.subs = symbols;
//       this.sendPrice();
//       this.sendRate(this.PRICE_RATE, MdConstant.USDT, MdConstant.HTU, MdConstant.HtuTransport);
//       this.processCrypto();
//       // this.processCurrency();
//    }
//    processCrypto = () => {
//       RatesProxy.event.on("d", (data: RatesData) => {
//          for (let i = 0; i < data.portfolios.length; i++) {
//             let symbolId = MdConstant.COINSID[data.portfolios[i].symbol];
//             this.sendRate(data.portfolios[i].cash, MdConstant.USDT, symbolId, MdConstant.HtuTransport);
//          }
//       });
//       this.rates.getRates();
//    };
//    // processCurrency = () => {
//    //    this.currencies.on("d", (data) => {
//    //       if (data.success) {
//    //          for (let i: number = 36; i < MdConstant.COINS.length; i++) {
//    //             let symbol = `USD${MdConstant.COINS[i]}`;
//    //             let value = data.quotes[symbol];
//    //             this.sendRate(value, i, MdConstant.USD, MdConstant.HtuTransport);
//    //          }
//    //       }
//    //    });
//    //    this.currencies.on("e", (data) => {
//    //       throw data;
//    //    });
//    //    this.currencies.getRates();
//    // };
//    sendRate = (rate, frontSymbol: number, backSymbol: number, venue: number) => {
//       Utilities.logger.debug("** RATES: ", rate, MdConstant.COINS[frontSymbol], MdConstant.COINS[backSymbol], venue);
//       this.parsers[Constant.Msg_Quote][Constant.Sub_Rate].buffer(
//          MdUtils.getMsSinceMidnight(),
//          frontSymbol,
//          backSymbol,
//          Math.trunc(rate),
//          Math.trunc(rate % 1 * Utilities.DIV),
//          venue);
//       this.callback.on(this.parsers[Constant.Msg_Quote][Constant.Sub_Rate]);
//    };
//    sendPrice = () => {
//       let price = this.PRICE_RATE;
//       this.parsers[Constant.Msg_Quote][Constant.Sub_Level1].buffer(MdUtils.getMsSinceMidnight(),
//          MdConstant.HTU,
//          Math.trunc(price), Math.trunc(price % 1 * Utilities.DIV), 0, 0,
//          Math.trunc(price), Math.trunc(price % 1 * Utilities.DIV),
//          MdConstant.HtuTransport, MdConstant.USDT);
//       this.callback.on(this.parsers[Constant.Msg_Quote][Constant.Sub_Level1]);
//    }
// }
