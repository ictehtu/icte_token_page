"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../../model/Constant");
var TimeMsg_1 = require("../../parser/TimeMsg");
var BuyMsg_1 = require("../../parser/BuyMsg");
var SellMsg_1 = require("../../parser/SellMsg");
var RateMsg_1 = require("../../parser/RateMsg");
var Utilities_1 = require("../../model/Utilities");
var IpfsProxy_1 = require("./IpfsProxy");
var MdUtils_1 = require("../api/MdUtils");
var MdConstant_1 = require("../api/MdConstant");
var Level1Msg_1 = require("../../parser/Level1Msg");
var RatesProxy_1 = require("./RatesProxy");
var CurrencyProxy_1 = require("../tools/CurrencyProxy");
var TickManager_1 = require("../api/TickManager");
require('es6-promise').polyfill();
var fetch = require('isomorphic-fetch');
var WebSocket = require('isomorphic-ws');
var IeoTransport = /** @class */ (function () {
    function IeoTransport(callback) {
        var _this = this;
        this.parsers = [];
        this.rates = new RatesProxy_1.RatesProxy();
        this.currencies = new CurrencyProxy_1.CurrencyProxy();
        this.PRICE_RATE = 1.00;
        this.utf8 = 'utf8';
        this.isConnected = function () {
            return _this._connected;
        };
        this.connect = function () {
            Utilities_1.Utilities.logger.info("IeoSocket transport connected");
            _this._connected = true;
            _this.subs = MdConstant_1.MdConstant.SYMBOLS;
            _this.sendPrice();
            _this.sendRate(_this.PRICE_RATE, MdConstant_1.MdConstant.USDT, MdConstant_1.MdConstant.IEO, MdConstant_1.MdConstant.IeoTransport);
            _this.processCrypto();
            _this.processCurrency();
        };
        this.processCrypto = function () {
            RatesProxy_1.RatesProxy.event.on("d", function (data) {
                for (var i = 0; i < data.portfolios.length; i++) {
                    var symbolId = MdConstant_1.MdConstant.COINSID[data.portfolios[i].symbol];
                    _this.sendRate(data.portfolios[i].cash, MdConstant_1.MdConstant.USDT, symbolId, MdConstant_1.MdConstant.IeoTransport);
                }
            });
            _this.rates.getRates();
        };
        this.processCurrency = function () {
            _this.currencies.on("d", function (data) {
                if (data.success) {
                    for (var i = 36; i < MdConstant_1.MdConstant.COINS.length; i++) {
                        var symbol = "USD" + MdConstant_1.MdConstant.COINS[i];
                        var value = data.quotes[symbol];
                        _this.sendRate(value, i, MdConstant_1.MdConstant.USD, MdConstant_1.MdConstant.IeoTransport);
                        _this.sendPrice();
                    }
                }
            });
            _this.currencies.on("e", function (data) {
                throw data;
            });
            _this.currencies.getRates();
        };
        this.sendRate = function (rate, frontSymbol, backSymbol, venue) {
            Utilities_1.Utilities.logger.debug("** RATES: ", rate, MdConstant_1.MdConstant.COINS[frontSymbol], MdConstant_1.MdConstant.COINS[backSymbol], venue);
            _this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Rate].buffer(MdUtils_1.MdUtils.getMsSinceMidnight(), frontSymbol, backSymbol, Math.trunc(rate), Math.trunc(rate % 1 * Utilities_1.Utilities.DIV), venue);
            _this.callback.on(_this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Rate]);
        };
        this.sendPrice = function () {
            var price = _this.PRICE_RATE;
            _this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Level1].buffer(MdUtils_1.MdUtils.getMsSinceMidnight(), MdConstant_1.MdConstant.IEO, Math.trunc(price), Math.trunc(price % 1 * Utilities_1.Utilities.DIV), 0, 0, Math.trunc(price), Math.trunc(price % 1 * Utilities_1.Utilities.DIV), MdConstant_1.MdConstant.IeoTransport, MdConstant_1.MdConstant.USDT);
            _this.callback.on(_this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Level1]);
            // fill ticks
            var tickername = MdConstant_1.MdConstant.IEO + "-" + MdConstant_1.MdConstant.USDT;
            TickManager_1.TickManager.addTick(MdConstant_1.MdConstant.IeoTransport, tickername, price);
        };
        this.callback = callback;
        this.ipfs = new IpfsProxy_1.IpfsProxy();
        this.parsers[Constant_1.Constant.Msg_Sys] = [];
        this.parsers[Constant_1.Constant.Msg_Sys][Constant_1.Constant.Sub_SysTime] = new TimeMsg_1.TimeMsg();
        this.parsers[Constant_1.Constant.Msg_Quote] = [];
        this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Buy] = new BuyMsg_1.BuyMsg();
        this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Sell] = new SellMsg_1.SellMsg();
        this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Rate] = new RateMsg_1.RateMsg();
        this.parsers[Constant_1.Constant.Msg_Quote][Constant_1.Constant.Sub_Level1] = new Level1Msg_1.Level1Msg();
    }
    Object.defineProperty(IeoTransport.prototype, "id", {
        get: function () {
            return 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IeoTransport.prototype, "name", {
        get: function () {
            return MdConstant_1.MdConstant.VENUES[this.id];
        },
        enumerable: true,
        configurable: true
    });
    return IeoTransport;
}());
exports.IeoTransport = IeoTransport;
