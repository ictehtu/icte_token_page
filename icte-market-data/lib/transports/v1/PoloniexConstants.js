"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 By: Marco A. Barragan @ ICTE
  */
/***
 * Constants for Poloniex Transport
 */
var PoloniexConstants = /** @class */ (function () {
    function PoloniexConstants() {
    }
    /***
     * Header for the Poloniex connection
     */
    PoloniexConstants.header = {
        headers: {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36",
            "Cookie": "cf_clearance=7332b18714d3aa45aed82424e50cf166d6093c6f-1501832791-1800"
        }
    };
    /***
     * Account key
     */
    PoloniexConstants.key = '39IAU1TX-90OSBA19-VRSPP9AK-LL6AIFBC';
    /***
     * Account signature
     */
    PoloniexConstants.sign = 'e3637a20ac600e1b128f9a55120e2a9e4a99e4fd4b5124714d1996e3b9bde3f493f71dd7bb3447725a713087faaec0d37bda932f447799c325a928071ba06d43';
    /***
     * Poloniex server url
     */
    PoloniexConstants.poloniexUrl = 'wss://api2.poloniex.com';
    /***
     * Valid Id by Pairs
     */
    PoloniexConstants.validPairs = {
        'BTC_BCN': 7,
        'BTC_BTS': 14,
        'BTC_BURST': 15,
        'BTC_CLAM': 20,
        'BTC_DGB': 25,
        'BTC_DOGE': 27,
        'BTC_DASH': 24,
        'BTC_GAME': 38,
        'BTC_HUC': 43,
        'BTC_LTC': 50,
        'BTC_MAID': 51,
        'BTC_OMNI': 58,
        'BTC_NAV': 61,
        'BTC_NMC': 64,
        'BTC_NXT': 69,
        'BTC_PPC': 75,
        'BTC_STR': 89,
        'BTC_SYS': 92,
        'BTC_VIA': 97,
        'BTC_VTC': 100,
        'BTC_XCP': 108,
        'BTC_XMR': 114,
        'BTC_XPM': 116,
        'BTC_XRP': 117,
        'BTC_XEM': 112,
        'BTC_ETH': 148,
        'BTC_SC': 150,
        'BTC_FCT': 155,
        'BTC_DCR': 162,
        'BTC_LSK': 163,
        'BTC_LBC': 167,
        'BTC_STEEM': 168,
        'BTC_SBD': 170,
        'BTC_ETC': 171,
        'BTC_REP': 174,
        'BTC_ARDR': 177,
        'BTC_ZEC': 178,
        'BTC_STRAT': 182,
        'BTC_PASC': 184,
        'BTC_GNT': 185,
        'BTC_BCH': 189,
        'BTC_ZRX': 192,
        'BTC_CVC': 194,
        'BTC_OMG': 196,
        'BTC_GAS': 198,
        'BTC_STORJ': 200,
        'BTC_EOS': 201,
        'BTC_SNT': 204,
        'BTC_KNC': 207,
        'BTC_BAT': 210,
        'BTC_LOOM': 213,
        'BTC_QTUM': 221,
        'BTC_BNT': 232,
        'BTC_MANA': 229,
        'XMR_BCN': 129,
        'XMR_DASH': 132,
        'XMR_LTC': 137,
        'XMR_MAID': 138,
        'XMR_NXT': 140,
        'XMR_ZEC': 181,
        'ETH_LSK': 166,
        'ETH_STEEM': 169,
        'ETH_ETC': 172,
        'ETH_REP': 176,
        'ETH_ZEC': 179,
        'ETH_GNT': 186,
        'ETH_BCH': 190,
        'ETH_ZRX': 193,
        'ETH_CVC': 195,
        'ETH_OMG': 197,
        'ETH_GAS': 199,
        'ETH_EOS': 202,
        'ETH_SNT': 205,
        'ETH_KNC': 208,
        'ETH_BAT': 211,
        'ETH_LOOM': 214,
        'ETH_QTUM': 222,
        'ETH_BNT': 233,
        'ETH_MANA': 230,
        'USDC_BTC': 224,
        // 'USDC_USDT': 226,
        'USDC_ETH': 225,
        'USDT_BTC': 121,
        'USDT_DOGE': 261,
        'USDT_DASH': 122,
        'USDT_LTC': 123,
        'USDT_NXT': 124,
        'USDT_STR': 125,
        'USDT_XMR': 126,
        'USDT_XRP': 127,
        'USDT_ETH': 149,
        'USDT_SC': 219,
        'USDT_LSK': 218,
        'USDT_ETC': 173,
        'USDT_REP': 175,
        'USDT_ZEC': 180,
        'USDT_GNT': 217,
        'USDT_BCH': 191,
        'USDT_ZRX': 220,
        'USDT_EOS': 203,
        'USDT_SNT': 206,
        'USDT_KNC': 209,
        'USDT_BAT': 212,
        'USDT_LOOM': 215,
        'USDT_QTUM': 223,
        'USDT_BNT': 234,
        'USDT_MANA': 231
    };
    /***
     * Valid pairs by Id
     */
    PoloniexConstants.validPairId = {
        7: 'BTC_BCN',
        14: 'BTC_BTS',
        15: 'BTC_BURST',
        20: 'BTC_CLAM',
        25: 'BTC_DGB',
        27: 'BTC_DOGE',
        24: 'BTC_DASH',
        38: 'BTC_GAME',
        43: 'BTC_HUC',
        50: 'BTC_LTC',
        51: 'BTC_MAID',
        58: 'BTC_OMNI',
        61: 'BTC_NAV',
        64: 'BTC_NMC',
        69: 'BTC_NXT',
        75: 'BTC_PPC',
        89: 'BTC_STR',
        92: 'BTC_SYS',
        97: 'BTC_VIA',
        100: 'BTC_VTC',
        108: 'BTC_XCP',
        114: 'BTC_XMR',
        116: 'BTC_XPM',
        117: 'BTC_XRP',
        112: 'BTC_XEM',
        148: 'BTC_ETH',
        150: 'BTC_SC',
        155: 'BTC_FCT',
        162: 'BTC_DCR',
        163: 'BTC_LSK',
        167: 'BTC_LBC',
        168: 'BTC_STEEM',
        170: 'BTC_SBD',
        171: 'BTC_ETC',
        174: 'BTC_REP',
        177: 'BTC_ARDR',
        178: 'BTC_ZEC',
        182: 'BTC_STRAT',
        184: 'BTC_PASC',
        185: 'BTC_GNT',
        189: 'BTC_BCH',
        192: 'BTC_ZRX',
        194: 'BTC_CVC',
        196: 'BTC_OMG',
        198: 'BTC_GAS',
        200: 'BTC_STORJ',
        201: 'BTC_EOS',
        204: 'BTC_SNT',
        207: 'BTC_KNC',
        210: 'BTC_BAT',
        213: 'BTC_LOOM',
        221: 'BTC_QTUM',
        232: 'BTC_BNT',
        229: 'BTC_MANA',
        236: 'BTC_BCHABC',
        238: 'BTC_BCHSV',
        121: 'USDT_BTC',
        216: 'USDT_DOGE',
        122: 'USDT_DASH',
        123: 'USDT_LTC',
        124: 'USDT_NXT',
        125: 'USDT_STR',
        126: 'USDT_XMR',
        127: 'USDT_XRP',
        149: 'USDT_ETH',
        219: 'USDT_SC',
        218: 'USDT_LSK',
        173: 'USDT_ETC',
        175: 'USDT_REP',
        180: 'USDT_ZEC',
        217: 'USDT_GNT',
        191: 'USDT_BCH',
        220: 'USDT_ZRX',
        203: 'USDT_EOS',
        206: 'USDT_SNT',
        209: 'USDT_KNC',
        212: 'USDT_BAT',
        215: 'USDT_LOOM',
        223: 'USDT_QTUM',
        234: 'USDT_BNT',
        231: 'USDT_MANA',
        129: 'XMR_BCN',
        132: 'XMR_DASH',
        137: 'XMR_LTC',
        138: 'XMR_MAID',
        140: 'XMR_NXT',
        181: 'XMR_ZEC',
        166: 'ETH_LSK',
        169: 'ETH_STEEM',
        172: 'ETH_ETC',
        176: 'ETH_REP',
        179: 'ETH_ZEC',
        186: 'ETH_GNT',
        190: 'ETH_BCH',
        193: 'ETH_ZRX',
        195: 'ETH_CVC',
        197: 'ETH_OMG',
        199: 'ETH_GAS',
        202: 'ETH_EOS',
        205: 'ETH_SNT',
        208: 'ETH_KNC',
        211: 'ETH_BAT',
        214: 'ETH_LOOM',
        222: 'ETH_QTUM',
        233: 'ETH_BNT',
        230: 'ETH_MANA',
        224: 'USDC_BTC',
        226: 'USDC_USDT',
        225: 'USDC_ETH',
        235: 'USDC_BCH',
        237: 'USDC_BCHABC',
        239: 'USDC_BCHSV',
    };
    PoloniexConstants.BUY = 1;
    PoloniexConstants.SELL = 0;
    PoloniexConstants.BID = 1;
    PoloniexConstants.ASK = 0;
    PoloniexConstants.MINPAIR = 0;
    PoloniexConstants.MAXPAIR = 1000;
    PoloniexConstants.TICKER = 1002;
    PoloniexConstants.HEARTBEAT = 1010;
    PoloniexConstants.PINGTIME = 60000;
    PoloniexConstants.TRADEEVENT = 't';
    PoloniexConstants.QUOTEEVENT = 'i';
    PoloniexConstants.ORDEREVENT = 'o';
    PoloniexConstants.TICKEREVENT = 'r';
    PoloniexConstants.ERROREVENT = 'e';
    PoloniexConstants.CLOSEEVENT = 'c';
    PoloniexConstants.OPENEVENT = 'n';
    PoloniexConstants.AMOUNT_ZERO = '0.00000000';
    PoloniexConstants.SUBSCRIBE = 'subscribe';
    PoloniexConstants.UNSUBSCRIBE = 'unsubscribe';
    PoloniexConstants.DIV = 1000000000;
    return PoloniexConstants;
}());
exports.PoloniexConstants = PoloniexConstants;
