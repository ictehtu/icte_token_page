"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmiter = require("events");
var BitfinexConstant_1 = require("./BitfinexConstant");
var Utilities_1 = require("../../model/Utilities");
var MdUtils_1 = require("../api/MdUtils");
var WebSocket = require('isomorphic-ws');
var BitfinexProxy = /** @class */ (function (_super) {
    __extends(BitfinexProxy, _super);
    function BitfinexProxy(tickers) {
        var _this = _super.call(this) || this;
        _this.wsState = WebSocket.CLOSE;
        _this._channels = {};
        _this.cid = MdUtils_1.MdUtils.getMsSinceMidnight();
        _this.tickers = tickers;
        var url = BitfinexConstant_1.BitfinexConstant.baseEndpoint;
        _this.webSocket = new WebSocket(url);
        _this.webSocket.onopen = function (e) {
            _this._keepAliveInterval = setInterval(function () {
                _this.webSocket.send("{\"event\":\"ping\",\"cid\":" + _this.cid + "}");
            }, 5000);
            _this.wsState = e.target.readyState;
            _this.emit(BitfinexConstant_1.BitfinexConstant.OPENEVENT, "Connected");
            _this.subscribe();
        };
        _this.webSocket.onclose = function (closeEvent) {
            _this.emit(BitfinexConstant_1.BitfinexConstant.CLOSEEVENT, closeEvent);
        };
        _this.webSocket.onerror = function (errorEvent) {
            _this.emit(BitfinexConstant_1.BitfinexConstant.ERROREVENT, errorEvent);
        };
        _this.webSocket.onmessage = function (receive) {
            var data = JSON.parse(receive.data);
            if (!Array.isArray(data)) {
                var event = data.event;
                switch (event) {
                    case 'pong':
                        //TODO: Get pong
                        break;
                    case 'subscribed':
                        if (data.channel == 'ticker') {
                            _this._channels[data.chanId] = data.pair;
                            _this.emit(BitfinexConstant_1.BitfinexConstant.SUBSCRIBEDEVENT, data);
                        }
                        break;
                    case 'error':
                        _this.emit(BitfinexConstant_1.BitfinexConstant.ERROREVENT, data.code + ", " + (data.channel ? data.channel : '-') + ", " + data.msg + ", " + (data.symbol ? data.symbol : '') + ", " + (data.pair ? data.pair : ''));
                        break;
                    case 'info':
                        if (data.code) {
                            _this.chkInfoMsg(data.code, data.msg);
                        }
                        else if (data.version) {
                            _this.chkInfoMsg("Version:" + data.version, "Platform status: " + data.platform.status);
                        }
                        break;
                }
            }
            else {
                _this.processMsg(data);
            }
        };
        return _this;
    }
    BitfinexProxy.prototype.utcTimeStamp = function () {
        var d = new Date();
        var dUtc = new Date(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(), d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds());
        return Math.floor(dUtc.getTime() / 1000);
    };
    BitfinexProxy.prototype.subscribe = function () {
        if (this.tickers) {
            for (var index = 0; index < BitfinexConstant_1.BitfinexConstant.VALIDPAIRS.length; index++) {
                for (var j = 0; j < this.tickers.length; j++) {
                    var tickerPair = "t" + this.tickers[j].replace('-', '').toUpperCase();
                    if (BitfinexConstant_1.BitfinexConstant.VALIDPAIRS[index] == tickerPair) {
                        var subscription = "{\"event\": \"subscribe\", \"channel\": \"ticker\", \"pair\": \"" + BitfinexConstant_1.BitfinexConstant.VALIDPAIRS[index] + "\"}";
                        Utilities_1.Utilities.logger.debug(subscription);
                        this.webSocket.send(subscription);
                    }
                }
            }
        }
        else {
            for (var index = 0; index < BitfinexConstant_1.BitfinexConstant.VALIDPAIRS.length; index++) {
                var subscription = "{\"event\": \"subscribe\", \"channel\": \"ticker\", \"pair\": \"" + BitfinexConstant_1.BitfinexConstant.VALIDPAIRS[index] + "\"}";
                Utilities_1.Utilities.logger.debug(subscription);
                this.webSocket.send(subscription);
            }
        }
    };
    BitfinexProxy.prototype.chkInfoMsg = function (infoId, infoMsg) {
        Utilities_1.Utilities.logger.info("!! Bitfinex", infoId, infoMsg);
    };
    BitfinexProxy.prototype.processMsg = function (msg) {
        if (msg[1] != 'hb') {
            var id = msg[BitfinexConstant_1.BitfinexConstant.MSG_CHANNEL_ID];
            var symbol = this._channels[id];
            if (symbol) {
                for (var i = 1; i < msg.length; i++) {
                    this.emit(BitfinexConstant_1.BitfinexConstant.TICKEREVENT, symbol, msg[i]);
                }
            }
        }
    };
    return BitfinexProxy;
}(EventEmiter)); //EOC
exports.BitfinexProxy = BitfinexProxy;
