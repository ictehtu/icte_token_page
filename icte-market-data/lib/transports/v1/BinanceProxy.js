"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmiter = require("events");
var BinanceConstant_1 = require("./BinanceConstant");
var WebSocket = require('isomorphic-ws');
var BinanceProxy = /** @class */ (function (_super) {
    __extends(BinanceProxy, _super);
    function BinanceProxy(tickers) {
        var _a;
        var _this = _super.call(this) || this;
        _this.wsState = WebSocket.CLOSE;
        var urlPairs = '';
        var validPairs = BinanceConstant_1.BinanceConstant.PAIRS;
        if (tickers) {
            validPairs = {};
            var pairs = Object.keys(BinanceConstant_1.BinanceConstant.PAIRS);
            for (var i = 0; i < pairs.length; i++) {
                for (var j = 0; j < tickers.length; j++) {
                    var tickerPair = tickers[j].replace('-', '').toUpperCase();
                    if (pairs[i] == tickerPair) {
                        var pairdata = BinanceConstant_1.BinanceConstant.PAIRS[tickerPair];
                        validPairs = __assign({}, validPairs, (_a = {}, _a[tickerPair] = pairdata, _a));
                    }
                }
            }
        }
        for (var pair in validPairs) {
            urlPairs += pair.toLowerCase() + "@ticker/";
        }
        var url = BinanceConstant_1.BinanceConstant.baseEndpoint + "/ws/" + urlPairs.substring(0, urlPairs.length - 1);
        _this.webSocket = new WebSocket(url);
        _this.webSocket.onopen = function (e) {
            _this._keepAliveInterval = setInterval(function () {
                //TODO: Send correct pong message
                //this.webSocket.send("pong");
            }, BinanceConstant_1.BinanceConstant.PINGTIME);
            _this.wsState = e.target.readyState;
            _this.emit(BinanceConstant_1.BinanceConstant.OPENEVENT, "Connected");
        };
        _this.webSocket.onmessage = function (receive) {
            var msgType = receive.type;
            if (msgType[0] == 'm') {
                var msg = JSON.parse(receive.data);
                // for (let i = 0; i < msg.length; i++) {
                _this.emit(BinanceConstant_1.BinanceConstant.TICKEREVENT, msg.s, msg);
                // }
            }
        };
        _this.webSocket.onclose = function (closeEvent) {
            clearInterval(_this._keepAliveInterval);
            _this.emit(BinanceConstant_1.BinanceConstant.CLOSEEVENT, closeEvent);
        };
        _this.webSocket.onerror = function (errorEvent) {
            _this.emit(BinanceConstant_1.BinanceConstant.ERROREVENT, errorEvent);
        };
        return _this;
    }
    return BinanceProxy;
}(EventEmiter));
exports.BinanceProxy = BinanceProxy;
