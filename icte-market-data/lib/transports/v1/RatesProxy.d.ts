import * as EventEmiter from 'events';
export declare class RatesProxy {
    static event: EventEmiter;
    private url;
    private readonly domain;
    private readonly filedateurl;
    private readonly jsZip;
    constructor();
    private static sendData;
    getRates(): void;
    getRatesNodeJS(): void;
    private downloadFileBlob;
    private downloadFile;
    private loadData;
}
