"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/*
 By: Marco A. Barragan @ ICTE
  */
var EventEmiter = require("events");
var PoloniexConstants_1 = require("./PoloniexConstants");
var WebSocket = require('isomorphic-ws');
/***
 * Poloniex Wrapper Class
 */
var PoloniexProxy = /** @class */ (function (_super) {
    __extends(PoloniexProxy, _super);
    function PoloniexProxy(tickers) {
        var _this = _super.call(this) || this;
        _this.wsState = WebSocket.CLOSE;
        _this.commandsQueue = [];
        _this.tickers = tickers;
        _this.webSocket = new WebSocket(PoloniexConstants_1.PoloniexConstants.poloniexUrl, [], PoloniexConstants_1.PoloniexConstants.header);
        _this.webSocket.onopen = function (e) {
            _this._keepAliveInterval = setInterval(function () {
                _this.webSocket.send(".");
            }, PoloniexConstants_1.PoloniexConstants.PINGTIME);
            _this.wsState = e.target.readyState;
            while (_this.commandsQueue.length)
                _this.send.apply(_this, _this.commandsQueue.shift());
            _this.emit(PoloniexConstants_1.PoloniexConstants.OPENEVENT, "Connected");
        };
        _this.webSocket.onmessage = function (content) {
            var sequence = 0;
            var message = JSON.parse(content.data);
            if (content.data.length === 0)
                throw new Error('Empty data');
            if ('error' in message) {
                // return this.emit('error', message);
            }
            else {
                var cid = message.shift();
                if (cid == PoloniexConstants_1.PoloniexConstants.TICKER) {
                    sequence = -1;
                    return _this._marketEvent(cid, sequence, message);
                }
                // if cid is currencyPair channel
                if (PoloniexConstants_1.PoloniexConstants.MINPAIR < cid && cid < PoloniexConstants_1.PoloniexConstants.MAXPAIR) {
                    sequence = message[0];
                    return _this._marketEvent(cid, sequence, message);
                }
                if (cid === PoloniexConstants_1.PoloniexConstants.HEARTBEAT)
                    return;
            }
        };
        _this.webSocket.onerror = function (errorEvent) {
            if (/403/.test(errorEvent.message)) {
                clearInterval(_this._keepAliveInterval);
                _this.webSocket.terminate();
                _this.emit(PoloniexConstants_1.PoloniexConstants.ERROREVENT, "Poloniex is protected with CloudFlare & reCaptcha.");
                process.exit();
            }
            else {
                _this.emit(PoloniexConstants_1.PoloniexConstants.ERROREVENT, errorEvent.message);
            }
        };
        _this.webSocket.onclose = function (closeEvent) {
            clearInterval(_this._keepAliveInterval);
            _this.emit(PoloniexConstants_1.PoloniexConstants.CLOSEEVENT, closeEvent);
        };
        _this.subscribe();
        return _this;
    }
    PoloniexProxy.prototype.subscribe = function () {
        var _this = this;
        this.send(PoloniexConstants_1.PoloniexConstants.SUBSCRIBE, "1002");
        if (this.tickers) {
            var pairs = Object.keys(PoloniexConstants_1.PoloniexConstants.validPairs);
            for (var i = 0; i < pairs.length; i++) {
                for (var j = 0; j < this.tickers.length; j++) {
                    var tickerCoin = this.tickers[j].split('-')[0];
                    var tickerBench = this.tickers[j].split('-')[1];
                    var tickerPair = (tickerBench + "_" + tickerCoin).toUpperCase();
                    if (pairs[i] == tickerPair) {
                        this.send(PoloniexConstants_1.PoloniexConstants.SUBSCRIBE, tickerPair);
                    }
                }
            }
            pairs.forEach(function (pair) {
            });
        }
        else {
            var pairs = Object.keys(PoloniexConstants_1.PoloniexConstants.validPairs);
            pairs.forEach(function (pair) {
                _this.send(PoloniexConstants_1.PoloniexConstants.SUBSCRIBE, pair);
            });
        }
    };
    PoloniexProxy.prototype.send = function (command, channel) {
        if (this.wsState != WebSocket.OPEN) {
            return this.commandsQueue.push([command, channel]);
        }
        var sign = PoloniexConstants_1.PoloniexConstants.sign;
        var key = PoloniexConstants_1.PoloniexConstants.key;
        var payload = "nonce=" + (Math.round(new Date().getTime() / 1000));
        var dataToSend = JSON.stringify({ command: command, channel: channel, key: key, payload: payload, sign: sign });
        this.webSocket.send(dataToSend);
    };
    PoloniexProxy.prototype._marketEvent = function (cid, sequence) {
        var message = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            message[_i - 2] = arguments[_i];
        }
        if (sequence > 1) {
            for (var _a = 0, message_1 = message; _a < message_1.length; _a++) {
                var messageData = message_1[_a];
                messageData.shift();
                var rawData = messageData.shift();
                var dataToSend = rawData.shift();
                var eventName = dataToSend.shift();
                this.emit(eventName, cid, sequence, dataToSend);
            }
        }
        else {
            var rawMessage = message.shift();
            //check if there is more than one update
            for (var i in rawMessage) {
                //Skip the first null in the message
                if (rawMessage[i] != null) {
                    var basicData = rawMessage[i];
                    var pairId = basicData[0];
                    this.emit(PoloniexConstants_1.PoloniexConstants.TICKEREVENT, pairId, basicData);
                }
            }
        }
    };
    return PoloniexProxy;
}(EventEmiter));
exports.PoloniexProxy = PoloniexProxy;
