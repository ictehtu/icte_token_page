"use strict";
/* Develop by Marco A. Barragan by ICTE */
Object.defineProperty(exports, "__esModule", { value: true });
var HitBtcConstant_1 = require("./HitBtcConstant");
var MdUtils_1 = require("../api/MdUtils");
var MdConstant_1 = require("../api/MdConstant");
var MsgFactory_1 = require("./MsgFactory");
var HitBtcProxy_1 = require("./HitBtcProxy");
var Utilities_1 = require("../../model/Utilities");
var HitBtcTransport = /** @class */ (function () {
    function HitBtcTransport(callback) {
        this._connected = false;
        this.cb = callback;
    }
    HitBtcTransport.prototype.isConnected = function () {
        return this._connected;
    };
    HitBtcTransport.prototype.connect = function () {
        this.init(); //Start connection
    };
    HitBtcTransport.prototype.init = function () {
        var _this = this;
        this.proxy = new HitBtcProxy_1.HitBtcProxy();
        this.proxy.on(HitBtcConstant_1.HitBtcConstant.OPENEVENT, function (data) {
            _this._connected = true;
            Utilities_1.Utilities.logger.info("HitBtc transport connected");
        });
        this.proxy.on(HitBtcConstant_1.HitBtcConstant.CLOSEEVENT, function (data) {
            Utilities_1.Utilities.logger.warn("** HitBtc transport closed!!!", data);
            _this._connected = false;
        });
        this.proxy.on(HitBtcConstant_1.HitBtcConstant.ERROREVENT, function (data) {
            //TODO: Error system
            Utilities_1.Utilities.logger.warn("** HitBtc transport ERROR!!!", data);
        });
        this.proxy.on(HitBtcConstant_1.HitBtcConstant.TICKEREVENT, function (pair, data) {
            var cqMsg = MsgFactory_1.MsgFactory.level1Msg;
            var price = Math.trunc(data.price * HitBtcConstant_1.HitBtcConstant.DIV) / HitBtcConstant_1.HitBtcConstant.DIV;
            var bestBid = Math.trunc(data.bid * HitBtcConstant_1.HitBtcConstant.DIV) / HitBtcConstant_1.HitBtcConstant.DIV;
            var bestAsk = Math.trunc(data.ask * HitBtcConstant_1.HitBtcConstant.DIV) / HitBtcConstant_1.HitBtcConstant.DIV;
            cqMsg.buffer(MdUtils_1.MdUtils.getMsSinceMidnight(), data.symbolId, Math.trunc(price), Math.trunc(price % 1 * HitBtcConstant_1.HitBtcConstant.DIV), Math.trunc(bestBid), Math.trunc(bestBid % 1 * HitBtcConstant_1.HitBtcConstant.DIV), Math.trunc(bestAsk), Math.trunc(bestAsk % 1 * HitBtcConstant_1.HitBtcConstant.DIV), MdConstant_1.MdConstant.HitBtcTransport, data.benchId);
            _this.cb.on(cqMsg);
        });
        this.proxy.on(HitBtcConstant_1.HitBtcConstant.SNAPSHOTEVENT, function (pair, data) {
            _this.quoteMsg(pair, data);
        });
        this.proxy.on(HitBtcConstant_1.HitBtcConstant.UPDATEBOOKEVENT, function (pair, data) {
            _this.quoteMsg(pair, data);
        });
        this.proxy.start();
    };
    HitBtcTransport.prototype.quoteMsg = function (pair, data) {
        var bidPrice = Math.trunc(parseFloat(data.bid) * HitBtcConstant_1.HitBtcConstant.DIV) / HitBtcConstant_1.HitBtcConstant.DIV;
        var askPrice = Math.trunc(parseFloat(data.ask) * HitBtcConstant_1.HitBtcConstant.DIV) / HitBtcConstant_1.HitBtcConstant.DIV;
        var bidSize = Math.trunc(parseFloat(data.bidSize) * HitBtcConstant_1.HitBtcConstant.DIV) / HitBtcConstant_1.HitBtcConstant.DIV;
        var askSize = Math.trunc(parseFloat(data.askSize) * HitBtcConstant_1.HitBtcConstant.DIV) / HitBtcConstant_1.HitBtcConstant.DIV;
        var cqMsg = MsgFactory_1.MsgFactory.quoteMsg;
        cqMsg.buffer(MdUtils_1.MdUtils.getMsSinceMidnight(), data.symbolId, Math.trunc(bidPrice), Math.trunc(bidPrice % 1 * HitBtcConstant_1.HitBtcConstant.DIV), Math.trunc(bidSize), Math.trunc(bidSize % 1 * HitBtcConstant_1.HitBtcConstant.DIV), Math.trunc(askPrice), Math.trunc(askPrice % 1 * HitBtcConstant_1.HitBtcConstant.DIV), Math.trunc(askSize), Math.trunc(askSize % 1 * HitBtcConstant_1.HitBtcConstant.DIV), MdConstant_1.MdConstant.HitBtcTransport, data.benchId);
        this.cb.on(cqMsg);
    };
    return HitBtcTransport;
}());
exports.HitBtcTransport = HitBtcTransport;
