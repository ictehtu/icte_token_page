/**
 By: Marco A. Barragan @ ICTE
 */
import { MdTransport } from "../api/MdTransport";
import { MdCallback } from "../api/MdCallback";
export declare class HtuSocketIoTransport implements MdTransport {
    private subs;
    private callback;
    private ipfs;
    private socket;
    private parsers;
    private readonly utf8;
    private _connected;
    constructor(callback: MdCallback);
    isConnected: () => boolean;
    connect: (symbols: number[]) => void;
    sendRate: (rate: any, frontSymbol: number, backSymbol: number, venue: number) => void;
}
