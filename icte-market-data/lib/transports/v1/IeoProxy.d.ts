import * as EventEmiter from 'events';
export declare class IeoProxy extends EventEmiter {
    private webSocket;
    private wsState;
    private _keepAliveInterval;
    constructor(tickers?: string[]);
}
