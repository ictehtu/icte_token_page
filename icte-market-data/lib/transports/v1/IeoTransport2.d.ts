import { MdTransport } from "../api/MdTransport";
import { MdCallback } from "../api/MdCallback";
export declare class IeoTransport2 implements MdTransport {
    private proxy;
    private subscriptions;
    private static _connected;
    private cb;
    private tickers;
    private wasm;
    constructor(callback: MdCallback);
    private initWasm;
    isConnected(): boolean;
    static wbCon(): boolean;
    readonly id: number;
    readonly name: string;
    connect(tickers?: string[]): void;
    private init;
    private processMessage;
}
