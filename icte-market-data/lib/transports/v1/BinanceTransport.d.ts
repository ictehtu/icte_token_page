import { MdTransport } from "../api/MdTransport";
import { MdCallback } from "../api/MdCallback";
export declare class BinanceTransport implements MdTransport {
    private proxy;
    private subscriptions;
    private _connected;
    private cb;
    private tickers;
    private wasm;
    constructor(callback: MdCallback);
    private initWasm;
    isConnected(): boolean;
    readonly id: number;
    readonly name: string;
    connect(tickers?: string[]): void;
    private init;
    private processMessage;
}
