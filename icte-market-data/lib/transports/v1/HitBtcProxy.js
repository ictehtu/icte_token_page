"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmiter = require("events");
var HitBtcConstant_1 = require("./HitBtcConstant");
var MdUtils_1 = require("../api/MdUtils");
var WebSocket = require('isomorphic-ws');
var HitBtcProxy = /** @class */ (function (_super) {
    __extends(HitBtcProxy, _super);
    function HitBtcProxy() {
        var _this = _super.call(this) || this;
        _this.subscription = {};
        return _this;
    }
    HitBtcProxy.prototype.start = function () {
        var _this = this;
        var url = HitBtcConstant_1.HitBtcConstant.baseEndpoint;
        this.webSocket = new WebSocket(url);
        this.webSocket.onopen = function () {
            _this.emit(HitBtcConstant_1.HitBtcConstant.OPENEVENT, 'HitBtc Transport connected');
            _this.onConnected();
        };
        this.webSocket.onclose = function (reason) {
            //TODO: Reconnection?
            _this.emit(HitBtcConstant_1.HitBtcConstant.CLOSEEVENT, reason);
        };
        this.webSocket.onerror = function (error) {
            _this.emit(HitBtcConstant_1.HitBtcConstant.ERROREVENT, error);
        };
        this.webSocket.onmessage = function (msg) {
            _this.onMessage(JSON.parse(msg.data));
        };
    };
    HitBtcProxy.prototype.onConnected = function () {
        var _this = this;
        var pairs = Object.keys(HitBtcConstant_1.HitBtcConstant.PAIRS);
        var id = MdUtils_1.MdUtils.getMsSinceMidnight();
        pairs.forEach(function (pair) {
            _this.webSocket.send("{\"method\":\"subscribeTicker\",\"params\":{\"symbol\":\"" + pair + "\"},\"id\": " + id + "}");
            _this.webSocket.send("{\"method\":\"subscribeOrderbook\",\"params\":{\"symbol\":\"" + pair + "\"},\"id\": " + id + "}");
            _this.subscription[pair] = {
                symbolPair: pair,
                symbolId: HitBtcConstant_1.HitBtcConstant.PAIRS[pair].symbol,
                benchId: HitBtcConstant_1.HitBtcConstant.PAIRS[pair].bench,
                bid: 0,
                ask: 0,
                bidSize: 0,
                askSize: 0,
                price: 0,
                size: 0,
                volume: 0
            };
        });
    };
    HitBtcProxy.prototype.onMessage = function (msg) {
        if (msg.method && msg.params) {
            switch (msg.method) {
                case HitBtcConstant_1.HitBtcConstant.snapshotOrderbook:
                    this.onSnapShot(msg, HitBtcConstant_1.HitBtcConstant.SNAPSHOTEVENT);
                    break;
                case HitBtcConstant_1.HitBtcConstant.ticker:
                    var symbol = msg.params.symbol;
                    this.subscription[symbol].price = msg.params.last;
                    this.subscription[symbol].volume = msg.params.volume;
                    this.subscription[symbol].bid = msg.params.bid;
                    this.subscription[symbol].ask = msg.params.ask;
                    this.emit(HitBtcConstant_1.HitBtcConstant.TICKEREVENT, symbol, this.subscription[symbol]);
                    break;
                case HitBtcConstant_1.HitBtcConstant.updateOrderbook:
                    this.onSnapShot(msg, HitBtcConstant_1.HitBtcConstant.UPDATEBOOKEVENT);
                    break;
            }
        }
        else if (msg.id) {
            //TODO: any important messages from here?
        }
    };
    HitBtcProxy.prototype.onSnapShot = function (msg, eventType) {
        var symbol = msg.params.symbol;
        var update = false;
        for (var i = msg.params.bid.length - 1; i >= 0; --i) {
            if (msg.params.bid[i].price == 0) {
                msg.params.bid.splice(i, 1);
            }
            if (msg.params.bid[i].size == 0) {
                msg.params.bid.splice(i, 1);
            }
        }
        for (var i = msg.params.ask.length - 1; i >= 0; --i) {
            if (msg.params.ask[i].price == 0) {
                msg.params.ask.splice(i, 1);
            }
            if (msg.params.ask[i].size == 0) {
                msg.params.ask.splice(i, 1);
            }
        }
        msg.params.bid.sort(function (a, b) { return (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0); });
        msg.params.ask.sort(function (a, b) { return (a.price > b.price) ? -1 : ((b.price > a.price) ? 1 : 0); });
        if (msg.params.ask.length > 0) {
            if (msg.params.ask[0].price < this.subscription[symbol].ask) {
                this.subscription[symbol].ask = msg.params.ask[0].price;
                this.subscription[symbol].askSize = msg.params.ask[0].size;
                update = true;
            }
        }
        if (msg.params.bid.length > 0) {
            if (msg.params.bid[0].price > this.subscription[symbol].bid) {
                this.subscription[symbol].bid = msg.params.bid[0].price;
                this.subscription[symbol].bidSize = msg.params.bid[0].size;
                update = true;
            }
        }
        if (update) {
            this.emit(eventType, symbol, this.subscription[symbol]);
        }
    };
    return HitBtcProxy;
}(EventEmiter));
exports.HitBtcProxy = HitBtcProxy;
