import * as EventEmiter from 'events';
export declare class IpfsProxy extends EventEmiter {
    private files;
    constructor();
    subscribe(files: string[]): void;
    Start(): void;
}
