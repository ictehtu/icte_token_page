"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import {HtuTransport} from "./v1/HtuTransport";
var IeoTransport_1 = require("./v1/IeoTransport");
var PoloniexTransport_1 = require("./v1/PoloniexTransport");
var BinanceTransport_1 = require("./v1/BinanceTransport");
var BitfinexTransport_1 = require("./v1/BitfinexTransport");
var IeoTransport2_1 = require("./v1/IeoTransport2");
var MdManager = /** @class */ (function () {
    function MdManager(callback) {
        var _this = this;
        this.transports = [];
        this.connect = function (tickers, exchanges) {
            for (var i = 0; i < _this.transports.length; i++) {
                if (exchanges) {
                    if (exchanges.includes(_this.transports[i].id.toString())) {
                        _this.transports[i].connect(tickers);
                    }
                }
                else {
                    _this.transports[i].connect(tickers);
                }
            }
        };
        this.callback = callback;
        this.transports = [];
        this.transports.push(new IeoTransport_1.IeoTransport(this.callback));
        this.transports.push(new PoloniexTransport_1.PoloniexTransport(this.callback));
        this.transports.push(new BinanceTransport_1.BinanceTransport(this.callback));
        this.transports.push(new BitfinexTransport_1.BitfinexTransport(this.callback));
        this.transports.push(new IeoTransport2_1.IeoTransport2(this.callback));
        // this.transports.push( new HitBtcTransport(this.callback));
    }
    MdManager.prototype.isConnected = function () {
    };
    return MdManager;
}());
exports.MdManager = MdManager;
