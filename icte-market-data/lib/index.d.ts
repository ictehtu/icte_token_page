import { Observable } from 'rxjs';
import { MdManager } from './transports/MdManager';
import { ModelManager } from './model/ModelManager';
import { Parser } from './parser/Parser';
import { MdCallback } from "./transports/api/MdCallback";
import { Symbols } from "./model/Symbols";
import { MatrixModel } from "./model/MatrixModel";
import { EventEmitter } from 'events';
export declare class ICTE implements MdCallback {
    readonly modelData: ModelManager;
    readonly marketData: MdManager;
    readonly matrixModel: MatrixModel;
    marketDataEvents: EventEmitter;
    tickers: any;
    exchanges: any;
    constructor(tickers?: string[], exchanges?: number[]);
    init: () => void;
    on(msg: Parser): void;
    getData(): Observable<Symbols>;
}
