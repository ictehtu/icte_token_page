import { APriceMsg } from "./APriceMsg";
export declare class AskMsg extends APriceMsg {
    constructor();
    getPrice: () => number;
    setPrice: (price: any) => void;
}
