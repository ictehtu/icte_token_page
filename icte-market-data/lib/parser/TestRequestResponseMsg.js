"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var Parser_1 = require("./Parser");
var TestRequestResponseMsg = /** @class */ (function (_super) {
    __extends(TestRequestResponseMsg, _super);
    function TestRequestResponseMsg(testReqID, status) {
        var _this = _super.call(this, TestRequestResponseMsg.Msg_Size) || this;
        _this.getDeltaTime = function () {
            return _super.prototype.getInt.call(_this, TestRequestResponseMsg.Fld_DeltaTime);
        };
        _this.parse = function (view) {
            _super.prototype.setLong.call(_this, TestRequestResponseMsg.Fld_DeltaTime, Number("" + view.getUint32(TestRequestResponseMsg.Fld_DeltaTime) + view.getUint32(TestRequestResponseMsg.Fld_DeltaTime + 4)));
        };
        _this.deltaTime = function () {
            return _this.getLong(TestRequestResponseMsg.Fld_DeltaTime);
        };
        _this.testReqId = function () {
            return _this.getInt(TestRequestResponseMsg.Fld_TestReqID);
        };
        _this.testReqStatus = function () {
            return _this.getByte(TestRequestResponseMsg.Fld_TestReqStatus);
        };
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Sys);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_TestRequest);
        _this.setLong(TestRequestResponseMsg.Fld_DeltaTime, Date.now());
        _this.setInt(TestRequestResponseMsg.Fld_TestReqID, testReqID);
        _this.setByte(TestRequestResponseMsg.Fld_TestReqStatus, status);
        return _this;
    }
    TestRequestResponseMsg.Fld_DeltaTime = 2;
    TestRequestResponseMsg.Fld_TestReqID = 10;
    TestRequestResponseMsg.Fld_TestReqStatus = 14;
    TestRequestResponseMsg.Msg_Size = 15;
    return TestRequestResponseMsg;
}(Parser_1.Parser));
exports.TestRequestResponseMsg = TestRequestResponseMsg;
