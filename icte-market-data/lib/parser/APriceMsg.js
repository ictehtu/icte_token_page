"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var MdConstant_1 = require("../transports/api/MdConstant");
var Utilities_1 = require("../model/Utilities");
var APriceMsg = /** @class */ (function (_super) {
    __extends(APriceMsg, _super);
    function APriceMsg() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.getDeltaTime = function () {
            return _super.prototype.getLong.call(_this, APriceMsg.Fld_DeltaTime);
        };
        _this.getId = function () {
            return _super.prototype.getInt.call(_this, APriceMsg.Fld_SymbolId);
        };
        _this.getPriceInt = function () {
            return _super.prototype.getInt.call(_this, APriceMsg.Fld_PriceInt);
        };
        _this.getPriceFrac = function () {
            return _super.prototype.getInt.call(_this, APriceMsg.Fld_PriceFrac);
        };
        _this.getSizeInt = function () {
            return _super.prototype.getInt.call(_this, APriceMsg.Fld_SizeInt);
        };
        _this.getSizeFrac = function () {
            return _super.prototype.getInt.call(_this, APriceMsg.Fld_SizeFrac);
        };
        _this.getVolumeInt = function () {
            return _super.prototype.getInt.call(_this, APriceMsg.Fld_VolumeInt);
        };
        _this.getVolumeFrac = function () {
            return _super.prototype.getInt.call(_this, APriceMsg.Fld_VolumeFrac);
        };
        _this.getVenue = function () {
            return _super.prototype.getShort.call(_this, APriceMsg.Fld_Venue);
        };
        _this.getBenchmarkId = function () {
            return _super.prototype.getInt.call(_this, APriceMsg.Fld_BenchmarkId);
        };
        _this.parse = function (view) {
            _super.prototype.setLong.call(_this, APriceMsg.Fld_DeltaTime, view.getFloat64(APriceMsg.Fld_DeltaTime));
            _super.prototype.setInt.call(_this, APriceMsg.Fld_SymbolId, view.getUint32(APriceMsg.Fld_SymbolId));
            _super.prototype.setInt.call(_this, APriceMsg.Fld_PriceInt, view.getUint32(APriceMsg.Fld_PriceInt));
            _super.prototype.setInt.call(_this, APriceMsg.Fld_PriceFrac, view.getUint32(APriceMsg.Fld_PriceFrac));
            _super.prototype.setInt.call(_this, APriceMsg.Fld_SizeInt, view.getUint32(APriceMsg.Fld_SizeInt));
            _super.prototype.setInt.call(_this, APriceMsg.Fld_SizeFrac, view.getUint32(APriceMsg.Fld_SizeFrac));
            _super.prototype.setInt.call(_this, APriceMsg.Fld_VolumeInt, view.getUint32(APriceMsg.Fld_VolumeInt));
            _super.prototype.setInt.call(_this, APriceMsg.Fld_VolumeFrac, view.getUint32(APriceMsg.Fld_VolumeFrac));
            _super.prototype.setShort.call(_this, APriceMsg.Fld_Venue, view.getUint16(APriceMsg.Fld_Venue));
            _super.prototype.setInt.call(_this, APriceMsg.Fld_BenchmarkId, view.getUint32(APriceMsg.Fld_BenchmarkId));
        };
        _this.buffer = function (deltaTime, symbolId, priceInt, priceFrac, sizeInt, sizeFrac, volumeInt, volumeFrac, venue, benchmark) {
            _super.prototype.setLong.call(_this, APriceMsg.Fld_DeltaTime, deltaTime);
            _super.prototype.setInt.call(_this, APriceMsg.Fld_SymbolId, symbolId);
            _super.prototype.setInt.call(_this, APriceMsg.Fld_PriceInt, priceInt);
            _super.prototype.setInt.call(_this, APriceMsg.Fld_PriceFrac, priceFrac);
            _super.prototype.setInt.call(_this, APriceMsg.Fld_SizeInt, sizeInt);
            _super.prototype.setInt.call(_this, APriceMsg.Fld_SizeFrac, sizeFrac);
            _super.prototype.setInt.call(_this, APriceMsg.Fld_VolumeInt, volumeInt);
            _super.prototype.setInt.call(_this, APriceMsg.Fld_VolumeFrac, volumeFrac);
            _super.prototype.setShort.call(_this, APriceMsg.Fld_Venue, venue);
            _super.prototype.setInt.call(_this, APriceMsg.Fld_BenchmarkId, benchmark);
            return _super.prototype.buffer.call(_this);
        };
        _this.toString = function () {
            return "{"
                + Constant_1.Constant.MsgTypesStr[_super.prototype.msgType.call(_this)] + ":" + Constant_1.Constant.Msg_QuoteSubStr[_super.prototype.msgSubtype.call(_this)] + "} "
                + MdConstant_1.MdConstant.COINS[_this.getId()] + " " + _this.getId() + "|"
                + MdConstant_1.MdConstant.COINS[_this.getBenchmarkId()] + " " + _this.getBenchmarkId() + "|"
                + MdConstant_1.MdConstant.VENUES[_this.getVenue()] + "|"
                + _this.getDeltaTime() + "|"
                + _this.getPriceInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getPriceFrac().toString(), '0', Utilities_1.Utilities.PADSIZE) + "|"
                + _this.getSizeInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getSizeFrac().toString(), '0', Utilities_1.Utilities.PADSIZE) + "|"
                + _this.getVolumeInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getVolumeFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
        };
        return _this;
    }
    //field offsets
    APriceMsg.Fld_DeltaTime = 2;
    APriceMsg.Fld_SymbolId = 10;
    APriceMsg.Fld_PriceInt = 14;
    APriceMsg.Fld_PriceFrac = 18;
    APriceMsg.Fld_SizeInt = 22;
    APriceMsg.Fld_SizeFrac = 26;
    APriceMsg.Fld_VolumeInt = 30;
    APriceMsg.Fld_VolumeFrac = 34;
    APriceMsg.Fld_Venue = 38; //2 bytes
    APriceMsg.Fld_BenchmarkId = 40;
    APriceMsg.Msg_Size = 44;
    return APriceMsg;
}(Parser_1.Parser));
exports.APriceMsg = APriceMsg;
