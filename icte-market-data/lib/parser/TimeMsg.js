"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var TimeMsg = /** @class */ (function (_super) {
    __extends(TimeMsg, _super);
    function TimeMsg() {
        var _this = _super.call(this, TimeMsg.Msg_Size) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Sys);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_SysTime);
        return _this;
    }
    TimeMsg.prototype.getDeltaTime = function () {
        return _super.prototype.getLong.call(this, TimeMsg.Fld_DeltaTime);
    };
    TimeMsg.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, TimeMsg.Fld_DeltaTime, dataView.getUint32(TimeMsg.Fld_DeltaTime));
    };
    TimeMsg.prototype.buffer = function (deltaTime) {
        _super.prototype.setInt.call(this, TimeMsg.Fld_DeltaTime, deltaTime);
        return _super.prototype.buffer.call(this);
    };
    TimeMsg.Fld_DeltaTime = 2;
    TimeMsg.Msg_Size = 6;
    return TimeMsg;
}(Parser_1.Parser));
exports.TimeMsg = TimeMsg;
