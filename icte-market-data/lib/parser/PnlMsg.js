"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var Parser_1 = require("./Parser");
var PnlMsg = /** @class */ (function (_super) {
    __extends(PnlMsg, _super);
    function PnlMsg() {
        var _this = _super.call(this, PnlMsg.Msg_Size) || this;
        _this.getDeltaTime = function () {
            return _super.prototype.getLong.call(_this, PnlMsg.Fld_DeltaTime);
        };
        _this.getId = function () {
            return _super.prototype.getInt.call(_this, PnlMsg.Fld_SymbolId);
        };
        _this.getSizeInt = function () {
            return _super.prototype.getInt.call(_this, PnlMsg.Fld_SizeInt);
        };
        _this.getSizeFrac = function () {
            return _super.prototype.getInt.call(_this, PnlMsg.Fld_SizeFrac);
        };
        _this.parse = function (view) {
            _super.prototype.setLong.call(_this, PnlMsg.Fld_DeltaTime, view.getFloat64(PnlMsg.Fld_DeltaTime));
            _super.prototype.setInt.call(_this, PnlMsg.Fld_SymbolId, view.getUint32(PnlMsg.Fld_SymbolId));
            _super.prototype.setInt.call(_this, PnlMsg.Fld_SizeInt, view.getUint32(PnlMsg.Fld_SizeInt));
            _super.prototype.setInt.call(_this, PnlMsg.Fld_SizeFrac, view.getUint32(PnlMsg.Fld_SizeFrac));
        };
        _this.buffer = function (deltaTime, symbolId, sizeInt, sizeFrac) {
            _super.prototype.setLong.call(_this, PnlMsg.Fld_DeltaTime, deltaTime);
            _super.prototype.setInt.call(_this, PnlMsg.Fld_SymbolId, symbolId);
            _super.prototype.setInt.call(_this, PnlMsg.Fld_SizeInt, sizeInt);
            _super.prototype.setInt.call(_this, PnlMsg.Fld_SizeFrac, sizeFrac);
            return _super.prototype.buffer.call(_this);
        };
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Position);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_PositionUpdate);
        return _this;
    }
    PnlMsg.Fld_DeltaTime = 2;
    PnlMsg.Fld_SymbolId = 10;
    PnlMsg.Fld_SizeInt = 14;
    PnlMsg.Fld_SizeFrac = 18;
    PnlMsg.Msg_Size = 22;
    return PnlMsg;
}(Parser_1.Parser));
exports.PnlMsg = PnlMsg;
