"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var base64js = require('base64-js');
var Parser = /** @class */ (function () {
    function Parser(size, buf) {
        var _this = this;
        this.toBase64 = function () {
            return base64js.fromByteArray(_this.buf);
        };
        if (buf != null) {
            if (size > buf.length) {
                throw new Error('buffer size is less then size data');
            }
            this.buf = buf;
        }
        else {
            this.buf = Buffer.allocUnsafe(size);
        }
    }
    Parser.prototype.msgType = function () {
        return this.getByte(Constant_1.Constant.Fld_Type);
    };
    Parser.prototype.msgSubtype = function () {
        return this.getByte(Constant_1.Constant.Fld_SubType);
    };
    Parser.prototype.parse = function (view) {
    };
    Parser.prototype.buffer = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return this.buf;
    };
    Parser.prototype.getByte = function (offset) {
        return this.buf.readUInt8(offset);
    };
    Parser.prototype.getShort = function (offset) {
        return this.buf.readUInt16BE(offset);
    };
    Parser.prototype.getInt = function (offset) {
        return this.buf.readUInt32BE(offset);
    };
    Parser.prototype.setByte = function (offset, value) {
        this.buf.writeUInt8(value, offset);
    };
    Parser.prototype.setShort = function (offset, value) {
        this.buf.writeUInt16BE(value, offset);
    };
    Parser.prototype.setInt = function (offset, value) {
        this.buf.writeUInt32BE(value, offset);
    };
    Parser.prototype.setBytes = function (offset, array) {
        this.buf.writeUInt32BE(array.length, offset);
        offset += 4;
        this.buf.fill(array, offset);
    };
    Parser.prototype.getBytes = function (offset) {
        function toArrayBuffer(buf) {
            var ab = new ArrayBuffer(buf.length);
            var view = new Uint8Array(ab);
            for (var i = 0; i < buf.length; ++i) {
                view[i] = buf[i];
            }
            return ab;
        }
        var size = this.buf.readUInt32BE(offset);
        offset += 4;
        var subbuffer = this.buf.subarray(offset, size + offset);
        return toArrayBuffer(subbuffer);
    };
    Parser.prototype.setLong = function (offset, value) {
        var MAX_UINT32 = 0xFFFFFFFF;
        var big = ~~(value / MAX_UINT32);
        var low = (value % MAX_UINT32) - big;
        this.buf.writeUInt32BE(big, offset);
        this.buf.writeUInt32BE(low, offset + 4);
    };
    Parser.prototype.getLong = function (offset) {
        return Number("" + this.buf.readUInt32BE(offset) + this.buf.readUInt32BE(offset + 4));
    };
    return Parser;
}());
exports.Parser = Parser;
