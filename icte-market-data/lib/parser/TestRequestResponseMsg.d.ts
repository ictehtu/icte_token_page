import { Parser } from "./Parser";
export declare class TestRequestResponseMsg extends Parser {
    static readonly Fld_DeltaTime: number;
    static readonly Fld_TestReqID: number;
    static readonly Fld_TestReqStatus: number;
    static readonly Msg_Size: number;
    constructor(testReqID: number, status: number);
    getDeltaTime: () => number;
    parse: (view: DataView) => void;
    deltaTime: () => number;
    testReqId: () => number;
    testReqStatus: () => number;
}
