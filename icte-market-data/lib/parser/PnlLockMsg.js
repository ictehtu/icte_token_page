"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var Parser_1 = require("./Parser");
var PnlLockMsg = /** @class */ (function (_super) {
    __extends(PnlLockMsg, _super);
    function PnlLockMsg() {
        var _this = _super.call(this, PnlLockMsg.Msg_Size) || this;
        _this.getDeltaTime = function () {
            return _super.prototype.getInt.call(_this, PnlLockMsg.Fld_DeltaTime);
        };
        _this.getId = function () {
            return _super.prototype.getInt.call(_this, PnlLockMsg.Fld_SymbolId);
        };
        _this.parse = function (view) {
            _super.prototype.setInt.call(_this, PnlLockMsg.Fld_DeltaTime, view.getUint32(PnlLockMsg.Fld_DeltaTime));
            _super.prototype.setInt.call(_this, PnlLockMsg.Fld_SymbolId, view.getUint32(PnlLockMsg.Fld_SymbolId));
        };
        _this.buffer = function (deltaTime, symbolId) {
            _super.prototype.setInt.call(_this, PnlLockMsg.Fld_DeltaTime, deltaTime);
            _super.prototype.setInt.call(_this, PnlLockMsg.Fld_SymbolId, symbolId);
            return _super.prototype.buffer.call(_this);
        };
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Position);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_PositionLock);
        return _this;
    }
    PnlLockMsg.Fld_DeltaTime = 2;
    PnlLockMsg.Fld_SymbolId = 6;
    PnlLockMsg.Msg_Size = 10;
    return PnlLockMsg;
}(Parser_1.Parser));
exports.PnlLockMsg = PnlLockMsg;
