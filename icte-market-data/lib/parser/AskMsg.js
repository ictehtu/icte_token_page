"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var APriceMsg_1 = require("./APriceMsg");
var Utilities_1 = require("../model/Utilities");
var AskMsg = /** @class */ (function (_super) {
    __extends(AskMsg, _super);
    function AskMsg() {
        var _this = _super.call(this, AskMsg.Msg_Size) || this;
        _this.getPrice = function () {
            var pricestr = _this.getPriceInt() + "." + Utilities_1.Utilities.padLeft(_this.getPriceFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
            var price = parseFloat(pricestr);
            return price;
        };
        _this.setPrice = function (price) {
            _super.prototype.setInt.call(_this, AskMsg.Fld_PriceInt, Math.trunc(price));
            _super.prototype.setInt.call(_this, AskMsg.Fld_PriceFrac, Math.trunc(price % 1 * Utilities_1.Utilities.DIV));
        };
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Quote);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_Ask);
        return _this;
    }
    return AskMsg;
}(APriceMsg_1.APriceMsg));
exports.AskMsg = AskMsg;
