"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var Parser_1 = require("./Parser");
var MdConstant_1 = require("../transports/api/MdConstant");
var Utilities_1 = require("../model/Utilities");
var QuoteMsg = /** @class */ (function (_super) {
    __extends(QuoteMsg, _super);
    function QuoteMsg() {
        var _this = _super.call(this, QuoteMsg.Msg_Size) || this;
        _this.getDeltaTime = function () {
            return _super.prototype.getLong.call(_this, QuoteMsg.Fld_DeltaTime);
        };
        _this.getId = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_SymbolId);
        };
        _this.getBidInt = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_BidInt);
        };
        _this.getBidFrac = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_BidFrac);
        };
        _this.getBidSizeInt = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_BidSizeInt);
        };
        _this.getBidSizeFrac = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_BidSizeFrac);
        };
        _this.getAskInt = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_AskInt);
        };
        _this.getAskFrac = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_AskFrac);
        };
        _this.getAskSizeInt = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_AskSizeInt);
        };
        _this.getAskSizeFrac = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_AskSizeFrac);
        };
        _this.getVenue = function () {
            return _super.prototype.getShort.call(_this, QuoteMsg.Fld_Venue);
        };
        _this.getBenchmarkId = function () {
            return _super.prototype.getInt.call(_this, QuoteMsg.Fld_BenchmarkId);
        };
        _this.parse = function (view) {
            _super.prototype.setLong.call(_this, QuoteMsg.Fld_DeltaTime, view.getFloat64(QuoteMsg.Fld_DeltaTime));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_SymbolId, view.getUint32(QuoteMsg.Fld_SymbolId));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BidInt, view.getUint32(QuoteMsg.Fld_BidInt));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BidFrac, view.getUint32(QuoteMsg.Fld_BidFrac));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BidSizeInt, view.getUint32(QuoteMsg.Fld_BidSizeInt));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BidSizeFrac, view.getUint32(QuoteMsg.Fld_BidSizeFrac));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskInt, view.getUint32(QuoteMsg.Fld_AskInt));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskFrac, view.getUint32(QuoteMsg.Fld_AskFrac));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskSizeInt, view.getUint32(QuoteMsg.Fld_AskSizeInt));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskSizeFrac, view.getUint32(QuoteMsg.Fld_AskSizeFrac));
            _super.prototype.setShort.call(_this, QuoteMsg.Fld_Venue, view.getUint16(QuoteMsg.Fld_Venue));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BenchmarkId, view.getUint32(QuoteMsg.Fld_BenchmarkId));
        };
        _this.buffer = function (deltaTime, symbolId, bidInt, bidFrac, bidSizeInt, bidSizeFrac, askInt, askFrac, askSizeInt, askSizeFrac, venue, benchId) {
            _super.prototype.setLong.call(_this, QuoteMsg.Fld_DeltaTime, deltaTime);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_SymbolId, symbolId);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BidInt, bidInt);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BidFrac, bidFrac);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BidSizeInt, bidSizeInt);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BidSizeFrac, bidSizeFrac);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskInt, askInt);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskFrac, askFrac);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskSizeInt, askSizeInt);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskSizeFrac, askSizeFrac);
            _super.prototype.setShort.call(_this, QuoteMsg.Fld_Venue, venue);
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_BenchmarkId, benchId);
            return _super.prototype.buffer.call(_this);
        };
        _this.getAsk = function () {
            var pricestr = _this.getAskInt() + "." + Utilities_1.Utilities.padLeft(_this.getAskFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
            var price = parseFloat(pricestr);
            return price;
        };
        _this.getBid = function () {
            var pricestr = _this.getBidInt() + "." + Utilities_1.Utilities.padLeft(_this.getBidFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
            var price = parseFloat(pricestr);
            return price;
        };
        _this.setAsk = function (price) {
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskInt, Math.trunc(price));
            _super.prototype.setInt.call(_this, QuoteMsg.Fld_AskFrac, Math.trunc(price % 1 * Utilities_1.Utilities.DIV));
        };
        _this.toString = function () {
            return "{"
                + Constant_1.Constant.MsgTypesStr[_super.prototype.msgType.call(_this)] + ":" + Constant_1.Constant.Msg_QuoteSubStr[_super.prototype.msgSubtype.call(_this)] + "} "
                + MdConstant_1.MdConstant.COINS[_this.getId()] + " " + _this.getId() + "|"
                + MdConstant_1.MdConstant.COINS[_this.getBenchmarkId()] + " " + _this.getBenchmarkId() + "|"
                + MdConstant_1.MdConstant.VENUES[_this.getVenue()] + "|"
                + _this.getDeltaTime() + "|"
                + _this.getBidInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getBidFrac().toString(), '0', Utilities_1.Utilities.PADSIZE) + "|"
                + _this.getBidSizeInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getBidSizeFrac().toString(), '0', Utilities_1.Utilities.PADSIZE) + "|"
                + _this.getAskInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getAskFrac().toString(), '0', Utilities_1.Utilities.PADSIZE) + "|"
                + _this.getAskSizeInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getAskSizeFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
        };
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Quote);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_Quote);
        return _this;
    }
    /*
        Start of the field constant sizes
     */
    QuoteMsg.Fld_DeltaTime = 2;
    QuoteMsg.Fld_SymbolId = 10;
    QuoteMsg.Fld_BidInt = 14;
    QuoteMsg.Fld_BidFrac = 18;
    QuoteMsg.Fld_BidSizeInt = 22;
    QuoteMsg.Fld_BidSizeFrac = 26;
    QuoteMsg.Fld_AskInt = 30;
    QuoteMsg.Fld_AskFrac = 34;
    QuoteMsg.Fld_AskSizeInt = 38;
    QuoteMsg.Fld_AskSizeFrac = 42;
    QuoteMsg.Fld_Venue = 46;
    QuoteMsg.Fld_BenchmarkId = 48;
    QuoteMsg.Msg_Size = 52;
    return QuoteMsg;
}(Parser_1.Parser));
exports.QuoteMsg = QuoteMsg;
