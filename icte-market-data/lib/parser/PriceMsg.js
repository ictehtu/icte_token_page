"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var APriceMsg_1 = require("./APriceMsg");
var PriceMsg = /** @class */ (function (_super) {
    __extends(PriceMsg, _super);
    function PriceMsg() {
        var _this = _super.call(this, PriceMsg.Msg_Size) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Quote);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_Price);
        return _this;
    }
    return PriceMsg;
}(APriceMsg_1.APriceMsg));
exports.PriceMsg = PriceMsg;
