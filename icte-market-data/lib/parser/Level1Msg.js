"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var Parser_1 = require("./Parser");
var MdConstant_1 = require("../transports/api/MdConstant");
var Utilities_1 = require("../model/Utilities");
var Level1Msg = /** @class */ (function (_super) {
    __extends(Level1Msg, _super);
    function Level1Msg() {
        var _this = _super.call(this, Level1Msg.Msg_Size) || this;
        _this.getDeltaTime = function () {
            return _super.prototype.getLong.call(_this, Level1Msg.Fld_DeltaTime);
        };
        _this.getId = function () {
            return _super.prototype.getInt.call(_this, Level1Msg.Fld_SymbolId);
        };
        _this.getPriceInt = function () {
            return _super.prototype.getInt.call(_this, Level1Msg.Fld_PriceInt);
        };
        _this.getPriceFrac = function () {
            return _super.prototype.getInt.call(_this, Level1Msg.Fld_PriceFrac);
        };
        _this.getBidInt = function () {
            return _super.prototype.getInt.call(_this, Level1Msg.Fld_BidInt);
        };
        _this.getBidFrac = function () {
            return _super.prototype.getInt.call(_this, Level1Msg.Fld_BidFrac);
        };
        _this.getAskInt = function () {
            return _super.prototype.getInt.call(_this, Level1Msg.Fld_AskInt);
        };
        _this.getAskFrac = function () {
            return _super.prototype.getInt.call(_this, Level1Msg.Fld_AskFrac);
        };
        _this.getVenue = function () {
            return _super.prototype.getShort.call(_this, Level1Msg.Fld_Venue);
        };
        _this.getBenchmarkId = function () {
            return _super.prototype.getInt.call(_this, Level1Msg.Fld_BenchId);
        };
        _this.parse = function (view) {
            _super.prototype.setLong.call(_this, Level1Msg.Fld_DeltaTime, view.getFloat64(Level1Msg.Fld_DeltaTime));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_SymbolId, view.getUint32(Level1Msg.Fld_SymbolId));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_PriceInt, view.getUint32(Level1Msg.Fld_PriceInt));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_PriceFrac, view.getUint32(Level1Msg.Fld_PriceFrac));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_BidInt, view.getUint32(Level1Msg.Fld_BidInt));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_BidFrac, view.getUint32(Level1Msg.Fld_BidFrac));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_AskInt, view.getUint32(Level1Msg.Fld_AskInt));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_AskFrac, view.getUint32(Level1Msg.Fld_AskFrac));
            _super.prototype.setShort.call(_this, Level1Msg.Fld_Venue, view.getUint16(Level1Msg.Fld_Venue));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_BenchId, view.getUint32(Level1Msg.Fld_BenchId));
        };
        _this.buffer = function (deltaTime, symbolId, priceInt, priceFrac, bidInt, bidFrac, askInt, askFrac, venue, benchmark) {
            _super.prototype.setLong.call(_this, Level1Msg.Fld_DeltaTime, deltaTime);
            _super.prototype.setInt.call(_this, Level1Msg.Fld_SymbolId, symbolId);
            _super.prototype.setInt.call(_this, Level1Msg.Fld_PriceInt, priceInt);
            _super.prototype.setInt.call(_this, Level1Msg.Fld_PriceFrac, priceFrac);
            _super.prototype.setInt.call(_this, Level1Msg.Fld_BidInt, bidInt);
            _super.prototype.setInt.call(_this, Level1Msg.Fld_BidFrac, bidFrac);
            _super.prototype.setInt.call(_this, Level1Msg.Fld_AskInt, askInt);
            _super.prototype.setInt.call(_this, Level1Msg.Fld_AskFrac, askFrac);
            _super.prototype.setShort.call(_this, Level1Msg.Fld_Venue, venue);
            _super.prototype.setInt.call(_this, Level1Msg.Fld_BenchId, benchmark);
            return _super.prototype.buffer.call(_this);
        };
        _this.getAsk = function () {
            var pricestr = _this.getAskInt() + "." + Utilities_1.Utilities.padLeft(_this.getAskFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
            var price = parseFloat(pricestr);
            return price;
        };
        _this.getBid = function () {
            var pricestr = _this.getBidInt() + "." + Utilities_1.Utilities.padLeft(_this.getBidFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
            var price = parseFloat(pricestr);
            return price;
        };
        _this.setAsk = function (price) {
            _super.prototype.setInt.call(_this, Level1Msg.Fld_AskInt, Math.trunc(price));
            _super.prototype.setInt.call(_this, Level1Msg.Fld_AskFrac, Math.trunc(price % 1 * Utilities_1.Utilities.DIV));
        };
        _this.toString = function () {
            return "{"
                + Constant_1.Constant.MsgTypesStr[_super.prototype.msgType.call(_this)] + ":" + Constant_1.Constant.Msg_QuoteSubStr[_super.prototype.msgSubtype.call(_this)] + "} "
                + MdConstant_1.MdConstant.COINS[_this.getId()] + " " + _this.getId() + "|"
                + MdConstant_1.MdConstant.COINS[_this.getBenchmarkId()] + " " + _this.getBenchmarkId() + "|"
                + MdConstant_1.MdConstant.VENUES[_this.getVenue()] + "|"
                + _this.getDeltaTime() + "|"
                + _this.getPriceInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getPriceFrac().toString(), '0', Utilities_1.Utilities.PADSIZE) + "|"
                + _this.getBidInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getBidFrac().toString(), '0', Utilities_1.Utilities.PADSIZE) + "|"
                + _this.getAskInt() + "."
                + Utilities_1.Utilities.padLeft(_this.getAskFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
        };
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Quote);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_Level1);
        return _this;
    }
    Level1Msg.Fld_DeltaTime = 2;
    Level1Msg.Fld_SymbolId = 10;
    Level1Msg.Fld_PriceInt = 14;
    Level1Msg.Fld_PriceFrac = 18;
    Level1Msg.Fld_BidInt = 22;
    Level1Msg.Fld_BidFrac = 26;
    Level1Msg.Fld_AskInt = 30;
    Level1Msg.Fld_AskFrac = 34;
    Level1Msg.Fld_Venue = 38;
    Level1Msg.Fld_BenchId = 40;
    Level1Msg.Msg_Size = 44;
    return Level1Msg;
}(Parser_1.Parser));
exports.Level1Msg = Level1Msg;
