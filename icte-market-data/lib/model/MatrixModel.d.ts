import { MatrixPrice } from "./MatrixPrice";
import { RateMsg } from "../parser/RateMsg";
export declare class MatrixModel {
    private readonly _buyId;
    private readonly _sellId;
    private readonly _rateMsg;
    private readonly _price;
    private readonly _bid;
    private readonly _ask;
    constructor(buyId: number, sellId: number);
    _volumeInt: number;
    readonly volumeInt: number;
    _volumeFrac: number;
    readonly rateValue: number;
    readonly volumeFrac: number;
    readonly price: MatrixPrice;
    readonly bid: MatrixPrice;
    readonly ask: MatrixPrice;
    readonly buyId: number;
    readonly sellId: number;
    readonly rateMsg: RateMsg;
    updateRate: (rateMsg: RateMsg) => void;
}
