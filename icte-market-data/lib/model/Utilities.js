"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utilities = /** @class */ (function () {
    function Utilities() {
    }
    Utilities.padLeft = function (text, padChar, size) {
        return (String(padChar).repeat(size) + text).substr((size * -1), size);
    };
    Utilities.prototype.wsLimiter = function (func) {
        var rateLimit = Utilities._RATE_LIMIT;
        var timeLimit = Date.now() + Utilities._TIME_LIMIT;
        var lastCall = 0;
        var now = 0;
        // let _that = this;
        return function () {
            now = Date.now();
            if (now <= timeLimit) {
                if (lastCall <= rateLimit) {
                    lastCall++;
                    return func.apply(this, arguments);
                }
            }
            else {
                // reset
                now = Date.now();
                timeLimit = now + Utilities._TIME_LIMIT;
                lastCall = 0;
            }
        };
    };
    Utilities.PADSIZE = 9;
    Utilities.DIV = 1000000000;
    Utilities._RATE_LIMIT = 100;
    Utilities._TIME_LIMIT = 1000;
    Utilities.logger = {
        debug: function () {
            var arg = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                arg[_i] = arguments[_i];
            }
            // console.log((new Date).toISOString(), 'DEBUG', ...arg)
        },
        info: function () {
            var arg = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                arg[_i] = arguments[_i];
            }
            // console.log((new Date).toISOString(), 'INFO', ...arg)
        },
        warn: function () {
            var arg = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                arg[_i] = arguments[_i];
            }
            // console.log((new Date).toISOString(), 'WARN', ...arg)
        }
    };
    return Utilities;
}());
exports.Utilities = Utilities;
