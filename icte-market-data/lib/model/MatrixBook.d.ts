import { MatrixModel } from "./MatrixModel";
export declare class MatrixBook {
    private readonly _sellSide;
    private readonly _buySide;
    constructor(buySide: MatrixModel, sellSide: MatrixModel);
    readonly buyName: string;
    readonly sellName: string;
    readonly buyBook: MatrixModel;
    readonly sellBook: MatrixModel;
}
