"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MdConstant_1 = require("../transports/api/MdConstant");
var MatrixBook = /** @class */ (function () {
    function MatrixBook(buySide, sellSide) {
        this._sellSide = sellSide;
        this._buySide = buySide;
    }
    Object.defineProperty(MatrixBook.prototype, "buyName", {
        get: function () {
            return MdConstant_1.MdConstant.COINS[this._buySide.buyId] + "/" + MdConstant_1.MdConstant.COINS[this._buySide.sellId];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixBook.prototype, "sellName", {
        get: function () {
            return MdConstant_1.MdConstant.COINS[this._sellSide.buyId] + "/" + MdConstant_1.MdConstant.COINS[this._sellSide.sellId];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixBook.prototype, "buyBook", {
        get: function () {
            return this._buySide;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixBook.prototype, "sellBook", {
        get: function () {
            return this._sellSide;
        },
        enumerable: true,
        configurable: true
    });
    return MatrixBook;
}());
exports.MatrixBook = MatrixBook;
