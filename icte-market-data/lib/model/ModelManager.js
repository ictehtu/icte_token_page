"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("./Constant");
var SymbolModel_1 = require("./SymbolModel");
var MdConstant_1 = require("../transports/api/MdConstant");
var RealtimeModel_1 = require("./RealtimeModel");
var Symbols_1 = require("./Symbols");
var RateMsg_1 = require("../parser/RateMsg");
var Utilities_1 = require("./Utilities");
var MatrixModel_1 = require("./MatrixModel");
var MdUtils_1 = require("../transports/api/MdUtils");
var ModelManager = /** @class */ (function () {
    function ModelManager() {
        var _this = this;
        this.connect = function () {
            _this.matrix = [];
            _this.timerMatrix = [];
            //create price matrix
            for (var i = 0; i < MdConstant_1.MdConstant.COINS.length; i++) {
                _this.matrix[i] = [];
                _this.timerMatrix[i] = [];
                for (var j = 0; j < MdConstant_1.MdConstant.COINS.length; j++) {
                    _this.matrix[i][j] = new MatrixModel_1.MatrixModel(i, j);
                    _this.timerMatrix[i][j] = new MatrixModel_1.MatrixModel(i, j);
                }
            }
            _this.benchModel = [];
            _this.reactModel = [];
            _this.timerModel = [];
            // @ts-ignore
            for (var i = 0; i < MdConstant_1.MdConstant.COINS.length; i++) {
                _this.benchModel[i] = new RealtimeModel_1.RealtimeModel(i);
                _this.reactModel[i] = new RealtimeModel_1.RealtimeModel(i);
                _this.timerModel[i] = new SymbolModel_1.SymbolModel(i);
            }
            _this.symbols = new Symbols_1.Symbols(_this.timerModel, _this.timerMatrix);
        };
        this.getModelTime = function () {
            return _this.time;
        };
        this.setModelTime = function (deltaTime) {
            _this.time = _this.getGmtZeroMidnight() + deltaTime;
        };
        this.getGmtZeroMidnight = function () {
            var d = new Date();
            var z = d.getTime() + (d.getTimezoneOffset() * 60000);
            var zd = new Date(z);
            zd.setHours(0);
            zd.setMinutes(0);
            zd.setSeconds(0);
            zd.setMilliseconds(0);
            return zd.getTime();
        };
        this.findSymbol = function (symbol) {
            var index = MdConstant_1.MdConstant.COINS.indexOf(symbol);
            if (index >= 0) {
                return index;
            }
            return -1;
        };
        this.getUpdate = function () {
            //sort and update
            _this.symbols.update();
            var arr = _this.symbols.arr;
            for (var i = 0; i < arr.length; i++) {
                _this.wrap(_this.reactModel[arr[i].id], _this.timerModel[arr[i].id]);
                for (var j = 0; j < arr.length; j++) {
                    _this.wrapMatrix(_this.matrix[arr[i].id][arr[j].id], _this.timerMatrix[arr[i].id][arr[j].id]);
                }
            }
            return _this.symbols;
        };
        this.setUpdate = function (data) {
            //type switch
            switch (data.msgType()) {
                case Constant_1.Constant.Msg_Sys:
                    //subtype switch
                    switch (data.msgSubtype()) {
                        case Constant_1.Constant.Sub_SysTime:
                            _this.updateTime(data);
                            break;
                    }
                    break;
                case Constant_1.Constant.Msg_Position:
                    switch (data.msgSubtype()) {
                        case Constant_1.Constant.Sub_PositionUpdate:
                            _this.updateCalc(data);
                            break;
                        case Constant_1.Constant.Sub_PositionLock:
                            _this.updateLockCalc(data);
                            break;
                    }
                    break;
                case Constant_1.Constant.Msg_Quote:
                    //subtype switch
                    switch (data.msgSubtype()) {
                        case Constant_1.Constant.Sub_Level1:
                            _this.updateLevel1(data);
                            break;
                        case Constant_1.Constant.Sub_Quote:
                            _this.updateQuote(data);
                            break;
                        case Constant_1.Constant.Sub_Buy:
                            _this.updateBuy(data);
                            break;
                        case Constant_1.Constant.Sub_Sell:
                            _this.updateSell(data);
                            break;
                        case Constant_1.Constant.Sub_Bid:
                            _this.updateBid(data);
                            break;
                        case Constant_1.Constant.Sub_Ask:
                            _this.updateAsk(data);
                            break;
                        case Constant_1.Constant.Sub_Rate:
                            _this.updateRate(data);
                            break;
                    }
                    break;
            }
        };
        this.getTime = function (deltaTime) {
            return _this.getGmtZeroMidnight() + deltaTime;
        };
        this.updateTime = function (data) {
            _this.setModelTime(_this.getGmtZeroMidnight() + data.getDeltaTime());
        };
        this.updateLockCalc = function (data) {
            var id = data.getId();
            _this.timerModel[id].wallet.setEnable(false);
        };
        this.updateCalc = function (data) {
            var id = data.getId();
            _this.timerModel[id].wallet.setSizeInt(data.getSizeInt());
            _this.timerModel[id].wallet.setSizeFrac(data.getSizeFrac());
            _this.timerModel[id].wallet.setEnable(true);
        };
        this._rateMsg = new RateMsg_1.RateMsg();
        this.sendRate = function (rate, frontSymbol, backSymbol, venue) {
            var r1 = Math.trunc(rate);
            var r2 = Math.trunc(rate % 1 * Utilities_1.Utilities.DIV);
            try {
                _this._rateMsg.buffer(MdUtils_1.MdUtils.getMsSinceMidnight(), frontSymbol, backSymbol, r1, r2, venue);
                _this.setUpdate(_this._rateMsg);
            }
            catch (e) {
                console.log(e);
            }
        };
        this.updateLevel1 = function (data) {
            Utilities_1.Utilities.logger.debug(data.toString());
            var id = data.getId();
            var benchId = data.getBenchmarkId();
            _this.matrix[id][benchId].price.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.matrix[id][benchId].price.setTime(_this.getTime(data.getDeltaTime()));
            _this.matrix[id][benchId].bid.setPrice(data.getBidInt(), data.getBidFrac());
            _this.matrix[id][benchId].bid.setTime(_this.matrix[id][benchId].price.time);
            _this.matrix[id][benchId].ask.setPrice(data.getAskInt(), data.getAskFrac());
            _this.matrix[id][benchId].ask.setTime(_this.matrix[id][benchId].price.time);
            if (benchId == MdConstant_1.MdConstant.USDT && id != MdConstant_1.MdConstant.IEO) {
                var ID_IEO_RATE = _this.matrix[MdConstant_1.MdConstant.USDT][MdConstant_1.MdConstant.IEO].rateValue * data.getAsk();
                _this.sendRate(ID_IEO_RATE, id, MdConstant_1.MdConstant.IEO, MdConstant_1.MdConstant.ModelManager);
                var IEO_ID_RATE = 1 / ID_IEO_RATE;
                _this.sendRate(IEO_ID_RATE, MdConstant_1.MdConstant.IEO, id, MdConstant_1.MdConstant.ModelManager);
            }
            _this.reactModel[id].price.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.reactModel[id].price.setTime(_this.matrix[id][benchId].price.time);
            _this.reactModel[id].price.setVenue(data.getVenue());
            _this.reactModel[id].bid.setPrice(data.getBidInt(), data.getBidFrac());
            _this.reactModel[id].bid.setTime(_this.matrix[id][benchId].price.time);
            _this.reactModel[id].ask.setPrice(data.getAskInt(), data.getAskFrac());
            _this.reactModel[id].ask.setTime(_this.matrix[id][benchId].price.time);
            _this.reactModel[id].benchmark.setBenchmarkId(data.getBenchmarkId());
        };
        this.updateQuote = function (data) {
            Utilities_1.Utilities.logger.debug(data.toString());
            var id = data.getId();
            var benchId = data.getBenchmarkId();
            _this.matrix[id][benchId].bid.setPrice(data.getBidInt(), data.getBidFrac());
            _this.matrix[id][benchId].bid.setSize(data.getBidSizeInt(), data.getBidSizeFrac());
            _this.matrix[id][benchId].bid.setTime(_this.getTime(data.getDeltaTime()));
            _this.matrix[id][benchId].ask.setPrice(data.getAskInt(), data.getAskFrac());
            _this.matrix[id][benchId].ask.setSize(data.getAskSizeInt(), data.getAskSizeFrac());
            _this.matrix[id][benchId].ask.setTime(_this.matrix[id][benchId].bid.time);
            if (benchId == MdConstant_1.MdConstant.USDT && id != MdConstant_1.MdConstant.IEO) {
                var ID_IEO_RATE = _this.matrix[MdConstant_1.MdConstant.USDT][MdConstant_1.MdConstant.IEO].rateValue * data.getAsk();
                _this.sendRate(ID_IEO_RATE, id, MdConstant_1.MdConstant.IEO, MdConstant_1.MdConstant.ModelManager);
                var IEO_ID_RATE = 1 / ID_IEO_RATE;
                _this.sendRate(IEO_ID_RATE, MdConstant_1.MdConstant.IEO, id, MdConstant_1.MdConstant.ModelManager);
            }
            _this.reactModel[id].bid.setPrice(data.getBidInt(), data.getBidFrac());
            _this.reactModel[id].bid.setSize(data.getBidSizeInt(), data.getBidSizeFrac());
            _this.reactModel[id].ask.setPrice(data.getAskInt(), data.getAskFrac());
            _this.reactModel[id].price.setVenue(data.getVenue());
            _this.reactModel[id].ask.setSize(data.getAskSizeInt(), data.getAskSizeFrac());
            _this.reactModel[id].ask.setTime(_this.matrix[id][benchId].bid.time);
            _this.reactModel[id].benchmark.setBenchmarkId(data.getBenchmarkId());
        };
        /** execution report from buy **/
        this.updateBuy = function (data) {
            Utilities_1.Utilities.logger.debug(data.toString());
            var id = data.getId();
            var benchId = data.getBenchmarkId();
            _this.matrix[id][benchId].price.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.matrix[id][benchId].price.setSize(data.getSizeInt(), data.getSizeFrac());
            _this.matrix[id][benchId].price.setVenue(data.getVenue());
            _this.matrix[id][benchId].price.setTime(_this.getTime(data.getDeltaTime()));
            _this.reactModel[id]._volumeInt = data.getVolumeInt();
            _this.reactModel[id]._volumeFrac = data.getVolumeFrac();
            _this.reactModel[id].price.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.reactModel[id].price.setSize(data.getSizeInt(), data.getSizeFrac());
            _this.reactModel[id].price.setVenue(data.getVenue());
            _this.reactModel[id].price.setTime(_this.matrix[id][benchId].price.time);
            _this.reactModel[id].benchmark.setBenchmarkId(data.getBenchmarkId());
        };
        /** execution report from sell **/
        this.updateSell = function (data) {
            Utilities_1.Utilities.logger.debug(data.toString());
            var id = data.getId();
            var benchId = data.getBenchmarkId();
            _this.matrix[id][benchId].price.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.matrix[id][benchId].price.setSize(data.getSizeInt(), data.getSizeFrac());
            _this.matrix[id][benchId].price.setVenue(data.getVenue());
            _this.matrix[id][benchId].price.setTime(_this.getTime(data.getDeltaTime()));
            _this.reactModel[id]._volumeInt = data.getVolumeInt();
            _this.reactModel[id]._volumeFrac = data.getVolumeFrac();
            _this.reactModel[id].price.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.reactModel[id].price.setSize(data.getSizeInt(), data.getSizeFrac());
            _this.reactModel[id].price.setVenue(data.getVenue());
            _this.reactModel[id].price.setTime(_this.matrix[id][benchId].price.time);
            _this.reactModel[id].benchmark.setBenchmarkId(data.getBenchmarkId());
        };
        /** generated from buy order **/
        this.updateBid = function (data) {
            Utilities_1.Utilities.logger.debug(data.toString());
            var id = data.getId();
            var benchId = data.getBenchmarkId();
            _this.matrix[id][benchId].bid.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.matrix[id][benchId].bid.setSize(data.getSizeInt(), data.getSizeFrac());
            _this.matrix[id][benchId].bid.setVenue(data.getVenue());
            _this.matrix[id][benchId].bid.setTime(_this.getTime(data.getDeltaTime()));
            _this.reactModel[id].bid.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.reactModel[id].bid.setSize(data.getSizeInt(), data.getSizeFrac());
            _this.reactModel[id].bid.setVenue(data.getVenue());
            _this.reactModel[id].bid.setTime(_this.matrix[id][benchId].bid.time);
            _this.reactModel[id].benchmark.setBenchmarkId(data.getBenchmarkId());
        };
        this.updateAsk = function (data) {
            Utilities_1.Utilities.logger.debug(data.toString());
            var id = data.getId();
            var benchId = data.getBenchmarkId();
            _this.matrix[id][benchId].ask.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.matrix[id][benchId].ask.setSize(data.getSizeInt(), data.getSizeFrac());
            _this.matrix[id][benchId].ask.setVenue(data.getVenue());
            _this.matrix[id][benchId].ask.setTime(_this.getTime(data.getDeltaTime()));
            if (benchId == MdConstant_1.MdConstant.USDT && id != MdConstant_1.MdConstant.IEO) {
                var ID_IEO_RATE = _this.matrix[MdConstant_1.MdConstant.USDT][MdConstant_1.MdConstant.IEO].rateValue * data.getPrice();
                _this.sendRate(ID_IEO_RATE, id, MdConstant_1.MdConstant.IEO, MdConstant_1.MdConstant.ModelManager);
                var IEO_ID_RATE = 1 / ID_IEO_RATE;
                _this.sendRate(IEO_ID_RATE, MdConstant_1.MdConstant.IEO, id, MdConstant_1.MdConstant.ModelManager);
            }
            _this.reactModel[id].ask.setPrice(data.getPriceInt(), data.getPriceFrac());
            _this.reactModel[id].ask.setSize(data.getSizeInt(), data.getSizeFrac());
            _this.reactModel[id].ask.setVenue(data.getVenue());
            _this.reactModel[id].ask.setBenchmarkId(data.getBenchmarkId());
            _this.reactModel[id].ask.setTime(_this.matrix[id][benchId].ask.time);
            _this.reactModel[id].benchmark.setBenchmarkId(data.getBenchmarkId());
        };
        this.updateRate = function (rateMsg) {
            Utilities_1.Utilities.logger.debug(rateMsg.toString());
            _this.matrix[rateMsg.getFrontSymbolId()][rateMsg.getBackBackSymbolId()].updateRate(rateMsg);
        };
        this.wrapMatrix = function (fromModel, toModel) {
            //renamed updateRate message function
            toModel.updateRate(fromModel.rateMsg);
            //model wrap functions
            fromModel.price.wrap(toModel.price);
            fromModel.bid.wrap(toModel.bid);
            fromModel.ask.wrap(toModel.ask);
        };
        this.wrap = function (fromModel, toModel) {
            //TODO: benchModel
            fromModel.price.wrap(toModel.price);
            fromModel.bid.wrap(toModel.bid);
            fromModel.ask.wrap(toModel.ask);
            fromModel.benchmark.wrap(toModel.benchmark);
        };
    }
    return ModelManager;
}());
exports.ModelManager = ModelManager;
