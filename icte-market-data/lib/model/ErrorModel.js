"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorModel = /** @class */ (function () {
    function ErrorModel(stack) {
        this._stack = stack || [];
    }
    ErrorModel.prototype.getMsg = function (length) {
        if (length > this.count) {
            var ct = length - this.count;
            for (var i = 0; i < ct; i++) {
                this.pop();
            }
        }
        return this._stack;
    };
    ErrorModel.prototype.addError = function (item) {
        this._stack.push(item);
    };
    ErrorModel.prototype.pop = function () {
        return this._stack.pop();
    };
    ErrorModel.prototype.clear = function () {
        this._stack = [];
    };
    Object.defineProperty(ErrorModel.prototype, "count", {
        get: function () {
            return this._stack.length;
        },
        enumerable: true,
        configurable: true
    });
    return ErrorModel;
}());
exports.ErrorModel = ErrorModel;
