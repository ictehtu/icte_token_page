export declare class MatrixPrice {
    private readonly _buyId;
    private readonly _sellId;
    private _priceInt;
    private _priceFrac;
    private _price;
    _sizeInt: number;
    _sizeFrac: number;
    private _size;
    _venue: number;
    private _time;
    constructor(buyId: number, sellId: number);
    wrap: (toModel: MatrixPrice) => void;
    readonly price: number;
    readonly size: number;
    setVenue(venue: number): void;
    readonly time: number;
    setTime(time: number): void;
    setPrice(priceInt: any, priceFrac: any): void;
    setSize(sizeInt: any, sizeFrac: any): void;
}
