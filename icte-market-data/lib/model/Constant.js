"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Constants of message types and message subtypes
 */
var Constant = /** @class */ (function () {
    function Constant() {
    }
    Constant.getMsgTypesStr = function () {
        var a = [];
        a[0] = "Msg_Sys";
        a[151] = "Msg_Quote";
        a[152] = "Msg_Position";
        return a;
    };
    Constant.getPositionSubStr = function () {
        var a = [];
        a[0] = "Sub_PositionUpdate";
        a[1] = "Sub_PositionLock";
        return a;
    };
    Constant.getQuoteSubStr = function () {
        var a = [];
        a[0] = "Sub_Quote";
        a[1] = "Sub_Bid";
        a[2] = "Sub_Ask";
        a[3] = "Sub_Buy";
        a[4] = "Sub_Sell";
        a[5] = "Sub_Price";
        a[6] = "Sub_Rate";
        a[7] = "Sub_Level1";
        return a;
    };
    //standard message fields
    Constant.Fld_Type = 0; // 1 byte
    Constant.Fld_SubType = 1; // 1 byte
    //custom message types and subtypes
    Constant.Msg_Sys = 0;
    Constant.Sub_SysTime = 0;
    Constant.Sub_TestRequest = 12;
    Constant.Sub_TestRespond = 13;
    //custom messages types and subtypes starts ## 151 - 201
    Constant.Msg_Quote = 151;
    Constant.Sub_Quote = 0;
    Constant.Sub_Bid = 1;
    Constant.Sub_Ask = 2;
    Constant.Sub_Buy = 3;
    Constant.Sub_Sell = 4;
    Constant.Sub_Price = 5;
    Constant.Sub_Rate = 6;
    Constant.Sub_Level1 = 7;
    Constant.Msg_QuoteSubStr = Constant.getQuoteSubStr();
    Constant.Msg_Position = 152;
    Constant.Sub_PositionUpdate = 0;
    Constant.Sub_PositionLock = 1;
    Constant.Msg_PositionSubStr = Constant.getPositionSubStr();
    Constant.MsgTypesStr = Constant.getMsgTypesStr();
    //model  names
    Constant.Mdl_SymbolModel = 0;
    return Constant;
}());
exports.Constant = Constant;
