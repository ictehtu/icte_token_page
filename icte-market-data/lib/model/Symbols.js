"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utilities_1 = require("./Utilities");
var MdConstant_1 = require("../transports/api/MdConstant");
var IeoTransport2_1 = require("../transports/v1/IeoTransport2");
var TickManager_1 = require("../transports/api/TickManager");
var Symbols = /** @class */ (function () {
    function Symbols(origModel, matrixModel) {
        var _this = this;
        this._tableState = false;
        this._utilityId = MdConstant_1.MdConstant.IEO;
        this._benchmarkId = MdConstant_1.MdConstant.USDT;
        this._currencyBenchmarkId = MdConstant_1.MdConstant.USD;
        this.getNanoSecTime = function () {
            var hrTime = process.hrtime();
            return hrTime[0] * 1000000000 + hrTime[1];
        };
        this.changeTableState = function () {
            _this._tableState = !_this._tableState;
        };
        this.changeBenchmark = function (index) {
            //this._benchmarkId = this._timerModel[index].id;
            _this._benchmarkId = _this._origModel[index].id;
            //TODO change prices of the benchmark calculation
            _this.update();
        };
        this.changeCurrencyBenchmark = function (index) {
            _this._currencyBenchmarkId = _this._origModel[index].id;
            _this.update();
        };
        this.isSymbolEnabled = function (index) {
            return _this._timerModel[index].wallet.getEnable();
        };
        this.enableSymbol = function (index) {
            _this._timerModel[index].wallet.setEnable(true);
        };
        this.disableSymbol = function (index) {
            _this._timerModel[index].wallet.setEnable(false);
        };
        this.changeSymbolQuantity = function (index, value) {
            _this._timerModel[index].wallet.setSizeInt(Math.trunc(value));
            _this._timerModel[index].wallet.setSizeFrac(Math.trunc(value % 1 * 10000000));
        };
        //sort order
        this.update = function () {
            _this._timerModel.sort(function (l, r) {
                if (l.id == 0) {
                    return 1;
                }
                else if (r.id == 0) {
                    return -1;
                }
                else if (l.wallet.getEnable() == false && r.wallet.getEnable() == true) {
                    return 1;
                }
                else if (l.wallet.getEnable() == true && r.wallet.getEnable() == false) {
                    return -1;
                }
                else {
                    // return 0;
                    if (l.wallet.getSizeInt() < r.wallet.getSizeInt()) {
                        return 1;
                    }
                    else if (l.wallet.getSizeInt() > r.wallet.getSizeInt()) {
                        return -1;
                    }
                    else {
                        if (l.wallet.getSizeFrac() < r.wallet.getSizeFrac()) {
                            return 1;
                        }
                        else if (l.wallet.getSizeFrac() > r.wallet.getSizeFrac()) {
                            return -1;
                        }
                        else {
                            return 0;
                        }
                    }
                }
            });
            return _this;
        };
        this.sortByIds = function () {
            _this._timerModel.sort(function (l, r) {
                if (l.wallet.getId() > r.wallet.getId())
                    return 1;
                else
                    return -1;
            });
            return _this;
        };
        this._origModel = origModel;
        // @ts-ignore
        this._timerModel = this._origModel.slice(0);
        this._timerMatrix = matrixModel;
    }
    Object.defineProperty(Symbols.prototype, "arr", {
        get: function () {
            return this._timerModel;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Symbols.prototype, "matrix", {
        get: function () {
            return this._timerMatrix;
        },
        enumerable: true,
        configurable: true
    });
    Symbols.prototype.isBenchmarkState = function () {
        return this._tableState;
    };
    Symbols.prototype.getUtilityName = function () {
        return this._origModel[this._utilityId].name;
    };
    Symbols.prototype.getBenchmarkName = function () {
        return this._origModel[this._benchmarkId].name;
    };
    Symbols.prototype.getCurrencyBenchmarkName = function () {
        return this._origModel[this._currencyBenchmarkId].name;
    };
    Symbols.prototype.getUtilityId = function () {
        return this._utilityId;
    };
    Symbols.prototype.getBenchmarkId = function () {
        return this._benchmarkId;
    };
    Symbols.prototype.getSymbolArray = function () {
        return this.arr;
    };
    Symbols.prototype.getMatrixArray = function () {
        return this._timerMatrix;
    };
    Symbols.prototype.getMsgBenchmarkId = function (rowId) {
        return this.arr[rowId].benchmark.benchmarkId;
    };
    Symbols.prototype.getMsgBenchmarkName = function (rowId) {
        return this._origModel[this.arr[rowId].benchmark.benchmarkId].name;
    };
    Symbols.prototype.getSymbolId = function (rowId) {
        return this.arr[rowId].id;
    };
    Symbols.prototype.getOrigSymbolId = function (rowId) {
        return this._origModel[rowId].id;
    };
    Symbols.prototype.getSymbolName = function (index) {
        return this.arr[index].name;
    };
    Symbols.prototype.getOrigSymbolName = function (index) {
        return this._origModel[index].name;
    };
    Symbols.prototype.changeSymbolStatus = function (index) {
        var status = this.isSymbolEnabled(index);
        if (status) {
            return this.disableSymbol(index);
        }
        else {
            return this.enableSymbol(index);
        }
    };
    Symbols.prototype.changeUtilitySymbol = function (index) {
        this._utilityId = index;
    };
    Symbols.prototype.changeSymbolQty = function (index, qty) {
        return this.changeSymbolQuantity(index, qty);
    };
    Symbols.prototype.getMatrixRate = function (id) {
        return this._timerMatrix[id][this._benchmarkId].rateValue;
    };
    Symbols.prototype.getMatrixPrice = function (id) {
        if (id == this._benchmarkId)
            return 1;
        var ret = this._timerMatrix[id][this._benchmarkId].price.price;
        if (!ret && this.getMatrixIEOPrice(id) && this.getMatrixIEOPrice(this._benchmarkId)) {
            ret = this.getMatrixIEOPrice(id) / this.getMatrixIEOPrice(this._benchmarkId);
        }
        return ret;
    };
    Symbols.prototype.getMatrixAskPrice = function (id) {
        return this._timerMatrix[id][this._benchmarkId].ask.price;
    };
    Symbols.prototype.getMatrixBidPrice = function (id) {
        return this._timerMatrix[id][this._benchmarkId].bid.price;
    };
    Symbols.prototype.getMatrixAskSize = function (id) {
        return this._timerMatrix[id][this._benchmarkId].ask.size;
    };
    Symbols.prototype.getMatrixBidSize = function (id) {
        return this._timerMatrix[id][this._benchmarkId].bid.size;
    };
    Symbols.prototype.getUtilityPrice = function () {
        return this._timerMatrix[this._utilityId][MdConstant_1.MdConstant.USDT].price.price;
    };
    Symbols.prototype.getPrice_UsdtAsBench = function (id) {
        if (id == MdConstant_1.MdConstant.USDT)
            return 1;
        return this._timerMatrix[id][MdConstant_1.MdConstant.USDT].price.price;
    };
    Symbols.prototype.getMatrixIEOPrice = function (id) {
        // TODO: Improve calc
        var symbolPriceInUSDT = this.getPrice_UsdtAsBench(id);
        return symbolPriceInUSDT / this.getUtilityPrice();
    };
    Symbols.prototype.getCurrencyBenchRate = function (id) {
        var symbolPriceInUSDT = this.getPrice_UsdtAsBench(id);
        return this._timerMatrix[this._currencyBenchmarkId][MdConstant_1.MdConstant.USD].rateValue * symbolPriceInUSDT;
    };
    Symbols.prototype.getMatrixSize = function (id) {
        return this._timerMatrix[id][this._benchmarkId].price.size;
    };
    Symbols.prototype.getPrice = function (id) {
        //TODO: Fix price
        return this.arr[id].price.priceInt + '.' + Utilities_1.Utilities.padLeft(this.arr[id].price.priceFrac.toString(), '0', Utilities_1.Utilities.PADSIZE);
    };
    Symbols.prototype.getAtomicSymbolData = function (id) {
        var atomicObj = {
            name: this.arr[id].name,
            price: this.arr[id].price.priceInt + '.' + Utilities_1.Utilities.padLeft(this.arr[id].price.priceFrac.toString(), '0', Utilities_1.Utilities.PADSIZE),
            bench: this._origModel[this.arr[id].benchmark.benchmarkId].name,
            venue: this.arr[id].venueName,
            timestamp: this.getNanoSecTime()
        };
        return atomicObj;
    };
    Symbols.prototype.getSize = function (id) {
        return this.arr[id].price.sizeInt + '.' + Utilities_1.Utilities.padLeft(this.arr[id].price.sizeFrac.toString(), '0', Utilities_1.Utilities.PADSIZE);
    };
    Symbols.prototype.getBidPrice = function (rowId) {
        return this.arr[rowId].bid.priceInt + '.' + Utilities_1.Utilities.padLeft(this.arr[rowId].bid.priceFrac.toString(), '0', Utilities_1.Utilities.PADSIZE);
    };
    Symbols.prototype.getAskPrice = function (rowId) {
        return this.arr[rowId].ask.priceInt + '.' + Utilities_1.Utilities.padLeft(this.arr[rowId].ask.priceFrac.toString(), '0', Utilities_1.Utilities.PADSIZE);
    };
    Symbols.prototype.getBidSize = function (rowId) {
        return this.arr[rowId].bid.sizeInt + '.' + Utilities_1.Utilities.padLeft(this.arr[rowId].bid.sizeFrac.toString(), '0', Utilities_1.Utilities.PADSIZE);
    };
    Symbols.prototype.getAskSize = function (rowId) {
        return this.arr[rowId].ask.sizeInt + '.' + Utilities_1.Utilities.padLeft(this.arr[rowId].ask.sizeFrac.toString(), '0', Utilities_1.Utilities.PADSIZE);
    };
    Symbols.prototype.getUtilityBalance = function () {
        // return this.getBenchmarkBalance / HTUBTC;
        return '1,283.067430000000';
    };
    Symbols.prototype.getBenchmarkBalance = function () {
        // Grab inputs from calculator quantities
        // go through list of every enabled transport
        // addition
        return '1,283.067430000000';
    };
    // book
    // public initBook(symbolId: number) {
    //    this.setAskObservable(symbolId);
    //    this.setBidObservable(symbolId);
    // }
    // private setAskObservable(symbolId: number) {
    //    this._askObservable = Observable.create((observer) => {
    //       let objAsk = {
    //          askSize: this.getMatrixAskSize(symbolId),
    //          askValue: this.getMatrixAskPrice(symbolId)
    //       }
    //       const intervalAsk = setInterval(() => {
    //          observer.next(objAsk = {
    //             askSize: this.getMatrixAskSize(symbolId),
    //             askValue: this.getMatrixAskPrice(symbolId)
    //          });
    //       }, 66);
    //       return () => clearInterval(intervalAsk);
    //    });
    // }
    // private setBidObservable(symbolId) {
    //    this._bidObservable = Observable.create((observer) => {
    //       let objBid = {
    //          bidSize: this.getMatrixBidSize(symbolId),
    //          bidValue: this.getMatrixBidPrice(symbolId)
    //       };
    //       const intervalbid = setInterval(() => {
    //          observer.next(objBid = {
    //             bidSize: this.getMatrixBidSize(symbolId),
    //             bidValue: this.getMatrixBidPrice(symbolId)
    //          });
    //       }, 66);
    //       return () => clearInterval(intervalbid);
    //    });
    // }
    Symbols.prototype.getAskObservable = function () {
        return this._askObservable;
    };
    Symbols.prototype.getBidObservable = function () {
        return this._bidObservable;
    };
    Symbols.prototype.getIssuersArray = function () {
        return MdConstant_1.MdConstant.ISSUERS;
    };
    Symbols.prototype.getCryptoStart = function () {
        return MdConstant_1.MdConstant.CRYPTOSTART;
    };
    Symbols.prototype.getCryptoEnd = function () {
        return MdConstant_1.MdConstant.CRYPTOEND;
    };
    Symbols.prototype.getCurrencyStart = function () {
        return MdConstant_1.MdConstant.CURRENCYSTART;
    };
    Symbols.prototype.getCurrencyEnd = function () {
        return MdConstant_1.MdConstant.CURRENCYEND;
    };
    Symbols.prototype.getPriceVenueName = function (id) {
        return this.arr[id].venueName;
    };
    Symbols.prototype.isIeoWebSocketConnected = function () {
        return IeoTransport2_1.IeoTransport2.wbCon();
    };
    Symbols.prototype.getAllTicks = function () {
        return TickManager_1.TickManager.getTicks();
    };
    Symbols.prototype.getTicksByProviderIdAndTicker = function (providerId, tickerpair) {
        return TickManager_1.TickManager.getTicksByProviderIdAndTicker(providerId, tickerpair);
    };
    Symbols.prototype.getTicksByProviderId = function (providerId) {
        return TickManager_1.TickManager.getTicksByProviderId(providerId);
    };
    return Symbols;
}());
exports.Symbols = Symbols;
