"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WalletModel = /** @class */ (function () {
    function WalletModel(id) {
        var _this = this;
        this._id = 0;
        this._sizeInt = 0;
        this._sizeFrac = 0;
        this._enable = false;
        this.wrap = function (toModel) {
            toModel._sizeInt = _this._sizeInt;
            toModel._sizeFrac = _this._sizeFrac;
            toModel._enable = _this._enable;
        };
        this.getId = function () {
            return _this._id;
        };
        this.getSizeInt = function () {
            return _this._sizeInt;
        };
        this.setSizeInt = function (value) {
            _this._sizeInt = value;
        };
        this.getSizeFrac = function () {
            return _this._sizeFrac;
        };
        this.setSizeFrac = function (value) {
            _this._sizeFrac = value;
        };
        this.getEnable = function () {
            return _this._enable;
        };
        this.setEnable = function (value) {
            _this._enable = value;
        };
        this._id = id;
    }
    return WalletModel;
}());
exports.WalletModel = WalletModel;
