import { Parser } from "../parser/Parser";
import { RealtimeModel } from "./RealtimeModel";
import { Symbols } from "./Symbols";
import { MatrixModel } from "./MatrixModel";
export declare class ModelManager {
    private time;
    private matrix;
    private timerMatrix;
    private book;
    private benchModel;
    private reactModel;
    private timerModel;
    private symbols;
    constructor();
    connect: () => void;
    getModelTime: () => number;
    private setModelTime;
    getGmtZeroMidnight: () => number;
    findSymbol: (symbol: string) => number;
    getUpdate: () => Symbols;
    setUpdate: (data: Parser) => void;
    private getTime;
    private updateTime;
    private updateLockCalc;
    private updateCalc;
    private _rateMsg;
    sendRate: (rate: any, frontSymbol: number, backSymbol: number, venue: number) => void;
    private updateLevel1;
    private updateQuote;
    /** execution report from buy **/
    private updateBuy;
    /** execution report from sell **/
    private updateSell;
    /** generated from buy order **/
    private updateBid;
    private updateAsk;
    private updateRate;
    wrapMatrix: (fromModel: MatrixModel, toModel: MatrixModel) => void;
    wrap: (fromModel: RealtimeModel, toModel: RealtimeModel) => void;
}
