"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PriceModel_1 = require("./PriceModel");
var RealtimeModel = /** @class */ (function () {
    function RealtimeModel(id) {
        this._volumeInt = 0;
        this._volumeFrac = 0;
        this.wrap = function (toModel) {
        };
        this._id = id;
        this._benchmark = new PriceModel_1.PriceModel(id);
        this._price = new PriceModel_1.PriceModel(id);
        this._bid = new PriceModel_1.PriceModel(id);
        this._ask = new PriceModel_1.PriceModel(id);
    }
    Object.defineProperty(RealtimeModel.prototype, "volumeInt", {
        get: function () {
            return this._volumeInt;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RealtimeModel.prototype, "volumeFrac", {
        get: function () {
            return this._volumeFrac;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RealtimeModel.prototype, "benchmark", {
        get: function () {
            return this._benchmark;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RealtimeModel.prototype, "price", {
        get: function () {
            return this._price;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RealtimeModel.prototype, "bid", {
        get: function () {
            return this._bid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RealtimeModel.prototype, "ask", {
        get: function () {
            return this._ask;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RealtimeModel.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    return RealtimeModel;
}());
exports.RealtimeModel = RealtimeModel;
