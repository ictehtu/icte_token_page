export declare class PriceModel {
    private _id;
    private _priceInt;
    private _priceFrac;
    private _price;
    private _sizeInt;
    private _sizeFrac;
    private _venue;
    private _benchmarkId;
    private _time;
    constructor(id: number);
    wrap: (toModel: PriceModel) => void;
    readonly benchmarkId: number;
    readonly priceInt: number;
    readonly priceFrac: number;
    readonly sizeInt: number;
    readonly sizeFrac: number;
    setPrice(priceInt: any, priceFrac: any): void;
    setSize(sizeInt: any, sizeFrac: any): void;
    setBenchmarkId(value: number): void;
    readonly time: number;
    setTime(time: any): void;
    readonly venue: number;
    setVenue(venue: number): void;
}
