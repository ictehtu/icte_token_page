"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MatrixPrice_1 = require("./MatrixPrice");
var RateMsg_1 = require("../parser/RateMsg");
var MatrixModel = /** @class */ (function () {
    function MatrixModel(buyId, sellId) {
        var _this = this;
        this._volumeInt = 0;
        this._volumeFrac = 0;
        this.updateRate = function (rateMsg) {
            _this._rateMsg.setInt(RateMsg_1.RateMsg.Fld_DeltaTime, rateMsg.getDeltaTime());
            _this._rateMsg.setInt(RateMsg_1.RateMsg.Fld_FrontSymbolId, rateMsg.getFrontSymbolId());
            _this._rateMsg.setInt(RateMsg_1.RateMsg.Fld_BackSymbolId, rateMsg.getBackBackSymbolId());
            _this._rateMsg.setInt(RateMsg_1.RateMsg.Fld_RateInt, rateMsg.getRateInt());
            _this._rateMsg.setInt(RateMsg_1.RateMsg.Fld_RateFrac, rateMsg.getRateFrac());
            _this._rateMsg.setShort(RateMsg_1.RateMsg.Fld_Venue, rateMsg.getVenue());
        };
        this._buyId = buyId;
        this._sellId = sellId;
        this._rateMsg = new RateMsg_1.RateMsg();
        this._price = new MatrixPrice_1.MatrixPrice(buyId, sellId);
        this._bid = new MatrixPrice_1.MatrixPrice(buyId, sellId);
        this._ask = new MatrixPrice_1.MatrixPrice(buyId, sellId);
    }
    Object.defineProperty(MatrixModel.prototype, "volumeInt", {
        get: function () {
            return this._volumeInt;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixModel.prototype, "rateValue", {
        get: function () {
            return this._rateMsg.getRateValue();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixModel.prototype, "volumeFrac", {
        get: function () {
            return this._volumeFrac;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixModel.prototype, "price", {
        get: function () {
            return this._price;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixModel.prototype, "bid", {
        get: function () {
            return this._bid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixModel.prototype, "ask", {
        get: function () {
            return this._ask;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixModel.prototype, "buyId", {
        get: function () {
            return this._buyId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixModel.prototype, "sellId", {
        get: function () {
            return this._sellId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixModel.prototype, "rateMsg", {
        get: function () {
            return this._rateMsg;
        },
        enumerable: true,
        configurable: true
    });
    return MatrixModel;
}());
exports.MatrixModel = MatrixModel;
