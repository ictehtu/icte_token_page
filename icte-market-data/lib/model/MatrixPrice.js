"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utilities_1 = require("./Utilities");
var MatrixPrice = /** @class */ (function () {
    function MatrixPrice(buyId, sellId) {
        var _this = this;
        this._priceInt = 0;
        this._priceFrac = 0;
        this._price = 0;
        this._sizeInt = 0;
        this._sizeFrac = 0;
        this._size = 0;
        this._venue = 0;
        this._time = 0;
        this.wrap = function (toModel) {
            toModel.setPrice(_this._priceInt, _this._priceFrac);
            toModel.setSize(_this._sizeInt, _this._sizeFrac);
            toModel._venue = _this._venue;
            toModel._time = _this._time;
        };
        this._buyId = buyId;
        this._sellId = sellId;
    }
    Object.defineProperty(MatrixPrice.prototype, "price", {
        get: function () {
            return this._price;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MatrixPrice.prototype, "size", {
        get: function () {
            return this._size;
        },
        enumerable: true,
        configurable: true
    });
    MatrixPrice.prototype.setVenue = function (venue) {
        this._venue = venue;
    };
    Object.defineProperty(MatrixPrice.prototype, "time", {
        get: function () {
            return this._time;
        },
        enumerable: true,
        configurable: true
    });
    MatrixPrice.prototype.setTime = function (time) {
        this._time = time;
    };
    MatrixPrice.prototype.setPrice = function (priceInt, priceFrac) {
        this._priceInt = priceInt;
        this._priceFrac = priceFrac;
        var priceStr = this._priceInt + "." + Utilities_1.Utilities.padLeft(this._priceFrac.toString(), '0', Utilities_1.Utilities.PADSIZE);
        this._price = parseFloat(priceStr);
    };
    MatrixPrice.prototype.setSize = function (sizeInt, sizeFrac) {
        this._sizeInt = sizeInt;
        this._sizeFrac = sizeFrac;
        var sizeStr = this._sizeInt + "." + Utilities_1.Utilities.padLeft(this._sizeFrac.toString(), '0', Utilities_1.Utilities.PADSIZE);
        this._size = parseFloat(sizeStr);
    };
    return MatrixPrice;
}());
exports.MatrixPrice = MatrixPrice;
