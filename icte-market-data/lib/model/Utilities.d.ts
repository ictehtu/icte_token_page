export declare class Utilities {
    static PADSIZE: number;
    static readonly DIV: number;
    static readonly _RATE_LIMIT: number;
    static readonly _TIME_LIMIT: number;
    static padLeft(text: string, padChar: string, size: number): string;
    wsLimiter(func: any): () => any;
    static logger: {
        debug: (...arg: any[]) => void;
        info: (...arg: any[]) => void;
        warn: (...arg: any[]) => void;
    };
}
