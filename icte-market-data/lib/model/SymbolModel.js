"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var WalletModel_1 = require("./WalletModel");
var RealtimeModel_1 = require("./RealtimeModel");
var MdConstant_1 = require("../transports/api/MdConstant");
var SymbolModel = /** @class */ (function (_super) {
    __extends(SymbolModel, _super);
    function SymbolModel(id) {
        var _this = _super.call(this, id) || this;
        _this.wrap = function (toModel) {
            toModel._volumeInt = _this._volumeInt;
            toModel._volumeFrac = _this._volumeFrac;
            _this.price.wrap(toModel.price);
            _this.bid.wrap(toModel.bid);
            _this.ask.wrap(toModel.ask);
            _this.benchmark.wrap(toModel.benchmark);
            _this.wallet.wrap(toModel.wallet);
        };
        _this._wallet = new WalletModel_1.WalletModel(id);
        return _this;
    }
    Object.defineProperty(SymbolModel.prototype, "name", {
        get: function () {
            return MdConstant_1.MdConstant.COINS[this.id];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SymbolModel.prototype, "venueName", {
        get: function () {
            return MdConstant_1.MdConstant.VENUES[this.price.venue];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SymbolModel.prototype, "wallet", {
        get: function () {
            return this._wallet;
        },
        enumerable: true,
        configurable: true
    });
    return SymbolModel;
}(RealtimeModel_1.RealtimeModel));
exports.SymbolModel = SymbolModel;
