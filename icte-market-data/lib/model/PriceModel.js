"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PriceModel = /** @class */ (function () {
    function PriceModel(id) {
        var _this = this;
        this._id = 0;
        this._priceInt = 0;
        this._priceFrac = 0;
        this._sizeInt = 0;
        this._sizeFrac = 0;
        this._venue = 0;
        this._benchmarkId = 1;
        this._time = 0;
        this.wrap = function (toModel) {
            toModel._priceInt = _this._priceInt;
            toModel._priceFrac = _this._priceFrac;
            toModel._sizeInt = _this._sizeInt;
            toModel._sizeFrac = _this._sizeFrac;
            toModel._venue = _this._venue;
            toModel._benchmarkId = _this._benchmarkId;
            toModel._time = _this._time;
        };
        this._id = id;
    }
    Object.defineProperty(PriceModel.prototype, "benchmarkId", {
        get: function () {
            return this._benchmarkId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PriceModel.prototype, "priceInt", {
        get: function () {
            return this._priceInt;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PriceModel.prototype, "priceFrac", {
        get: function () {
            return this._priceFrac;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PriceModel.prototype, "sizeInt", {
        get: function () {
            return this._sizeInt;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PriceModel.prototype, "sizeFrac", {
        get: function () {
            return this._sizeFrac;
        },
        enumerable: true,
        configurable: true
    });
    PriceModel.prototype.setPrice = function (priceInt, priceFrac) {
        this._priceInt = priceInt;
        this._priceFrac = priceFrac;
    };
    PriceModel.prototype.setSize = function (sizeInt, sizeFrac) {
        this._sizeInt = sizeInt;
        this._sizeFrac = sizeFrac;
    };
    PriceModel.prototype.setBenchmarkId = function (value) {
        this._benchmarkId = value;
    };
    Object.defineProperty(PriceModel.prototype, "time", {
        get: function () {
            return this._time;
        },
        enumerable: true,
        configurable: true
    });
    PriceModel.prototype.setTime = function (time) {
        this._time = time;
    };
    Object.defineProperty(PriceModel.prototype, "venue", {
        get: function () {
            return this._venue;
        },
        enumerable: true,
        configurable: true
    });
    PriceModel.prototype.setVenue = function (venue) {
        this._venue = venue;
    };
    return PriceModel;
}());
exports.PriceModel = PriceModel;
