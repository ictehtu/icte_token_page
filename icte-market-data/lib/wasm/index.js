const loader = require("@assemblyscript/loader")
const myImports = { /* ... */ }
/*`loader.instantiate` will use `WebAssembly.instantiateStreaming`
   if possible. Overwise fallback to `WebAssembly.instantiate`. */

let load = async () => {
  return await loader.instantiate(
    fetch("assets/optimized.wasm"),
    myImports
  )
}
module.exports  = {
  load
};