FROM ubuntu:18.04
RUN apt update && \
    apt -y install curl git dirmngr apt-transport-https lsb-release ca-certificates -y 
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && apt -y install nodejs
RUN npm install -g @angular/cli
COPY ./.ssh /root/.ssh
RUN chmod 600 /root/.ssh/* && git config --global user.email "api@htu.io" && git config --global user.name "API user"
ARG CACHE_DATE=2020
RUN git clone git@bitbucket.org:ictehtu/icte-mobile-wallet.git 
WORKDIR icte-mobile-wallet
RUN git checkout translations 
RUN npm install 
RUN bash -x ./update_wallet.sh
