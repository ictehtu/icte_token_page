import { CompilerInput } from './shared/types';
import { WorkerMessage } from './shared/worker-message.model';
import * as solc from 'solc/wrapper';

var compileJSON: ((input: CompilerInput) => string) | null = (input) => { return '' }
var versionLoaded: boolean = false;
const missingInputs: string[] = [];

export class AppWorkers {
    workerCtx: any;
    created: Date;
    constructor(workerCtx: any) {
        this.workerCtx = workerCtx;
        this.created = new Date();
    }
    workerBroker($event: MessageEvent): void {
        const { topic, data } = $event.data as WorkerMessage;
        switch (topic) {
            case 'loadVersion': {
                if (!versionLoaded) {

                    delete this.workerCtx.Module
                    // NOTE: workaround some browsers?
                    this.workerCtx.Module = undefined
                    compileJSON = null
                    //importScripts() method of synchronously imports one or more scripts into the worker's scope
                    this.workerCtx.importScripts(data.url)
                    const compiler: solc = solc(this.workerCtx.Module)
                    compileJSON = (input) => {
                        try {
                            const missingInputsCallback = (path) => {
                                missingInputs.push(path)
                                return { 'error': 'Deferred import' }
                            }
                            return compiler.compile(input, { import: missingInputsCallback })
                        } catch (exception) {
                            return JSON.stringify({ error: 'Uncaught JavaScript exception:\n' + exception })
                        }
                    }
                    versionLoaded = true;
                }
                this.returnWorkResults({
                    topic,
                    data: { versionLoaded }
                })
                break
            }

            case 'compile': {
                missingInputs.length = 0
                if (data.input && compileJSON) {
                    this.returnWorkResults({
                        topic,
                        data: {
                            job: data.job,
                            data: compileJSON(data.input),
                            missingInputs: missingInputs
                        }
                    })
                }
                break;
            }
        }
    }
    private returnWorkResults(message: WorkerMessage): void {
        this.workerCtx.postMessage(message);
    }
}