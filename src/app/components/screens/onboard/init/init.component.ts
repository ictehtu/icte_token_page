import { Component, OnInit } from '@angular/core';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { sleep } from 'src/app/lib/sleep';
import { ICTEMarketDataService } from 'src/app/services/ictemarket-data.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { BlockchainApiService } from 'src/app/services/blockchain-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as geolocator from 'geolocator';
import { WsConnection } from 'src/app/lib/websocket';
declare var $: any;

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.scss']
})
export class InitComponent implements OnInit {

  tasksQueue = [];
  currentTask: string = 'Setting up tasks';
  currentTaskId: number = 0;
  shouldBreak: boolean;
  error: string;
  backroute: string = '/onboard';

  constructor(
    private _ors: IcteOrderRouteService,
    private _md: ICTEMarketDataService,
    private _ids: InternalDataService,
    private _bc: BlockchainApiService,
    private _router: Router,
    private _route: ActivatedRoute

  ) { }

  async ngOnInit() {
    this.popUpStatus()
  }

  async startComponent() {
    await this._ids.initForm();
    this.setTaskQueue();
    await this.startTaskQueue();

    this._ids.setWalletData({
      accounts: [
        {
          address: '0x' + this._ids.state.address
        }
      ]
    });
    //set coins
    let coins = this._ids.state.coins;
    let finalAraray = [];
    coins.forEach(element => {
      if (element != "") {
        finalAraray.push(Number(element.split(',')[0]));
      }
    });
    this._ids.watchlists[0].coins = finalAraray;

    // fees
    this._ids.globalFees = this._ids.state.fees;

    // SC addresses
    this._ids.tokenManagerAddress = this._ids.getSmartContractAddress('MANAGER');
    this._ids.tokenCompilerAddress = this._ids.getSmartContractAddress('COMPILER');
    this._ids.tokenLibraryAddress = this._ids.getSmartContractAddress('LIBRARY');
  }

  async startTaskQueue() {
    let i = 0;
    for (const task of this.tasksQueue) {
      this.currentTaskId = i;
      this.currentTask = task.label;
      if (!this.checkTask(await task.execFunction()))
        break;
      i++;
    }
    if (this.shouldBreak) {
      this.openModal("errorModal");
    } else if (this._route.snapshot.queryParamMap.get('fromNodeProductButton')) {
      this._router.navigateByUrl("/main/tab-settings/operator/enter-domain");
    } else {
      this._router.navigateByUrl("/main");
    }
  }

  setTaskQueue() {
    this.pushTask("Setting up secure connection", this.setGroupID);
 

    // next tasks bellow here
    this.pushTask("Checking location", this.checkIPLocation);
    this.pushTask("Loading address book", this.loadAddressBooks);
    this.pushTask("Loading market data", this.initMarketData);
    this.pushTask("Setting up websocket connection", this.websocketConnection);

  }

  pushTask(label, execFunction) {
    this.tasksQueue.push({
      label,
      execFunction
    })
  }

  checkTask(task) {
    // if a task finish with error, then display it
    if (!task) {
      this.shouldBreak = true;
      return false;
    }
    return true;
  }

  timeout(promise) {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        reject(() => {
          return false;
        });
      }, 6000)
      promise.then(resolve, reject)
    })
  }

  // tasks

  private loadAddressBooks = async () => {
    try {

      let msgId = this._ors.callMethod.getXamarinData('addressbook');
      let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_GetData, msgId);
      if (!result.orderroute.error) {
        this._ids.addressBook = JSON.parse(result.data.Value)
      }
    } catch (error) {
      // no metter if there is error.
    }
    return true;
  }

  private setGroupID = async () => {
    let msgId = this._ors.callMethod.reqSetGroupId("5c94364c3808cad945d55e6ff4dc1813");
    let resp = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Sys, this._ors.types.Sub_GroupID, msgId);
    if (resp.orderroute.error) {
      return false;
    } else {
      return true;
    }
    // handle error
  }

  private initMarketData = async () => {
    while (true) {
      if (this._md.d && this._md.d.getSymbolArray()) {
        break;
      }
      await sleep(10);
    }
    return true;
  }

  private checkIPLocation = async () => {
    let resp = await this._bc.getBlacklistedCountries();
    let blacklistedCountries = null;
    if (!resp.orderroute.error && resp.data.success) {
      blacklistedCountries = resp.data.result;
    } else {
      return false;
    }
    let ipData: any = await this.utilLocationResolver();
    if (ipData && ipData.address && ipData.address.countryCode) {
      let userCountry = ipData.address.countryCode;
      for (const blackListedCountry of blacklistedCountries) {
        if (blackListedCountry.code.toUpperCase() == userCountry.toUpperCase()) {
          this._ids.isBlackListed = true;
          break;
        }
      }
      return true;
    } else {
      return false;
    }
  }

  private websocketConnection = async () => {
    let msgId = this._ors.callMethod.reqWsPort();
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_SocketPort, msgId);
    if (!result.orderroute.error) {
      WsConnection.setPort(result.data);
      WsConnection.startConnection();
      return true;
    } else {
      return false;
    }
  }

  async popUpStatus() {
    let msgId = this._ors.callMethod.getXamarinData('pop_up_status');
    let result = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_GetData, msgId);
    if (result.orderroute.error == true) {
      this.openModal('agreementModal');
    } else {
      this.startComponent();
    }
  }

  async agreeModal() {
    let msgId = this._ors.callMethod.setXamarinData('pop_up_status', '1');
    let result = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_SaveData, msgId);
    if (result.orderroute.error == false) {
      this.startComponent();
    }
  }

  private utilLocationResolver() {
    return new Promise((resolve, reject) => {
      let options = {
        addressLookup: false,
      };
      geolocator.locateByIP(options, (err, location) => {
        if (err)
          reject(err)
        resolve(location)
      });
    });
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

}
