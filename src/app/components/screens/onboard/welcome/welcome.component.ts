import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';

declare var $: any;
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  public debug;
  public transactions;
  public platform;
  public link;

  constructor(
    private router: Router,
    private deviceService: DeviceDetectorService
  ) { }

  async ngOnInit() {
    this.platform = this.deviceService.getDeviceInfo().os;
  }

  getLink() {
    this.link = this.platform.toLowerCase().includes('android') ? 'https://icte.io/wallet/go.icte.io' : 'icte-wallet://icte.io/wallet/go.icte.io';
  }

  navigateToNodeProduct() {
    this.router.navigateByUrl('/onboard/init?fromNodeProductButton=true')
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  exit() {
    this.router.navigateByUrl('/onboard/exit')
  }
}
