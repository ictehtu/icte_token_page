import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { sleep } from 'icte-order-route/lib/lib/sleep';

@Component({
  selector: 'app-first-screen',
  templateUrl: './first-screen.component.html',
  styleUrls: ['./first-screen.component.scss']
})
export class FirstScreenComponent implements OnInit {

  constructor(private router: Router) { }

  async ngOnInit() {
    await sleep(100)
    this.router.navigateByUrl('/onboard/welcome');
  }

}
