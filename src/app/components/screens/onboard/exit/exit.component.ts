import { Component, OnInit } from '@angular/core';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exit',
  templateUrl: './exit.component.html',
  styleUrls: ['./exit.component.scss']
})
export class ExitComponent implements OnInit {

  constructor(private _ors: IcteOrderRouteService,
    private router: Router) { }

  ngOnInit() {
    this._ors.callMethod.exit();
  }

}
