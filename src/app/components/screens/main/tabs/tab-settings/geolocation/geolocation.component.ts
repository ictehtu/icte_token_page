import { Component, OnInit } from '@angular/core';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { BlockchainApiService } from 'src/app/services/blockchain-api.service';
import { sleep } from 'src/app/lib/sleep';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { NavigationStart, Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-geolocation',
  templateUrl: './geolocation.component.html',
  styleUrls: ['./geolocation.component.scss']
})
export class GeolocationComponent implements OnInit {

  public lat: number;
  public lon: number;
  fetching: boolean = true;
  error: any;
  public cities: any = [];

  public checkStatus = false;
  public readLat: number = 0;
  public readLon: number = 0;
  public interval;
  routeSub: any;
  searchInput: any;
  closestCity: string;

  constructor(public _ids: InternalDataService,
    private _bs: BlockchainApiService,
    private _ors: IcteOrderRouteService,
    private router: Router) { }

  async ngOnInit() {
    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.interval) {
          window.clearInterval(this.interval);
          this.interval = undefined;
        }
      }
    });
    await this.fetchAll();
    if (this.checkStatus) {
      this.startPolling();
    }
  }

  async fetchAll() {
    this.fetching = true;
    this.error = false;
    await sleep(10);
    await this.getLocations();
    await this.getLocationData();
    this.fetching = false;
  }

  async getLocations() {
    let resp = await this._bs.listCities();
    if (!resp.orderroute.error && resp.data.length > 0) {
      this.cities = resp.data.split('\n');
      this.cities.pop();
    } else {
      this.error = 'Unable to get cities';
    }
  }

  async getLocationData() {
    let msgId = await this._ors.callMethod.reqGetLocationData();
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_GetLocationData, msgId);
    if (!resp.orderroute.error) {
      this.lat = resp.data.latitude;
      this.lon = resp.data.longitude;
      this.checkStatus = resp.data.automatic;
      this.calculateClosestLocation();
    } else {
      this.error = 'Unable to get location information from device';
    }
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  public calculateDistance(lat1: number, lon1: number, lat2: number, lon2: number) {

    if (lat1 === lat2 && lon1 == lon2) {
      return { m: 0, k: 0 };
    }

    let deg2rad = (angle: number) => {
      return angle * 0.017453292519943295;
    }
    let rad2deg = (angle: number) => {
      return angle * 57.29577951308232;
    }

    let theta: number = lon1 - lon2;
    let dist: number = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
      + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
    dist = Math.acos(dist);
    dist = rad2deg(dist);
    dist = dist * 60 * 1.1515;
    return { m: dist, k: dist * 1.609344 };

  }

  calculateClosestLocation() {
    let distances = [];
    let distancesUnit = []
    for (const c of this.cities) {
      let dist = this.calculateDistance(Number(this.lat), Number(this.lon), Number(this.parseLatitude(c)), Number(this.parseLongitude(c)));
      distancesUnit.push(dist);
      distances.push(dist.k);
    }
    let min = Math.min(...distances);
    let minIndex = distances.indexOf(min);
    this.closestCity = `${this.parseCity(this.cities[minIndex])} ${distancesUnit[minIndex].k.toFixed(2)} km (${distancesUnit[minIndex].m.toFixed(2)} mi)`;
  }

  async setCoords() {
    this.openModal('loadingModal')
    await sleep(10);
    await this._ors.callMethod.reqLocationCoords(this.lat, this.lon);
    await this.getLocationData();
    // let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_LocationSetup, msgId);
    this.closeModal('loadingModal');
  }

  async setCheckBoxStatus() {
    await this._ors.callMethod.reqLocationCheckBox(this.checkStatus);
  }

  async changeCheckStatus() {
    this.checkStatus = !this.checkStatus;
    this.openModal('loadingModal')
    await sleep(10);
    await this.setCheckBoxStatus();
    await this.getLocationData();
    if (this.checkStatus) {
      this.startPolling()
    } else {
      if (this.interval) {
        window.clearInterval(this.interval);
        this.interval = undefined;
      }
    }
    this.closeModal('loadingModal')
  }

  parseCity(str: string) {
    return str.split('|')[0].replace(/_/g, ' ');
  }

  public async startPolling() {
    if (!this.interval) {
      this.interval = window.setInterval(async () => {
        await this.getLocationData()
      }, this._ids.INTERVAL_MS)
    }
  }

  parseLatitude(str: string) {
    return Number(str.split('|')[1].split(',')[0].replace(',', ''));
  }

  parseLongitude(str: string) {
    return Number(str.split('|')[1].split(',')[1]);
  }

  selectLocation(index) {
    if (!this.checkStatus) {
      this.searchInput = this.parseCity(this.cities[index]);
      this.lat = this.parseLatitude(this.cities[index]);
      this.lon = this.parseLongitude(this.cities[index]);
    }
  }

  filterSearch(str: string) {
    if (!this.searchInput || this.searchInput == '') return true;
    if (this.searchInput && str.toLowerCase().includes(this.searchInput.toLowerCase())) return true;
    return false;
  }

}