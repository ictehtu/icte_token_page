import { Component, OnInit } from '@angular/core';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';

@Component({
  selector: 'app-main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.scss']
})
export class MainScreenComponent implements OnInit {
  public fetching = true;

  constructor(public _ids: InternalDataService,
    public _ors: IcteOrderRouteService) { }

  async ngOnInit() {

  }
  navigateToIcteWallet() {
    this._ors.callMethod.xamarinRedirect('0');
  }

}
