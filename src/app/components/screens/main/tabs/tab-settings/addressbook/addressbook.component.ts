import { Component, OnInit } from '@angular/core';
import { copy } from 'src/app/lib/copy';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { sleep } from 'icte-order-route/lib/lib/sleep';
declare let $: any;

@Component({
  selector: 'app-addressbook',
  templateUrl: './addressbook.component.html',
  styleUrls: ['./addressbook.component.scss']
})
export class AddressbookComponent implements OnInit {

  public searchInput = '';
  public action = 'Add';

  public selectedBook: any;
  public errorMsgs = [];

  public editBook: any = {
    bookId: 0,
    book: {
      name: '',
      address: ''
    }
  }

  public newBook: any = {
    name: '',
    address: ''
  }

  public $: any;

  public addressBook = [];
  public addressBookFiltered = [];

  constructor(
    public _ids: InternalDataService,
    private _ors: IcteOrderRouteService
  ) { }

  ngOnInit() {
    this.addressBook = this._ids.addressBook;
    if (this._ids.tempAddressToSave) {
      this.newBook.address = this._ids.tempAddressToSave;
      this.openModal('addModal');
      this.validateAddress(this.newBook);
      this._ids.tempAddressToSave = false;
    }
    this.filterBySearch();
  }

  remove() {
    let book = this.selectedBook;
    let index = this.addressBook.findIndex((el: any) => {
      return (el.name == book.name && el.address == book.address);
    });
    this.addressBook.splice(index, 1);
    this.selectedBook = null;
    this.updateAddressBook();
  }

  selectBook(book) {
    this.selectedBook = book;
  }

  selectEditBook(i, book: any) {
    let { name, address } = book;
    this.editBook.bookId = i;
    this.editBook.book = { name, address };
  }

  saveEdit() {
    this.addressBook[this.editBook.bookId] = this.editBook.book;
    this.updateAddressBook();
  }

  createAddress() {
    let { name, address } = this.newBook;
    this.addressBook.push({ name, address });
    this.newBook.name = '';
    this.newBook.address = '';
    this.updateAddressBook();
  }

  copyAddress(addr) {
    copy(addr);
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  validateAddress(book: any) {

    this.errorMsgs = [];
    let addressRex = /^(0x)?[0-9a-fA-F]{40}$/g;

    if (book.address.match(addressRex) == null) {
      this.errorMsgs.push('The address must be valid (IEO Address format)');
    } else if (book.address.match(addressRex).length != 1) {
      this.errorMsgs.push('The address must be valid (IEO Address format)');
    }

    let exist = '';
    let bookExist = this.addressBook.find(({ name, address }) => {
      if (name == book.name) {
        exist = 'name';
        return true;
      } else if (address == book.address) {
        exist = 'address';
        return true;
      }
      return false;
    });

    if (bookExist) {
      this.errorMsgs.push(`The ${exist} already exist`);
    }

    if (book.name.length > 16 || book.name.length < 3) {
      this.errorMsgs.push(`The name must be greater than 3 characters and less than 16 characters`)
    }
  }

  async updateAddressBook() {
    this.openModal('loadingModal');
    await sleep(10);

    let msgId = this._ors.callMethod.setXamarinData('addressbook', JSON.stringify(this.addressBook));
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_SaveData, msgId);
    if (!result.orderroute.error) {
      this._ids.hashBook = this.addressBook;
    }
    this.closeModal('loadingModal');
  }

  filterBySearch() {
    if (this.searchInput != '') {
      this.addressBookFiltered = this.addressBook.filter((el: any) => {
        return el.name.toLowerCase().includes(this.searchInput.toLowerCase()) || el.address.toLowerCase().includes(this.searchInput.toLowerCase())
      })
    } else {
      this.addressBookFiltered = this.addressBook;
    }
  }

}
