import { Component, OnInit } from '@angular/core';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { ICTEMarketDataService } from 'src/app/services/ictemarket-data.service';
import { Symbols } from 'icte-market-data/lib/model/Symbols';
import { CalculatorService } from 'src/app/services/calculator.service';
import { sleep } from 'src/app/lib/sleep';
declare var $: any;

@Component({
  selector: 'app-cryptos',
  templateUrl: './cryptos.component.html',
  styleUrls: ['./cryptos.component.scss']
})
export class CryptosComponent implements OnInit {

  public fakeArray: any = new Array(93);
  public searchInput: string;

  public wlName: string = "";

  public wlEditName: string = "";
  public wlEditSelected: number;

  constructor(
    private service: ICTEMarketDataService,
    public _ids: InternalDataService,
    private _cs: CalculatorService,
  ) { }

  ngOnInit() {
    this._cs.enableInitialTokens();
    this.selectWatchlist(this._ids.getSelectedWatchlist());
  }

  get s(): Symbols {
    return this.service.d;
  }

  canDisable(id) {
    if (this.s.getSymbolId(id) === 0 || this.s.getSymbolId(id) === 1) return false;
    return true;
  }

  filterSearch(tokenName) {
    return this._cs.filterSearch(tokenName, this.searchInput)
  }

  toggleSymbol(id) {
    this.s.changeSymbolStatus(id);
    this._ids.editWatchlistCoins(this.getSelectedSymbols());
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  async selectWatchlist(id) {
    this.openModal("loadingModal");
    this._ids.selectWatchlist(id);
    await sleep(500);
    this.closeModal("loadingModal");
  }

  createWatchlist() {
    this.wlName = '';
    this.openModal('modalWatchlistNew')
  }

  editWatchlist(id) {
    this.wlEditName = this._ids.getWatchlists()[id].name;
    this.wlEditSelected = id;
    this.openModal('modalWatchlistEdit');
  }

  saveEditWatchlist() {
    this._ids.editWatchlist(this.wlEditSelected, this.wlEditName);
  }

  deleteWatchlist(id) {
    this._ids.removeWatchlist(id);
  }

  addWatchlist() {
    let wl = {
      name: this.wlName,
      coins: [0, 1]
    }
    this._ids.addWatchlist(wl);
    this.selectWatchlist(this._ids.getWatchlists().length - 1);
  }

  getSelectedSymbols() {
    let coins = [];
    for (let i = 0; i < this.fakeArray.length; i++) {
      if (this.s.isSymbolEnabled(i)) {
        coins.push(this.s.getSymbolId(i))
      }
    }
    return coins;
  }
}
