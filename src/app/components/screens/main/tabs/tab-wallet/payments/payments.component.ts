import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICTEMarketDataService } from 'src/app/services/ictemarket-data.service';
import { Symbols } from 'icte-market-data/lib/model/Symbols';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { BlockchainApiService } from 'src/app/services/blockchain-api.service';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { sleep } from 'src/app/lib/sleep';
import { copy } from 'src/app/lib/copy';
import { format } from 'timeago.js';
import { Router, NavigationStart } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit, OnDestroy {

  public userAccount: any;
  public walletData: any = {};
  public transactions: any = [];
  public transactionsPending: any = false;
  public transaction: any = false;
  public checkingBalance = false;
  public copyTextText = ""
  public debug;

  public addressBook = [];
  transactionTab: string = 'tx';
  routeSub: any;



  constructor(private service: ICTEMarketDataService,
    private _ors: IcteOrderRouteService,
    public _ids: InternalDataService,
    private _bc: BlockchainApiService,
    public router: Router,
  ) { }

  async ngOnInit() {
    this._ids.loadGetIeoWatchlist();
    this._ids.loadMockSymbols();
    this.walletData = this._ids.getWalletData();

    this.transactions = this._ids.transactions;
    this.addressBook = this._ids.addressBook;
    await this.checkBalance();
    this._ids.transactions = this.transactions;

    let localRefInterval = window.setInterval(() => {
      this.checkBalance();
    }, this._ids.INTERVAL_MS);

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        window.clearInterval(localRefInterval);
      }
    });

  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  async checkBalance() {


    if (!this.checkingBalance) {
      this.checkingBalance = true;
      if (this._ids.addressSummary != null) {
        this.userAccount = this._ids.addressSummary;
      }
      this.copyTextText = '...'

      let msgId = this._ors.callMethod.reqBalance(this.walletData.accounts[0].address);
      let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
      if (!req.orderroute.error) {
        this._ids.addressSummary = req.data;
        this.userAccount = this._ids.addressSummary;
        this.copyTextText = `${this.userAccount.address.substr(0, 5)}...${this.userAccount.address.substr(this.userAccount.address.length - 5)}`;
        this.loadUserTransactions();
        this.loadUserPendingTransactions();
      } else {
        this.openModal('errorModal');
      }
      this.checkingBalance = false
    }
  }

  get s(): Symbols {
    return this.service.d;
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  formatDate(date) {
    return format(date);
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  async loadUserTransactions() {
    let totalTx = this.userAccount.transactionCount;
    let txend = totalTx - 50;

    let from, to;
    if (txend >= 0) {
      from = txend;
    } else {
      from = 0
    }
    if (totalTx <= 0) {
      to = 1;
    } else {
      to = totalTx;
    }
    let res: any = await this._bc.getTransactions(this.walletData.accounts[0].address, from, to);
    if (!res.orderroute.error) {
      this.transactions = res.data.result.reverse();
    } else {
      this.transactions = [];
    }
  }

  async loadUserPendingTransactions() {
    let totalTx = this.userAccount.pendingTransactionCount;
    let txend = totalTx - 50;

    let from, to;
    if (txend >= 0) {
      from = txend;
    } else {
      from = 0
    }
    if (totalTx <= 0) {
      to = 1;
    } else {
      to = totalTx;
    }
    let res: any = await this._bc.getPendingTransactions(this.walletData.accounts[0].address, from, to);
    if (!res.orderroute.error) {
      for (const e of res.data.result) {
        this.transactions.unshift({ ...e, pending: true });
      }
    } else {
    }
  }

  openTransactionDetails(tx) {
    this._ids.transaction = tx;
    this.router.navigateByUrl('/main/tab-wallet/payments/transaction-view');
  }

  scroll(el) {
    document.getElementById(el).scrollIntoView();
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextText = 'Copied!';
    await sleep(1000);
    this.copyTextText = `${this.userAccount.address.substr(0, 5)}...${this.userAccount.address.substr(this.userAccount.address.length - 5)}`;
  }

  displayAddressName(address) {
    let name = this._ids.displayAddressName(address, this.addressBook);
    if (name) {
      return name;
    } return `${address.substr(0, 9)}...`;
  }

}
