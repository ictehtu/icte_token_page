import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionViewResultComponent } from './transaction-view-result.component';

describe('TransactionViewResultComponent', () => {
  let component: TransactionViewResultComponent;
  let fixture: ComponentFixture<TransactionViewResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionViewResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionViewResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
