import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BlockchainApiService } from 'src/app/services/blockchain-api.service';
import { InternalDataService } from 'src/app/services/internal-data.service';

@Component({
  selector: 'app-transaction-view-result',
  templateUrl: './transaction-view-result.component.html',
  styleUrls: ['./transaction-view-result.component.scss']
})
export class TransactionViewResultComponent implements OnInit {

  public transactionResult: any;
  fetching: boolean = false;
  addressBookContract: any;



  constructor(private _bc: BlockchainApiService,
    private _ids: InternalDataService,
    public router: Router) { }

  ngOnInit() {
    this.loadTransactionResult();
  }

  displayAddressName(address) {
    let name = this._ids.displayAddressName(address, this._ids.addressBook);
    if (name) {
      return name;
    } return false;
  }

  async loadTransactionResult() {
    this.fetching = true;
    let res: any = await this._bc.getTransactionResult(this._ids.transaction.hash);
    debugger
    this.transactionResult = res.data.result;
    this.addressBookContract = this.displayAddressName(this.transactionResult.contractAddress);
    this.fetching = false;
  }

  addAddress(addr) {
    this._ids.tempAddressToSave = addr;
    this.router.navigateByUrl('/main/tab-settings/addressbook');
  }
  

}
