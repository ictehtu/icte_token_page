import { Component, OnInit } from '@angular/core';
import { InternalDataService } from 'src/app/services/internal-data.service';
import * as hexyjs from 'hexyjs';
import { Router } from '@angular/router';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { Constant } from 'icte-order-route/lib/model/Constant';

@Component({
  selector: 'app-transaction-view',
  templateUrl: './transaction-view.component.html',
  styleUrls: ['./transaction-view.component.scss']
})
export class TransactionViewComponent implements OnInit {

  public transaction: any = false;
  public walletData: any;

  public addressBook = [];

  public addressBookFrom: any = false;
  public addressBookTo: any = false;

  public txDataProduct: any;

  public hexCtrl = false;
  txData: any = '';
  displayRfqData: boolean;

  constructor(
    public _ids: InternalDataService,
    private _ors: IcteOrderRouteService,
    public router: Router
  ) { }

  ngOnInit() {
    this.walletData = this._ids.getWalletData();
    this.transaction = this._ids.transaction;
    if(this.transaction.type.toLowerCase() === "behaviorend"){
      this.transaction.data = this.transaction.data.slice(46).replace('0x', '');
    }
    this.txData = this.transaction.data;
    this.addressBook = this._ids.addressBook;


    // check one time only 
    this.addressBookFrom = this.displayAddressName(this.transaction.from);
    this.addressBookTo = this.displayAddressName(this.transaction.to);
  }

  async toggleHex() {
    this.hexCtrl = !this.hexCtrl;
    this.displayRfqData = false;
    if (this.hexCtrl) {
      if (this.transaction.type.toLowerCase() == 'product') {
        let dataTypes = this._ors.callMethod.parseDataTypes(this.transaction.data.replace('0x', ''));
        if (dataTypes.type == 99 && dataTypes.subtype == 0) {
          this.txData = this._ors.callMethod.decodeData(this.transaction.data.replace('0x', ''))
        } else {
          this.displayRfqData = true;
          if (!this.txDataProduct) {
            let msgId = this._ors.callMethod.reqRfqParseOffer(this.transaction.data.replace('0x', ''));
            let resp = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Exchange, this._ors.types.Sub_RfqParseOffer, msgId);
            debugger;
            if (!resp.orderroute.error) {
              // handle success
              this.txDataProduct = resp.data
            } else {
              // handle error
            }
          }
        }
      } else {
        this.txData = hexyjs.hexToStr(this.transaction.data.replace('0x', ''));
      }
    } else {
      this.txData = this.transaction.data;
    }
  }

  displayAddressName(address) {
    let name = this._ids.displayAddressName(address, this.addressBook);
    if (name) {
      return name;
    } return false;
  }

  addAddress(addr) {
    this._ids.tempAddressToSave = addr;
    this.router.navigateByUrl('/main/tab-settings/addressbook');
  }

}
