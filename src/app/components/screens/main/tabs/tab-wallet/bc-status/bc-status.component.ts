import { Component, OnInit } from '@angular/core';
import { BlockchainApiService } from 'src/app/services/blockchain-api.service';
import { InternalDataService } from 'src/app/services/internal-data.service';

@Component({
  selector: 'app-bc-status',
  templateUrl: './bc-status.component.html',
  styleUrls: ['./bc-status.component.scss']
})
export class BcStatusComponent implements OnInit {
  public info: any;
  public delegate: any;
  public error: any;

  constructor(
    private _bc: BlockchainApiService,
    public _ids: InternalDataService) { }

  async ngOnInit() {
    await this.loadInfo();
    await this.loadDelegate();
    this.loadAccountForCoinbase();
    this.loadAccountForBurn();
  }

  async loadInfo() {
    let infoResp = await this._bc.getInfo();
    if (!infoResp.orderroute.error && infoResp.data.success) {
      this.info = infoResp.data.result;
    } else {
      this.error = 'Error trying to fetch info, try again later.'
      return;
    }
  }

  async loadDelegate() {
    this._ids.NANO
    let delegateResp = await this._bc.getDelegate(this.info.coinbase);
    if (!delegateResp.orderroute.error && delegateResp.data.success) {
      this.delegate = delegateResp.data.result;
    } else {
      this.error = 'Error trying to fetch delegate, try again later.'
      return;
    }
  }
  async loadAccountForCoinbase() {
    let accCoinbaseResp = await this._bc.getAccount2(this.info.coinbase);
    if (!accCoinbaseResp.orderroute.error && accCoinbaseResp.data.success) {
      this.delegate = { ...this.delegate, available: accCoinbaseResp.data.result.available };
    } else {
      this.error = 'Error trying to fetch account, try again later.'
      return;
    }
  }
  async loadAccountForBurn() {
    let accBurnResp = await this._bc.getAccount2("0x0000000000000000000000000000000000000000");
    if (!accBurnResp.orderroute.error && accBurnResp.data.success) {
      this.info = { ...this.info, totalBurned: accBurnResp.data.result.available };
    } else {
      this.error = 'Error trying to fetch account, try again later.'
      return;
    }
  }

}

