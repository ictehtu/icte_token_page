import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BcStatusComponent } from './bc-status.component';

describe('BcStatusComponent', () => {
  let component: BcStatusComponent;
  let fixture: ComponentFixture<BcStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BcStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BcStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
