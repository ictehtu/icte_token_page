import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { Router } from '@angular/router';
import { sleep } from 'src/app/lib/sleep';
import { copy } from 'src/app/lib/copy';
import { getMobileOS } from 'src/app/lib/mobileOs';
declare var $: any;

@Component({
  selector: 'app-payments-send',
  templateUrl: './payments-send.component.html',
  styleUrls: ['./payments-send.component.scss']
})
export class PaymentsSendComponent implements OnInit {

  public amount: number;
  public total: number = 0;
  public qrData: any = '';
  public debug: any;
  public txLimitsData: any;
  public sendData: string = '';

  public userAccount: any;

  public serialQr: string = "";

  public walletData: any = {};

  public errorMsgs: any = [];

  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  displayAddressBook: boolean;
  addressBook: any[] = [];
  addressBookFiltered: any[] = [];

  @ViewChild("valueInput") valueEl: ElementRef;
  setExtraSpace: boolean = false;

  constructor(
    private _ors: IcteOrderRouteService,
    private _internalDataService: InternalDataService,
    private router: Router,
    public _ids: InternalDataService
  ) { }

  async ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('TRANSFER');
    this.walletData = this._internalDataService.getWalletData();
    this.validateIputs()
    await this.getUserAccount();
    this.addressBook = this._ids.addressBook;
    this.filterBySearch();
  }

  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this.walletData.accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }

  }

  async transfer() {
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    debugger;
    let msgId = this._ors.callMethod.reqTransferAction(this.walletData.accounts[0].address, this.qrData, this.amount.toString(), String(this.txLimitsData.fee), this.getNonce(), this.sendData.toString());
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    debugger;
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('transactionSuccessModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  getNonce() {
    debugger;
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }

  async reqQRScan() {
    // TODO: Test
    let msgId = this._ors.callMethod.reqQRScan();
    let resp = await this._ors.callMethod.getQRData();
    this.parseQrData(resp);
    this.calculateTotal();
  }

  goToPayments() {
    this.closeModal('transactionSuccessModal')
    this.router.navigateByUrl('/main/tab-wallet/payments')
  }

  calculateTotal() {
    if (this.amount != NaN || this.amount != 0) {
      this.total = this.amount + this.txLimitsData.fee;
    }
  }

  parseQrData(str: string) {
    let walletOrig = "";
    // let walletAdrs = "";
    let walletOrigRegex = /.*:/g;
    let walletAddressRegex = /:.*(.*\?)/g;
    let walletAddressRegex2 = /:.*/g;
    let amountRegex = /amount=.*/g;

    if (str.match(walletOrigRegex)) {
      let wostr = str.match(walletOrigRegex)[0];
      walletOrig = wostr.substr(0, wostr.length - 1);
      this.debug = walletOrig;
      if (str.match(walletAddressRegex)) {
        let wastr = str.match(walletAddressRegex)[0];
        this.qrData = wastr.substr(1, wastr.length - 2);
        if (str.match(amountRegex)) {
          let astr = str.match(amountRegex)[0];
          this.amount = parseFloat(astr.substr(7));
        } else {
          this.amount = 0.0;
        }
      } else {
        if (str.match(walletAddressRegex2)) {
          let wa2str = str.match(walletAddressRegex2)[0];
          this.qrData = wa2str.substr(1);
        } else {
          this.qrData = str;
        }
      }
    }
  }

  validateIputs() {

    this.errorMsgs = [];
    let addressRex = /^(0x)?[0-9a-fA-F]{40}$/g;

    if (this.qrData.match(addressRex) == null) {
      this.errorMsgs.push('The address must be valid (IEO Address format)');
    } else if (this.qrData.match(addressRex).length != 1) {
      this.errorMsgs.push('The address must be valid (IEO Address format)');
    }

    if (this.amount <= 0 || this.amount == undefined) {
      this.errorMsgs.push('The amount must be greater than 0');
    }

    if (this.txLimitsData.fee < 0.0025) {
      this.errorMsgs.push('The fee must be equal or greater than 0.0025');
    }

  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  keyboardExtraSpace(scrollTarget?) {
    if (getMobileOS() == 'android') {
      this.setExtraSpace = !this.setExtraSpace;
      if (scrollTarget)
        this.scroll(scrollTarget);
    }
  }

  scroll(el) {
    document.getElementById(el).scrollIntoView();
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

  showList() {
    this.displayAddressBook = true;
  }

  async hideList() {
    await sleep(100);
    this.displayAddressBook = false;
  }

  setAddress(address) {
    this.qrData = address;
    this.validateIputs();
    this.valueEl.nativeElement.focus();
  }

  filterBySearch() {
    if (this.qrData != '') {
      this.addressBookFiltered = this.addressBook.filter((el: any) => {
        return el.name.toLowerCase().includes(this.qrData.toLowerCase()) || el.address.toLowerCase().includes(this.qrData.toLowerCase());
      })
    } else {
      this.addressBookFiltered = this.addressBook;
    }
  }
}
