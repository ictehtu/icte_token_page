import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsSendComponent } from './payments-send.component';

describe('PaymentsSendComponent', () => {
  let component: PaymentsSendComponent;
  let fixture: ComponentFixture<PaymentsSendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsSendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsSendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
