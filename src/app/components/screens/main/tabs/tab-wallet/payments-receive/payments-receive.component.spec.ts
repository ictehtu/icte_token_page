import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsReceiveComponent } from './payments-receive.component';

describe('PaymentsReceiveComponent', () => {
  let component: PaymentsReceiveComponent;
  let fixture: ComponentFixture<PaymentsReceiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsReceiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsReceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
