import { Component, OnInit } from '@angular/core';
import { copy } from 'src/app/lib/copy';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { sleep } from 'src/app/lib/sleep';

@Component({
  selector: 'app-payments-receive',
  templateUrl: './payments-receive.component.html',
  styleUrls: ['./payments-receive.component.scss']
})
export class PaymentsReceiveComponent implements OnInit {

  public walletData: any = {};
  public copyTextText = 'Copy address'

  constructor(private _ids: InternalDataService) { }

  ngOnInit() {
    this.walletData = this._ids.getWalletData();
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextText = 'Copied!';
    await sleep(1000);
    this.copyTextText = 'Copy address';
  }
}
