import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsSendSentComponent } from './payments-send-sent.component';

describe('PaymentsSendSentComponent', () => {
  let component: PaymentsSendSentComponent;
  let fixture: ComponentFixture<PaymentsSendSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsSendSentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsSendSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
