import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Symbols } from 'icte-market-data/lib/model/Symbols';
import { ICTEMarketDataService } from 'src/app/services/ictemarket-data.service';
import { CalculatorService } from 'src/app/services/calculator.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { sleep } from "src/app/lib/sleep";
declare let $: any;

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {

  public calculatorViewState: boolean = true; // true -> main view | false -> select view
  public calculatorSymbols;
  public calculateInit = false;
  public searchInput = '';
  public fakeArray = new Array(93);

  public amounts = {
    utility: 0,
    bench: 0
  };

  constructor(
    private service: ICTEMarketDataService,
    private _cs: CalculatorService,
    private _ids: InternalDataService,
    private cdRef: ChangeDetectorRef,
  ) { }

  async ngOnInit() {
    this._ids.loadDefaultWatchlist();

    this._ids.loadMockSymbols();
    this.calculatorSymbols = await this._ids.getCalculatorMock();
    this.calculateTotals(0);
    this.cdRef.detectChanges();
  }

  get s(): Symbols {
    return this.service.d;
  }
  
  toggleSymbol(id) {
    this.s.changeSymbolStatus(id);
    this._ids.editWatchlistCoins(this.getSelectedSymbols());
  }

  getSelectedSymbols() {
    let coins = [];
    for (let i = 0; i < this.fakeArray.length; i++) {
      if (this.s.isSymbolEnabled(i)) {
        coins.push(this.s.getSymbolId(i))
      }
    }
    return coins;
  }

  async calculateTotals(rowId) {

    if (!this.calculateInit) {
      this.calculateInit = true;
      while (this.calculateInit) {
        this.amounts = {
          bench: 0,
          utility: 0
        }
        for (let i = 0; i < this.fakeArray.length; i++) {
          if (this.s.getMatrixPrice(i) != NaN) {
            this.amounts.bench += this.calculatorSymbols.qty[i] * this.s.getMatrixPrice(this.s.getSymbolId(i));
            this.amounts.utility += this.calculatorSymbols.qty[i] * this.s.getMatrixIEOPrice(this.s.getSymbolId(i));
          }
        }
        await sleep(1000);
      }
    }
  }

  doChange() {
    this.calculatorViewState = !this.calculatorViewState;
  }

  setBenchmark(rowId: number) {
    this._cs.setBenchmark(rowId);
  }

  changeStatus(rowId) {
    this._cs.changeSymbolStatus(rowId);
  }

  getSelectedClass(rowId: number) {
    return this._cs.getSelectedClass(rowId);
  }

  getStatus(rowId: number) {
    return this._cs.getSymbolStatus(rowId);
  }

  filterSearch(tokenName) {
    return this._cs.filterSearch(tokenName, this.searchInput);
  }
}
