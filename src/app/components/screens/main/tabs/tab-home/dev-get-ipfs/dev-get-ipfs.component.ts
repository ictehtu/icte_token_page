import { Component, OnInit } from '@angular/core';
import { copy } from 'src/app/lib/copy';
import { sleep } from 'src/app/lib/sleep';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import Web3 from 'web3';
declare var $: any;

@Component({
  selector: 'app-dev-get-ipfs',
  templateUrl: './dev-get-ipfs.component.html',
  styleUrls: ['./dev-get-ipfs.component.scss']
})
export class DevGetIpfsComponent implements OnInit {

  public functionComplete: string = '';
  public ipfsHash = '';
  public functionName: string = '';
  public functionParams: any[] = [];
  public renderForm: boolean = false;
  public readyToCall: boolean = false;
  errors: any[] = [];
  userAccount: any;
  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  public readytoSend = false;
  rawCode: string;
  displayAddressBook: boolean;
  scAddress: any;
  addressBookFiltered: any[] = [];

  gasTransaction = '0.005';
  txLimitsData;
  clientOrderId: any;

  constructor(public _ids: InternalDataService,
    private _ors: IcteOrderRouteService) { }

  ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('BehaviorStart');
  }

  extractParams() {
    let regex = /[^\(]+(?=\))/
    this.functionName = this.functionComplete.substr(0, this.functionComplete.indexOf('('));
    let content = this.functionComplete.match(regex);
    if (content && this.functionName) {
      this.functionParams = [];
      let params = content[0].replace(/\s/g, '').split(',');
      for (const p of params) {
        this.functionParams.push({
          name: p,
          value: ''
        })
      }
      this.renderForm = true;
      this.readyToCall = true;
    } else {
      this.functionParams = []
      this.renderForm = false;
    }
  }

  validate() {
    this.errors = [];
    for (const param of this.functionParams) {
      if (!param.value) this.errors.push(`Param ${param.name} cannot be null\n`)
    }
    if (this.errors.length == 0) return true;
    return false;
  }

  async compile() {
    try {

      let execParams;
      let buf = Buffer.from(this.ipfsHash);
      let uintString = new Uint8Array(buf);
      execParams = {
        _ipfshsh: uintString,
        bhvmsg: 'GtIPFS'
      }

      let msgId = this._ors.callMethod.reqTransferBehavior(String(this.txLimitsData.feeNano), "Get from IPFS", 0, Number(this.clientOrderId), execParams);
      let behavior: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_BehaviorTransaction, msgId);
      this.txHash = behavior.data.hash;
      this.openModal('successModal')
      debugger;

    } catch (error) {
      this.errors.push(error.message)
    }
  }

  compile2() {
    if (this.readytoSend) {
      return this.openModal('modalConfirm');
    }
    const web3 = new Web3();
    this.errors = [];

    try {
      if (this.validate()) {
        let functionSign = web3.eth.abi.encodeFunctionSignature('ipfsGet(string)');
        let encParams = web3.eth.abi.encodeParameters(['string'], [this.ipfsHash])
        let paramCode = functionSign + encParams.replace(/0x/g, '');
        this.rawCode = paramCode;
        this.readytoSend = true;
      }

    } catch (error) {
      this.errors.push(error.message)
    }
  }

  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this._ids.getWalletData().accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }

  }

  async transfer() {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    let msgId = this._ors.callMethod.reqCustomTx(this._ids.getWalletData().accounts[0].address, this._ids.scAddress, "0", this.txLimitsData.fee, this.getNonce(), this.rawCode, this.gasTransaction, "0.000000001", "DOMAINREQUEST");
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('successModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

  getTotal() {
    return this.txLimitsData.fee + Number(this.gasTransaction);
  }

  getNonce() {
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }

}