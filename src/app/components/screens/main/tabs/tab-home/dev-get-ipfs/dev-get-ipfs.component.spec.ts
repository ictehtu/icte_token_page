import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevGetIpfsComponent } from './dev-get-ipfs.component';

describe('DevGetIpfsComponent', () => {
  let component: DevGetIpfsComponent;
  let fixture: ComponentFixture<DevGetIpfsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevGetIpfsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevGetIpfsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
