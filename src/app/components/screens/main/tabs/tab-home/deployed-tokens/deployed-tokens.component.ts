import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BlockchainApiService } from 'src/app/services/blockchain-api.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
declare var $: any;
@Component({
  selector: 'app-deployed-tokens',
  templateUrl: './deployed-tokens.component.html',
  styleUrls: ['./deployed-tokens.component.scss']
})
export class DeployedTokensComponent implements OnInit {

  public items = []

  public stages = [
    'Developing',
    'Testing',
    'Production'
  ]
  public states = [
    'Disabled',
    'Active',
    'Halted'
  ]

  public search: string = '';

  public userStages = [
    0, 1, 2
  ]

  public userStates = [
    0, 1, 2
  ]

  constructor(
    public _bcs: BlockchainApiService,
    public _ids: InternalDataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getTokenList()
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  getStateClass(state) {
    if (state === 0) return 'muted';
    if (state === 1) return 'success'
    if (state === 2) return 'orange'
  }

  editState(index) {
    let i = this.userStates.findIndex(e => e === index);
    if (i != -1) {
      this.userStates.splice(i, 1);
    } else {
      this.userStates.push(index);
    }
  }

  isUserInState(index) {
    let i = this.userStates.findIndex(e => e === index);
    if (i != -1) return true;
    return false;
  }

  getStateName() {
    if (this.userStates.length > 1) return 'Custom';
    if (this.userStates.length === 0) return 'None';
    return this.states[this.userStates[0]];
  }

  editStage(index) {
    let i = this.userStages.findIndex(e => e === index);
    if (i != -1) {
      this.userStages.splice(i, 1);
    } else {
      this.userStages.push(index);
    }
  }

  isUserInStage(index) {
    let i = this.userStages.findIndex(e => e === index);
    if (i != -1) return true;
    return false;
  }

  getStageName() {
    if (this.userStages.length > 1) return 'Custom';
    if (this.userStages.length === 0) return 'None';
    return this.stages[this.userStages[0]];
  }

  async getTokenList() {
    let res = await this._bcs.listProductsTokens();
    if (!res.orderroute.error) {
      let parsedData = this._ids.parseByteResponse(res.data.result)
      this.items = parsedData;

    } else {
      this.items = [];
    }
  }

  setTokenAndNavigate(token) {
    this._ids.tokenSelected = token;
    this.router.navigateByUrl("/main/tab-settings/options-tokens");
  }

  display(i) {
    let element = this.items[i];

    let stateIndex = this.userStates.findIndex(e => e === element.state);
    if (stateIndex == -1) return false;

    let stageIndex = this.userStages.findIndex(e => e === element.stage);
    if (stageIndex == -1) return false;

    if (this.search && this.search.length > 0) {
      if (!element.name.toLowerCase().includes(this.search.toLowerCase())) {
        return false;
      }
    }
    return true;
  }


}
