import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeployedTokensComponent } from './deployed-tokens.component';

describe('DeployedTokensComponent', () => {
  let component: DeployedTokensComponent;
  let fixture: ComponentFixture<DeployedTokensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeployedTokensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeployedTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
