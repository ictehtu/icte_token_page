import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { strToHex } from 'hexyjs';
import { copy } from 'src/app/lib/copy';
import { sleep } from 'src/app/lib/sleep';
import Web3 from 'web3';
declare var $: any;

@Component({
  selector: 'app-option-url',
  templateUrl: './option-url.component.html',
  styleUrls: ['./option-url.component.scss']
})
export class OptionUrlComponent implements OnInit {
  public url;
  public title;
  public smFunction;

  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  public hexadecimalCode: string = '';
  userAccount: any;
  public rawCode;
  readytoSend: boolean = false;
  public dataType = false;
  errorMsgs: any[] = [];
  gasTransaction = '0.005';
  txLimitsData;
  tokenId: number = 0;
  token: any;

  constructor(
    private route: ActivatedRoute,
    public _ids: InternalDataService,
    private _ors: IcteOrderRouteService
  ) { }

  ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('CALL');
    if (this.route.snapshot.queryParamMap.has('title')) {
      this.title = this.route.snapshot.queryParamMap.get('title');
      this.smFunction = this.route.snapshot.queryParamMap.get('smFunction');
    }
    this.token = this._ids.tokenSelected
  }


  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this._ids.getWalletData().accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }

  }

  compile() {
    if (this.readytoSend) {
      return this.openModal('modalConfirm');
    }
    const web3 = new Web3();
    this.errorMsgs = [];
    let functionSignature = '';

    // sm functions
    switch (this.smFunction) {
      case 'compileFormIpfs': {
        functionSignature = 'compileHtmlIPFSHash(int32, string)';
        break;
      }
      case 'compileSolIpfs': {
        functionSignature = 'compileSolIPFSHash(int32, string)';
        break;
      }
      case 'compileGoIpfs': {
        functionSignature = 'compileGoIPFSHash(int32, string)';
        break;
      }
    }

    try {
      let functionSign = web3.eth.abi.encodeFunctionSignature(functionSignature);
      let dataToSend = '';
      if (this.dataType) {
        dataToSend = this.hexadecimalCode;
      } else {
        dataToSend = '0x' + strToHex(this.hexadecimalCode)
      }
      let encParams = web3.eth.abi.encodeParameters(['int32', 'string'], [this.token.id, web3.utils.fromAscii(this.url)])
      let paramCode = functionSign + encParams.replace(/0x/g, '');
      this.rawCode = paramCode;
      debugger;
      this.readytoSend = true;

    } catch (error) {
      this.errorMsgs.push(error.message)
    }
  }

  changeStatus() {
    this.dataType = !this.dataType;
  }
  async transfer() {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    debugger;
    let msgId = this._ors.callMethod.reqCustomTx(this._ids.getWalletData().accounts[0].address, '0x73d6e444ff5a46f620449fad42c45dfa8c1b88b9', "0", this.txLimitsData.fee, this.getNonce(), this.rawCode, this.gasTransaction, "0.000000001", "DOMAINREQUEST");
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    debugger;
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('successModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

  getTotal() {
    return this.txLimitsData.fee + Number(this.gasTransaction);
  }

  getNonce() {
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }

}
