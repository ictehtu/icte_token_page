import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionUrlComponent } from './option-url.component';

describe('OptionUrlComponent', () => {
  let component: OptionUrlComponent;
  let fixture: ComponentFixture<OptionUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
