import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevGoCompilerComponent } from './dev-go-compiler.component';

describe('DevGoCompilerComponent', () => {
  let component: DevGoCompilerComponent;
  let fixture: ComponentFixture<DevGoCompilerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevGoCompilerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevGoCompilerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
