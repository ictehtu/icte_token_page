import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { copy } from 'src/app/lib/copy';
import { sleep } from 'src/app/lib/sleep';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import Web3 from 'web3';
declare var $: any;

@Component({
  selector: 'app-dev-go-compiler',
  templateUrl: './dev-go-compiler.component.html',
  styleUrls: ['./dev-go-compiler.component.scss']
})
export class DevGoCompilerComponent implements OnInit {

  public functionComplete: string = '';
  public githubURL = 'https://github.com/talhaanisicte/go-precompiled-contract.git';
  public clientOrderId = '';
  public tokenId = '';
  public functionName: string = '';
  public functionParams: any[] = [];
  public renderForm: boolean = false;
  public readyToCall: boolean = false;
  errors: any[] = [];
  userAccount: any;
  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  public readytoSend = false;
  rawCode: string;
  displayAddressBook: boolean;
  scAddress: any;
  addressBookFiltered: any[] = [];

  note: string = ''

  gasTransaction = '0.005';
  txLimitsData;

  smFunction = '';
  title: string = '';

  constructor(
    private route: ActivatedRoute,
    public _ids: InternalDataService,
    private _ors: IcteOrderRouteService) { }

  @ViewChild("valueInput") valueEl: ElementRef;

  ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('BehaviorStart');
    if (this.route.snapshot.queryParamMap.has('title')) {
      this.title = this.route.snapshot.queryParamMap.get('title');
      this.smFunction = this.route.snapshot.queryParamMap.get('smFunction');
    }
  }

  extractParams() {
    let regex = /[^\(]+(?=\))/
    this.functionName = this.functionComplete.substr(0, this.functionComplete.indexOf('('));
    let content = this.functionComplete.match(regex);
    if (content && this.functionName) {
      this.functionParams = [];
      let params = content[0].replace(/\s/g, '').split(',');
      for (const p of params) {
        this.functionParams.push({
          name: p,
          value: ''
        })
      }
      this.renderForm = true;
      this.readyToCall = true;
    } else {
      this.functionParams = []
      this.renderForm = false;
    }
  }

  validate() {
    this.errors = [];
    for (const param of this.functionParams) {
      if (!param.value) this.errors.push(`Param ${param.name} cannot be null\n`)
    }
    if (this.errors.length == 0) return true;
    return false;
  }

  async compile() {
    if (this.readytoSend) {
      return this.openModal('modalConfirm');
    }
    this.errors = [];

    try {
      if (this.validate()) {
        let execParams = {}

        switch (this.smFunction) {
          case "compileContract": {
            let buf = Buffer.from(this.githubURL);
            let uintString = new Uint8Array(buf);
            execParams = {
              u: uintString,
              bhvmsg: 'CmplSlPgrm'
            }
            break;
          }
          case "compilePage": {
            let buf = Buffer.from(this.githubURL);
            let uintString = new Uint8Array(buf);
            execParams = {
              u: uintString,
              bhvmsg: 'CmplHTMLPgrm'
            }
            break;
          }
          case "compilePlugin": {
            let buf = Buffer.from(this.githubURL);
            let uintString = new Uint8Array(buf);
            execParams = {
              u: uintString,
              tId: Number(this.tokenId),
              bhvmsg: 'CmplGPgrm'
            }
            break;
          }
        }
        let msgId = this._ors.callMethod.reqTransferBehavior(String(this.txLimitsData.feeNano), this.note, 0, Number(this.clientOrderId), execParams);
        let behavior: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_BehaviorTransaction, msgId);
        this.txHash = behavior.data.hash;
        this.openModal('successModal')
        debugger;
      }

    } catch (error) {
      this.errors.push(error.message)
    }
  }

  compile2() {
    if (this.readytoSend) {
      return this.openModal('modalConfirm');
    }
    const web3 = new Web3();
    this.errors = [];

    try {
      if (this.validate()) {
        let functionSignatureStr = '';
        let paramsTypes = [];
        let paramsValues = [];

        switch (this.smFunction) {
          case "compileContract": {
            functionSignatureStr = 'compileContract(string)';
            paramsTypes = ['string'];
            paramsValues = [this.githubURL]
            break;
          }
          case "compilePage": {
            functionSignatureStr = 'compilePage(string)';
            paramsTypes = ['string'];
            paramsValues = [this.githubURL]
            break;
          }
          case "compilePlugin": {
            functionSignatureStr = 'compilePlugin(int32,string)';
            paramsTypes = ['int32', 'string'];
            paramsValues = [0, this.githubURL]
            break;
          }
        }

        let functionSign = web3.eth.abi.encodeFunctionSignature(functionSignatureStr);
        let encParams = web3.eth.abi.encodeParameters(paramsTypes, paramsValues)
        let paramCode = functionSign + encParams.replace(/0x/g, '');
        this.rawCode = paramCode;
        this.readytoSend = true;
      }

    } catch (error) {
      this.errors.push(error.message)
    }
  }

  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this._ids.getWalletData().accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }

  }

  async transfer() {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    let msgId = this._ors.callMethod.reqCustomTx(this._ids.getWalletData().accounts[0].address, this._ids.scAddress, "0", String(this.txLimitsData.fee), this.getNonce(), this.rawCode, this.gasTransaction, "0.000000001", "DOMAINREQUEST");
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        await this.saveHash()
        this.openModal('successModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

  showList() {
    this.displayAddressBook = true;
  }

  async hideList() {
    await sleep(100);
    this.displayAddressBook = false;
  }

  setAddress(address) {
    this.scAddress = address;
    this.valueEl.nativeElement.focus();
  }

  filterBySearch() {
    if (this.scAddress != '') {
      this.addressBookFiltered = this._ids.addressBook.filter((el: any) => {
        return el.name.toLowerCase().includes(this.scAddress.toLowerCase()) || el.address.toLowerCase().includes(this.scAddress.toLowerCase());
      })
    } else {
      this.addressBookFiltered = this._ids.addressBook;
    }
  }

  getTotal() {
    if (this.txLimitsData) {
      return this.txLimitsData.fee + Number(this.gasTransaction);
    } else {
      return Number(this.gasTransaction)
    }
  }

  getNonce() {
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }

  async saveHash() {
    let compilation = '';
    switch (this.smFunction) {
      case "compileContract": {
        compilation = 'Contract';

        break;
      }
      case "compilePage": {
        compilation = 'Page';
        break;
      }
      case "compilePlugin": {
        compilation = 'Plugin';
        break;
      }
    }
    await this._ids.createHash(`${compilation} comp - ${Date.now()}`, this.txHash)
  }
}
