import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { copy } from 'src/app/lib/copy';
import { sleep } from 'src/app/lib/sleep';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import Web3 from 'web3';
declare var $: any;
@Component({
  selector: 'app-option-hash',
  templateUrl: './option-hash.component.html',
  styleUrls: ['./option-hash.component.scss']
})
export class OptionHashComponent implements OnInit {
  public hash;
  public title;
  public smFunction;

  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  public hexadecimalCode: string = '';
  userAccount: any;
  public rawCode;
  readytoSend: boolean = false;
  public dataType = false;
  errors: any[] = [];
  gasTransaction = '0.005';
  txLimitsData;
  token: any;

  constructor(
    private route: ActivatedRoute,
    public _ids: InternalDataService,
    private _ors: IcteOrderRouteService
  ) { }

  ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('CALL');
    if (this.route.snapshot.queryParamMap.has('title')) {
      this.title = this.route.snapshot.queryParamMap.get('title');
      this.smFunction = this.route.snapshot.queryParamMap.get('smFunction');
    }
    this.token = this._ids.tokenSelected;
  }


  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this._ids.getWalletData().accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }

  }

  compile() {
    if (this.readytoSend) {
      return this.openModal('modalConfirm');
    }
    const web3 = new Web3();
    this.errors = [];
    let functionSignature = '';

    // sm functions
    switch (this.smFunction) {
      case 'AddFormHashIpfsToToken': {
        functionSignature = 'setFormHash(int32,string)';
        break;
      }
      case 'AddSolIpfsHashToToken': {
        functionSignature = 'setContractHash(int32,string)';
        break;
      }
      case 'AddGoIpfsHashToToken': {
        functionSignature = 'setPluginHash(int32,string)';
        break;
      }
    }

    try {
      let functionSign = web3.eth.abi.encodeFunctionSignature(functionSignature);
      let encParams = web3.eth.abi.encodeParameters(['int32', 'string'], [this.token.id, this.hash])
      let paramCode = functionSign + encParams.replace(/0x/g, '');
      this.rawCode = paramCode;
      debugger;
      this.readytoSend = true;

    } catch (error) {
      this.errors.push(error.message)
    }
  }

  changeStatus() {
    this.dataType = !this.dataType;
  }
  async transfer() {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    debugger;
    let msgId = this._ors.callMethod.reqCustomTx(this._ids.getWalletData().accounts[0].address, '0xede41279188dd17c5f0cb5a7f91dffc791fb90da', "0", this.txLimitsData.fee, this.getNonce(), this.rawCode, this.gasTransaction, "0.000000001", "CALL");
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    debugger;
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('successModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

  getTotal() {
    return this.txLimitsData.fee + Number(this.gasTransaction);
  }

  getNonce() {
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }

}
