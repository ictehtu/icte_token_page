import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionHashComponent } from './option-hash.component';

describe('OptionHashComponent', () => {
  let component: OptionHashComponent;
  let fixture: ComponentFixture<OptionHashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionHashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionHashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
