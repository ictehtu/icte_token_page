import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionAccessComponent } from './option-access.component';

describe('OptionAccessComponent', () => {
  let component: OptionAccessComponent;
  let fixture: ComponentFixture<OptionAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
