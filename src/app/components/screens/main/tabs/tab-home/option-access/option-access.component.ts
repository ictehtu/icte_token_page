import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-option-access',
  templateUrl: './option-access.component.html',
  styleUrls: ['./option-access.component.scss']
})
export class OptionAccessComponent implements OnInit {

  public name = '';
  public address = '';

  public roles = [
    'Admin',
    'Developer',
    'Blacklist',
    'Whitelist',
    'Unknown'
  ]

  constructor() { }

  ngOnInit() {
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

}
