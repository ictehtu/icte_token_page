import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HashbookComponent } from './hashbook.component';

describe('HashbookComponent', () => {
  let component: HashbookComponent;
  let fixture: ComponentFixture<HashbookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HashbookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HashbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
