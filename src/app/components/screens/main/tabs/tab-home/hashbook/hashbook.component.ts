import { Component, OnInit } from '@angular/core';
import { copy } from 'src/app/lib/copy';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { sleep } from 'icte-order-route/lib/lib/sleep';
import { Router } from '@angular/router';
declare let $: any;

@Component({
  selector: 'app-hashbook',
  templateUrl: './hashbook.component.html',
  styleUrls: ['./hashbook.component.scss']
})
export class HashbookComponent implements OnInit {

  public searchInput = '';
  public action = 'Add';

  public selectedBook: any;
  public errorMsgs = [];

  public editBook: any = {
    bookId: 0,
    book: {
      name: '',
      hash: ''
    }
  }

  public newBook: any = {
    name: '',
    hash: ''
  }

  public $: any;

  public hashBook = [];
  public hashBookFiltered = [];

  constructor(
    public _ids: InternalDataService,
    private _ors: IcteOrderRouteService,
    private router: Router
  ) { }

  async ngOnInit() {
    if (this._ids.hashBook.length == 0) {
      this.openModal('loadingModal');
      let msgId = this._ors.callMethod.getXamarinData('hashbook');
      let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_GetData, msgId);
      if (!result.orderroute.error) {
        this._ids.hashBook = JSON.parse(result.data.Value)
      }
      this.closeModal('loadingModal');
    }
    this.hashBook = this._ids.hashBook;
    this.filterBySearch();
  }

  remove() {
    let book = this.selectedBook;
    let index = this.hashBook.findIndex((el: any) => {
      return (el.name == book.name && el.hash == book.hash);
    });
    this.hashBook.splice(index, 1);
    this.selectedBook = null;
    this.updateHashBook();
  }

  selectBook(book) {
    this.selectedBook = book;
  }

  selectEditBook(i, book: any) {
    let { name, hash } = book;
    this.editBook.bookId = i;
    this.editBook.book = { name, hash };
  }

  saveEdit() {
    this.hashBook[this.editBook.bookId] = this.editBook.book;
    this.updateHashBook();
  }

  createHash() {
    let { name, hash } = this.newBook;
    this.hashBook.push({ name, hash });
    this.newBook.name = '';
    this.newBook.hash = '';
    this.updateHashBook();
  }

  copyHash(addr) {
    copy(addr);
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  validateHash(book: any) {

    this.errorMsgs = [];
    let addressRex = /^(0x)?[0-9a-fA-F]{64}$/g;

    if (book.hash.match(addressRex) == null) {
      this.errorMsgs.push('The hash must be valid (ICTE hash format)');
    } else if (book.hash.match(addressRex).length != 1) {
      this.errorMsgs.push('The hash must be valid (ICTE hash format)');
    }

    let exist = '';
    let bookExist = this.hashBook.find(({ name, hash }) => {
      if (name == book.name) {
        exist = 'name';
        return true;
      } else if (hash == book.hash) {
        exist = 'hash';
        return true;
      }
      return false;
    });

    if (bookExist) {
      this.errorMsgs.push(`The ${exist} already exist`);
    }

    if (book.name.length > 16 || book.name.length < 3) {
      this.errorMsgs.push(`The name must be greater than 3 characters and less than 16 characters`)
    }
  }

  async updateHashBook() {
    this.openModal('loadingModal');
    await sleep(10);

    let msgId = this._ors.callMethod.setXamarinData('hashbook', JSON.stringify(this.hashBook));
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_SaveData, msgId);
    debugger;
    if (!result.orderroute.error) {
      this._ids.hashBook = this.hashBook;
    }
    this.closeModal('loadingModal');
  }

  filterBySearch() {
    if (this.searchInput != '') {
      this.hashBookFiltered = this.hashBook.filter((el: any) => {
        return el.name.toLowerCase().includes(this.searchInput.toLowerCase()) || el.hash.toLowerCase().includes(this.searchInput.toLowerCase())
      })
    } else {
      this.hashBookFiltered = this.hashBook;
    }
  }

  openResult(hash) {
    this._ids.transaction = { hash };
    this.router.navigateByUrl('/main/tab-wallet/payments/transaction-view-result');
  }
}
