import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevSolCompilerComponent } from './dev-sol-compiler.component';

describe('DevSolCompilerComponent', () => {
  let component: DevSolCompilerComponent;
  let fixture: ComponentFixture<DevSolCompilerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevSolCompilerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevSolCompilerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
