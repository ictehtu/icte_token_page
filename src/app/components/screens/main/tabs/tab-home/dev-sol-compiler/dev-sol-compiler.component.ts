import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { copy } from 'src/app/lib/copy';
import { sleep } from 'src/app/lib/sleep';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { WorkerService } from 'src/app/services/worker.service';
import { WorkerMessage } from 'src/worker/app-workers/shared/worker-message.model';
import { WORKER_TOPIC } from 'src/worker/app-workers/shared/worker-topic.constants';
declare var $: any;

@Component({
  selector: 'app-dev-sol-compiler',
  templateUrl: './dev-sol-compiler.component.html',
  styleUrls: ['./dev-sol-compiler.component.scss']
})
export class DevSolCompilerComponent implements OnInit {

  public tempScontractCode = "";
  public scontractCode = "";
  public errors: any = [];
  public inputreadOnly = false;
  public readytoSend = false;
  public compiling = false;

  public userAccount: any;

  workerTopic;
  workerServiceSubscription: Subscription;
  versionLoaded: boolean;
  gasTransaction = '0.005';
  txLimitsData;

  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  placeholderAddress: any;

  constructor(
    private workerService: WorkerService,
    private _ids: InternalDataService,
    private _ors: IcteOrderRouteService) { }

  ngOnInit() {
    this.listenForWorkerResponse();

    this.txLimitsData = this._ids.getFeeForType('CREATE');

    let url = "https://solc-bin.ethereum.org/bin/soljson-v0.7.4+commit.3f05b770.js"
    const workerMessage = new WorkerMessage(WORKER_TOPIC.loadVersion, { url });
    this.workerService.doWork(workerMessage);
  }

  private listenForWorkerResponse() {
    this.workerServiceSubscription = this.workerService.workerUpdate$
      .subscribe(data => this.workerResponseParser(data));
  }

  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this._ids.getWalletData().accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }

  }

  async transfer() {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    let msgId = this._ors.callMethod.reqCustomTx(this._ids.getWalletData().accounts[0].address, "0x0000000000000000000000000000000000000000", "0", String(this.txLimitsData.fee), this.getNonce(), this.scontractCode, this.gasTransaction, "0.000000001", "CREATE");
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('transactionSuccessModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  getNonce() {
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }

  async processInWorker() {

    this.errors = [];
    if (this.readytoSend) {
      this.openModal('modalConfirm');
    } else {
      this.compiling = true;
      let input = {
        language: 'Solidity',
        sources: {
          file: {
            content: this.scontractCode
          }
        },
        settings: {
          outputSelection: {
            '*': {
              '*': ['*']
            }
          }
        }
      };
      const workerMessageCompile = new WorkerMessage(WORKER_TOPIC.compile, { input: JSON.stringify(input) });
      this.workerService.doWork(workerMessageCompile);
    }
  }

  private workerResponseParser(message: WorkerMessage) {
    if (message.topic == WORKER_TOPIC.compile) {
      let res = JSON.parse(message.data.data);
      if (res.errors) {
        for (const error of res.errors) {
          if (error.severity === "error") {
            this.errors.push(error.formattedMessage)
          }
        }
      }

      this.compiling = false;
      if (this.errors.length === 0) {

        if (!res.contracts.file.NewContract) {
          return this.errors.push("Smart contract should be called 'NewContract'");
        }

        if (!res.contracts.file.NewContract.evm.deployedBytecode.linkReferences.file.IcteLib) {
          return this.errors.push("Smart contract needs 'IcteLib'");
        }

        if (Object.values(res.contracts.file.IcteLib.evm.methodIdentifiers).length != 3) {
          return this.errors.push("Error in IcteLib: Expecting 3 methods, found " + Object.values(res.contracts.file.IcteLib.evm.methodIdentifiers).length);
        }

        if (!res.contracts.file.IcteLib.evm.methodIdentifiers['addIPFS(bytes,bytes storage)']) {
          return this.errors.push("IcteLib doesn't have 'getIPFS' method");
        }

        if (!res.contracts.file.IcteLib.evm.methodIdentifiers['getIPFS(string,bytes storage)']) {
          return this.errors.push("IcteLib doesn't have 'getIPFS' method");
        }

        if (!res.contracts.file.IcteLib.evm.methodIdentifiers['compileGoProgram(string,bytes storage)']) {
          return this.errors.push("IcteLib doesn't have 'compileGoProgram' method");
        }

        let references = res.contracts.file.NewContract.evm.bytecode.linkReferences.file.IcteLib;
        let outputString: string = res.contracts.file.NewContract.evm.bytecode.object;

        for (let i = 0; i < references.length; i++) {
          let startAt = references[i].start * 2;
          let len = references[i].length * 2;
          let toReplace = outputString.substring(startAt, startAt + len);
          outputString = outputString.replace(toReplace, '0x13730dc5204bc6a0e4aa1a989afb646308e1a018');
        }

        this.inputreadOnly = true
        this.tempScontractCode = this.scontractCode;
        let bytecode = outputString;
        this.scontractCode = bytecode;
        this.readytoSend = true
      }
    }

    if (message.topic == WORKER_TOPIC.loadVersion) {
      this.versionLoaded = true;
    }
  }

  ngOnDestroy(): void {
    if (this.workerServiceSubscription) {
      this.workerServiceSubscription.unsubscribe();
    }
  }

  getTotal() {
    return this.txLimitsData.fee + Number(this.gasTransaction);
  }


  editBack() {
    this.inputreadOnly = false;
    this.scontractCode = this.tempScontractCode;
    this.readytoSend = false;
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

}
