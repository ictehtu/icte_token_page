import { Component, OnInit } from '@angular/core';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { copy } from 'src/app/lib/copy';
import { sleep } from 'src/app/lib/sleep';
import Web3 from 'web3';
declare var $: any;
@Component({
  selector: 'app-dev-options',
  templateUrl: './dev-options.component.html',
  styleUrls: ['./dev-options.component.scss']
})
export class DevOptionsComponent implements OnInit {

  public functionComplete: string = '';
  public githubURL = '';
  public functionName: string = '';
  public functionParams: any[] = [];
  public renderForm: boolean = false;
  public readyToCall: boolean = false;
  errors: any[] = [];
  userAccount: any;
  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  public readytoSend = false;
  rawCode: string;
  displayAddressBook: boolean;
  scAddress: any;
  addressBookFiltered: any[] = [];

  gasTransaction = '0.005';
  txLimitsData;

  smFunction = '';

  constructor(
    public _ids: InternalDataService,
    private _ors: IcteOrderRouteService
  ) { }

  ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('DOMAINREQUEST');
  }

  getTotal() {
    if (this.txLimitsData.fee)
      return this.txLimitsData.fee + Number(this.gasTransaction);
    return Number(this.gasTransaction)
  }

  compile() {
    const web3 = new Web3();
    this.errors = [];

    try {
      let functionSignatureStr = '';
      let paramsTypes = [];
      let paramsValues = [];

      switch (this.smFunction) {
        case "clear": {
          functionSignatureStr = 'clear()';
          break;
        }
        case "get": {
          functionSignatureStr = 'get()';
          break;
        }
        case "getAndClear": {
          functionSignatureStr = 'getAndClear()';
          break;
        }
      }

      let functionSign = web3.eth.abi.encodeFunctionSignature(functionSignatureStr);
      let encParams = web3.eth.abi.encodeParameters(paramsTypes, paramsValues)
      let paramCode = functionSign + encParams.replace(/0x/g, '');
      this.rawCode = paramCode;
      this.readytoSend = true;


    } catch (error) {
      this.errors.push(error.message)
    }
  }

  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this._ids.getWalletData().accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }
  }

  async transfer() {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    let msgId = this._ors.callMethod.reqCustomTx(this._ids.getWalletData().accounts[0].address, "0x73d6e444ff5a46f620449fad42c45dfa8c1b88b9", "0", String(this.txLimitsData.fee), this.getNonce(), this.rawCode, this.gasTransaction, "0.000000001", "DOMAINREQUEST");
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('successModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

  openConfirm(smFunction) {
    this.smFunction = smFunction;
    this.openModal('modalConfirm');
  }

  compileAndCall() {
    this.compile();
    if (this.errors.length > 0) {
      // TODO: HANDLING ERROR
      return;
    }
    this.transfer();
  }

  getNonce() {
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }
}
