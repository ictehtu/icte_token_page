import { Component, OnInit } from '@angular/core';
import { strToHex } from 'hexyjs';
import { copy } from 'src/app/lib/copy';
import { sleep } from 'src/app/lib/sleep';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import Web3 from 'web3';
declare var $: any;
@Component({
  selector: 'app-dev-upload-ipfs',
  templateUrl: './dev-upload-ipfs.component.html',
  styleUrls: ['./dev-upload-ipfs.component.scss']
})
export class DevUploadIpfsComponent implements OnInit {

  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  public hexadecimalCode: string = '';
  userAccount: any;
  public rawCode;
  readytoSend: boolean = false;
  public dataType = false;
  errors: any[] = [];
  gasTransaction = '0.005';
  txLimitsData;
  clientOrderId: any;


  constructor(public _ids: InternalDataService,
    private _ors: IcteOrderRouteService) { }


  ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('BehaviorStart');
  }


  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this._ids.getWalletData().accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }

  }

  async compile() {
    try {
      let dataToSend = '';
      if (this.dataType) {
        dataToSend = this.hexadecimalCode;
      } else {
        dataToSend = '0x' + strToHex(this.hexadecimalCode)
      }

      let execParams;
      let buf = Buffer.from(dataToSend);
      let uintString = new Uint8Array(buf);
      execParams = {
        d: uintString,
        bhvmsg: 'AdIPFS'
      }

      let msgId = this._ors.callMethod.reqTransferBehavior(String(this.txLimitsData.feeNano), "Add to IPFS", 0, Number(this.clientOrderId), execParams);
      let behavior: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_BehaviorTransaction, msgId);
      this.txHash = behavior.data.hash;
      this.openModal('successModal')
      debugger;

    } catch (error) {
      this.errors.push(error.message)
    }
  }

  compile2() {
    if (this.readytoSend) {
      return this.openModal('modalConfirm');
    }
    const web3 = new Web3();
    this.errors = [];

    try {
      let functionSign = web3.eth.abi.encodeFunctionSignature('ipfsAdd(bytes)');
      let dataToSend = '';
      if (this.dataType) {
        dataToSend = this.hexadecimalCode;
      } else {
        dataToSend = '0x' + strToHex(this.hexadecimalCode)
      }
      let encParams = web3.eth.abi.encodeParameters(['bytes'], [dataToSend])
      let paramCode = functionSign + encParams.replace(/0x/g, '');
      this.rawCode = paramCode;
      this.readytoSend = true;

    } catch (error) {
      this.errors.push(error.message)
    }
  }

  changeStatus() {
    this.dataType = !this.dataType;
  }
  async transfer() {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    let msgId = this._ors.callMethod.reqCustomTx(this._ids.getWalletData().accounts[0].address, this._ids.scAddress, "0", this.txLimitsData.fee, this.getNonce(), this.rawCode, this.gasTransaction, "0.000000001", "DOMAINREQUEST");
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('successModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

  getTotal() {
    return this.txLimitsData.fee + Number(this.gasTransaction);
  }

  getNonce() {
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }

}
