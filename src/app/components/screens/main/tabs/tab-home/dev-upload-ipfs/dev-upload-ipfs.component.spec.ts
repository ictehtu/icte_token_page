import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevUploadIpfsComponent } from './dev-upload-ipfs.component';

describe('DevUploadIpfsComponent', () => {
  let component: DevUploadIpfsComponent;
  let fixture: ComponentFixture<DevUploadIpfsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevUploadIpfsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevUploadIpfsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
