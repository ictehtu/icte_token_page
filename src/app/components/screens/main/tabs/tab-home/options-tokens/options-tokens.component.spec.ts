import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionsTokensComponent } from './options-tokens.component';

describe('OptionsTokensComponent', () => {
  let component: OptionsTokensComponent;
  let fixture: ComponentFixture<OptionsTokensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionsTokensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionsTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
