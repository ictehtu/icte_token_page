import { Component, OnInit } from '@angular/core';
import { sleep } from 'src/app/lib/sleep';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
import Web3 from 'web3';
declare var $: any;

@Component({
  selector: 'app-options-tokens',
  templateUrl: './options-tokens.component.html',
  styleUrls: ['./options-tokens.component.scss']
})
export class OptionsTokensComponent implements OnInit {

  public searchInput = '';
  public name = '';

  public devOptions = {
    title: 'Developer options',
    arr: [
      {
        name: 'Add visual form IPFS hash to token',
        route: '/main/tab-settings/option-hash',
        queryValue: 'AddFormHashIpfsToToken'
      },
      {
        name: 'Add solidity IPFS hash to token',
        route: '/main/tab-settings/option-hash',
        queryValue: 'AddSolIpfsHashToToken'
      },
      {
        name: 'Add C (Go) plugin IPFS hash to token',
        route: '/main/tab-settings/option-hash',
        queryValue: 'AddGoIpfsHashToToken'
      }
    ]
  }
  public adminOptions = {
    title: 'Admin options',
    arr: [
      {
        name: 'Change token name',
        modal: 'tokenNameModal'
      },
      {
        name: 'Select state',
        modal: 'stateModal'
      },
      {
        name: 'Select stage',
        modal: 'stageModal'
      },
      {
        name: 'Set role access',
        route: '/main/tab-settings/option-access'
      },
    ]
  }
  public stages = [
    'Developing',
    'Testing',
    'Production'
  ]
  public states = [
    'Disabled',
    'Active',
    'Halted'
  ]
  public optionsController = true;
  public selectedOptions = {
    title: '',
    arr: []
  };
  token: any;
  errorMsgs: any[];
  smFunction: any;
  txLimitsData: any;
  gasTransaction = '0.005';
  txHash: any;
  smData: any;

  constructor(
    private _ids: InternalDataService,
    private _ors: IcteOrderRouteService
  ) { }

  ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('CALL');
    this.token = this._ids.tokenSelected;
    this.selectedOptions = this.devOptions;
    this.name = this.token.name;
  }

  selectOptions(val: boolean) {
    this.optionsController = val;
    if (this.optionsController) {
      this.selectedOptions = this.devOptions;
    } else {
      this.selectedOptions = this.adminOptions;
    }
  }

  compile() {
    const web3 = new Web3();
    this.errorMsgs = [];
    let functionSignature = '';

    // sm functions
    switch (this.smFunction) {
      case 'setStage': {
        functionSignature = 'setStage(int32, uint8)';
        break;
      }
      case 'setState': {
        functionSignature = 'setState(int32, uint8)';
        break;
      }
      case 'setTokenName': {
        functionSignature = 'setTokenName(int32, uint8)';
        break;
      }
    }

    try {
      let functionSign = web3.eth.abi.encodeFunctionSignature(functionSignature);
      let encParams = web3.eth.abi.encodeParameters(['int32', 'string'], [this.token.id, this.smData])
      let paramCode = functionSign + encParams.replace(/0x/g, '');
      this.transfer(paramCode)

    } catch (error) {
      this.errorMsgs.push(error.message)
    }
  }

  async transfer(paramcode: string) {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    let msgId = this._ors.callMethod.reqCustomTx(this._ids.getWalletData().accounts[0].address, '0x73d6e444ff5a46f620449fad42c45dfa8c1b88b9', "0", this.txLimitsData.fee, '0', paramcode, this.gasTransaction, "0.000000001", "CALL");
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    debugger;
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('successModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  setTransactionFuncion(fnName: string, data: any) {
    this.smFunction = fnName;
    this.smData = data;
    this.compile();
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  filterBySearch() {
    if (this.searchInput != '') {
      return this.selectedOptions.arr.map(e => { if (e.name.toLowerCase().includes(this.searchInput.toLowerCase())) return e })
    } else {
      return this.selectedOptions.arr;
    }
  }

}
