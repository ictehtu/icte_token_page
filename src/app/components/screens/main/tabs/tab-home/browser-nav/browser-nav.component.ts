import { Component, OnInit } from '@angular/core';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
declare var $: any;

@Component({
  selector: 'app-browser-nav',
  templateUrl: './browser-nav.component.html',
  styleUrls: ['./browser-nav.component.scss']
})
export class BrowserNavComponent implements OnInit {

  public tokenId: string = '1';
  constructor(
    public _ors: IcteOrderRouteService
  ) { }

  ngOnInit() {
  }

  public redirectTo() {
    this.openModal("loadingModal");
    this._ors.callMethod.sendLastUrl(window.location.href);
    this._ors.callMethod.xamarinRedirect(this.tokenId);
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

}
