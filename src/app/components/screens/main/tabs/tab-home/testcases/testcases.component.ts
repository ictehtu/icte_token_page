import { Component, OnInit } from '@angular/core';
import { ICTEMarketDataService } from 'src/app/services/ictemarket-data.service';
import { Symbols } from 'icte-market-data/lib/model/Symbols';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InitMsg } from 'src/app/lib/initMsg';
import { WsConnection } from 'src/app/lib/websocket';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { Parser } from 'icte-order-route/lib/parser/Parser';

@Component({
  selector: 'app-testcases',
  templateUrl: './testcases.component.html',
  styleUrls: ['./testcases.component.scss']
})
export class TestcasesComponent implements OnInit {
  public socket;
  public port: number = 0;
  public qtyMsg: number = 1;
  public wsUrl: string = "";
  public status: string = "";
  public wsEndpoint: string = ""
  public logs: string = '';
  public fullurl: string = '';

  public onmessageCounter: number = 0;
  public onmessageLastId: number = 0;

  public onmessageLastType: number = 0;
  public onmessageLastSubtype: number = 0;
  private _endBench: any;
  private _startBench: number;
  timeBench: number;
  onmessageLastByteLength: number;

  constructor(
    private service: ICTEMarketDataService,
    private _ors: IcteOrderRouteService,
    private _ids: InternalDataService
  ) {
  }

  async ngOnInit() {
    let _this = this;


    try {
      this.log('Loading websocket info...');
      this.getWsUrl();

      WsConnection.socket.onmessage = function (event: any) {
        _this._endBench = performance.now();
        let buffer = Buffer.from(event.data)
        let msg = new Parser(buffer.byteLength, buffer);
        _this.log(`Msg received - ${msg.buf.byteLength}`);
        _this.onmessageLastType = msg.getByte(0);
        _this.onmessageLastSubtype = msg.getByte(1);
        _this.onmessageLastId = msg.getInt(10);
        _this.onmessageLastByteLength = msg.buf.byteLength;
        _this.onmessageCounter++;

      };

    } catch (error) {
      this.log(`[error] ${error.message}`);
    }
  }

  sendInitMessage() {
    let secret = this.tokenGen();
    let initMsg: InitMsg = new InitMsg(secret, 7);
    WsConnection.socket.send(initMsg.buf.buffer);
    this.log('InitMsg sent');
  }

  sendEchoSimpleMessage() {
    let msg = this._ors.callMethod.echoWebSocketMsg('a');
    WsConnection.socket.send(msg.buffer);
    this.log(`Msg sent - ${msg.buffer.byteLength} Bytes`);
  }

  sendEchoCustomMessage() {
    // generate echo message
    let content = '';
    for (let i = 0; i < this.qtyMsg; i++) {
      content += 'a';
    }
    let msg = this._ors.callMethod.echoWebSocketMsg(content);

    this._startBench = performance.now();
    WsConnection.socket.send(msg.buffer);
    this.log(`Msg sent - ${msg.buffer.byteLength} Bytes`);

  }

  getBenchTime() {
    return this._endBench = this._startBench;
  }

  getWsBalanceMessage() {
    WsConnection.socket.send(this._ors.callMethod.reqBalanceWebsocket(this._ids.getWalletData().accounts[0].address).buffer);
    this.log('GetBalanceMsg sent');
  }

  public tokenGen() {
    const array = new Uint8Array(32);
    window.crypto.getRandomValues(array);
    return array;
  }

  async getWsUrl() {
    this.fullurl = WsConnection.getFullUrl();
  }

  clearLog() {
    this.logs = '';
  }

  log(msg) {
    this.logs += msg + '\n';
  }

  get s(): Symbols {
    return this.service.d;
  }

}
