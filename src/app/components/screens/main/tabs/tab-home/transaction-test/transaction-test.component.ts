import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { sleep } from 'icte-order-route/lib/lib/sleep';
import { copy } from 'src/app/lib/copy';
import { getMobileOS } from 'src/app/lib/mobileOs';
import { IcteOrderRouteService } from 'src/app/services/icte-order-route.service';
import { InternalDataService } from 'src/app/services/internal-data.service';
declare var $: any;

@Component({
  selector: 'app-transaction-test',
  templateUrl: './transaction-test.component.html',
  styleUrls: ['./transaction-test.component.scss']
})
export class TransactionTestComponent implements OnInit {

  public amount: number;
  public total: number = 0;
  public to: any = '';
  public debug: any;
  public txLimitsData: any;
  public sendData: string = '';
  public fee: string = '';
  public gas: string = '';
  public gasPrice: string = '';
  public txType: string = '';

  public userAccount: any;

  public serialQr: string = "";

  public walletData: any = {};

  public errorMsgs: any = [];

  public txHash: string = '';
  public copyTextTextHash: string = 'Copy hash';
  displayAddressBook: boolean;
  addressBook: any[] = [];
  addressBookFiltered: any[] = [];

  @ViewChild("valueInput") valueEl: ElementRef;
  setExtraSpace: boolean = false;

  constructor(
    private _ors: IcteOrderRouteService,
    private _internalDataService: InternalDataService,
    private router: Router,
    public _ids: InternalDataService
  ) { }

  async ngOnInit() {
    this.txLimitsData = this._ids.getFeeForType('TRANSFER');
    this.walletData = this._internalDataService.getWalletData();
    // this.validateIputs()
    await this.getUserAccount();
    this.addressBook = this._ids.addressBook;
    this.filterBySearch();
  }

  async getUserAccount() {
    if (this._ids.addressSummary != null) {
      this.userAccount = this._ids.addressSummary;
    }
    let msgId = this._ors.callMethod.reqBalance(this.walletData.accounts[0].address);
    let req: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_Balance, msgId);
    if (req.orderroute.error) {
      this.openModal('errorModal');
    } else {
      this._ids.addressSummary = req.data;
      this.userAccount = this._ids.addressSummary;
    }

  }

  async transfer() {
    // TODO: chech nonce
    this.openModal('loadingModal');
    await sleep(10);
    await this.getUserAccount();
    let msgId = this._ors.callMethod.reqCustomTx(this.walletData.accounts[0].address, this.to, this.amount.toString(), this.fee, this.getNonce(), this.sendData, this.gas, this.gasPrice, this.txType);
    let transaction: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_ReqTokenTransfer, msgId);
    debugger;
    this.closeModal('loadingModal');
    if (transaction.orderroute.error) {
      this.openModal('errorModal')
    } else {
      if (transaction.data.success) {
        this.txHash = transaction.data.result;
        this.openModal('transactionSuccessModal')
      } else {
        this.openModal('errorModal');
      }
    }
  }

  getNonce() {
    if (this.userAccount.pendingTransactionCount > 0) {
      return this.userAccount.nonce + this.userAccount.pendingTransactionCount;
    } else {
      return this.userAccount.nonce;
    }
  }

  async reqQRScan() {
    // TODO: Test
    let msgId = this._ors.callMethod.reqQRScan();
    let resp = await this._ors.callMethod.getQRData();
    this.parseQrData(resp);
    this.calculateTotal();
  }

  goToPayments() {
    this.closeModal('transactionSuccessModal')
    this.router.navigateByUrl('/main/tab-wallet/payments')
  }

  calculateTotal() {
    let amount: number = this.amount || 0;
    let fee: number = Number(this.fee) || 0;
    let gas: number = Number(this.gas) || 0;
    let gasPrice: number = Number(this.gasPrice) || 0;
    this.total = amount + (fee + (gas * gasPrice));
  }

  parseQrData(str: string) {
    let walletOrig = "";
    // let walletAdrs = "";
    let walletOrigRegex = /.*:/g;
    let walletAddressRegex = /:.*(.*\?)/g;
    let walletAddressRegex2 = /:.*/g;
    let amountRegex = /amount=.*/g;

    if (str.match(walletOrigRegex)) {
      let wostr = str.match(walletOrigRegex)[0];
      walletOrig = wostr.substr(0, wostr.length - 1);
      this.debug = walletOrig;
      if (str.match(walletAddressRegex)) {
        let wastr = str.match(walletAddressRegex)[0];
        this.to = wastr.substr(1, wastr.length - 2);
        if (str.match(amountRegex)) {
          let astr = str.match(amountRegex)[0];
          this.amount = parseFloat(astr.substr(7));
        } else {
          this.amount = 0.0;
        }
      } else {
        if (str.match(walletAddressRegex2)) {
          let wa2str = str.match(walletAddressRegex2)[0];
          this.to = wa2str.substr(1);
        } else {
          this.to = str;
        }
      }
    }
  }

  validateIputs() {

    this.errorMsgs = [];
    let addressRex = /^(0x)?[0-9a-fA-F]{40}$/g;

    if (this.to.match(addressRex) == null) {
      this.errorMsgs.push('The address must be valid (IEO Address format)');
    } else if (this.to.match(addressRex).length != 1) {
      this.errorMsgs.push('The address must be valid (IEO Address format)');
    }

    if (this.amount == undefined) {
      this.errorMsgs.push('Error: Amount not defined');
    }
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  keyboardExtraSpace(scrollTarget?) {
    if (getMobileOS() == 'android') {
      this.setExtraSpace = !this.setExtraSpace;
      if (scrollTarget)
        this.scroll(scrollTarget);
    }
  }

  scroll(el) {
    document.getElementById(el).scrollIntoView();
  }

  async copyText(value: string) {
    copy(value);
    this.copyTextTextHash = 'Copied!';
    await sleep(1000);
    this.copyTextTextHash = 'Copy hash';
  }

  showList() {
    this.displayAddressBook = true;
  }

  async hideList() {
    await sleep(100);
    this.displayAddressBook = false;
  }

  setAddress(address) {
    this.to = address;
    this.validateIputs();
    this.valueEl.nativeElement.focus();
  }

  filterBySearch() {
    if (this.to != '') {
      this.addressBookFiltered = this.addressBook.filter((el: any) => {
        return el.name.toLowerCase().includes(this.to.toLowerCase()) || el.address.toLowerCase().includes(this.to.toLowerCase());
      })
    } else {
      this.addressBookFiltered = this.addressBook;
    }
  }

}
