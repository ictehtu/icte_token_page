import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-tab-settings-layout',
  templateUrl: './tab-settings-layout.component.html',
  styleUrls: ['./tab-settings-layout.component.scss'],
})
export class TabSettingsLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  public getRouteAnimation(outlet: RouterOutlet) {
    const res =
      outlet.activatedRouteData.num === undefined
        ? -1
        : outlet.activatedRouteData.num;
    return res;
  }

}
