import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabSettingsLayoutComponent } from './tab-settings-layout.component';

describe('TabSettingsLayoutComponent', () => {
  let component: TabSettingsLayoutComponent;
  let fixture: ComponentFixture<TabSettingsLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabSettingsLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabSettingsLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
