import { Component, OnInit } from '@angular/core';
import { RouterOutlet, Router, NavigationEnd, NavigationStart } from '@angular/router';
import { WsConnection } from 'src/app/lib/websocket';

declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public showTabBar: boolean;
  public urls: any = [
    // tab wallet
    '/main/tab-wallet/payments/payments-send',
    '/main/tab-wallet/payments/payments-receive',
    '/main/tab-wallet/get-ieos/invest',
    '/main/tab-wallet/payments/payments-send/sent',
    '/main/tab-wallet/fund/details',
    '/main/tab-wallet/payments/transaction-view',
    '/main/tab-wallet/payments/transaction-view-result',
    '/main/tab-settings/access',

    // tab products
    '/main/tab-products/scan-product',
    '/main/tab-products/product-laod',
    '/main/tab-products/product-form',
    '/main/tab-products/form-rfq',
    '/main/tab-products/request-load',
    '/main/tab-products/offer-list',
    '/main/tab-products/offer-load'
  ]
  public tempCount: number;

  constructor(private router: Router) { }

  ngOnInit() {
    this.showTabBar = true;
    this.mustShowTab();
  }

  getWsStatus() {
    return WsConnection.getConnectionStatus();
  }

  public getRouteAnimation(outlet: RouterOutlet) {
    const res =
      outlet.activatedRouteData.num === undefined
        ? -1
        : outlet.activatedRouteData.num;
    return res;
  }

  mustShowTab() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        $('.modal').modal('hide');
        this.tempCount = 0;
        for (let i = 0; i < this.urls.length; i++) {
          if (this.router.url == this.urls[i]) {
            this.tempCount++;
          }
        }
        if (this.tempCount > 0) {
          this.showTabBar = false;
        } else {
          this.showTabBar = true;
        }
      }
    });
  }
}
