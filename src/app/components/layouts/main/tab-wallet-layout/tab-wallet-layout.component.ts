import { Component, OnInit } from '@angular/core';
import { RouterOutlet, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-tab-wallet-layout',
  templateUrl: './tab-wallet-layout.component.html',
  styleUrls: ['./tab-wallet-layout.component.scss']
})
export class TabWalletLayoutComponent implements OnInit {

  public showTabBar: boolean;
  public urls: any = [
    '/main/tab-wallet/payments/payments-send',
    '/main/tab-wallet/payments/payments-receive',
    '/main/tab-wallet/get-ieos/invest',
    '/main/tab-wallet/payments/payments-send/sent',
    '/main/tab-wallet/fund/details',
    '/main/tab-wallet/payments/transaction-view',
    '/main/tab-wallet/payments/transaction-view-result'
  ]
  public tempCount: number;

  constructor(
    private router: Router
  ) { }
  public getRouteAnimation(outlet: RouterOutlet) {
    const res =
      outlet.activatedRouteData.num === undefined
        ? -1
        : outlet.activatedRouteData.num;
    return res;
  }

  ngOnInit() {
    this.showTabBar = true;
    this.mustShowTab();
  }

  mustShowTab() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.tempCount = 0;
        for (let i = 0; i < this.urls.length; i++) {
          if (this.router.url == this.urls[i]) {
            this.tempCount++;
          }
        }
        if (this.tempCount > 0) {
          this.showTabBar = false;
        } else {
          this.showTabBar = true;
        }
      }
    });
  }

}
