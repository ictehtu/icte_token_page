import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabWalletLayoutComponent } from './tab-wallet-layout.component';

describe('TabWalletLayoutComponent', () => {
  let component: TabWalletLayoutComponent;
  let fixture: ComponentFixture<TabWalletLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabWalletLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabWalletLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
