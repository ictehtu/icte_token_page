import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabCalculatorLayoutComponent } from './tab-calculator-layout.component';

describe('TabCalculatorLayoutComponent', () => {
  let component: TabCalculatorLayoutComponent;
  let fixture: ComponentFixture<TabCalculatorLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabCalculatorLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabCalculatorLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
