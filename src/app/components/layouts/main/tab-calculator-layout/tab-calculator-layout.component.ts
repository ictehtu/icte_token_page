import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-tab-calculator-layout',
  templateUrl: './tab-calculator-layout.component.html',
  styleUrls: ['./tab-calculator-layout.component.scss'],
})
export class TabCalculatorLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public getRouteAnimation(outlet: RouterOutlet) {
    const res =
      outlet.activatedRouteData.num === undefined
        ? -1
        : outlet.activatedRouteData.num;
    return res;
  }

}
