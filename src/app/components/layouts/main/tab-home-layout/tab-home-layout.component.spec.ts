import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabHomeLayoutComponent } from './tab-home-layout.component';

describe('TabHomeLayoutComponent', () => {
  let component: TabHomeLayoutComponent;
  let fixture: ComponentFixture<TabHomeLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabHomeLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabHomeLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
