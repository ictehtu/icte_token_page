import { Component, OnInit } from '@angular/core';
import { Router, RouterOutlet, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-tab-home-layout',
  templateUrl: './tab-home-layout.component.html',
  styleUrls: ['./tab-home-layout.component.scss']
})
export class TabHomeLayoutComponent implements OnInit {

  public showTabBar: boolean;
  public urls: any = [
    '/main/tab-products/scan-product',
    '/main/tab-products/product-laod',
    '/main/tab-products/form-rfq',
    '/main/tab-products/request-load',
    '/main/tab-products/offer-list',
    '/main/tab-products/offer-load'
  ]
  public tempCount: number;
  public displayHeader = false;

  constructor(
    private router: Router
  ) { }
  public getRouteAnimation(outlet: RouterOutlet) {
    const res =
      outlet.activatedRouteData.num === undefined
        ? -1
        : outlet.activatedRouteData.num;
    return res;
  }

  ngOnInit() {
    this.showTabBar = true;
    this.mustShowTab();
  }

  mustShowTab() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.tempCount = 0;
        for (let i = 0; i < this.urls.length; i++) {
          if (this.router.url == this.urls[i]) {
            this.tempCount++;
          }
        }
        if (this.tempCount > 0) {
          this.showTabBar = false;
        } else {
          this.showTabBar = true;
        }
      }
    });
  }

}
