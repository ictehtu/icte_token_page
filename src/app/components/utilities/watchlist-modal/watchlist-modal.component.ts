import { Component, OnInit } from '@angular/core';
import { InternalDataService } from 'src/app/services/internal-data.service';
import { sleep } from 'src/app/lib/sleep';
declare let $: any;

@Component({
  selector: 'app-watchlist-modal',
  templateUrl: './watchlist-modal.component.html',
  styleUrls: ['./watchlist-modal.component.scss']
})
export class WatchlistModalComponent implements OnInit {

  constructor(public _ids: InternalDataService) { }

  ngOnInit() {
  }

  async selectWatchlist(id: number) {
    this.openModal('loadingModal')
    this._ids.selectWatchlist(id);
    await sleep(500);
    this.closeModal('loadingModal')
  }

  openModal(modalId: string) {
    $(`#${modalId}`).modal('show');
  }

  closeModal(modalId: string) {
    $(`#${modalId}`).modal('hide');
  }
}
