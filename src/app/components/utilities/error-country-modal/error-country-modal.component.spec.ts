import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorCountryModalComponent } from './error-country-modal.component';

describe('ErrorCountryModalComponent', () => {
  let component: ErrorCountryModalComponent;
  let fixture: ComponentFixture<ErrorCountryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorCountryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorCountryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
