import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnboardComponent } from './components/layouts/onboard/onboard.component';
import { WelcomeComponent } from './components/screens/onboard/welcome/welcome.component';
import { TabWalletLayoutComponent } from './components/layouts/main/tab-wallet-layout/tab-wallet-layout.component'
import { MainComponent } from './components/layouts/main/main.component';
import { PaymentsComponent } from './components/screens/main/tabs/tab-wallet/payments/payments.component';
import { TabCalculatorLayoutComponent } from './components/layouts/main/tab-calculator-layout/tab-calculator-layout.component';
import { CalculatorComponent } from './components/screens/main/tabs/tab-calculator/calculator/calculator.component';
import { TabSettingsLayoutComponent } from './components/layouts/main/tab-settings-layout/tab-settings-layout.component';
import { MainScreenComponent } from './components/screens/main/tabs/tab-settings/main-screen/main-screen.component';
import { PaymentsSendComponent } from './components/screens/main/tabs/tab-wallet/payments-send/payments-send.component';
import { PaymentsReceiveComponent } from './components/screens/main/tabs/tab-wallet/payments-receive/payments-receive.component';
import { PaymentsSendSentComponent } from './components/screens/main/tabs/tab-wallet/payments-send-sent/payments-send-sent.component';
import { ExitComponent } from './components/screens/onboard/exit/exit.component';
import { TransactionViewComponent } from './components/screens/main/tabs/tab-wallet/transaction-view/transaction-view.component';
import { AddressbookComponent } from './components/screens/main/tabs/tab-settings/addressbook/addressbook.component';
import { InitComponent } from './components/screens/onboard/init/init.component';
import { TestcasesComponent } from './components/screens/main/tabs/tab-home/testcases/testcases.component';
import { TabHomeLayoutComponent } from './components/layouts/main/tab-home-layout/tab-home-layout.component';
import { GeolocationComponent } from './components/screens/main/tabs/tab-settings/geolocation/geolocation.component';
import { BcStatusComponent } from './components/screens/main/tabs/tab-wallet/bc-status/bc-status.component';
import { TransactionTestComponent } from './components/screens/main/tabs/tab-home/transaction-test/transaction-test.component';
import { TransactionViewResultComponent } from './components/screens/main/tabs/tab-wallet/transaction-view-result/transaction-view-result.component';
import { DevOptionsComponent } from './components/screens/main/tabs/tab-home/dev-options/dev-options.component';
import { DevGoCompilerComponent } from './components/screens/main/tabs/tab-home/dev-go-compiler/dev-go-compiler.component';
import { DevUploadIpfsComponent } from './components/screens/main/tabs/tab-home/dev-upload-ipfs/dev-upload-ipfs.component';
import { DevSolCompilerComponent } from './components/screens/main/tabs/tab-home/dev-sol-compiler/dev-sol-compiler.component';
import { DevGetIpfsComponent } from './components/screens/main/tabs/tab-home/dev-get-ipfs/dev-get-ipfs.component';
import { NewTokenComponent } from './components/screens/main/tabs/tab-home/new-token/new-token.component';
import { DeployedTokensComponent } from './components/screens/main/tabs/tab-home/deployed-tokens/deployed-tokens.component';
import { OptionsTokensComponent } from './components/screens/main/tabs/tab-home/options-tokens/options-tokens.component';
import { OptionHashComponent } from './components/screens/main/tabs/tab-home/option-hash/option-hash.component';
import { OptionUrlComponent } from './components/screens/main/tabs/tab-home/option-url/option-url.component';
import { OptionAccessComponent } from './components/screens/main/tabs/tab-home/option-access/option-access.component';
import { HashbookComponent } from './components/screens/main/tabs/tab-home/hashbook/hashbook.component';
import { FirstScreenComponent } from './components/screens/onboard/first-screen/first-screen.component';
import { BrowserNavComponent } from './components/screens/main/tabs/tab-home/browser-nav/browser-nav.component';
import { CryptosComponent } from './components/screens/main/tabs/tab-settings/cryptos/cryptos.component';
import { HomeMainComponent } from './components/screens/main/tabs/tab-home/home-main/home-main.component';

const routes: Routes = [
  {
    path: 'onboard',
    component: OnboardComponent, // onboard layout
    children: [
      {
        path: 'welcome',
        component: WelcomeComponent
      },
      {
        path: 'init',
        component: InitComponent
      },
      {
        path: 'exit',
        component: ExitComponent
      },
      {
        path: 'fs',
        component: FirstScreenComponent
      },
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full',
      },
    ]
  },
  {
    path: 'main',
    component: MainComponent, // main layout - tabs, etc
    children: [
      {
        path: 'tab-wallet',
        component: TabWalletLayoutComponent,
        children: [
          {
            path: 'payments',
            component: PaymentsComponent,
          },
          {
            path: 'payments/payments-send',
            component: PaymentsSendComponent,
          },
          {
            path: 'payments/payments-send/sent',
            component: PaymentsSendSentComponent,
          },
          {
            path: 'payments/payments-receive',
            component: PaymentsReceiveComponent,
          },
          {
            path: 'payments/transaction-view',
            component: TransactionViewComponent,
          },
          {
            path: 'payments/transaction-view-result',
            component: TransactionViewResultComponent,
          },
          {
            path: 'bc-status',
            component: BcStatusComponent,
          },
          {
            path: '',
            redirectTo: 'payments',
            pathMatch: 'full'
          }
        ]
      },
      {
        path: 'tab-calculator',
        data: { num: 2 },
        component: TabCalculatorLayoutComponent,
        children: [
          {
            path: 'calculator',
            component: CalculatorComponent,
            data: { num: 1 }
          },
          {
            path: '',
            redirectTo: 'calculator',
            pathMatch: 'full'
          }
        ]
      },
      {
        path: 'tab-home',
        component: TabHomeLayoutComponent,
        children: [
          {
            path: 'home-main',
            component: HomeMainComponent
          },
          {
            path: 'tests',
            component: TestcasesComponent,
          },
          {
            path: 'transaction-test',
            component: TransactionTestComponent
          },
          {
            path: 'dev-options',
            component: DevOptionsComponent
          },
          {
            path: 'dev-go-compiler',
            component: DevGoCompilerComponent
          },
          {
            path: 'browser',
            component: BrowserNavComponent
          },
          {
            path: 'new-token',
            component: NewTokenComponent
          },
          {
            path: 'options-tokens',
            component: OptionsTokensComponent
          },
          {
            path: 'option-hash',
            component: OptionHashComponent
          },
          {
            path: 'option-access',
            component: OptionAccessComponent
          },
          {
            path: 'option-url',
            component: OptionUrlComponent
          },
          {
            path: 'dev-upload-ipfs',
            component: DevUploadIpfsComponent
          },
          {
            path: 'dev-get-ipfs',
            component: DevGetIpfsComponent
          },
          {
            path: 'deployed-tokens',
            component: DeployedTokensComponent
          },
          {
            path: 'dev-sol-compiler',
            component: DevSolCompilerComponent
          },
          {
            path: 'hashbook',
            component: HashbookComponent
          },
          {
            path: '',
            redirectTo: 'home-main',
            // redirectTo: 'offer-list',
            pathMatch: 'full'
          }
        ]
      },
      {
        path: 'tab-settings',
        component: TabSettingsLayoutComponent,
        children: [
          {
            path: 'settings-main',
            component: MainScreenComponent,
          },
          {
            path: 'addressbook',
            component: AddressbookComponent,
          },
          {
            path: 'geolocation',
            component: GeolocationComponent,
          },
          {
            path: 'cryptos',
            component: CryptosComponent,
          },
          {
            path: '',
            redirectTo: 'settings-main',
            pathMatch: 'full'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'tab-home',
        pathMatch: 'full',
      },
    ]
  },
  {
    path: '',
    redirectTo: 'onboard',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'disabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
