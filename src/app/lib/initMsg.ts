import { Parser } from "icte-market-data/lib/parser/Parser";
import { Constant } from "icte-order-route/lib/model/Constant";

export class InitMsg extends Parser {

    public static SECRET_LENGTH = 294;

    public static Fld_Timestamp = 2;
    public static Fld_Level = 10;
    public static Fld_Secret = 14;
    public static MsgSize = 312;

    constructor(secret, level?: number, buf?) {
        if (level) {
            super(InitMsg.MsgSize);
            this.setByte(Constant.Fld_Type, Constant.Msg_Chain);
            this.setByte(Constant.Fld_SubType, Constant.Sub_HandshakeInit);
            this.setLong(InitMsg.Fld_Timestamp, new Date().getTime());
            this.setInt(InitMsg.Fld_Level, level);
            this.setBytes(InitMsg.Fld_Secret, secret);
        } else {
            if (secret) {
                super(secret);
            } else {
                super(InitMsg.MsgSize, buf)
            }

        }
    }

    // public InitMsg(ByteBuf buf) {
    //     super(buf);
    // }

    // public validate() {
    //     return this.getSecret() != null && this.getSecret().length == InitMsg.SECRET_LENGTH
    //             && getTimestamp() > 0;
    // }

    public getSecret() {
        return this.getBytes(InitMsg.Fld_Secret);
    }

    public getTimestamp() {
        return this.getLong(InitMsg.Fld_Timestamp);
    }

    public getLevel() {
        return this.getInt(InitMsg.Fld_Level);
    }

    setLong(offset: number, value: number) {
        this.buf.writeFloatBE(value, offset);
    }

    getLong(offset: number): number {
        return this.buf.readFloatBE(offset);
    }
}