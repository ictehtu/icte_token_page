let mem: any[] = [];
/**
 * Generate a int random unique number.
 * @param min - Min range
 * @param max - Max range
 */
export function random(min?: number, max?: number): number {
    let minNumber: number, maxNumber: number, random: number;
    minNumber = min || 1;
    maxNumber = max || 32767;
    while (true) {
        let r = Math.floor(Math.random() * maxNumber) + minNumber;
        if (mem.indexOf(r) === -1) {
            mem.push(r);
            break;
        }
    }
    random = mem[mem.length - 1];
    return random;
}