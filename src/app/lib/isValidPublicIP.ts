/**
 * Function that check if ip is valid and public.
 * @param ip - Ip to check.
 */
function isPrivateIP(ip: string): boolean {
    var parts = ip.split('.');
    return parts[0] === '10' || 
    (parts[0] === '172' && (parseInt(parts[1], 10) >= 16 && parseInt(parts[1], 10) <= 31)) || 
    (parts[0] === '192' && parts[1] === '168');
}
export function  isValidPublicIP(ip: string): boolean{
    if (!(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)))
    {
        return false;
    }
    return !isPrivateIP(ip);
}