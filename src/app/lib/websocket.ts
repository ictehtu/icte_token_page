export class WsConnection {

    private static port: number;
    private static wsUrl: string = "ws://127.0.0.1";
    private static wsEndpoint: string = "/bc/001";
    public static socket;
    public static isConnected: string = '1';

    constructor() {
    }

    static setPort(port: number) {
        WsConnection.port = port;
    }

    static getFullUrl() {
        return `${WsConnection.wsUrl}:${WsConnection.port}${WsConnection.wsEndpoint}`;
    }

    static getConnectionStatus() {
        return WsConnection.isConnected;
    }

    static startConnection() {
        debugger;
        try {
            WsConnection.socket = new WebSocket(`${WsConnection.wsUrl}:${WsConnection.port}${WsConnection.wsEndpoint}`);
            // WsConnection.socket = new WebSocket(`wss://echo.websocket.org`);
            WsConnection.socket.binaryType = "arraybuffer";

            WsConnection.socket.onopen = function (event) {
                WsConnection.isConnected = '1';
                console.log('[WEBSOCKET] Connection open')
                debugger
            }
            // WsConnection.socket.onmessage = function (event) {
            //     console.log('[WEBSOCKET] new message');
            //     let msg = new InitMsg(null, null, event.data);
            //     WsConnection.socket.send(msg.buf.buffer);
            //     debugger
            // }

            WsConnection.socket.onclose = function (event) {
                WsConnection.isConnected = '0';
                console.log('[WEBSOCKET] Connection close')
                debugger
            }
        } catch (error) {
            debugger;
        }
    }
}