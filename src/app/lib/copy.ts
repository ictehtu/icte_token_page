/**
 * Function that copy a string to the user clipboard.
 * @param value - String to copy.
 */
export function copy(value: string): void {
    let selBox = window.document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = value;
    window.document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    window.document.execCommand('copy');
    window.document.body.removeChild(selBox);
}