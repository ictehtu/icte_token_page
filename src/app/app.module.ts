import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InViewportModule } from 'ng-in-viewport';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from "@angular/forms";
import { OnboardComponent } from './components/layouts/onboard/onboard.component';
import { LoadingModalComponent } from './components/utilities/loading-modal/loading-modal.component';
import { WelcomeComponent } from './components/screens/onboard/welcome/welcome.component';
import { HeaderComponent } from './components/screens/onboard/header/header.component';
import { ICTEMarketDataService } from './services/ictemarket-data.service';
import { InternalDataService } from './services/internal-data.service';
import { BlockchainApiService } from './services/blockchain-api.service';
import { QRCodeModule } from 'angularx-qrcode';
import { MainComponent } from './components/layouts/main/main.component';
import { TabWalletLayoutComponent } from './components/layouts/main/tab-wallet-layout/tab-wallet-layout.component';
import { MainHeaderComponent } from './components/utilities/main-header/main-header.component';
import { PaymentsComponent } from './components/screens/main/tabs/tab-wallet/payments/payments.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalculatorComponent } from './components/screens/main/tabs/tab-calculator/calculator/calculator.component';
import { TabCalculatorLayoutComponent } from './components/layouts/main/tab-calculator-layout/tab-calculator-layout.component';
import { TabSettingsLayoutComponent } from './components/layouts/main/tab-settings-layout/tab-settings-layout.component';
import { MainScreenComponent } from './components/screens/main/tabs/tab-settings/main-screen/main-screen.component';
import { PaymentsSendComponent } from './components/screens/main/tabs/tab-wallet/payments-send/payments-send.component';
import { PaymentsReceiveComponent } from './components/screens/main/tabs/tab-wallet/payments-receive/payments-receive.component';
import { PaymentsSendSentComponent } from './components/screens/main/tabs/tab-wallet/payments-send-sent/payments-send-sent.component';
import { WatchlistModalComponent } from './components/utilities/watchlist-modal/watchlist-modal.component';
import { ExitComponent } from './components/screens/onboard/exit/exit.component';
import { ErrorComponent } from './components/utilities/error/error.component';
import { TransactionViewComponent } from './components/screens/main/tabs/tab-wallet/transaction-view/transaction-view.component';
import { AddressbookComponent } from './components/screens/main/tabs/tab-settings/addressbook/addressbook.component';
import { InitComponent } from './components/screens/onboard/init/init.component';
import { TestcasesComponent } from './components/screens/main/tabs/tab-home/testcases/testcases.component';
import { ErrorCountryModalComponent } from './components/utilities/error-country-modal/error-country-modal.component';
import { TabHomeLayoutComponent } from './components/layouts/main/tab-home-layout/tab-home-layout.component';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { GeolocationComponent } from './components/screens/main/tabs/tab-settings/geolocation/geolocation.component';
import { BcStatusComponent } from './components/screens/main/tabs/tab-wallet/bc-status/bc-status.component';
import { TransactionTestComponent } from './components/screens/main/tabs/tab-home/transaction-test/transaction-test.component';
import { WorkerService } from './services/worker.service';
import { TransactionViewResultComponent } from './components/screens/main/tabs/tab-wallet/transaction-view-result/transaction-view-result.component';
import { DevOptionsComponent } from './components/screens/main/tabs/tab-home/dev-options/dev-options.component';
import { DevGoCompilerComponent } from './components/screens/main/tabs/tab-home/dev-go-compiler/dev-go-compiler.component';
import { DevUploadIpfsComponent } from './components/screens/main/tabs/tab-home/dev-upload-ipfs/dev-upload-ipfs.component';
import { DevSolCompilerComponent } from './components/screens/main/tabs/tab-home/dev-sol-compiler/dev-sol-compiler.component';
import { DevGetIpfsComponent } from './components/screens/main/tabs/tab-home/dev-get-ipfs/dev-get-ipfs.component';
import { NewTokenComponent } from './components/screens/main/tabs/tab-home/new-token/new-token.component';
import { DeployedTokensComponent } from './components/screens/main/tabs/tab-home/deployed-tokens/deployed-tokens.component';
import { OptionsTokensComponent } from './components/screens/main/tabs/tab-home/options-tokens/options-tokens.component';
import { OptionUrlComponent } from './components/screens/main/tabs/tab-home/option-url/option-url.component';
import { OptionHashComponent } from './components/screens/main/tabs/tab-home/option-hash/option-hash.component';
import { OptionAccessComponent } from './components/screens/main/tabs/tab-home/option-access/option-access.component';
import { IcteOrderRouteService } from './services/icte-order-route.service';
import { ICTEOrderRoute } from 'icte-order-route/lib';
import { HashbookComponent } from './components/screens/main/tabs/tab-home/hashbook/hashbook.component';
import { FirstScreenComponent } from './components/screens/onboard/first-screen/first-screen.component';
import { BrowserNavComponent } from './components/screens/main/tabs/tab-home/browser-nav/browser-nav.component';
import { CryptosComponent } from './components/screens/main/tabs/tab-settings/cryptos/cryptos.component';
import { HomeMainComponent } from './components/screens/main/tabs/tab-home/home-main/home-main.component';


@NgModule({
  declarations: [
    AppComponent,
    OnboardComponent,
    WelcomeComponent,
    LoadingModalComponent,
    HeaderComponent,
    MainComponent,
    TabWalletLayoutComponent,
    MainHeaderComponent,
    PaymentsComponent,
    CalculatorComponent,
    TabCalculatorLayoutComponent,
    TabSettingsLayoutComponent,
    MainScreenComponent,
    PaymentsSendComponent,
    PaymentsReceiveComponent,
    PaymentsSendSentComponent,
    WatchlistModalComponent,
    ExitComponent,
    ErrorComponent,
    TransactionViewComponent,
    AddressbookComponent,
    InitComponent,
    TestcasesComponent,
    ErrorCountryModalComponent,
    TabHomeLayoutComponent,
    GeolocationComponent,
    BcStatusComponent,
    TransactionTestComponent,
    TransactionViewResultComponent,
    DevOptionsComponent,
    DevGoCompilerComponent,
    DevUploadIpfsComponent,
    DevSolCompilerComponent,
    DevGetIpfsComponent,
    NewTokenComponent,
    DeployedTokensComponent,
    OptionsTokensComponent,
    OptionUrlComponent,
    OptionHashComponent,
    OptionAccessComponent,
    HashbookComponent,
    FirstScreenComponent,
    BrowserNavComponent,
    CryptosComponent,
    HomeMainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InViewportModule,
    FormsModule,
    QRCodeModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DeviceDetectorModule
  ],
  providers: [ICTEMarketDataService, InternalDataService, BlockchainApiService, IcteOrderRouteService, ICTEOrderRoute, WorkerService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
