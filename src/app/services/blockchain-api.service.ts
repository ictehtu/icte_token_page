import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Password } from 'src/app/lib/password';
import { InternalDataService } from './internal-data.service';
import { IcteOrderRouteService } from './icte-order-route.service';
import { AgentsApiService } from './agents-api.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  })
};

export type WalletState = { wallet: Wallet, password: Password } | undefined

export interface Wallet {
  version: number
  network: string
  cipher: { salt: string, iv: string }
  accounts: Account[]
}

export interface Account {
  address: string
  encrypted: string
}

@Injectable({
  providedIn: 'root'
})
export class BlockchainApiService {
  // public domain = window.location.hostname.replace('wallet.', '');
  // public endpoint = `https://icte-worker.cund.org/?component=execChainServiceCmd&cmd=`;
  public endpoint = `http://192.168.1.120:8080/api/bc/`; // Eivar local endpoint

  constructor(
    private http: HttpClient,
    public _ids: InternalDataService,
    public _ors: IcteOrderRouteService,
    private _aas: AgentsApiService
  ) { }

  async getFunds() {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'funds'
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getFundsDetailsByIndex(address) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'funds/detailsByIndex'
        },
        {
          param: 'address',
          value: address
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getJoinedFunds(address) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'account/votes'
        },
        {
          param: 'address',
          value: address
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getAccount2(address) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'account'
        },
        {
          param: 'address',
          value: address
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getTransaction(hash) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'transaction'
        },
        {
          param: 'hash',
          value: hash
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async getTransactionResult(hash) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'transaction-result'
        },
        {
          param: 'hash',
          value: hash
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async getBlacklistedCountries() {
    let payload = {
      comp: 'getBlacklistedCountries',
      params: [
        {
          param: 'none',
          value: 'none'
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async getInfo() {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'info'
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getProductTransactionsPending(address, from, to) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'account/pending-products'
        },
        {
          param: 'address',
          value: address
        },
        {
          param: 'from',
          value: from
        },
        {
          param: 'to',
          value: to
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async getProductTransactions(address, count) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'account/product-transactions'
        },
        {
          param: 'address',
          value: address
        },
        {
          param: 'count',
          value: count
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async getTransactions(address, from, to) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'account/transactions'
        },
        {
          param: 'address',
          value: address
        },
        {
          param: 'from',
          value: from
        },
        {
          param: 'to',
          value: to
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async listProducts(stage: number) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'list-products'
        },
        {
          param: 'stage',
          value: stage
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async listProductsTokens() {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'list-tokens'
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getPendingTransactions(address, from, to) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'account/pending-transactions'
        },
        {
          param: 'address',
          value: address
        },
        {
          param: 'from',
          value: from
        },
        {
          param: 'to',
          value: to
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getFees() {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'transaction-limit-list'
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async addDomain(domainName) {
    let payload = {
      comp: 'addDomain',
      params: [
        {
          param: 'domainName',
          value: domainName
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getVotes(address) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'votes'
        },
        {
          param: 'delegate',
          value: address
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async getDelegate(address) {
    let payload = {
      comp: 'execChainServiceCmd',
      params: [
        {
          param: 'cmd',
          value: 'delegate'
        },
        {
          param: 'address',
          value: address
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  getIeos(coinbase, coin) {
    return "18cBEMRxXHqzWWCxZNtU91F5sbUNKhL5PX";
    //return this.http.get(`${this.endpoint}/get-ieos?coinbase=${coinbase}&coin=${coin}`, httpOptions);
  }
  getUrlBase() {
    return this.endpoint;
  }

  async getAndLoadDomainsList() {
    let payload = {
      comp: 'listDomains',
      params: [
        {
          param: 'null',
          value: 'null'
        },
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    let arrayList = result.data.split('\n');
    return arrayList;
  }
  async getLocationByDomain(domain) {
    let payload = {
      comp: 'getLocationByDomain',
      params: [
        {
          param: 'domain',
          value: domain
        },
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return result;
  }
  async listCities() {
    let payload = {
      comp: 'listCities',
      params: [
        {
          param: 'none',
          value: null
        },
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return result;
  }
  async getDomainByIndex(index) {
    let payload = {
      comp: 'getDomainById',
      params: [
        {
          param: 'index',
          value: index
        },
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return result;
  }

  async loadGetIeoWatchlist() {
    let payload = {
      comp: 'listSymbolsByStatus',
      params: [
        {
          param: 'status',
          value: '1'
        },
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    if (result.orderroute.error) {
      return [];
    }
    let arrayList = result.data.split('\n');
    return arrayList;
  }

  processExchangeOrder() {
    return true;
  }

  async scanProductTransactions(domain, skipHash?: boolean) {
    // Satatus:
    // pending - transaction is pending
    // pending-all - there is transactions pending
    // success - transaction found in completed transactions
    // not-found - transaction not found
    // error - error trying to fetch transactions

    let pendingTxs = [];
    let txs = [];
    let hash;

    try {
      // GET HASH
      if (!skipHash) {
        let respHash = await this._aas.getHash(domain, this._ids.getWalletData().accounts[0].address);
        if (respHash.orderroute.error || !respHash.data.success) {
          return 'not-found';
        }
        if (!respHash.data.result.includes('0x')) {
          hash = `0x${respHash.data.result}`;
        } else {
          hash = respHash.data.result
        }
      }
      // GET PRODUCT PENDING TXS
      let respPendingTxs = await this.getProductTransactionsPending(this._ids.getWalletData().accounts[0].address, 0, 999);
      if (!respPendingTxs.orderroute.error && respPendingTxs.data.success) {
        if (respPendingTxs.data.result.length > 0) {
          pendingTxs = respPendingTxs.data.result;
          // IF SKIP HASH IS TRUE, COMPARE DOMAIN INSTEAD
          if (skipHash) {
            let indexPendingTx = false;
            for (const tx of pendingTxs) {
              let data = tx.data.replace('0x', '');
              let txDecodedData = this._ors.callMethod.decodeProductSecretData(data);
              if (txDecodedData.domain == domain) {
                indexPendingTx = true;
                break;
              }
            }
            if (indexPendingTx) {
              // TX EXIST IN PENDING
              return 'pending-all';
            }
            return 'not-found-all';
          } else {
            let indexPendingTx = pendingTxs.findIndex((e: any) => { return e.hash == hash });
            if (indexPendingTx != -1) {
              // TX EXIST IN PENDING
              return 'pending';
            }
          }
        }
      } else {
        return 'error';
      }
      // GET PRODUCTS TXS
      let respTxs = await this.getProductTransactions(this._ids.getWalletData().accounts[0].address, 999);
      if (!respTxs.orderroute.error && respTxs.data.success) {
        if (respTxs.data.result.length > 0) {
          txs = respTxs.data.result;
          // IF SKIP HASH IS TRUE, COMPARE DOMAIN INSTEAD
          if (skipHash) {
            let indexTx = false;
            for (const tx of txs) {
              let data = tx.data.replace('0x', '');
              let txDecodedData = this._ors.callMethod.decodeProductSecretData(data);
              if (txDecodedData.domain == domain) {
                this._ids.productTx = tx;
                indexTx = true;
                break;
              }
            }
            if (indexTx) {
              // TX EXIST
              return 'success';
            }
            return 'not-found';
          } else {
            let indexTx = txs.findIndex((e: any) => { return e.hash == hash });
            if (indexTx != -1) {
              // TX EXIST
              this._ids.productTx = txs[indexTx];
              return 'success';
            }
          }
        }
        return 'not-found';
      } else {
        return 'error';
      }
    } catch (error) {
      return 'error'
    }
  }
  async getProductTransactionsDomains() {
    let pendingTxs = [];
    let txs = [];
    let domains = [];
    let hash;
    let domainRegex = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/;
    try {
      // GET PRODUCT PENDING TXS
      let respPendingTxs = await this.getProductTransactionsPending(this._ids.getWalletData().accounts[0].address, 0, 999);
      if (!respPendingTxs.orderroute.error && respPendingTxs.data.success) {
        if (respPendingTxs.data.result.length > 0) {
          pendingTxs = respPendingTxs.data.result;
          // IF SKIP HASH IS TRUE, COMPARE DOMAIN INSTEAD
          for (const tx of pendingTxs) {
            let data = tx.data.replace('0x', '');
            let txDecodedData = this._ors.callMethod.decodeProductSecretData(data);
            let match = txDecodedData.domain.match(domainRegex);
            if (match && match[0]) {
              domains.push(txDecodedData.domain);
            }
          }
        }
      } else {
        return [];
      }
      // GET PRODUCTS TXS
      let respTxs = await this.getProductTransactions(this._ids.getWalletData().accounts[0].address, 999);
      if (!respTxs.orderroute.error && respTxs.data.success) {
        if (respTxs.data.result.length > 0) {
          txs = respTxs.data.result;
          // IF SKIP HASH IS TRUE, COMPARE DOMAIN INSTEAD
          for (const tx of txs) {
            let data = tx.data.replace('0x', '');
            let txDecodedData = this._ors.callMethod.decodeProductSecretData(data);
            let match = txDecodedData.domain.match(domainRegex);
            if (match && match[0]) {
              domains.push(txDecodedData.domain);
            }
          }
        }
      } else {
        return [];
      }

      // debugger;
      return domains;
    } catch (error) {
      return []
    }
  }
}
