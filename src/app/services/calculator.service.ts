import { Injectable } from '@angular/core';
import { ICTEMarketDataService } from './ictemarket-data.service';
import { Symbols } from 'icte-market-data/lib/model/Symbols';
import { sleep } from '../lib/sleep';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {
  data: Symbols;
  private initialTokensEnabled: boolean = false;

  public displayArray: any = [];

  constructor(private service: ICTEMarketDataService) { }

  get s(): Symbols {
    return this.service.d;
  }

  async enableInitialTokens() {
    while (true) {
      if (this.s && this.s.getSymbolArray()) {
        if (!this.initialTokensEnabled) {
          for (let i = 0; i <= this.s.getSymbolArray().length; i++) {
            this.s.enableSymbol(i);
          }
          this.initialTokensEnabled = true;
        }
        break;
      }
      await sleep(100);
    }
  }

  setBenchmark(rowId: number) {
    // let symbolId = this.s.getSymbolId(rowId);
    this.s.changeBenchmark(rowId);
  }

  getSymbolIcon(rowId: number): string {
    return this.s.isSymbolEnabled(rowId) ? this.s.getOrigSymbolName(rowId) + '.png' : this.s.getOrigSymbolName(rowId) + '-off.svg';
  }

  enableAllSymbols() {
    for (let i = 0; i < this.s.arr.length; i++) {
      if (!this.s.isSymbolEnabled(i)) {
        this.s.changeSymbolStatus(i);
      }
    }
  }

  disableAllSymbols() {
    for (let i = 0; i < this.s.arr.length; i++) {
      if (this.s.isSymbolEnabled(i)) {
        this.s.changeSymbolStatus(i);
      }
    }
  }

  changeSymbolStatus(rowId: number) {
    this.s.changeSymbolStatus(rowId);
  }

  getSymbolIconSrc(rowId: number): string {
    return this.s.isSymbolEnabled(rowId) ? "assets/images/minus.png" : "assets/images/plus.png";
  }

  getSelectedClass(rowId: number): string {
    return this.s.getBenchmarkName() == this.s.getOrigSymbolName(rowId) ? 'bg-secondary text-light' : ' ';
  }

  getDisabledClass(rowId: number) {
    return this.getSymbolStatus(rowId) ? "col-disabled" : "";
  }

  getInputDisabledClass(rowId: number) {
    return this.getSymbolStatus(rowId) ? "input-disabled" : "";
  }

  getSymbolStatus(rowId: number): boolean {
    return this.s.isSymbolEnabled(rowId) ? false : true;
  }

  filterBenchRow(rowId) {
    return this.s.getBenchmarkId() == this.s.getSymbolId(rowId) ? false : true;
  }

  filterSearch(tokenName: string, input?: string) {
    if (input) {
      if (tokenName.toUpperCase().includes(input.toUpperCase())) {
        return true;
      }
      return false;
    }
    return true;
  }

  // Should be here
  // unsubscribePoloniexSymbol(symbol: number) {
  //   this.s.unsubscribePoloniexSymbol(symbol)
  // }

}
