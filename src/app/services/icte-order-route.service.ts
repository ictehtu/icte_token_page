import { Injectable } from '@angular/core';
import { ICTEOrderRoute } from 'icte-order-route/lib';

@Injectable({
  providedIn: 'root'
})
export class IcteOrderRouteService {

  constructor(private _or: ICTEOrderRoute) { }

  get callMethod() {
    return this._or;
  }
  get types() {
    return this._or.getConstant();
  }
}
