import { Injectable } from '@angular/core';
import { ICTEMarketDataService } from './ictemarket-data.service';
import { Symbols } from 'icte-market-data/lib/model/Symbols';
import { Password } from "src/app/lib/password";
import { sleep } from '../lib/sleep';
import { IcteOrderRouteService } from './icte-order-route.service';
import { Parser } from 'icte-order-route/lib/parser/Parser';

@Injectable({
  providedIn: 'root'
})
export class InternalDataService {
  public state;

  public scAddress = '0x13866e0116f94650e910922e40f419cd2b8ad588';
  
  public tokenManagerAddress = '0xab9b0ac349d645200e2ae22d5edd6b58655b6c98';
  public tokenCompilerAddress = '';
  public tokenLibraryAddress = '';

  public tempAddressToSave: any = false;

  public NANO = 10e8;

  private data = {}
  private readyOrders: any = [];
  private orderEntry: any = {}
  private ieoData: any = {};
  private walletData: any = {};
  private password: Password;
  private mockSymbols = {
    balance: [],
    qty: []
  }
  public addressSummary: any = null;
  public watchlistSelected = 1;
  public watchlists: any = [{
    name: "Get IEO's",
    coins: [],
    locked: true
  },
  {
    name: "Default",
    coins: [
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30
    ]
  },
  {
    name: "Favorites",
    coins: [
      0, 1, 2, 3, 4, 5
    ]
  }
  ];

  public domainList = [];
  public transaction: any;
  public fundDetailsData: string = '';

  private mockInvestSymbols = {
    isInit: false,
    balance: [],
    qty: []
  }
  private calculatorSymbols = {
    qty: []
  }
  private defaultWatchlistLoaded: boolean = false;
  private isMockEnabled = false;
  private exchangePlatforms = {
    lastId: 0,
    array: [{
      "id": 0,
      "name": "OKEx",
      "url": "https://www.okex.com/",
      "api_key": "",
      "enabled": true
    },
    {
      "id": 1,
      "name": "Binance",
      "url": "https://www.binance.com/en",
      "api_key": "",
      "enabled": false
    },
    {
      "id": 2,
      "name": "DigiFinex",
      "url": "https://www.digifinex.com/en-ww/",
      "api_key": "",
      "enabled": true
    },
    {
      "id": 3,
      "name": "BW",
      "url": "https://www.bw.com/",
      "api_key": "",
      "enabled": false
    },
    {
      "id": 4,
      "name": "CoinBene",
      "url": "https://www.coinbene.com/",
      "api_key": "",
      "enabled": false
    },
    {
      "id": 5,
      "name": "ZB COM",
      "url": "https://www.zb.com/",
      "api_key": "",
      "enabled": false
    },
    {
      "id": 6,
      "name": "Bit-Z",
      "url": "https://www.bit-z.com/",
      "api_key": "",
      "enabled": false
    },
    {
      "id": 7,
      "name": "Coineal",
      "url": "https://www.coineal.com/#en_US",
      "api_key": "",
      "enabled": false
    },
    {
      "id": 8,
      "name": "Bibox",
      "url": "https://www.bibox.com/",
      "api_key": "",
      "enabled": false
    },
    {
      "id": 9,
      "name": "IDAX",
      "url": "https://www.idax.pro/#/",
      "api_key": "",
      "enabled": false
    },
    {
      "id": 10,
      "name": "Huobi Global",
      "url": "https://www.hbg.com/en-us/",
      "api_key": "",
      "enabled": false
    }]
  }

  public fundlist = {
    type: '',
    funds: [],
    joinedfunds: []
  }

  public addressBook = [];
  public hashBook = [];
  public walletName: string = '';
  public transactions: any = [];
  public globalInterval: any = 0;
  loginData: any;
  openAction: string;
  INTERVAL_MS: number = 15000;
  public isBlackListed: boolean = false;
  registerDomainData: any;
  registerDomain: string;
  domainDashboard: string;
  rawData: any;
  productTx: any;
  nodes: any;
  public nodePackagePrice: number;
  public minTxFee: number = 0.0025;
  globalFees: any[] = [];
  tokenSelected: any;

  constructor(private service: ICTEMarketDataService, private _ors: IcteOrderRouteService) {
  }

  get s(): Symbols {
    return this.service.d;
  }

  setData(data) {
    this.data = data;
  }

  getGlobalInterval() {
    return this.globalInterval;
  }

  getData() {
    return this.data;
  }

  setReadyOrders(data) {
    this.readyOrders = data;
  }

  getReadyOrders() {
    return this.readyOrders;
  }

  setOrderEntry(data) {
    this.orderEntry = data;
  }

  getOrderEntry() {
    return this.orderEntry;
  }

  setIeoData(data) {
    this.ieoData = data;
  }

  getIeoData() {
    return this.ieoData;
  }

  setWalletData(data) {
    this.walletData = data;
  }

  getWalletData() {
    return this.walletData;
  }

  setMock(mock) {
    this.mockSymbols = mock;
  }

  getMock() {
    return this.mockSymbols;
  }
  setCalculatorMock(mock) {
    this.mockSymbols = mock;
  }

  getCalculatorMock() {
    return this.calculatorSymbols;
  }

  setMockInvest(mock) {
    this.mockInvestSymbols = mock;
  }

  getMockInvest() {
    return this.mockInvestSymbols;
  }

  getExchangePlatforms() {
    return this.exchangePlatforms;
  }

  setExchangePlatforms(newData) {
    this.exchangePlatforms = newData;
  }

  validateInput(rowId) {
    let symbId = this.s.getSymbolId(rowId);
    if (this.mockSymbols.qty[symbId] > this.mockSymbols.balance[symbId] || this.mockSymbols.qty[symbId] < 0 || this.mockSymbols.qty[symbId] == null) {
      return false;
    } else {
      return true;
    }
  }

  setPassword(password: Password) {
    this.password = password;
  }
  getPassword(): Password {
    return this.password
  }

  async loadMockSymbols() {
    while (true) {
      if (this.s && this.s.getSymbolArray()) {
        break;
      }
      await sleep(100);
    }
    if (!this.isMockEnabled) {
      let rNum;
      for (let i = 0; i < this.s.getSymbolArray().length; i++) {
        rNum = Math.floor((Math.random() * 250) + 1);
        this.mockSymbols.balance[i] = rNum;
        this.mockSymbols.qty[i] = rNum;
        this.mockInvestSymbols.qty[i] = 0;
        this.calculatorSymbols.qty[i] = 0;
      }
      this.isMockEnabled = true;
    }
  }

  // Watchlists

  addWatchlist(watchlist) {
    this.watchlists.push(watchlist);
  }

  getWatchlists() {
    return this.watchlists;
  }

  removeWatchlist(id: number) {
    if (this.watchlistSelected >= id) {
      this.selectWatchlist(id - 1);
    }
    this.watchlists.splice(id, 1);
  }

  editWatchlist(id, name) {
    this.watchlists[id].name = name;
  }

  editWatchlistCoins(coins) {
    this.watchlists[this.watchlistSelected].coins = coins;
  }

  selectWatchlist(id) {
    this.watchlistSelected = id;
    this.loadWatchlist(id);
  }

  getSelectedWatchlist() {
    return this.watchlistSelected;
  }

  loadDefaultWatchlist() {
    if (!this.defaultWatchlistLoaded) {
      this.selectWatchlist(1);
    }
  }

  loadGetIeoWatchlist() {
    if (this.watchlistSelected != 0) {
      this.loadWatchlist(0);
    }
  }

  displayAddressName(addr, addressBook): string | false {
    let element = addressBook.find((el: any) => {
      return el.address == addr;
    });

    if (element) {
      return element.name;
    }
    return false;
  }

  public minTransactionFee(value: number) {
    if (value <= 1) {
      return this.minTxFee;
    } else {
      let calculatedFee = this.minTxFee * value;
      return calculatedFee;
    }
  }

  public getFeeForType(txType: string) {
    let index = this.globalFees.findIndex((e: any) => e.transactionType.toLowerCase() == txType.toLowerCase());
    if (index == -1) {
      return 0;
    }
    let fee = Number(this.globalFees[index].minTransactionFee);
    let burnAmount = Number(this.globalFees[index].minDelegateBurnAmount || 0);
    return {
      fee: fee > 0 ? fee / this.NANO : 0,
      burnAmount: burnAmount > 0 ? burnAmount / this.NANO : burnAmount,
      feeNano: fee > 0 ? fee : 0, // raw fee
    };
  }

  public getSmartContractAddress(type: string) {
    let index = this.globalFees.findIndex((e: any) => e.transactionType.toLowerCase() == type.toLowerCase());
    return this.globalFees[index].minTransactionFee;
  }

  loadWatchlist(id) {
    let coins: any = this.getWatchlists()[id].coins;
    console.log(coins)
    let count = 0;
    for (let i = 0; i < this.s.getSymbolArray().length; i++) {
      if (this.s.isSymbolEnabled(i)) {
        this.s.changeSymbolStatus(i);
      }
      if (count <= coins.length) {
        if (coins.includes(this.s.getSymbolId(i))) {
          count++;
          if (!this.s.isSymbolEnabled(i)) {
            this.s.changeSymbolStatus(i);
          }
        } else {
          if (this.s.isSymbolEnabled(i)) {
            this.s.changeSymbolStatus(i);
          }
        }
      }
    }
  }

  public async initForm() {
    // TODO: validate resp data
    let msgId = this._ors.callMethod.getXamarinState();
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_GetState, msgId);
    this.state = resp.data;
  }

  async updateHashBook() {
    await sleep(10);
    let msgId = this._ors.callMethod.setXamarinData('hashbook', JSON.stringify(this.hashBook));
    let result: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Command, this._ors.types.Sub_SaveData, msgId);
    debugger;
    if (!result.orderroute.error) {
      // this.hashBook = this.hashBook;
    }
  }

  async createHash(name, hash) {
    this.hashBook.push({ name, hash });
    await this.updateHashBook();
  }

  parseByteResponse(hexData: string) {
    debugger;
    let hexDataParsed = hexData.split(',');

    let parsedData = [];

    for (const hex of hexDataParsed) {
      let buf = Buffer.from(hex, 'hex');
      let parsed = new Parser(buf.byteLength, buf);

      let id = parsed.getInt(0);
      let stage = parsed.getByte(4)
      let state = parsed.getByte(5)
      let name = String.fromCharCode.apply(null, parsed.getBytes(6))

      parsedData.push({
        id,
        stage,
        state,
        name
      })
    }

    return parsedData;
  }

}
