import { Injectable } from '@angular/core';
import {ICTE} from 'icte-market-data';
import {Symbols} from 'icte-market-data/lib/model/Symbols';

@Injectable({
  providedIn: 'root'
})
export class ICTEMarketDataService {
  private icteMd;
  private marketData: Symbols;

  constructor() {
    this.icteMd = new ICTE();
    this.icteMd.init();
    debugger;
    console.log('Started market data');
    let _this = this;
    this.icteMd.getData().subscribe(m => {
      _this.marketData = m;
    });
    console.log('Start subscriptios');
  }

  public get d():Symbols {
    return this.marketData;
  }
}
