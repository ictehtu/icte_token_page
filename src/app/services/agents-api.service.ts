import { Injectable } from '@angular/core';
import { InternalDataService } from './internal-data.service';
import { IcteOrderRouteService } from './icte-order-route.service';
@Injectable({
  providedIn: 'root'
})

export class AgentsApiService {

  constructor(
    public _ids: InternalDataService,
    public _ors: IcteOrderRouteService
  ) { }
  async getStatus(domain, owner) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'getStatus'
        },
        {
          param: 'domain',
          value: domain
        },
        {
          param: 'owner',
          value: owner
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getHash(domain, owner) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'getHash'
        },
        {
          param: 'domain',
          value: domain
        },
        {
          param: 'owner',
          value: owner
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async updateLocation(domain, owner, data, reqType) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'updateLocation'
        },
        {
          param: 'domain',
          value: domain
        },
        {
          param: 'owner',
          value: owner
        },
        {
          param: 'data',
          value: data
        },
        {
          param: 'reqType',
          value: reqType
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getSecret(domain, owner, raw) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'getSecret'
        },
        {
          param: 'domain',
          value: domain
        },
        {
          param: 'raw',
          value: raw
        },
        {
          param: 'owner',
          value: owner
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getOwnerInfo(owner) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'getOwnerInfo'
        },
        {
          param: 'owner',
          value: owner
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async updateStatus(domain, owner, status) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'updateStatus'
        },
        {
          param: 'domain',
          value: domain
        },
        {
          param: 'owner',
          value: owner
        },
        {
          param: 'status',
          value: status
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async signKey(domain, owner, key) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'signKey'
        },
        {
          param: 'domain',
          value: domain
        },
        {
          param: 'owner',
          value: owner
        },
        {
          param: 'key',
          value: key
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async createAgent(domain, owner) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'createAgent'
        },
        {
          param: 'domain',
          value: domain
        },
        {
          param: 'owner',
          value: owner
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
  async updateIP(domain, owner, ip) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'updateIP'
        },
        {
          param: 'domain',
          value: domain
        },
        {
          param: 'owner',
          value: owner
        },
        {
          param: 'ip',
          value: ip
        }
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }

  async getDomainStats(url) {
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    }
    const controller = new AbortController();
    const timeout = setTimeout(
      () => { controller.abort(); },
      3000,
    );
    return new Promise((resolve, reject) => {
      try {
        fetch(url,
          { headers: headers, signal: controller.signal })
          .then(res => {
            clearTimeout(timeout);
            if (res.status === 200) {
              resolve('Up');
            } else {
              reject('Down')
            }

          })
          .catch(err => {
            clearTimeout(timeout);
            reject('Down');
          });
      } catch (err) {
        clearTimeout(timeout);
        reject('Down');
      }
    })
  }

  async getInfo(nodes: any[], subdomain = "", route = "") {
    const promises = [];
    for (const node of nodes) {
      var url = 'https://' + subdomain + node + route;
      promises.push(this.getDomainStats(url));
    }
    let resp = await Promise.all(promises.map(p => p.catch(e => e)));
    return resp;
  }

  async getNodesInfo(domainsIds: string) {
    let payload = {
      comp: 'execAgentApi',
      params: [
        {
          param: 'cmd',
          value: 'getOwnerInfo'
        },
        {
          param: 'domains',
          value: domainsIds
        },
      ]
    }
    let msgId = this._ors.callMethod.reqWorkerCmd(payload);
    let resp: any = await this._ors.callMethod.getMsgData(this._ors.types.Msg_Wallet, this._ors.types.Sub_WorkerCmd, msgId);
    return resp;
  }
}
