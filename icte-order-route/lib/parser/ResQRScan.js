"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var ResQRScan = /** @class */ (function (_super) {
    __extends(ResQRScan, _super);
    function ResQRScan(_size, buf) {
        var _this = _super.call(this, _size, buf) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Command);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_QrScan);
        return _this;
    }
    ResQRScan.prototype.getDeltaTime = function () {
        return _super.prototype.getInt.call(this, ResQRScan.Fld_DeltaTime);
    };
    ResQRScan.prototype.setDeltaTime = function (value) {
        _super.prototype.setInt.call(this, ResQRScan.Fld_DeltaTime, value);
    };
    ResQRScan.prototype.getMessageId = function () {
        return _super.prototype.getInt.call(this, ResQRScan.Fld_MsgId);
    };
    ResQRScan.prototype.setMessageId = function (value) {
        _super.prototype.setInt.call(this, ResQRScan.Fld_MsgId, value);
    };
    ResQRScan.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, ResQRScan.Fld_DeltaTime, dataView.getUint32(ResQRScan.Fld_DeltaTime));
        _super.prototype.setInt.call(this, ResQRScan.Fld_MsgId, dataView.getUint32(ResQRScan.Fld_MsgId));
        _super.prototype.setBytes.call(this, ResQRScan.Fld_Data, new Uint8Array(dataView.buffer.slice(ResQRScan.Fld_Data)));
        for (var i = ResQRScan.Fld_QRCode; i < dataView.byteLength; i++) {
            _super.prototype.setByte.call(this, i, dataView[i]);
        }
        return this.buf;
    };
    ResQRScan.prototype.buffer = function (deltaTime, messageId, data) {
        _super.prototype.setInt.call(this, ResQRScan.Fld_DeltaTime, deltaTime);
        _super.prototype.setInt.call(this, ResQRScan.Fld_MsgId, messageId);
        _super.prototype.setBytes.call(this, ResQRScan.Fld_Data, data);
        return _super.prototype.buffer.call(this);
    };
    ResQRScan.prototype.getQRCode = function () {
        var array = this.buf.slice(ResQRScan.Fld_QRCode);
        var str = String.fromCharCode.apply(null, array);
        return str.toString();
    };
    ResQRScan.Fld_DeltaTime = 2;
    ResQRScan.Fld_MsgId = 6;
    ResQRScan.Fld_Data = 10;
    ResQRScan.Fld_QRCode = 14;
    return ResQRScan;
}(Parser_1.Parser));
exports.ResQRScan = ResQRScan;
