"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var Parser_1 = require("./Parser");
var TokenBehaviorMsg = /** @class */ (function (_super) {
    __extends(TokenBehaviorMsg, _super);
    function TokenBehaviorMsg(msgSubtype, tokenId, clientOrderId, executionMsg, buf) {
        var _this = _super.call(this, TokenBehaviorMsg.Msg_Size + executionMsg.length, buf) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Token);
        _this.setByte(Constant_1.Constant.Fld_SubType, msgSubtype);
        _this.setLong(Constant_1.Constant.Fld_Timestamp, Date.now());
        _this.setInt(TokenBehaviorMsg.Token_ID, tokenId);
        _this.setInt(TokenBehaviorMsg.Client_Order_ID, clientOrderId);
        _this.setBytes(TokenBehaviorMsg.Execution_Msg, executionMsg);
        return _this;
    }
    TokenBehaviorMsg.prototype.getTokenId = function () {
        return this.getInt(TokenBehaviorMsg.Token_ID);
    };
    TokenBehaviorMsg.prototype.getClientOrderId = function () {
        return this.getInt(TokenBehaviorMsg.Client_Order_ID);
    };
    TokenBehaviorMsg.prototype.getExecutionMsg = function () {
        return this.getBytes(TokenBehaviorMsg.Execution_Msg);
    };
    TokenBehaviorMsg.Token_ID = 10;
    TokenBehaviorMsg.Client_Order_ID = 14;
    TokenBehaviorMsg.Execution_Msg = 18;
    TokenBehaviorMsg.Msg_Size = 22;
    return TokenBehaviorMsg;
}(Parser_1.Parser));
exports.TokenBehaviorMsg = TokenBehaviorMsg;
