"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utilities_1 = require("../model/Utilities");
var hexyjs_1 = require("hexyjs");
var ProductTransferMsg_1 = require("./ProductTransferMsg");
var WalletMsg = /** @class */ (function () {
    function WalletMsg() {
    }
    // public generateCryptoBalanceData(address, coin){
    //     let dataObject = {
    //         address,
    //         coin
    //     }
    //     let dataUint8: Uint8Array = Utilities.strToUtf8Bytes(JSON.stringify(dataObject));
    //     return dataUint8;
    // }
    WalletMsg.prototype.generateWalletData = function (walletName, pwd) {
        var password = pwd.value;
        var dataObject = {
            walletName: walletName,
            password: password
        };
        var dataUint8 = Utilities_1.Utilities.strToUtf8Bytes(JSON.stringify(dataObject));
        return dataUint8;
    };
    WalletMsg.prototype.generateCustomTxData = function (from, to, value, fee, nonce, data, gas, gasPrice, type) {
        var dataObject = {
            network: 0,
            from: from,
            to: to,
            type: type,
            value: value,
            fee: fee,
            nonce: nonce,
            data: data ? data : hexyjs_1.strToHex('0x'),
            gas: gas,
            gasPrice: gasPrice
        };
        var dataUint8 = Utilities_1.Utilities.strToUtf8Bytes(JSON.stringify(dataObject));
        return dataUint8;
    };
    WalletMsg.prototype.generateTransferData = function (from, to, value, fee, nonce, data) {
        var dataObject = {
            network: 0,
            from: from,
            to: to,
            type: 1,
            value: value,
            fee: fee,
            nonce: nonce,
            data: data ? hexyjs_1.strToHex(data) : hexyjs_1.strToHex('0x'),
            legend: 10,
            subLegend: 1
        };
        var dataUint8 = Utilities_1.Utilities.strToUtf8Bytes(JSON.stringify(dataObject));
        return dataUint8;
    };
    WalletMsg.prototype.generateProductTransactionForRaw = function (from, to, value, nonce, domain, data, timestamp, legend, subLegend, fee, key) {
        var dataObject = {
            network: 0,
            from: from,
            to: to,
            type: 10,
            value: value,
            fee: fee,
            nonce: nonce,
            timestamp: timestamp,
            data: data,
            legend: legend,
            subLegend: subLegend,
            // extra data
            domain: domain
        };
        if (key) {
            dataObject = __assign({}, dataObject, { key: key });
        }
        var dataUint8 = Utilities_1.Utilities.strToUtf8Bytes(JSON.stringify(dataObject));
        return dataUint8;
    };
    WalletMsg.prototype.generateProductTransaction = function (from, to, value, nonce, domain, packageLvl, domainIndex, nodeLocationIndex, fee) {
        var domaInBytes = Utilities_1.Utilities.strToUtf8Bytes(domain);
        // type,subType,productNode,productTier,packageLvl,originIdex,domain
        var msgInData = new ProductTransferMsg_1.ProductTransferMsg(packageLvl, domaInBytes, domainIndex, nodeLocationIndex);
        var dataObject = {
            network: 0,
            from: from,
            to: to,
            type: 10,
            value: value,
            fee: fee,
            nonce: nonce,
            data: msgInData.getBufferHex(),
            legend: 10,
            subLegend: 1,
        };
        var dataUint8 = Utilities_1.Utilities.strToUtf8Bytes(JSON.stringify(dataObject));
        return dataUint8;
    };
    WalletMsg.prototype.generateAddressData = function (address) {
        var dataUint8 = Utilities_1.Utilities.strToUtf8Bytes(address);
        return dataUint8;
    };
    WalletMsg.prototype.generateData = function (obj) {
        var dataUint8 = Utilities_1.Utilities.strToUtf8Bytes(JSON.stringify(obj));
        return dataUint8;
    };
    return WalletMsg;
}());
exports.WalletMsg = WalletMsg;
