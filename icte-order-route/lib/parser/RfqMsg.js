"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var RfqMsg = /** @class */ (function (_super) {
    __extends(RfqMsg, _super);
    function RfqMsg(buf, productId, priceStart, priceEnd, qtyStart, qtyEnd, timeStart, timeEnd) {
        var _this = _super.call(this, RfqMsg.SIZE, buf || null) || this;
        _this.setMsg();
        _this.setLong(RfqMsg.Fld_Timestamp, Date.now());
        var setPriceStart = Number(Math.round(priceStart).toFixed(Constant_1.Constant.DIVSIZE));
        var setPriceEnd = Number(Math.round(priceEnd).toFixed(Constant_1.Constant.DIVSIZE));
        var setQtyStart = Number(Math.round(qtyStart).toFixed(Constant_1.Constant.DIVSIZE));
        var setQtyEnd = Number(Math.round(qtyEnd).toFixed(Constant_1.Constant.DIVSIZE));
        _this.setLong(RfqMsg.Fld_TimeEnd, timeEnd);
        _this.setLong(RfqMsg.Fld_TimeFrom, timeStart);
        _this.setInt(RfqMsg.Fld_ProductId, productId);
        _this.setInt(RfqMsg.Fld_PriceStartInt, Math.trunc(setPriceStart));
        _this.setInt(RfqMsg.Fld_PriceStartFrc, Math.trunc(setPriceStart % 1 * Constant_1.Constant.DIV));
        _this.setInt(RfqMsg.Fld_PriceEndInt, Math.trunc(setPriceEnd));
        _this.setInt(RfqMsg.Fld_PriceEndFrc, Math.trunc(setPriceEnd % 1 * Constant_1.Constant.DIV));
        _this.setInt(RfqMsg.Fld_QtyStartInt, Math.trunc(setQtyStart));
        _this.setInt(RfqMsg.Fld_QtyStartFrc, Math.trunc(setQtyStart % 1 * Constant_1.Constant.DIV));
        _this.setInt(RfqMsg.Fld_QtyEndInt, Math.trunc(setQtyEnd));
        _this.setInt(RfqMsg.Fld_QtyEndFrc, Math.trunc(setQtyEnd % 1 * Constant_1.Constant.DIV));
        return _this;
    }
    RfqMsg.prototype.setMsg = function () {
        this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Exchange);
        this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_RfqOrder);
    };
    RfqMsg.Fld_Timestamp = 2;
    RfqMsg.Fld_TimeFrom = 10;
    RfqMsg.Fld_TimeEnd = 18;
    RfqMsg.Fld_ProductId = 26;
    RfqMsg.Fld_PriceStartInt = 30;
    RfqMsg.Fld_PriceStartFrc = 34;
    RfqMsg.Fld_PriceEndInt = 38;
    RfqMsg.Fld_PriceEndFrc = 42;
    RfqMsg.Fld_QtyStartInt = 46;
    RfqMsg.Fld_QtyStartFrc = 50;
    RfqMsg.Fld_QtyEndInt = 54;
    RfqMsg.Fld_QtyEndFrc = 58;
    RfqMsg.SIZE = 62;
    return RfqMsg;
}(Parser_1.Parser));
exports.RfqMsg = RfqMsg;
