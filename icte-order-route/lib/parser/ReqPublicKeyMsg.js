"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var ReqPublicKeyMsg = /** @class */ (function (_super) {
    __extends(ReqPublicKeyMsg, _super);
    function ReqPublicKeyMsg(buf) {
        var _this = _super.call(this, ReqPublicKeyMsg.Msg_Size, buf) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Portfolio);
        return _this;
    }
    ReqPublicKeyMsg.prototype.setSubType = function (subType) {
        this.setByte(Constant_1.Constant.Fld_SubType, subType);
    };
    ReqPublicKeyMsg.prototype.getReqId = function () {
        return _super.prototype.getInt.call(this, ReqPublicKeyMsg.Fld_MsgId);
    };
    ReqPublicKeyMsg.prototype.getSymbolId = function () {
        return _super.prototype.getInt.call(this, ReqPublicKeyMsg.Fld_SymbolId);
    };
    ReqPublicKeyMsg.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, ReqPublicKeyMsg.Fld_MsgId, dataView.getUint32(ReqPublicKeyMsg.Fld_MsgId));
        _super.prototype.setInt.call(this, ReqPublicKeyMsg.Fld_SymbolId, dataView.getUint32(ReqPublicKeyMsg.Fld_SymbolId));
    };
    ReqPublicKeyMsg.prototype.buffer = function (reqId, symbolId) {
        _super.prototype.setInt.call(this, ReqPublicKeyMsg.Fld_MsgId, reqId);
        _super.prototype.setInt.call(this, ReqPublicKeyMsg.Fld_SymbolId, symbolId);
        return _super.prototype.buffer.call(this);
    };
    // field offsets
    ReqPublicKeyMsg.Fld_MsgId = 2;
    ReqPublicKeyMsg.Fld_SymbolId = 6;
    ReqPublicKeyMsg.Msg_Size = 10;
    return ReqPublicKeyMsg;
}(Parser_1.Parser));
exports.ReqPublicKeyMsg = ReqPublicKeyMsg;
