import { Parser } from "./Parser";
import { RfqMsg } from "./RfqMsg";
export declare class ListRfqMsg extends Parser {
    static readonly Fld_Timestamp: number;
    static readonly Fld_RfqListCount: number;
    static readonly Fld_RfqList: number;
    constructor(list: RfqMsg[]);
    GetList(): RfqMsg[];
    private setMsg;
}
