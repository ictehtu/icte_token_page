"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var ReqInvestMsg = /** @class */ (function (_super) {
    __extends(ReqInvestMsg, _super);
    function ReqInvestMsg(buf) {
        var _this = _super.call(this, ReqInvestMsg.Msg_Size, buf) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Portfolio);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_ReqInvestPk);
        return _this;
    }
    ReqInvestMsg.prototype.getReqId = function () {
        return _super.prototype.getInt.call(this, ReqInvestMsg.Fld_ReqId);
    };
    ReqInvestMsg.prototype.getSymbolId = function () {
        return _super.prototype.getInt.call(this, ReqInvestMsg.Fld_SymbolId);
    };
    ReqInvestMsg.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, ReqInvestMsg.Fld_ReqId, dataView.getUint32(ReqInvestMsg.Fld_ReqId));
        _super.prototype.setInt.call(this, ReqInvestMsg.Fld_SymbolId, dataView.getUint32(ReqInvestMsg.Fld_SymbolId));
    };
    ReqInvestMsg.prototype.buffer = function (reqId, symbolId) {
        _super.prototype.setInt.call(this, ReqInvestMsg.Fld_ReqId, reqId);
        _super.prototype.setInt.call(this, ReqInvestMsg.Fld_SymbolId, symbolId);
        return _super.prototype.buffer.call(this);
    };
    // field offsets
    ReqInvestMsg.Fld_ReqId = 2;
    ReqInvestMsg.Fld_SymbolId = 6;
    ReqInvestMsg.Msg_Size = 10;
    return ReqInvestMsg;
}(Parser_1.Parser));
exports.ReqInvestMsg = ReqInvestMsg;
