"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var Parser_1 = require("./Parser");
var NewOrderMsg = /** @class */ (function (_super) {
    __extends(NewOrderMsg, _super);
    function NewOrderMsg(Client_ID, Book_Type, AON, Token_ID, Order_Quantity_Integer, Order_Quantity_Decimal, Order_Price_Integer, Order_Price_Decimal, Order_Side, buf) {
        var _this = _super.call(this, NewOrderMsg.Msg_Size, buf) || this;
        _this.setLong(Constant_1.Constant.Fld_Timestamp, Date.now());
        _this.setInt(NewOrderMsg.Client_ID, Client_ID);
        _this.setByte(NewOrderMsg.Book_Type, Book_Type);
        _this.setByte(NewOrderMsg.Order_Side, Order_Side);
        _this.setByte(NewOrderMsg.AON, AON);
        _this.setInt(NewOrderMsg.Token_ID, Token_ID);
        _this.setInt(NewOrderMsg.Order_Quantity_Integer, Order_Quantity_Integer);
        _this.setInt(NewOrderMsg.Order_Quantity_Decimal, Order_Quantity_Decimal);
        _this.setInt(NewOrderMsg.Order_Price_Integer, Order_Price_Integer);
        _this.setInt(NewOrderMsg.Order_Price_Decimal, Order_Price_Decimal);
        return _this;
    }
    NewOrderMsg.prototype.setType = function (type) {
        _super.prototype.setByte.call(this, Constant_1.Constant.Fld_Type, type);
    };
    NewOrderMsg.prototype.setSubType = function (sub) {
        _super.prototype.setByte.call(this, Constant_1.Constant.Fld_SubType, sub);
    };
    NewOrderMsg.Client_ID = 10;
    NewOrderMsg.Book_Type = 14;
    NewOrderMsg.Order_Side = 15;
    NewOrderMsg.AON = 16;
    NewOrderMsg.Token_ID = 17;
    NewOrderMsg.Order_Quantity_Integer = 21;
    NewOrderMsg.Order_Quantity_Decimal = 25;
    NewOrderMsg.Order_Price_Integer = 29;
    NewOrderMsg.Order_Price_Decimal = 33;
    NewOrderMsg.Msg_Size = 37;
    return NewOrderMsg;
}(Parser_1.Parser));
exports.NewOrderMsg = NewOrderMsg;
