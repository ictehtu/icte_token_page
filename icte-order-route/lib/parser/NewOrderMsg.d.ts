import { Parser } from "./Parser";
export declare class NewOrderMsg extends Parser {
    private static Client_ID;
    private static Book_Type;
    private static Order_Side;
    private static AON;
    private static Token_ID;
    private static Order_Quantity_Integer;
    private static Order_Quantity_Decimal;
    private static Order_Price_Integer;
    private static Order_Price_Decimal;
    private static Msg_Size;
    constructor(Client_ID: number, Book_Type: number, AON: number, Token_ID: number, Order_Quantity_Integer: number, Order_Quantity_Decimal: number, Order_Price_Integer: number, Order_Price_Decimal: number, Order_Side: number, buf?: Buffer);
    setType(type: number): void;
    setSubType(sub: number): void;
}
