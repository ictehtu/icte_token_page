import { Parser } from "./Parser";
export declare class ReqQRScan extends Parser {
    static readonly Fld_DeltaTime: number;
    static readonly Fld_MsgId: number;
    static readonly Msg_Size: number;
    constructor(buf?: Buffer);
    getDeltaTime(): number;
    setDeltaTime(value: number): void;
    getMessageId(): number;
    setMessageId(value: number): void;
    parse(dataView: DataView): void;
    buffer(deltaTime: number, messageId: number): Buffer;
}
