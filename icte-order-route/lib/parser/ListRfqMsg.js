"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var RfqMsg_1 = require("./RfqMsg");
var ListRfqMsg = /** @class */ (function (_super) {
    __extends(ListRfqMsg, _super);
    function ListRfqMsg(list) {
        var _this = _super.call(this, ListRfqMsg.Fld_RfqList + ((RfqMsg_1.RfqMsg.SIZE + 4) * list.length)) || this;
        _this.setMsg();
        _this.setLong(ListRfqMsg.Fld_Timestamp, Date.now());
        _this.setInt(ListRfqMsg.Fld_RfqListCount, list.length);
        for (var i = 0; i < list.length; i++) {
            var position = ListRfqMsg.Fld_RfqList + ((RfqMsg_1.RfqMsg.SIZE + 4) * i);
            _this.setBytes(position, list[i].buf);
        }
        return _this;
    }
    ListRfqMsg.prototype.GetList = function () {
        var list = [];
        var count = this.getInt(ListRfqMsg.Fld_RfqListCount);
        for (var i = 0; i < count; i++) {
            var position = ListRfqMsg.Fld_RfqList + ((RfqMsg_1.RfqMsg.SIZE + 4) * i);
            // list.push(new RfqMsg(this.getBytes(position)));
        }
        return list;
    };
    ListRfqMsg.prototype.setMsg = function () {
        this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Exchange);
        this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_RfqFilterList);
    };
    ListRfqMsg.Fld_Timestamp = 2;
    ListRfqMsg.Fld_RfqListCount = 10;
    ListRfqMsg.Fld_RfqList = 14;
    return ListRfqMsg;
}(Parser_1.Parser));
exports.ListRfqMsg = ListRfqMsg;
