export declare class Parser {
    readonly buf: Buffer;
    constructor(size: number, buf?: Buffer);
    msgType(): number;
    msgSubtype(): number;
    buffer(...args: any[]): Buffer;
    getByte(offset: number): number;
    getShort(offset: number): number;
    getInt(offset: number): number;
    setByte(offset: number, value: number): void;
    setShort(offset: number, value: number): void;
    setInt(offset: number, value: number): void;
    setLong(offset: number, value: number): void;
    getLong(offset: number): number;
    setBytes(offset: number, array: Uint8Array): void;
    getBytes(offset: number, length?: number): Uint8Array;
    toBase64: () => any;
    parseUint8(data: Uint8Array): Buffer;
    static fromBase64: (value: string) => any;
}
