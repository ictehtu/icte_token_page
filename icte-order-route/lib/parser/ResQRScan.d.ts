import { Parser } from "./Parser";
export declare class ResQRScan extends Parser {
    static readonly Fld_DeltaTime: number;
    static readonly Fld_MsgId: number;
    static readonly Fld_Data: number;
    static readonly Fld_QRCode: number;
    constructor(_size: number, buf?: Buffer);
    getDeltaTime(): number;
    setDeltaTime(value: number): void;
    getMessageId(): number;
    setMessageId(value: number): void;
    parse(dataView: DataView): Buffer;
    buffer(deltaTime: number, messageId: number, data: Uint8Array): Buffer;
    getQRCode(): string;
}
