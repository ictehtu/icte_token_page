import { Parser } from "./Parser";
export declare class ErrorMsg extends Parser {
    static readonly Fld_DeltaTime: number;
    static readonly Fld_Sequence: number;
    static readonly Fld_Provider: number;
    static readonly Fld_ErrorId: number;
    static readonly Msg_Size: number;
    constructor(buf?: Buffer);
    getDeltaTime(): number;
    setDeltaTime(value: number): void;
    getSequence(): number;
    setSequence(value: number): void;
    getProvider(): number;
    setProvider(value: number): void;
    getErrorId(): number;
    setErrorId(value: number): void;
    parse(dataView: DataView): void;
    buffer(deltaTime: number, sequence: number, provider: number, errorId: number): Buffer;
}
