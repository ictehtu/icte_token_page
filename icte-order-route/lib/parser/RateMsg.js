"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var Utilities_1 = require("../model/Utilities");
var RateMsg = /** @class */ (function (_super) {
    __extends(RateMsg, _super);
    function RateMsg(buf) {
        var _this = _super.call(this, RateMsg.Msg_Size, buf) || this;
        _this.getVenue = function () {
            return _super.prototype.getShort.call(_this, RateMsg.Fld_Venue);
        };
        _this.getRateValue = function () {
            var ratestr = _this.getRateInt() + "." + Utilities_1.Utilities.padLeft(_this.getRateFrac().toString(), '0', Utilities_1.Utilities.PADSIZE);
            var rate = parseFloat(ratestr);
            return rate;
        };
        _this.setRate = function (rate) {
            _super.prototype.setInt.call(_this, RateMsg.Fld_RateInt, Math.trunc(rate));
            _super.prototype.setInt.call(_this, RateMsg.Fld_RateFrac, Math.trunc(rate % 1 * Utilities_1.Utilities.DIV));
        };
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Quote);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_Rate);
        return _this;
    }
    RateMsg.prototype.getDeltaTime = function () {
        return _super.prototype.getInt.call(this, RateMsg.Fld_DeltaTime);
    };
    RateMsg.prototype.getFrontSymbolId = function () {
        return _super.prototype.getInt.call(this, RateMsg.Fld_FrontSymbolId);
    };
    RateMsg.prototype.getBackBackSymbolId = function () {
        return _super.prototype.getInt.call(this, RateMsg.Fld_BackSymbolId);
    };
    RateMsg.prototype.getRateInt = function () {
        return _super.prototype.getInt.call(this, RateMsg.Fld_RateInt);
    };
    RateMsg.prototype.getRateFrac = function () {
        return _super.prototype.getInt.call(this, RateMsg.Fld_RateFrac);
    };
    RateMsg.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, RateMsg.Fld_DeltaTime, dataView.getUint32(RateMsg.Fld_DeltaTime));
        _super.prototype.setInt.call(this, RateMsg.Fld_FrontSymbolId, dataView.getUint32(RateMsg.Fld_FrontSymbolId));
        _super.prototype.setInt.call(this, RateMsg.Fld_BackSymbolId, dataView.getUint32(RateMsg.Fld_BackSymbolId));
        _super.prototype.setInt.call(this, RateMsg.Fld_RateInt, dataView.getUint32(RateMsg.Fld_RateInt));
        _super.prototype.setInt.call(this, RateMsg.Fld_RateFrac, dataView.getUint32(RateMsg.Fld_RateFrac));
        _super.prototype.setShort.call(this, RateMsg.Fld_Venue, dataView.getUint16(RateMsg.Fld_Venue));
    };
    RateMsg.prototype.buffer = function (deltaTime, frontSymbolId, backSymbolId, rateInt, rateFrac, venue) {
        _super.prototype.setInt.call(this, RateMsg.Fld_DeltaTime, deltaTime);
        _super.prototype.setInt.call(this, RateMsg.Fld_FrontSymbolId, frontSymbolId);
        _super.prototype.setInt.call(this, RateMsg.Fld_BackSymbolId, backSymbolId);
        _super.prototype.setInt.call(this, RateMsg.Fld_RateInt, rateInt);
        _super.prototype.setInt.call(this, RateMsg.Fld_RateFrac, rateFrac);
        _super.prototype.setShort.call(this, RateMsg.Fld_Venue, venue);
        return _super.prototype.buffer.call(this);
    };
    //field offsets
    RateMsg.Fld_DeltaTime = 2;
    RateMsg.Fld_FrontSymbolId = 6;
    RateMsg.Fld_BackSymbolId = 10;
    RateMsg.Fld_RateInt = 14;
    RateMsg.Fld_RateFrac = 18;
    RateMsg.Fld_Venue = 22; //2 bytes
    RateMsg.Msg_Size = 24;
    return RateMsg;
}(Parser_1.Parser));
exports.RateMsg = RateMsg;
