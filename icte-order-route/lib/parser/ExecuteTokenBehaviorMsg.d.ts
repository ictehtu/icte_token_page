import { TokenBehaviorMsg } from "./TokenBehaviorMsg";
export declare class ExecuteTokenBehaviorMsg extends TokenBehaviorMsg {
    constructor(tokenId: number, clientOrderId: number, executionMsg: Uint8Array, buf?: Buffer);
}
