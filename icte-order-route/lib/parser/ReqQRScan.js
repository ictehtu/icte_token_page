"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var ReqQRScan = /** @class */ (function (_super) {
    __extends(ReqQRScan, _super);
    function ReqQRScan(buf) {
        var _this = _super.call(this, ReqQRScan.Msg_Size, buf) || this;
        _super.prototype.setByte.call(_this, Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Command);
        _super.prototype.setByte.call(_this, Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_RequestQrScan);
        return _this;
    }
    ReqQRScan.prototype.getDeltaTime = function () {
        return _super.prototype.getInt.call(this, ReqQRScan.Fld_DeltaTime);
    };
    ReqQRScan.prototype.setDeltaTime = function (value) {
        _super.prototype.setInt.call(this, ReqQRScan.Fld_DeltaTime, value);
    };
    ReqQRScan.prototype.getMessageId = function () {
        return _super.prototype.getInt.call(this, ReqQRScan.Fld_MsgId);
    };
    ReqQRScan.prototype.setMessageId = function (value) {
        _super.prototype.setInt.call(this, ReqQRScan.Fld_MsgId, value);
    };
    ReqQRScan.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, ReqQRScan.Fld_DeltaTime, dataView.getUint32(ReqQRScan.Fld_DeltaTime));
        _super.prototype.setInt.call(this, ReqQRScan.Fld_MsgId, dataView.getUint32(ReqQRScan.Fld_MsgId));
    };
    ReqQRScan.prototype.buffer = function (deltaTime, messageId) {
        _super.prototype.setInt.call(this, ReqQRScan.Fld_DeltaTime, deltaTime);
        _super.prototype.setInt.call(this, ReqQRScan.Fld_MsgId, messageId);
        return _super.prototype.buffer.call(this);
    };
    ReqQRScan.Fld_DeltaTime = 2;
    ReqQRScan.Fld_MsgId = 6;
    ReqQRScan.Msg_Size = 10;
    return ReqQRScan;
}(Parser_1.Parser));
exports.ReqQRScan = ReqQRScan;
