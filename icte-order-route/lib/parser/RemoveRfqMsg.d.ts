import { Parser } from "./Parser";
export declare class RemoveRfqMsg extends Parser {
    static readonly Fld_Timestamp: number;
    static readonly Fld_RfqMsgIndex: number;
    static readonly Fld_ProductId: number;
    static readonly SIZE: number;
    constructor(rfqIndex: number, productId: number, buf?: Buffer);
    private setMsg;
}
