import { Parser } from "./Parser";
export declare class ProductTransferMsg extends Parser {
    static readonly Fld_PackageLvl: number;
    static readonly Fld_PackageMetaData: number;
    static readonly Fld_OriginIndex: number;
    static readonly Fld_Domain: number;
    static readonly Msg_Size: number;
    constructor(packageLvl: any, domain: Uint8Array, domainIndex: any, nodeLocationIndex: any);
    getBufferHex(): string;
}
