import { Parser } from "./Parser";
export declare class ReqInvestMsg extends Parser {
    static readonly Fld_ReqId: number;
    static readonly Fld_SymbolId: number;
    static readonly Msg_Size: number;
    constructor(buf?: Buffer);
    getReqId(): number;
    getSymbolId(): number;
    parse(dataView: DataView): void;
    buffer(reqId: number, symbolId: number): Buffer;
}
