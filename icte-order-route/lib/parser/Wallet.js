"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utilities_1 = require("../model/Utilities");
var Wallet = /** @class */ (function () {
    function Wallet() {
    }
    Wallet.prototype.createWallet = function (walletName, pwd) {
        var password = pwd.value;
        var dataObject = {
            walletName: walletName,
            password: password
        };
        var str = JSON.stringify(dataObject);
        var uint8 = Utilities_1.Utilities.strToUtf8Bytes(str);
        // Configure message headers
        //              0    1  ...
        var headers = [155, 200];
        var array = Utilities_1.Utilities.uint8PushToArray(headers, uint8);
        var binary = Utilities_1.Utilities.arrayToBinary(array);
        var b64encoded = btoa(binary);
        console.log(str, array, binary, b64encoded);
        return b64encoded;
    };
    return Wallet;
}());
exports.Wallet = Wallet;
