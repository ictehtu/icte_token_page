"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var base64js = require('base64-js');
var Buffer = require('buffer/').Buffer;
var Parser = /** @class */ (function () {
    function Parser(size, buf) {
        var _this = this;
        this.toBase64 = function () {
            return base64js.fromByteArray(_this.buf);
        };
        if (buf != null) {
            this.buf = buf;
        }
        else {
            this.buf = Buffer.allocUnsafe(size);
        }
    }
    Parser.prototype.msgType = function () {
        return this.getByte(Constant_1.Constant.Fld_Type);
        // return this.buf[Constant.Fld_Type];
    };
    Parser.prototype.msgSubtype = function () {
        return this.getByte(Constant_1.Constant.Fld_SubType);
        // return this.buf[Constant.Fld_SubType];
    };
    Parser.prototype.buffer = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return this.buf;
    };
    Parser.prototype.getByte = function (offset) {
        return this.buf[offset];
    };
    Parser.prototype.getShort = function (offset) {
        return this.buf.readInt16BE(offset);
    };
    // getInt(offset: number): number {
    //    return this.buf.readInt32BE(offset);
    // }
    Parser.prototype.getInt = function (offset) {
        return ((this.buf[offset + 3] & 0xff))
            | ((this.buf[offset + 2] & 0xff) << 8)
            | ((this.buf[offset + 1] & 0xff) << 16)
            | ((this.buf[offset] & 0xff) << 24);
    };
    Parser.prototype.setByte = function (offset, value) {
        this.buf[offset] = value;
    };
    Parser.prototype.setShort = function (offset, value) {
        this.buf.writeInt16BE(value, offset);
    };
    Parser.prototype.setInt = function (offset, value) {
        this.buf.writeInt32BE(value, offset);
    };
    Parser.prototype.setLong = function (offset, value) {
        this.buf.writeFloatBE(value, offset);
    };
    Parser.prototype.getLong = function (offset) {
        return this.buf.readFloatBE(offset);
    };
    Parser.prototype.setBytes = function (offset, array) {
        this.setInt(offset, array.length);
        offset += 4;
        this.buf.fill(array, offset);
    };
    Parser.prototype.getBytes = function (offset, length) {
        return this.buf.subarray(offset, length);
    };
    Parser.prototype.parseUint8 = function (data) {
        for (var i = 0; i < data.length; i++) {
            this.buf[i] = data[i];
        }
        return this.buf;
    };
    Parser.fromBase64 = function (value) {
        return base64js.toByteArray(value);
    };
    return Parser;
}());
exports.Parser = Parser;
