"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var RfqMsg_1 = require("./RfqMsg");
var RemoveRfqMsg = /** @class */ (function (_super) {
    __extends(RemoveRfqMsg, _super);
    function RemoveRfqMsg(rfqIndex, productId, buf) {
        var _this = _super.call(this, RfqMsg_1.RfqMsg.SIZE, buf) || this;
        _this.setMsg();
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Exchange);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_RfqRemoveFilter);
        _this.setLong(RemoveRfqMsg.Fld_Timestamp, Date.now());
        _this.setInt(RemoveRfqMsg.Fld_RfqMsgIndex, rfqIndex);
        _this.setInt(RemoveRfqMsg.Fld_ProductId, productId);
        return _this;
    }
    RemoveRfqMsg.prototype.setMsg = function () {
        this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Exchange);
        this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_RfqRemoveFilter);
    };
    RemoveRfqMsg.Fld_Timestamp = 2;
    RemoveRfqMsg.Fld_RfqMsgIndex = 10;
    RemoveRfqMsg.Fld_ProductId = 14;
    RemoveRfqMsg.SIZE = 18;
    return RemoveRfqMsg;
}(Parser_1.Parser));
exports.RemoveRfqMsg = RemoveRfqMsg;
