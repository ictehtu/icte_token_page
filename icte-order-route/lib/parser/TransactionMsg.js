"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utilities_1 = require("../model/Utilities");
var WalletMsg = /** @class */ (function () {
    function WalletMsg() {
    }
    WalletMsg.prototype.getTransaciton = function (walletAddress) {
        var dataObject = {
            walletAddress: walletAddress
        };
        var str = JSON.stringify(dataObject);
        var uint8 = Utilities_1.Utilities.strToUtf8Bytes(str);
        // Configure message headers
        //              0    1  ...
        var headers = [155, 200];
        var array = Utilities_1.Utilities.uint8PushToArray(headers, uint8);
        var binary = Utilities_1.Utilities.arrayToBinary(array);
        var b64encoded = btoa(binary);
        console.log(str, array, binary, b64encoded);
        return b64encoded;
    };
    return WalletMsg;
}());
exports.WalletMsg = WalletMsg;
// import {Parser} from "./Parser";
// import {Constant} from "../model/Constant";
// export class TransactionMsg extends Parser {
//    static readonly Fld_DeltaTime: number = 2;
//    static readonly Fld_Sequence: number = 6;
//    static readonly Fld_Provider: number = 10;
//    static readonly Fld_SymbolId: number = 14;
//    static readonly Fld_Quantity: number = 18;
//    static readonly Fld_Side: number = 22;
//    static readonly Msg_Size: number = 23;
//    constructor( buf?: Buffer) {
//       super(TransactionMsg.Msg_Size, buf);
//       this.setByte(Constant.Fld_Type, Constant.Msg_Wallet);
//       this.setByte(Constant.Fld_SubType, Constant.Sub_WalletTransaction);
//    }
//    public getDeltaTime() {
//       return super.getInt(TransactionMsg.Fld_DeltaTime);
//    }
//    public getSequence() {
//       return super.getInt(TransactionMsg.Fld_Sequence);
//    }
//    public getProvider() {
//       return super.getInt(TransactionMsg.Fld_Provider);
//    }
//    public getSymbolId(){
//       return super.getInt(TransactionMsg.Fld_SymbolId);
//    }
//    public getQuantity(){
//       return super.getInt(TransactionMsg.Fld_Quantity);
//    }
//    public getSide() {
//       return super.getByte(TransactionMsg.Fld_Side);
//    }
//    parse(dataView: DataView) {
//       super.setInt(TransactionMsg.Fld_DeltaTime, dataView.getUint32(TransactionMsg.Fld_DeltaTime));
//       super.setInt(TransactionMsg.Fld_Sequence, dataView.getUint32(TransactionMsg.Fld_Sequence));
//       super.setInt(TransactionMsg.Fld_Provider, dataView.getUint32(TransactionMsg.Fld_Sequence));
//       super.setInt(TransactionMsg.Fld_SymbolId, dataView.getUint32(TransactionMsg.Fld_Sequence));
//       super.setInt(TransactionMsg.Fld_Quantity, dataView.getUint32(TransactionMsg.Fld_Sequence));
//       super.setByte(TransactionMsg.Fld_Side, dataView.getUint8(TransactionMsg.Fld_Sequence));
//    }
//    buffer(deltaTime: number, sequence: number, provider: number, quantity: number, side: number): Buffer {
//       super.setInt(TransactionMsg.Fld_DeltaTime, deltaTime);
//       super.setInt(TransactionMsg.Fld_Sequence, sequence);
//       super.setInt(TransactionMsg.Fld_Provider, provider);
//       super.setInt(TransactionMsg.Fld_Quantity, quantity);
//       super.setByte(TransactionMsg.Fld_Side, side); //0 = Buy ; 1 = Sell;
//       return super.buffer();
//    }
// }
