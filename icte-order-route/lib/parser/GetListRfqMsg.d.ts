import { Parser } from "./Parser";
export declare class GetListRfqMsg extends Parser {
    static readonly Fld_Timestamp: number;
    static readonly Fld_ProductId: number;
    static readonly SIZE: number;
    constructor(productId: number, buf?: Buffer);
    private setMsg;
}
