import { Parser } from "./Parser";
export declare class DynamicMsg extends Parser {
    static readonly Fld_DeltaTime: number;
    static readonly Fld_MsgId: number;
    static readonly Fld_SymbolId: number;
    static readonly Fld_Status: number;
    static readonly Msg_Size: number;
    constructor(size?: number, buf?: Buffer);
    setDeltaTime(delta: number): void;
    getDeltaTime(): number;
    getStatus(): number;
    setType(type: number): void;
    setSubType(sub: number): void;
    setSymbolId(id: number): void;
    getSymbolId(): number;
    getMesageId(): number;
    setMessageId(id: number): void;
    setData(data: Uint8Array): void;
    static getAndParseData(msg: Uint8Array): string;
    getData(): Uint8Array;
}
