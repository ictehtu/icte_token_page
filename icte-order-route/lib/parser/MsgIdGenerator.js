"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
var MsgIdGenerator = /** @class */ (function () {
    function MsgIdGenerator() {
    }
    MsgIdGenerator.getNextId = function () {
        MsgIdGenerator.id += parseInt(moment().format('mmss')) + 1;
        if (MsgIdGenerator.id >= 2147483640) {
            MsgIdGenerator.id = 0;
        }
        return MsgIdGenerator.id;
    };
    MsgIdGenerator.id = 0;
    return MsgIdGenerator;
}());
exports.MsgIdGenerator = MsgIdGenerator;
