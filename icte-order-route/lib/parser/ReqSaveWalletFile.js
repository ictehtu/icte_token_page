"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utilities_1 = require("../model/Utilities");
var ReqSaveWalletFile = /** @class */ (function () {
    function ReqSaveWalletFile() {
    }
    ReqSaveWalletFile.prototype.buffer = function (content) {
        var std = new Array();
        std.push(155);
        std.push(200);
        var str = Utilities_1.Utilities.strToUtf8Bytes(content);
        for (var i = 0; i < str.length; i++) {
            std.push(str[i]);
        }
        var binary = '';
        for (var i = 0; i < std.length; i++) {
            binary += String.fromCharCode(std[i]);
        }
        var b64encoded = btoa(binary);
        return b64encoded;
    };
    return ReqSaveWalletFile;
}());
exports.ReqSaveWalletFile = ReqSaveWalletFile;
