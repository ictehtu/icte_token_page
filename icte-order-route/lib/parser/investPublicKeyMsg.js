"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var InvestPublicKeyMsg = /** @class */ (function (_super) {
    __extends(InvestPublicKeyMsg, _super);
    function InvestPublicKeyMsg(buf) {
        var _this = _super.call(this, InvestPublicKeyMsg.Msg_Size, buf) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Portfolio);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_ReqInvestPk);
        return _this;
    }
    InvestPublicKeyMsg.prototype.getReqId = function () {
        return _super.prototype.getInt.call(this, InvestPublicKeyMsg.Fld_ReqId);
    };
    InvestPublicKeyMsg.prototype.getSymbolId = function () {
        return _super.prototype.getInt.call(this, InvestPublicKeyMsg.Fld_SymbolId);
    };
    InvestPublicKeyMsg.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, InvestPublicKeyMsg.Fld_ReqId, dataView.getUint32(InvestPublicKeyMsg.Fld_ReqId));
        _super.prototype.setInt.call(this, InvestPublicKeyMsg.Fld_SymbolId, dataView.getUint32(InvestPublicKeyMsg.Fld_SymbolId));
    };
    InvestPublicKeyMsg.prototype.buffer = function (reqId, symbolId) {
        _super.prototype.setInt.call(this, InvestPublicKeyMsg.Fld_ReqId, reqId);
        _super.prototype.setInt.call(this, InvestPublicKeyMsg.Fld_SymbolId, symbolId);
        return _super.prototype.buffer.call(this);
    };
    // field offsets
    InvestPublicKeyMsg.Fld_ReqId = 2;
    InvestPublicKeyMsg.Fld_SymbolId = 6;
    InvestPublicKeyMsg.Msg_Size = 10;
    return InvestPublicKeyMsg;
}(Parser_1.Parser));
exports.InvestPublicKeyMsg = InvestPublicKeyMsg;
