import { Parser } from "./Parser";
export declare class ResPublicKeyMsg extends Parser {
    static readonly Fld_MsgId: number;
    static readonly Fld_SymbolId: number;
    static readonly Fld_MsgSize: number;
    static readonly Fld_PublicKey: number;
    constructor(size: number);
    parse(dataView: DataView): Buffer;
    getMsgId(): number;
    getPublicKey(): string;
}
