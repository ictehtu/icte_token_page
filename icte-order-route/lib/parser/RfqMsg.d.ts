import { Parser } from "./Parser";
export declare class RfqMsg extends Parser {
    static readonly Fld_Timestamp = 2;
    static readonly Fld_TimeFrom = 10;
    static readonly Fld_TimeEnd = 18;
    static readonly Fld_ProductId = 26;
    static readonly Fld_PriceStartInt = 30;
    static readonly Fld_PriceStartFrc = 34;
    static readonly Fld_PriceEndInt = 38;
    static readonly Fld_PriceEndFrc = 42;
    static readonly Fld_QtyStartInt = 46;
    static readonly Fld_QtyStartFrc = 50;
    static readonly Fld_QtyEndInt = 54;
    static readonly Fld_QtyEndFrc = 58;
    static readonly SIZE = 62;
    constructor(buf: Buffer | null, productId?: number, priceStart?: number, priceEnd?: number, qtyStart?: number, qtyEnd?: number, timeStart?: number, timeEnd?: number);
    private setMsg;
}
