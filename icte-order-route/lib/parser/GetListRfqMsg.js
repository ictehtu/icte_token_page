"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var RfqMsg_1 = require("./RfqMsg");
var GetListRfqMsg = /** @class */ (function (_super) {
    __extends(GetListRfqMsg, _super);
    function GetListRfqMsg(productId, buf) {
        var _this = _super.call(this, RfqMsg_1.RfqMsg.SIZE, buf) || this;
        _this.setMsg();
        _this.setLong(GetListRfqMsg.Fld_Timestamp, Date.now());
        _this.setInt(GetListRfqMsg.Fld_ProductId, productId);
        return _this;
    }
    GetListRfqMsg.prototype.setMsg = function () {
        this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Exchange);
        this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_RfqFilterList);
    };
    GetListRfqMsg.Fld_Timestamp = 2;
    GetListRfqMsg.Fld_ProductId = 10;
    GetListRfqMsg.SIZE = 14;
    return GetListRfqMsg;
}(Parser_1.Parser));
exports.GetListRfqMsg = GetListRfqMsg;
