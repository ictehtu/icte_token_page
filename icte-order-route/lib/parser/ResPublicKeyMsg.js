"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var ResPublicKeyMsg = /** @class */ (function (_super) {
    __extends(ResPublicKeyMsg, _super);
    function ResPublicKeyMsg(size) {
        var _this = _super.call(this, size) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Portfolio);
        return _this;
    }
    ResPublicKeyMsg.prototype.parse = function (dataView) {
        _super.prototype.setByte.call(this, Constant_1.Constant.Fld_Type, dataView[Constant_1.Constant.Fld_Type]);
        _super.prototype.setByte.call(this, Constant_1.Constant.Fld_SubType, dataView[Constant_1.Constant.Fld_Type]);
        _super.prototype.setInt.call(this, ResPublicKeyMsg.Fld_MsgId, dataView.getUint32(ResPublicKeyMsg.Fld_MsgId));
        _super.prototype.setInt.call(this, ResPublicKeyMsg.Fld_SymbolId, dataView.getUint32(ResPublicKeyMsg.Fld_SymbolId));
        for (var i = ResPublicKeyMsg.Fld_PublicKey; i < dataView.byteLength; i++) {
            _super.prototype.setByte.call(this, i, dataView[i]);
        }
        return this.buf;
    };
    ResPublicKeyMsg.prototype.getMsgId = function () {
        return _super.prototype.getInt.call(this, ResPublicKeyMsg.Fld_MsgId);
    };
    ResPublicKeyMsg.prototype.getPublicKey = function () {
        var array = this.buf.slice(ResPublicKeyMsg.Fld_PublicKey);
        var str = String.fromCharCode.apply(null, array);
        return str.toString();
    };
    // field offsets
    ResPublicKeyMsg.Fld_MsgId = 2;
    ResPublicKeyMsg.Fld_SymbolId = 6;
    ResPublicKeyMsg.Fld_MsgSize = 10;
    ResPublicKeyMsg.Fld_PublicKey = 14;
    return ResPublicKeyMsg;
}(Parser_1.Parser));
exports.ResPublicKeyMsg = ResPublicKeyMsg;
