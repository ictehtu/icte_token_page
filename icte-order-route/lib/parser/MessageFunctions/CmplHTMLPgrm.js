"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("../Parser");
var CmplHTMLPgrm = /** @class */ (function (_super) {
    __extends(CmplHTMLPgrm, _super);
    function CmplHTMLPgrm(coi, u, buf) {
        var _this = _super.call(this, CmplHTMLPgrm.Msg_Size + u.byteLength, buf) || this;
        _this.setByte(0, 0x00);
        _this.setByte(1, 0xa4);
        _this.setInt(2, 3 + 4 + 4 + u.byteLength);
        _this.setByte(6, 0x00);
        _this.setByte(7, 0xfb);
        _this.setByte(8, 0x02);
        _this.setInt(9, coi);
        _this.setBytes(13, u);
        return _this;
    }
    CmplHTMLPgrm.Msg_Size = 18;
    return CmplHTMLPgrm;
}(Parser_1.Parser));
exports.CmplHTMLPgrm = CmplHTMLPgrm;
