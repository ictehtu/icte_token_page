"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("../Parser");
var AdIPFS = /** @class */ (function (_super) {
    __extends(AdIPFS, _super);
    function AdIPFS(d, buf) {
        var _this = _super.call(this, AdIPFS.Msg_Size + d.byteLength, buf) || this;
        _this.setByte(0, 0x00);
        _this.setByte(1, 0xa1);
        _this.setInt(2, 3 + 4 + 4 + d.byteLength);
        _this.setByte(6, 0x00);
        _this.setByte(7, 0xfc);
        _this.setByte(8, 0x01);
        _this.setInt(9, 0);
        _this.setBytes(13, d);
        return _this;
    }
    AdIPFS.Msg_Size = 18;
    return AdIPFS;
}(Parser_1.Parser));
exports.AdIPFS = AdIPFS;
