"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("../Parser");
var CmplGPgrm = /** @class */ (function (_super) {
    __extends(CmplGPgrm, _super);
    function CmplGPgrm(coi, u, tId, buf) {
        var _this = _super.call(this, CmplGPgrm.Msg_Size + u.byteLength, buf) || this;
        _this.setByte(0, 0x00);
        _this.setByte(1, 0xa3);
        _this.setInt(2, 3 + 4 + 4 + u.byteLength + 4);
        _this.setByte(6, 0x00);
        _this.setByte(7, 0xfb);
        _this.setByte(8, 0x01);
        _this.setInt(9, coi);
        _this.setBytes(13, u);
        _this.setInt(13 + 4 + u.byteLength, tId);
        return _this;
    }
    CmplGPgrm.Msg_Size = 22;
    return CmplGPgrm;
}(Parser_1.Parser));
exports.CmplGPgrm = CmplGPgrm;
