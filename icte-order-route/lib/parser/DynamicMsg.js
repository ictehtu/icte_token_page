"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var Utilities_1 = require("../model/Utilities");
var DynamicMsg = /** @class */ (function (_super) {
    __extends(DynamicMsg, _super);
    // private static Data_Size: number = 0;
    function DynamicMsg(size, buf) {
        var _this = this;
        var s = size || 0;
        _this = _super.call(this, DynamicMsg.Msg_Size + s, buf) || this;
        return _this;
        // DynamicMsg.Data_Size = size;
    }
    DynamicMsg.prototype.setDeltaTime = function (delta) {
        _super.prototype.setLong.call(this, DynamicMsg.Fld_DeltaTime, delta);
    };
    DynamicMsg.prototype.getDeltaTime = function () {
        return _super.prototype.getLong.call(this, DynamicMsg.Fld_DeltaTime);
    };
    DynamicMsg.prototype.getStatus = function () {
        // return super.getByte(DynamicMsg.Fld_Status);
        return this.buf[DynamicMsg.Fld_Status];
    };
    DynamicMsg.prototype.setType = function (type) {
        _super.prototype.setByte.call(this, Constant_1.Constant.Fld_Type, type);
    };
    DynamicMsg.prototype.setSubType = function (sub) {
        _super.prototype.setByte.call(this, Constant_1.Constant.Fld_SubType, sub);
    };
    DynamicMsg.prototype.setSymbolId = function (id) {
        _super.prototype.setInt.call(this, DynamicMsg.Fld_SymbolId, id);
    };
    DynamicMsg.prototype.getSymbolId = function () {
        return _super.prototype.getInt.call(this, DynamicMsg.Fld_SymbolId);
    };
    DynamicMsg.prototype.getMesageId = function () {
        return _super.prototype.getInt.call(this, DynamicMsg.Fld_MsgId);
        // return this.buf[DynamicMsg.Fld_MsgId];
    };
    DynamicMsg.prototype.setMessageId = function (id) {
        _super.prototype.setInt.call(this, DynamicMsg.Fld_MsgId, id);
    };
    DynamicMsg.prototype.setData = function (data) {
        var i = DynamicMsg.Msg_Size;
        for (var z = 0; z < data.length; z++) {
            _super.prototype.setByte.call(this, i, data[z]);
            i++;
        }
    };
    DynamicMsg.getAndParseData = function (msg) {
        var data = Utilities_1.Utilities.Utf8ArrayToStr(msg);
        return data;
    };
    DynamicMsg.prototype.getData = function () {
        var i = 0;
        var len = this.buf.length - DynamicMsg.Msg_Size;
        var array = new Uint8Array(len);
        for (var z = DynamicMsg.Msg_Size; z < this.buf.length; z++) {
            array[i] = this.buf[z];
            i++;
        }
        return array;
    };
    DynamicMsg.Fld_DeltaTime = 2;
    DynamicMsg.Fld_MsgId = 10;
    DynamicMsg.Fld_SymbolId = 14;
    DynamicMsg.Fld_Status = 18;
    DynamicMsg.Msg_Size = 19;
    return DynamicMsg;
}(Parser_1.Parser));
exports.DynamicMsg = DynamicMsg;
