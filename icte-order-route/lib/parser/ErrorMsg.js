"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var ErrorMsg = /** @class */ (function (_super) {
    __extends(ErrorMsg, _super);
    function ErrorMsg(buf) {
        var _this = _super.call(this, ErrorMsg.Msg_Size, buf) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Sys);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_Error);
        return _this;
    }
    ErrorMsg.prototype.getDeltaTime = function () {
        return _super.prototype.getInt.call(this, ErrorMsg.Fld_DeltaTime);
    };
    ErrorMsg.prototype.setDeltaTime = function (value) {
        _super.prototype.setInt.call(this, ErrorMsg.Fld_DeltaTime, value);
    };
    ErrorMsg.prototype.getSequence = function () {
        return _super.prototype.getInt.call(this, ErrorMsg.Fld_Sequence);
    };
    ErrorMsg.prototype.setSequence = function (value) {
        _super.prototype.setInt.call(this, ErrorMsg.Fld_Sequence, value);
    };
    ErrorMsg.prototype.getProvider = function () {
        return _super.prototype.getInt.call(this, ErrorMsg.Fld_Provider);
    };
    ErrorMsg.prototype.setProvider = function (value) {
        _super.prototype.setInt.call(this, ErrorMsg.Fld_Provider, value);
    };
    ErrorMsg.prototype.getErrorId = function () {
        return _super.prototype.getInt.call(this, ErrorMsg.Fld_ErrorId);
    };
    ErrorMsg.prototype.setErrorId = function (value) {
        _super.prototype.setInt.call(this, ErrorMsg.Fld_ErrorId, value);
    };
    ErrorMsg.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, ErrorMsg.Fld_DeltaTime, dataView.getUint32(ErrorMsg.Fld_DeltaTime));
        _super.prototype.setInt.call(this, ErrorMsg.Fld_Sequence, dataView.getUint32(ErrorMsg.Fld_Sequence));
        _super.prototype.setInt.call(this, ErrorMsg.Fld_Provider, dataView.getUint32(ErrorMsg.Fld_Provider));
        _super.prototype.setInt.call(this, ErrorMsg.Fld_ErrorId, dataView.getUint32(ErrorMsg.Fld_ErrorId));
    };
    ErrorMsg.prototype.buffer = function (deltaTime, sequence, provider, errorId) {
        _super.prototype.setInt.call(this, ErrorMsg.Fld_DeltaTime, deltaTime);
        _super.prototype.setInt.call(this, ErrorMsg.Fld_Sequence, sequence);
        _super.prototype.setInt.call(this, ErrorMsg.Fld_Provider, provider);
        _super.prototype.setInt.call(this, ErrorMsg.Fld_ErrorId, errorId);
        return _super.prototype.buffer.call(this);
    };
    ErrorMsg.Fld_DeltaTime = 2;
    ErrorMsg.Fld_Sequence = 6;
    ErrorMsg.Fld_Provider = 10;
    ErrorMsg.Fld_ErrorId = 14;
    ErrorMsg.Msg_Size = 18;
    return ErrorMsg;
}(Parser_1.Parser));
exports.ErrorMsg = ErrorMsg;
