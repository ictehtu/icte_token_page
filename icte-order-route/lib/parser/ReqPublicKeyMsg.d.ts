import { Parser } from "./Parser";
export declare class ReqPublicKeyMsg extends Parser {
    static readonly Fld_MsgId: number;
    static readonly Fld_SymbolId: number;
    static readonly Msg_Size: number;
    constructor(buf?: Buffer);
    setSubType(subType: any): void;
    getReqId(): number;
    getSymbolId(): number;
    parse(dataView: DataView): void;
    buffer(reqId: number, symbolId: number): Buffer;
}
