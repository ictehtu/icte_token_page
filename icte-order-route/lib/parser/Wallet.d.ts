interface Password {
    value: string;
}
export declare class Wallet {
    createWallet(walletName: string, pwd: Password): string;
}
export {};
