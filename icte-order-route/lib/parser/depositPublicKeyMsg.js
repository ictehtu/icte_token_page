"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var depositPublicKeyMsg = /** @class */ (function (_super) {
    __extends(depositPublicKeyMsg, _super);
    function depositPublicKeyMsg(buf) {
        var _this = _super.call(this, depositPublicKeyMsg.Msg_Size, buf) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Portfolio);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_ReqInvestPk);
        return _this;
    }
    depositPublicKeyMsg.prototype.getReqId = function () {
        return _super.prototype.getInt.call(this, depositPublicKeyMsg.Fld_ReqId);
    };
    depositPublicKeyMsg.prototype.getSymbolId = function () {
        return _super.prototype.getInt.call(this, depositPublicKeyMsg.Fld_SymbolId);
    };
    depositPublicKeyMsg.prototype.parse = function (dataView) {
        _super.prototype.setInt.call(this, depositPublicKeyMsg.Fld_ReqId, dataView.getUint32(depositPublicKeyMsg.Fld_ReqId));
        _super.prototype.setInt.call(this, depositPublicKeyMsg.Fld_SymbolId, dataView.getUint32(depositPublicKeyMsg.Fld_SymbolId));
    };
    depositPublicKeyMsg.prototype.buffer = function (reqId, symbolId) {
        _super.prototype.setInt.call(this, depositPublicKeyMsg.Fld_ReqId, reqId);
        _super.prototype.setInt.call(this, depositPublicKeyMsg.Fld_SymbolId, symbolId);
        return _super.prototype.buffer.call(this);
    };
    // field offsets
    depositPublicKeyMsg.Fld_ReqId = 2;
    depositPublicKeyMsg.Fld_SymbolId = 6;
    depositPublicKeyMsg.Msg_Size = 10;
    return depositPublicKeyMsg;
}(Parser_1.Parser));
exports.depositPublicKeyMsg = depositPublicKeyMsg;
