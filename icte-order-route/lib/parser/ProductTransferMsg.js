"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("./Parser");
var Constant_1 = require("../model/Constant");
var ProductTransferMsg = /** @class */ (function (_super) {
    __extends(ProductTransferMsg, _super);
    function ProductTransferMsg(packageLvl, domain, domainIndex, nodeLocationIndex) {
        var _this = _super.call(this, ProductTransferMsg.Msg_Size + domain.byteLength) || this;
        _this.setByte(Constant_1.Constant.Fld_Type, Constant_1.Constant.Msg_Product);
        _this.setByte(Constant_1.Constant.Fld_SubType, Constant_1.Constant.Sub_GetProduct);
        _this.setLong(Constant_1.Constant.Fld_Timestamp, Date.now());
        _this.setInt(Constant_1.Constant.Fld_ProductType, Constant_1.Constant.Product_Node);
        _this.setInt(Constant_1.Constant.Fld_ProductSubType, Constant_1.Constant.Lvl_2);
        _this.setByte(ProductTransferMsg.Fld_PackageLvl, packageLvl);
        _this.setByte(ProductTransferMsg.Fld_PackageMetaData, nodeLocationIndex);
        _this.setInt(ProductTransferMsg.Fld_OriginIndex, domainIndex);
        _this.setBytes(ProductTransferMsg.Fld_Domain, domain);
        return _this;
    }
    ProductTransferMsg.prototype.getBufferHex = function () {
        return this.buf.toString('hex');
    };
    // field offsets
    ProductTransferMsg.Fld_PackageLvl = 18;
    ProductTransferMsg.Fld_PackageMetaData = 19;
    ProductTransferMsg.Fld_OriginIndex = 20;
    ProductTransferMsg.Fld_Domain = 24;
    ProductTransferMsg.Msg_Size = 28;
    return ProductTransferMsg;
}(Parser_1.Parser));
exports.ProductTransferMsg = ProductTransferMsg;
