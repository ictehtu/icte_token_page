import { Parser } from "./Parser";
export declare class TokenBehaviorMsg extends Parser {
    private static Token_ID;
    private static Client_Order_ID;
    private static Execution_Msg;
    private static Msg_Size;
    constructor(msgSubtype: number, tokenId: number, clientOrderId: number, executionMsg: Uint8Array, buf?: Buffer);
    getTokenId(): number;
    getClientOrderId(): number;
    getExecutionMsg(): Uint8Array;
}
