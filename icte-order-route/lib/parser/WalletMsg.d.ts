interface Password {
    value: string;
}
export declare class WalletMsg {
    generateWalletData(walletName: string, pwd: Password): Uint8Array;
    generateCustomTxData(from: string, to: string, value: string, fee: string, nonce: string, data: string, gas: string, gasPrice: string, type: string): Uint8Array;
    generateTransferData(from: string, to: string, value: string, fee: string, nonce: string, data: string): Uint8Array;
    generateProductTransactionForRaw(from: string, to: string, value: string, nonce: string, domain: string, data: string, timestamp: number, legend: number, subLegend: number, fee: string, key?: string): Uint8Array;
    generateProductTransaction(from: string, to: string, value: string, nonce: string, domain: string, packageLvl: number, domainIndex: number, nodeLocationIndex: number, fee: string): Uint8Array;
    generateAddressData(address: string): Uint8Array;
    generateData(obj: any): Uint8Array;
}
export {};
