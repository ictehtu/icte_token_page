"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("../model/Constant");
var TokenBehaviorMsg_1 = require("./TokenBehaviorMsg");
var ExecuteTokenBehaviorMsg = /** @class */ (function (_super) {
    __extends(ExecuteTokenBehaviorMsg, _super);
    function ExecuteTokenBehaviorMsg(tokenId, clientOrderId, executionMsg, buf) {
        return _super.call(this, Constant_1.Constant.Sub_ExecuteTokenBehavior, tokenId, clientOrderId, executionMsg, buf) || this;
    }
    return ExecuteTokenBehaviorMsg;
}(TokenBehaviorMsg_1.TokenBehaviorMsg));
exports.ExecuteTokenBehaviorMsg = ExecuteTokenBehaviorMsg;
