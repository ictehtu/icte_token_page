"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Constant_1 = require("./model/Constant");
var Parser_1 = require("./parser/Parser");
var WalletMsg_1 = require("./parser/WalletMsg");
var sleep_1 = require("./lib/sleep");
var DynamicMsg_1 = require("./parser/DynamicMsg");
var random_1 = require("./lib/random");
var MsgIdGenerator_1 = require("./parser/MsgIdGenerator");
var Utilities_1 = require("./model/Utilities");
var RfqMsg_1 = require("./parser/RfqMsg");
var ExecuteTokenBehaviorMsg_1 = require("./parser/ExecuteTokenBehaviorMsg");
var CmplGPgrm_1 = require("./parser/MessageFunctions/CmplGPgrm");
var CmplHTMLPgrm_1 = require("./parser/MessageFunctions/CmplHTMLPgrm");
var CmplSlPgrm_1 = require("./parser/MessageFunctions/CmplSlPgrm");
var AdIPFS_1 = require("./parser/MessageFunctions/AdIPFS");
var GtIPFS_1 = require("./parser/MessageFunctions/GtIPFS");
var NewOrderMsg_1 = require("./parser/NewOrderMsg");
var ICTEOrderRoute = /** @class */ (function () {
    function ICTEOrderRoute() {
        this._debug = false; // for test stuff in browser only
        if (this.constructor !== ICTEOrderRoute) {
            throw new Error('Subclassing is not allowed');
        }
        this.init();
    }
    ICTEOrderRoute.QueueNofity = function (msgb64) {
        // debugger;
        var msg = Parser_1.Parser.fromBase64(msgb64);
        var _type = msg[Constant_1.Constant.Fld_Type];
        var _sub = msg[Constant_1.Constant.Fld_SubType];
        var dynMsg = new DynamicMsg_1.DynamicMsg(msg.length, msg);
        ICTEOrderRoute._DataQueue.push(dynMsg);
        // switch (_type) {
        //    case Constant.Msg_Portfolio: {
        //       switch (_sub) {
        //          case Constant.Sub_InvestPubKey: { }
        //          case Constant.Sub_DepositPubKey: {
        //             let dynMsg: DynamicMsg = new DynamicMsg(msg.length, msg);
        //             ICTEOrderRoute._DataQueue.push(dynMsg);
        //             break;
        //          }
        //       }
        //       break;
        //    }
        //    case Constant.Msg_Wallet: {
        //       switch (_sub) {
        //          case Constant.Sub_WalletAccount: {
        //             let dynMsg: DynamicMsg = new DynamicMsg(msg.length, msg);
        //             ICTEOrderRoute._DataQueue.push(dynMsg);
        //             break;
        //          }
        //          case Constant.Sub_CreateWallet: {
        //             let dynMsg: DynamicMsg = new DynamicMsg(msg.length, msg);
        //             ICTEOrderRoute._DataQueue.push(dynMsg);
        //             break;
        //          }
        //       }
        //       break;
        //    }
        //    case Constant.Msg_Command: {
        //       switch (_sub) {
        //          case Constant.Sub_QrScan: {
        //             let dynMsg: DynamicMsg = new DynamicMsg(msg.length, msg);
        //             ICTEOrderRoute._DataQueue.push(dynMsg);
        //             break;
        //          }
        //       }
        //       break;
        //    }
        // }
    };
    ICTEOrderRoute.prototype.init = function () {
        this.setupBridge();
    };
    ICTEOrderRoute.prototype.setupBridge = function () {
        var observer = {
            update: function (payload) {
                ICTEOrderRoute.ObservableCollection.push(payload);
            }
        };
        if (!this._debug) {
            addObserver(observer);
        }
    };
    // public openWallet(walletName: string, pwd: any) {
    //    let dataUint8: Uint8Array = new WalletMsg().generateWalletData(walletName, pwd);
    //    let dynamicMsg: DynamicMsg = new DynamicMsg(dataUint8.length);
    //    dynamicMsg.setType(Constant.Msg_Wallet);
    //    dynamicMsg.setSubType(Constant.Sub_WalletAccount);
    //    dynamicMsg.setSymbolId(Constant.IEO);
    //    dynamicMsg.setMessageId(MsgIdGenerator.getNextId());
    //    dynamicMsg.setData(dataUint8);
    //    bridgeCall(dynamicMsg.toBase64());
    //    return dynamicMsg.getMesageId();
    // }
    ICTEOrderRoute.prototype.openWallet = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Wallet);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_WalletAccount);
        dynamicMsg.setSymbolId(Constant_1.Constant.IEO);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.createWallet = function (walletName, pwd) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateWalletData(walletName, pwd);
        var dynamicMsg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        dynamicMsg.setType(Constant_1.Constant.Msg_Wallet);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_CreateWallet);
        dynamicMsg.setSymbolId(Constant_1.Constant.IEO);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        dynamicMsg.setData(dataUint8);
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.setAccessToken = function (clientid, clientsecret) {
        var obj = { clientid: clientid, clientsecret: clientsecret };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Sys);
        msg.setSubType(Constant_1.Constant.Sub_ClientToken);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getOpenWalletData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2: return [4 /*yield*/, sleep_1.sleep(100)];
                    case 3:
                        _a.sent();
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        if (msg.getStatus()) {
                            return [2 /*return*/, JSON.parse(strObj)];
                        }
                        else {
                            return [2 /*return*/, { error: true, message: strObj }];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.getWalletAddress = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        if (msg.getStatus()) {
                            return [2 /*return*/, JSON.parse(strObj)];
                        }
                        else {
                            return [2 /*return*/, { error: true, message: strObj }];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqQRScan = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Command);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_RequestQrScan);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.getQRData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, strObj];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqLastUser = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Wallet);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_LastUser);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.getLastUser = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, name_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        if (msg.getStatus()) {
                            name_1 = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                            return [2 /*return*/, name_1];
                        }
                        else {
                            return [2 /*return*/, ''];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqPublicKey = function (symbolId) {
        var msg = new DynamicMsg_1.DynamicMsg();
        msg.setType(Constant_1.Constant.Msg_Portfolio);
        msg.setSubType(Constant_1.Constant.Sub_ReqInvestPk);
        msg.setSymbolId(symbolId);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getPublicKey = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, str;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        str = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, str];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqCryptoBalance = function (address, coin) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateAddressData(address);
        var dynamicMsg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        dynamicMsg.setType(Constant_1.Constant.Msg_Wallet);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_ListBalance);
        dynamicMsg.setSymbolId(coin);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        dynamicMsg.setData(dataUint8);
        var b64msg = dynamicMsg.toBase64();
        bridgeCall(b64msg);
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.getCryptoBalance = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqJoinFund = function (from, to, value, fee) {
        var obj = { from: from, to: to, value: value, fee: fee, timestamp: Date.now() };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Funds);
        msg.setSubType(Constant_1.Constant.Sub_JoinFund);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getJoinFund = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqDepositHoldings = function (from, to, value, fee) {
        var obj = { from: from, to: to, value: value, fee: fee, timestamp: Date.now() };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Funds);
        msg.setSubType(Constant_1.Constant.Sub_DepositHoldings);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getDepositHoldings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqWithdrawHoldings = function (from, to, value, fee) {
        var obj = { from: from, to: to, value: value, fee: fee, timestamp: Date.now() };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Funds);
        msg.setSubType(Constant_1.Constant.Sub_WithdrawHoldings);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getWithdrawHoldings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqUnjoinFund = function (from, to, value, fee) {
        var obj = { from: from, to: to, value: value, fee: fee, timestamp: Date.now() };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Funds);
        msg.setSubType(Constant_1.Constant.Sub_UnjoinFund);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getUnjoinFund = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqCretaeFund = function (from, to, value, fee) {
        var obj = { from: from, to: to, value: value, fee: fee, timestamp: Date.now() };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Funds);
        msg.setSubType(Constant_1.Constant.Sub_CreateFund);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getCreateFund = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqListFund = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Funds);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_ListFunds);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.getListFund = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqFundDetails = function (address) {
        var obj = { address: address };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Funds);
        msg.setSubType(Constant_1.Constant.Sub_FundDetails);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getFundDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqFundDetailsByIndex = function (index) {
        var obj = { index: index };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Funds);
        msg.setSubType(Constant_1.Constant.Sub_FundDetailsIndex);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getFundDetailsByIndex = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqJoinedList = function (address, fundAddress) {
        var obj = { address: address, fundAddress: fundAddress };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Funds);
        msg.setSubType(Constant_1.Constant.Sub_JoinedList);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getJoinedList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqTransferAction = function (from, to, value, fee, nonce, data) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateTransferData(from, to, value, fee, nonce, data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_ReqTokenTransfer);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqTransferBehavior = function (fee, data, tokenId, clientOrderId, executionMsgParams) {
        debugger;
        var executionMsg;
        switch (executionMsgParams.bhvmsg) {
            case 'CmplSlPgrm': {
                executionMsg = new CmplSlPgrm_1.CmplSlPgrm(clientOrderId, executionMsgParams.u);
                break;
            }
            case 'CmplHTMLPgrm': {
                executionMsg = new CmplHTMLPgrm_1.CmplHTMLPgrm(clientOrderId, executionMsgParams.u);
                break;
            }
            case 'CmplGPgrm': {
                executionMsg = new CmplGPgrm_1.CmplGPgrm(clientOrderId, executionMsgParams.u, executionMsgParams.tId);
                break;
            }
            case 'AdIPFS': {
                executionMsg = new AdIPFS_1.AdIPFS(executionMsgParams.d);
                break;
            }
            case 'GtIPFS': {
                executionMsg = new GtIPFS_1.GtIPFS(executionMsgParams._ipfshsh);
                break;
            }
        }
        var behaviorMsg = new ExecuteTokenBehaviorMsg_1.ExecuteTokenBehaviorMsg(tokenId, clientOrderId, executionMsg.buf);
        debugger;
        // parse data
        var bufferData = Buffer.from(data, 'utf-8');
        var txObj = {
            fee: fee,
            data: bufferData.toJSON().data,
            message: behaviorMsg.buf.toJSON().data
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(txObj);
        debugger;
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_BehaviorTransaction);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqCustomTx = function (from, to, value, fee, nonce, data, gas, gasPrice, type) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateCustomTxData(from, to, value, fee, nonce, data, gas, gasPrice, type);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_ReqTokenTransfer);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqProductTransferAaction = function (from, to, value, nonce, domain, packageLvl, domainIndex, nodeLocationIndex, fee) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateProductTransaction(from, to, value, nonce, domain, packageLvl, domainIndex, nodeLocationIndex, fee);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_ReqTokenTransfer);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqProductTransferAactionForRetry = function (from, to, value, nonce, domain, data, timestamp, legend, subLegend, fee) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateProductTransactionForRaw(from, to, value, nonce, domain, data, timestamp, legend, subLegend, fee);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Product);
        msg.setSubType(Constant_1.Constant.Sub_ProductRetry);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqProductTransferAactionForSignKey = function (from, to, value, nonce, domain, data, timestamp, legend, subLegend, fee, key) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateProductTransactionForRaw(from, to, value, nonce, domain, data, timestamp, legend, subLegend, fee, key);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Product);
        msg.setSubType(Constant_1.Constant.Sub_ProductKey);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqProductTransferAactionForSecret = function (from, to, value, nonce, domain, data, timestamp, legend, subLegend, fee) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateProductTransactionForRaw(from, to, value, nonce, domain, data, timestamp, legend, subLegend, fee);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Product);
        msg.setSubType(Constant_1.Constant.Sub_ProductSecret);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getTransferData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqBalance = function (address) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateAddressData(address);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_ListBalance);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqBalanceWebsocket = function (address) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateAddressData(address);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_ListBalance);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        return {
            msgid: msg.getMesageId(),
            buffer: msg.buf.buffer
        };
    };
    ICTEOrderRoute.prototype.echoWebSocketMsg = function (data) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateAddressData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Sys);
        msg.setSubType(Constant_1.Constant.Sub_RequestEcho);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        return {
            msgid: msg.getMesageId(),
            buffer: msg.buf.buffer
        };
    };
    ICTEOrderRoute.prototype.newOrderWebSocketMsg = function (Client_ID, Book_Type, AON, Token_ID, Order_Quantity_Integer, Order_Quantity_Decimal, Order_Price_Integer, Order_Price_Decimal, Order_Side) {
        var msg = new NewOrderMsg_1.NewOrderMsg(Client_ID, Book_Type, AON, Token_ID, Order_Quantity_Integer, Order_Quantity_Decimal, Order_Price_Integer, Order_Price_Decimal, Order_Side);
        msg.setType(Constant_1.Constant.Msg_Trade);
        msg.setSubType(Constant_1.Constant.Sub_NewOrder);
        // msg.setMessageId(MsgIdGenerator.getNextId());
        return {
            // msgid: msg.getMesageId(),
            buffer: msg.buf.buffer
        };
    };
    ICTEOrderRoute.prototype.getBalance = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        return [2 /*return*/, JSON.parse(strObj)];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqWorkerCmd = function (payload) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(payload);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_WorkerCmd);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getWorkerCmd = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, strObj, strObjson;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        strObj = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        strObjson = JSON.parse(strObj);
                        return [2 /*return*/, strObjson];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqGetIEO = function (coinbase, coin, amount) {
        var obj = {
            Id: random_1.random(),
            coinbase: coinbase,
            coin: coin,
            amount: amount
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_GetIEO);
        msg.setSymbolId(Constant_1.Constant.IEO);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getGetIEO = function () {
        return __awaiter(this, void 0, void 0, function () {
            var msg, data, dataObjson;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 2];
                        return [4 /*yield*/, sleep_1.sleep(10)];
                    case 1:
                        _a.sent();
                        msg = ICTEOrderRoute._DataQueue.pop();
                        if (this.isSet(msg))
                            return [3 /*break*/, 2];
                        return [3 /*break*/, 0];
                    case 2:
                        data = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                        if (msg.getStatus()) {
                            dataObjson = JSON.parse(data);
                            return [2 /*return*/, dataObjson];
                        }
                        else {
                            return [2 /*return*/, false];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqReadAddressBook = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Wallet);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_ReadAddressBook);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqWsPort = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Command);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_SocketPort);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqLocationCheckBox = function (checkBoxStatus) {
        var data = {
            latitude: "0",
            longitude: "0",
            automatic: checkBoxStatus
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Command);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setSubType(Constant_1.Constant.Sub_LocationActivation);
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqLocationCoords = function (lat, lon) {
        var data = {
            latitude: String(lat),
            longitude: String(lon),
            automatic: false
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Command);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setSubType(Constant_1.Constant.Sub_LocationSetup);
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqFilter = function (data) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Exchange);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setSubType(Constant_1.Constant.Sub_RfqFilter);
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
        ;
    };
    ICTEOrderRoute.prototype.reqRfqFilterList = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Exchange);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_RfqFilterList);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqListOrders = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Exchange);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_RfqListOrders);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqListOffers = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Exchange);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_RfqListOffers);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqListMyOrders = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Exchange);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_RfqListMyOrders);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqRemoveFilter = function (productid, productsubid) {
        var data = {
            productid: productid,
            productsubid: productsubid,
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Exchange);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setSubType(Constant_1.Constant.Sub_RfqRemoveFilter);
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqAccept = function (offer) {
        var data = { offer: offer };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Exchange);
        msg.setSubType(Constant_1.Constant.Sub_RfqAccept);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqIgnoreOrder = function (clientid) {
        var data = {
            clientid: clientid
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Exchange);
        msg.setSubType(Constant_1.Constant.Sub_RfqIgnoreOrder);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqRemoveMyOrder = function (clientid) {
        var data = {
            clientid: clientid
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Exchange);
        msg.setSubType(Constant_1.Constant.Sub_RfqRemoveMyOrder);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqParseOffer = function (hexData) {
        var data = {
            data: Utilities_1.Utilities.hexStringToArray(hexData)
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Exchange);
        msg.setSubType(Constant_1.Constant.Sub_RfqParseOffer);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqOffer = function (productid, productsubid, order, note) {
        var data = {
            productid: productid,
            productsubid: productsubid,
            order: order,
            note: note
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Exchange);
        msg.setSubType(Constant_1.Constant.Sub_RfqOffer);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqRfqOrder = function (productid, productsubid, note) {
        var data = {
            productid: productid,
            productsubid: productsubid,
            note: note
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Exchange);
        msg.setSubType(Constant_1.Constant.Sub_RfqOrder);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqGetLocationData = function () {
        var msg = new DynamicMsg_1.DynamicMsg();
        msg.setType(Constant_1.Constant.Msg_Command);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setSubType(Constant_1.Constant.Sub_GetLocationData);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqUpdateAddressBook = function (addressbook) {
        var data = {
            addressbook: addressbook
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(data);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setSubType(Constant_1.Constant.Sub_UpdateAddressBook);
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.adminPanel = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Command);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_Admin);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.openFR = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Command);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_FairPricer);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqGetPrivateKey = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Wallet);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_GetKey);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.reqUpdateKey = function (key) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateAddressData(key);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Wallet);
        msg.setSubType(Constant_1.Constant.Sub_LoadKey);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getXamarinState = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Command);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_GetState);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.getXamarinData = function (key) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData({ key: key });
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Command);
        msg.setSubType(Constant_1.Constant.Sub_GetData);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.setXamarinData = function (key, value) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData({ key: key, value: value });
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Command);
        msg.setSubType(Constant_1.Constant.Sub_SaveData);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.xamarinRedirect = function (index) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateAddressData(index);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Command);
        msg.setSubType(Constant_1.Constant.Sub_Redirect);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.sendLastUrl = function (url) {
        var dataUint8 = new WalletMsg_1.WalletMsg().generateAddressData(url);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Command);
        msg.setSubType(Constant_1.Constant.Sub_SendLastUrl);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.resetLastUrl = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Command);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_ResetLastUrl);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.exit = function () {
        var dynamicMsg = new DynamicMsg_1.DynamicMsg();
        dynamicMsg.setType(Constant_1.Constant.Msg_Command);
        dynamicMsg.setSubType(Constant_1.Constant.Sub_Close);
        dynamicMsg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        bridgeCall(dynamicMsg.toBase64());
        return dynamicMsg.getMesageId();
    };
    ICTEOrderRoute.prototype.getDebugData = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, sleep_1.sleep(100)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, ICTEOrderRoute._debugData];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.reqSetGroupId = function (groupid) {
        var obj = {
            groupid: groupid
        };
        var dataUint8 = new WalletMsg_1.WalletMsg().generateData(obj);
        var msg = new DynamicMsg_1.DynamicMsg(dataUint8.length);
        msg.setType(Constant_1.Constant.Msg_Sys);
        msg.setSubType(Constant_1.Constant.Sub_GroupID);
        msg.setMessageId(MsgIdGenerator_1.MsgIdGenerator.getNextId());
        msg.setData(dataUint8);
        bridgeCall(msg.toBase64());
        return msg.getMesageId();
    };
    ICTEOrderRoute.prototype.getMsgData = function (msgType, msgSubType, requiredMsgId) {
        return __awaiter(this, void 0, void 0, function () {
            var msg, response, msgLifetime, queue, i, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        response = {};
                        msgLifetime = [];
                        _a.label = 1;
                    case 1:
                        if (!true) return [3 /*break*/, 3];
                        // loop that check every 100ms if _DataQueue has the required message
                        return [4 /*yield*/, sleep_1.sleep(100)];
                    case 2:
                        // loop that check every 100ms if _DataQueue has the required message
                        _a.sent();
                        queue = ICTEOrderRoute._DataQueue;
                        // check if queue has data
                        if (queue.length > 0) {
                            for (i = 0; i < queue.length; i++) {
                                msgLifetime[i] = msgLifetime[i] ? msgLifetime[i] + 1 : 1;
                                if (msgLifetime[i] === 10) {
                                    msgLifetime[i] = 0;
                                    ICTEOrderRoute._DataQueue.splice(i, 1);
                                    continue;
                                }
                                msg = queue[i];
                                // check if sata are not null or undefined
                                if (this.isSet(msg)) {
                                    // check for the required message in the client
                                    if (msg.msgType() === msgType && msg.msgSubtype() === msgSubType && msg.getMesageId() === requiredMsgId) {
                                        // now we can clear space in the queue
                                        ICTEOrderRoute._DataQueue.splice(i, 1);
                                        data = DynamicMsg_1.DynamicMsg.getAndParseData(msg.getData());
                                        // check if is json or plain text
                                        if (this.isJson(data)) {
                                            response = __assign({}, response, { orderroute: __assign({}, response.orderroute, { dataType: 'json' }), data: JSON.parse(data) });
                                        }
                                        else {
                                            response = __assign({}, response, { orderroute: __assign({}, response.orderroute, { dataType: 'plain' }), data: data });
                                        }
                                        // check for message status
                                        if (msg.getStatus() === 1) {
                                            response = __assign({}, response, { orderroute: __assign({}, response.orderroute, { error: false }) });
                                            return [2 /*return*/, response];
                                        }
                                        else {
                                            response = __assign({}, response, { orderroute: __assign({}, response.orderroute, { error: true, message: data || 'Status is 0' }) });
                                            return [2 /*return*/, response];
                                        }
                                    }
                                }
                            }
                        }
                        return [3 /*break*/, 1];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ICTEOrderRoute.prototype.getConstant = function () {
        return Constant_1.Constant;
    };
    ICTEOrderRoute.prototype.isSet = function (val) {
        if (typeof val === 'undefined' || val === null)
            return false;
        return true;
    };
    ICTEOrderRoute.prototype.isJson = function (str) {
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    };
    ICTEOrderRoute.prototype.parseDataTypes = function (hexData) {
        return Utilities_1.Utilities.getDataTypes(hexData);
    };
    ICTEOrderRoute.prototype.decodeProductSecretData = function (data) {
        return Utilities_1.Utilities.parseProductDataSecretToObj(data);
    };
    ICTEOrderRoute.prototype.decodeData = function (data) {
        return Utilities_1.Utilities.parseProductDataToStr(data);
    };
    ICTEOrderRoute.prototype.bufferRfqMsg = function (productId, priceStart, priceEnd, qtyStart, qtyEnd, timeStart, timeEnd) {
        var rfqMsg = new RfqMsg_1.RfqMsg(null, productId, priceStart, priceEnd, qtyStart, qtyEnd, timeStart, timeEnd);
        return rfqMsg.buf.buffer;
    };
    ICTEOrderRoute.ObservableCollection = {
        push: function (msg) {
            ICTEOrderRoute.ObservableCollection.on(msg);
        },
        on: function (msg) {
            if (JSON.stringify(msg.message) === "{}") {
                ICTEOrderRoute.QueueNofity(msg.data);
            }
            else {
                ICTEOrderRoute.QueueNofity(msg.message);
            }
        }
    };
    ICTEOrderRoute._debugData = "init";
    ICTEOrderRoute._DataQueue = [];
    return ICTEOrderRoute;
}());
exports.ICTEOrderRoute = ICTEOrderRoute;
