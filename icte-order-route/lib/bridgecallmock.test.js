"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var test = /** @class */ (function () {
    function test(_or) {
        this._or = _or;
        _or.bridgeCallMock("INVEST_PUB_KEY_0").then(function (investPubKey) {
            console.log("Invest", investPubKey);
        });
        _or.bridgeCallMock("DEPOSIT_PUB_KEY_0").then(function (depositPubKey) {
            console.log("Deposit", depositPubKey);
        });
    }
    ;
    return test;
}());
exports.test = test;
