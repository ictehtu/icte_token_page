"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Parser_1 = require("../parser/Parser");
var Constant_1 = require("./Constant");
var Utilities = /** @class */ (function () {
    function Utilities() {
    }
    Utilities.padLeft = function (text, padChar, size) {
        return (String(padChar).repeat(size) + text).substr((size * -1), size);
    };
    Utilities.getMsSinceMidnight = function () {
        return this.getGmtZeroNow() - this.getGmtZeroMidnight();
    };
    Utilities.getGmtZeroNow = function () {
        var d = new Date();
        var z = d.getTime() + (d.getTimezoneOffset() * 60000);
        var zd = new Date(z);
        return zd.getTime();
    };
    Utilities.prepareMsg = function (headers, dataObjectToSend) {
        var str = JSON.stringify(dataObjectToSend);
        var uint8 = Utilities.strToUtf8Bytes(str);
        var array = Utilities.uint8PushToArray(headers, uint8);
        var binary = Utilities.arrayToBinary(array);
        var b64encoded = btoa(binary);
        return b64encoded;
    };
    Utilities.getGmtZeroMidnight = function () {
        var d = new Date();
        var z = d.getTime() + (d.getTimezoneOffset() * 60000);
        var zd = new Date(z);
        zd.setHours(0);
        zd.setMinutes(0);
        zd.setSeconds(0);
        zd.setMilliseconds(0);
        return zd.getTime();
    };
    Utilities.uint8PushToArray = function (headers, uint8) {
        var array = new Array();
        for (var i = 0; i < headers.length; i++) {
            array.push(headers[i]);
        }
        for (var i = 0; i < uint8.length; i++) {
            array.push(uint8[i]);
        }
        return array;
    };
    Utilities.arrayToBinary = function (array) {
        var binary = "";
        for (var i = 0; i < array.length; i++) {
            binary += String.fromCharCode(array[i]);
        }
        return binary;
    };
    Utilities.isObjectEmpty = function (obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    };
    Utilities.Utf8ArrayToStr = function (array, startAt) {
        var sa = startAt || 0;
        var out = "";
        for (var i = sa; i < array.length; i++) {
            out += String.fromCharCode(array[i]);
        }
        return out;
    };
    Utilities.strToUtf8Bytes = function (str) {
        var utf8 = [];
        for (var ii = 0; ii < str.length; ii++) {
            var charCode = str.charCodeAt(ii);
            if (charCode < 0x80)
                utf8.push(charCode);
            else if (charCode < 0x800) {
                utf8.push(0xc0 | (charCode >> 6), 0x80 | (charCode & 0x3f));
            }
            else if (charCode < 0xd800 || charCode >= 0xe000) {
                utf8.push(0xe0 | (charCode >> 12), 0x80 | ((charCode >> 6) & 0x3f), 0x80 | (charCode & 0x3f));
            }
            else {
                ii++;
                charCode = 0x10000 + (((charCode & 0x3ff) << 10) | (str.charCodeAt(ii) & 0x3ff));
                utf8.push(0xf0 | (charCode >> 18), 0x80 | ((charCode >> 12) & 0x3f), 0x80 | ((charCode >> 6) & 0x3f), 0x80 | (charCode & 0x3f));
            }
        }
        var uint8 = new Uint8Array(utf8);
        return uint8;
    };
    Utilities.parseProductDataToStr = function (dataStr) {
        // License for Level 2 Node (Business Package) on nyqex.org for yourdomain.com
        debugger;
        var packages = [
            'Community',
            'Business'
        ];
        var buff = Buffer.from(dataStr, "hex");
        var data = new Parser_1.Parser(buff.byteLength, buff);
        var str = 'License for level $level Node ($package Package) on $domain= for $userdomain';
        str = str.replace('$level', String(data.getInt(Constant_1.Constant.Fld_ProductSubType)));
        str = str.replace('$package', packages[data.getInt(18)]);
        str = str.replace('$domain=', "$domain=" + data.getByte(20));
        str = str.replace('$userdomain', Utilities.Utf8ArrayToStr(data.getBytes(24 + 4)));
        return str;
    };
    Utilities.hexStringToArray = function (str) {
        if (!str) {
            return [];
        }
        var a = [];
        for (var i = 0, len = str.length; i < len; i += 2) {
            a.push(parseInt(str.substr(i, 2), 16));
        }
        return a;
    };
    Utilities.getDataTypes = function (hexData) {
        var buff = Buffer.from(hexData, "hex");
        var data = new Parser_1.Parser(buff.byteLength, buff);
        return { type: data.getByte(0), subtype: data.getByte(1) };
    };
    Utilities.parseProductDataSecretToObj = function (dataStr) {
        var buff = Buffer.from(dataStr, "hex");
        var data = new Parser_1.Parser(buff.byteLength, buff);
        return {
            level: data.getInt(Constant_1.Constant.Fld_ProductType),
            package: data.getInt(Constant_1.Constant.Fld_ProductSubType),
            location: data.getByte(19),
            origindomain: data.getByte(20),
            domain: Utilities.Utf8ArrayToStr(data.getBytes(24 + 4))
        };
    };
    Utilities.PADSIZE = 9;
    Utilities.DIV = 1000000000;
    return Utilities;
}());
exports.Utilities = Utilities;
