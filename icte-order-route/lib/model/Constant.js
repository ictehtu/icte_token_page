"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Constants of message types and message subtypes
 */
var Constant = /** @class */ (function () {
    function Constant() {
    }
    //standard message fields
    Constant.Fld_Type = 0; // 1 byte
    Constant.Fld_SubType = 1; // 1 byte
    Constant.Fld_Timestamp = 2; // 8 bytes
    Constant.Fld_ProductType = 10;
    Constant.Fld_ProductSubType = 14;
    Constant.Product_Node = 0;
    Constant.Lvl_0 = 0;
    Constant.Lvl_1 = 1;
    Constant.Lvl_2 = 2;
    Constant.Lvl_3 = 3;
    Constant.Lvl_4 = 4;
    //custom message types and subtypes
    Constant.Msg_Sys = 0;
    Constant.Sub_SysTime = 0;
    Constant.Sub_Error = 100;
    Constant.Sub_GroupID = 50;
    Constant.Sub_ClientToken = 51;
    Constant.Sub_RequestEcho = 130;
    Constant.Sub_RespEcho = 131;
    //custom messages types and subtypes starts ## 151 - 201
    Constant.Msg_Quote = 151;
    Constant.Sub_Quote = 0;
    Constant.Sub_Bid = 1;
    Constant.Sub_Ask = 2;
    Constant.Sub_Buy = 3;
    Constant.Sub_Sell = 4;
    Constant.Sub_Price = 5;
    Constant.Sub_Rate = 6;
    Constant.Sub_Level1 = 7;
    Constant.Msg_Position = 152;
    Constant.Sub_PositionUpdate = 0;
    Constant.Sub_PositionLock = 1;
    //model  names
    Constant.Mdl_SymbolModel = 0;
    // Wallet Message
    Constant.Msg_Wallet = 153;
    Constant.Sub_CreateWallet = 1;
    Constant.Sub_WalletCreated = 2;
    Constant.Sub_Balance = 3;
    Constant.Sub_ListBalance = 4;
    Constant.Sub_BalanceList = 5;
    Constant.Sub_ReqTokenTransfer = 6;
    Constant.Sub_TokenTransfered = 7;
    Constant.Sub_WalletAccount = 8;
    Constant.Sub_WalletTransaction = 9;
    Constant.Sub_GetIEO = 10;
    Constant.Sub_WorkerCmd = 11;
    Constant.Sub_Backup = 12;
    Constant.Sub_GetCrypto = 13;
    Constant.Sub_LastUser = 14;
    Constant.Sub_Nonce = 15;
    Constant.Sub_ReadAddressBook = 16;
    Constant.Sub_UpdateAddressBook = 17;
    Constant.Sub_UpdateUrl = 18;
    Constant.Sub_LoadKey = 19;
    Constant.Sub_GetKey = 20;
    Constant.Sub_AccountTransactions = 67;
    Constant.Sub_BehaviorTransaction = 68;
    //Order Route
    Constant.Msg_Portfolio = 154;
    Constant.Sub_CompRate = 0;
    Constant.Sub_RateRequest = 1;
    Constant.Sub_ReqInvestPk = 2;
    Constant.Sub_ReqDepositPk = 3;
    Constant.Sub_InvestPubKey = 4;
    Constant.Sub_DepositPubKey = 5;
    //Commands
    Constant.Msg_Command = 155;
    Constant.Sub_QrScan = 0;
    Constant.Sub_RequestQrScan = 1;
    Constant.Sub_Close = 2;
    Constant.Sub_Admin = 3;
    Constant.Sub_Peers = 4;
    Constant.Sub_AddNode = 5;
    Constant.Sub_SocketPort = 6;
    Constant.Sub_LocationSetup = 7;
    Constant.Sub_LocationActivation = 8;
    Constant.Sub_GetLocationData = 9;
    Constant.Sub_FairPricer = 10;
    Constant.Sub_GetState = 11;
    Constant.Sub_SetState = 12;
    Constant.Sub_Redirect = 13;
    Constant.Sub_SaveData = 14;
    Constant.Sub_GetData = 15;
    Constant.Sub_SendLastUrl = 16;
    Constant.Sub_ResetLastUrl = 17;
    // Funds
    Constant.Msg_Funds = 160;
    Constant.Sub_CreateFund = 0;
    Constant.Sub_JoinFund = 1;
    Constant.Sub_UnjoinFund = 2;
    Constant.Sub_DepositHoldings = 3;
    Constant.Sub_WithdrawHoldings = 4;
    Constant.Sub_ListFunds = 5;
    Constant.Sub_FundDetails = 6;
    Constant.Sub_FundDetailsIndex = 7;
    Constant.Sub_JoinedList = 8;
    // Products
    Constant.Msg_Product = 99;
    Constant.Sub_GetProduct = 0;
    Constant.Sub_ProductRetry = 1;
    Constant.Sub_ProductSecret = 2;
    Constant.Sub_ProductKey = 3;
    // RRQ
    Constant.Msg_Exchange = 2;
    Constant.Sub_RfqFilter = 1;
    Constant.Sub_RfqFilterList = 2;
    Constant.Sub_RfqRemoveFilter = 3;
    Constant.Sub_RfqOrder = 4;
    Constant.Sub_RfqOffer = 5;
    Constant.Sub_RfqAccept = 6;
    Constant.Sub_RfqListOrders = 20;
    Constant.Sub_RfqListOffers = 21;
    Constant.Sub_RfqListMyOrders = 22;
    Constant.Sub_RfqIgnoreOrder = 23;
    Constant.Sub_RfqRemoveMyOrder = 24;
    Constant.Sub_RfqListReplies = 25;
    Constant.Sub_RfqParseOffer = 26;
    // Token Messages
    Constant.Msg_Token = 0x10;
    Constant.Sub_ExecuteTokenBehavior = 0x00;
    Constant.Sub_EndTokenBehavior = 0x01;
    Constant.Sub_UpdateTokenBehavior = 0x02;
    Constant.Sub_FaultTokenBehavior = 0x03;
    // Constants
    Constant.SystemMsgTopic = '0';
    Constant.BusinessMsgTopic = '1';
    Constant.IEOSocketIoTransport = 0;
    Constant.PoloniexTransport = 1;
    Constant.CoinCapTransport = 2;
    Constant.BinanceTransport = 3;
    Constant.BitfinexTransport = 4;
    Constant.HitBtcTransport = 5;
    Constant.ModelManager = 6;
    Constant.DIVSIZE = 9;
    Constant.DIV = 100000000;
    // Trade
    Constant.Msg_Trade = 140;
    // Trade - Orders
    Constant.Sub_NewOrder = 2;
    Constant.Sub_CancelOrder = 3;
    Constant.Sub_ReplaceOrder = 4;
    Constant.Sub_RemoveFromMarket = 5;
    Constant.Sub_AddToMarket = 6;
    Constant.Sub_CancelAllOrders = 7;
    Constant.Sub_GetOrderId = 8;
    // Trade - Reports
    Constant.Sub_OrderAccepted = 21;
    Constant.Sub_OrderCreateRejected = 22;
    Constant.Sub_OrderOnMarket = 23;
    Constant.Sub_OrderOffMarket = 24;
    Constant.Sub_OrderCancelRejected = 25;
    Constant.Sub_OrderCancelled = 26;
    Constant.Sub_OrderReplaceRejected = 27;
    Constant.Sub_OrderReplaced = 28;
    Constant.Sub_OrderFilled = 29;
    Constant.Sub_OrderDone = 30;
    // Chain
    Constant.Msg_Chain = 1;
    Constant.Sub_HandshakeInit = 8;
    Constant.VENUES = {
        0: 'HTU Trans',
        1: 'Poloniex',
        2: 'CoinCap',
        3: 'Binance',
        4: 'Bitfinex',
        5: 'HitBTC',
        6: 'ModelManager'
    };
    Constant.USDT = 0;
    Constant.IEO = 1;
    Constant.BTC = 2;
    Constant.ETH = 3;
    Constant.ZEC = 4;
    Constant.XTZ = 5;
    Constant.SEM = 6;
    Constant.IOTA = 7;
    Constant.XRP = 8;
    Constant.XLM = 9;
    Constant.WAVES = 10;
    Constant.DOGE = 11;
    Constant.XMR = 12;
    Constant.BCN = 13;
    Constant.AMP = 14;
    Constant.BTG = 15;
    Constant.DGB = 16;
    Constant.GAME = 17;
    Constant.LSK = 18;
    Constant.NAV = 19;
    Constant.NLG = 20;
    Constant.NXT = 21;
    Constant.POT = 22;
    Constant.STRAT = 23;
    Constant.SYS = 24;
    Constant.XVG = 25;
    Constant.DLT = 26; /*RADIX*/
    Constant.LTC = 27;
    Constant.EOS = 28;
    Constant.DASH = 29;
    Constant.QTUM = 30;
    Constant.OMNI = 31;
    Constant.TRX = 32;
    Constant.MKR = 33;
    Constant.ONT = 34;
    Constant.VET = 35;
    Constant.SYMBOLS = [
        Constant.USDT,
        Constant.IEO,
        Constant.BTC,
        Constant.ETH,
        Constant.ZEC,
        Constant.XTZ,
        Constant.SEM,
        Constant.IOTA,
        Constant.XRP,
        Constant.XLM,
        Constant.WAVES,
        Constant.DOGE,
        Constant.XMR,
        Constant.BCN,
        Constant.AMP,
        Constant.BTG,
        Constant.DGB,
        Constant.GAME,
        Constant.LSK,
        Constant.NAV,
        Constant.NLG,
        Constant.NXT,
        Constant.POT,
        Constant.STRAT,
        Constant.SYS,
        Constant.XVG,
        Constant.DLT,
        Constant.LTC,
        Constant.EOS,
        Constant.DASH,
        Constant.QTUM,
        Constant.OMNI,
        Constant.TRX,
        Constant.MKR,
        Constant.ONT,
        Constant.VET
    ];
    Constant.COINS = [
        "USDT",
        "IEO",
        "BTC",
        "ETH",
        "ZEC",
        "XTZ",
        "SEM",
        "MIOTA",
        "XRP",
        "XLM",
        "WAVES",
        "DOGE",
        "XMR",
        "BCN",
        "AMP",
        "BTG",
        "DGB",
        "GAME",
        "LSK",
        "NAV",
        "NLG",
        "NXT",
        "STRAT",
        "SYS",
        "XVG",
        "DLT",
        "LTC",
        "EOS",
        "DASH",
        "QTUM",
        "OMNI",
        "TRX",
        "MKR",
        "ONT",
        "VET"
    ];
    Constant.COINSID = {
        'USDT': Constant.USDT,
        'IEO': Constant.IEO,
        'BTC': Constant.BTC,
        'ETH': Constant.ETH,
        'ZEC': Constant.ZEC,
        'XTZ': Constant.XTZ,
        'SEM': Constant.SEM,
        'MIOTA': Constant.IOTA,
        'XRP': Constant.XRP,
        'XLM': Constant.XLM,
        'WAVES': Constant.WAVES,
        'DOGE': Constant.DOGE,
        'XMR': Constant.XMR,
        'BCN': Constant.BCN,
        'AMP': Constant.AMP,
        'BTG': Constant.BTC,
        'DGB': Constant.DGB,
        'GAME': Constant.GAME,
        'LSK': Constant.LSK,
        'NAV': Constant.NAV,
        'NLG': Constant.NLG,
        'NXT': Constant.NXT,
        'STRAT': Constant.STRAT,
        'SYS': Constant.SYS,
        'XVG': Constant.XVG,
        'DLT': Constant.DLT,
        'LTC': Constant.LTC,
        'EOS': Constant.EOS,
        'DASH': Constant.DASH,
        'QTUM': Constant.QTUM,
        'OMNI': Constant.OMNI,
        'TRX': Constant.TRX,
        'MKR': Constant.MKR,
        'ONT': Constant.ONT,
        'VET': Constant.VET
    };
    return Constant;
}());
exports.Constant = Constant;
