export declare class Utilities {
    static PADSIZE: number;
    static readonly DIV: number;
    static padLeft(text: string, padChar: string, size: number): string;
    static getMsSinceMidnight(): number;
    static getGmtZeroNow(): number;
    static prepareMsg(headers: any[], dataObjectToSend: object): string;
    static getGmtZeroMidnight(): number;
    static uint8PushToArray(headers: any[], uint8: any): any[];
    static arrayToBinary(array: any): string;
    static isObjectEmpty(obj: object): boolean;
    static Utf8ArrayToStr(array: any, startAt?: number): string;
    static strToUtf8Bytes(str: string): Uint8Array;
    static parseProductDataToStr(dataStr: string): string;
    static hexStringToArray(str: any): any[];
    static getDataTypes(hexData: any): {
        type: number;
        subtype: number;
    };
    static parseProductDataSecretToObj(dataStr: string): {
        level: number;
        package: number;
        location: number;
        origindomain: number;
        domain: string;
    };
}
