"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorParse = /** @class */ (function () {
    function ErrorParse() {
    }
    ErrorParse.Error_Socket = 100;
    ErrorParse.Error_InvalidData = 101;
    ErrorParse.ERRORS = {
        100: 'Error from socket',
        101: 'Invalid data'
    };
    return ErrorParse;
}());
exports.ErrorParse = ErrorParse;
