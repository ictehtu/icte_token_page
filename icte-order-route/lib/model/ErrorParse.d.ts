export declare class ErrorParse {
    static readonly Error_Socket: number;
    static readonly Error_InvalidData: number;
    static readonly ERRORS: {
        100: string;
        101: string;
    };
}
