/**
 * Generate a int random unique number.
 * @param min - Min range
 * @param max - Max range
 */
export declare function random(min?: number, max?: number): number;
