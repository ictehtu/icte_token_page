export interface ClientResponse {
    orderroute: {
        error: boolean;
        message?: string;
        dataType?: string;
    };
    data?: any;
}
