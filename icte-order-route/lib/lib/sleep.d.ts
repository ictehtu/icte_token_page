/**
 * Sleep the the current proccess.
 * @param ms Milliseconds
 */
export declare function sleep(ms: number): Promise<{}>;
