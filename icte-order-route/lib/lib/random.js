"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mem = [];
/**
 * Generate a int random unique number.
 * @param min - Min range
 * @param max - Max range
 */
function random(min, max) {
    var minNumber, maxNumber, random;
    minNumber = min || 1;
    maxNumber = max || 32767;
    while (true) {
        var r = Math.floor(Math.random() * maxNumber) + minNumber;
        if (mem.indexOf(r) === -1) {
            mem.push(r);
            break;
        }
    }
    random = mem[mem.length - 1];
    return random;
}
exports.random = random;
