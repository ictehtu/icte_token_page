"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Sleep the the current proccess.
 * @param ms Milliseconds
 */
function sleep(ms) {
    return new Promise(function (resolve) { return setTimeout(resolve, ms); });
}
exports.sleep = sleep;
