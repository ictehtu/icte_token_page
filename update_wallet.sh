#!/bin/bash
set -e
curr_dir=${PWD}
arr=""
cat translation-website.txt | while IFS='' read -r line; do
    domain=$(echo $line | awk '{print $1}')
    src=$(echo $line | awk '{print $2}')
    git clone git@bitbucket.org:ictehtu/$domain.git
    rm -rf $domain/Qm*/www/demo-wallet.$domain/*
    cd $domain/Qm*/www/ && mkdir -p demo-wallet.$domain/; cd $curr_dir
    if [[ "$src" != "en" ]] ;then
        if [[ " ${arr[*]} " != *" $src "* ]]; then ng build --prod --configuration=locale-$src; fi
        cp -r dist/locale-$src/* $domain/Qm*/www/demo-wallet.$domain/
    elif [[ "$src" == "en" ]] ; then 
	if [[ " ${arr[*]} " != *" $src "* ]]; then ng build --prod; fi
        cp -r dist/htu-base/* $domain/Qm*/www/demo-wallet.$domain/
    fi
    arr+=("$src")
    cd $domain; git add Qm*/www > /dev/null; set +e; git commit -m "Updated wallet for $domain"  > /dev/null; set -e; git push; cd $curr_dir; rm -rf $domain
done
